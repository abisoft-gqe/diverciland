import os
import sys

"""
    CUSTOM SETTINGS
"""

#Apache settings
ROOT_URL = '/'

# Variable for custom context processors
DJANGO_ROOT_SETTING = ''

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST':'',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'ATOMIC_REQUESTS': True,
    }
}


TIME_ZONE = 'UTC'

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = '/var/store/'

# Used by Django File Form module 
DEFAULT_FILE_STORAGE = MEDIA_ROOT

#Countries to display in first in country selection forms
COUNTRIES_FIRST = ('FR','DK','SE','LV','DE','PL',\
                   'GB','ES','IT','CH','CZ','SK',\
                   'PK','NL','BE','HU','AT','CZ')

#To order first countries alphabetically
COUNTRIES_FIRST_SORT = True

ALLOWED_HOSTS = ['127.0.0.1']

GECKO_LOG_PATH = '/var/log/'