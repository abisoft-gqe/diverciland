"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from django.apps import apps
from django.contrib.postgres.fields import JSONField
from django.db import models
from django_countries.fields import CountryField
from django.core.exceptions import ValidationError

from accession.managers import AccessionManager
from commonfct.constants import ATTRIBUTE_TYPES


CATEGORY = ((1,'Default'),
            )

class AccessionType(models.Model):
    """
      The AccessionType model gives information about the type of accessions.
      
      :var CharField name: name of the accession type
      :var TextField description: description of the accession type
      :var IntegerField category 
    """
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    category = models.IntegerField(choices=CATEGORY)
    
    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ['name']
        

class AccessionTypeAttribute(models.Model):
    """
      The AccessionTypeAttribute model gives information about the attributes 
      linked to the accession type
      It's not possible to create an attribute with the same name than an existing 
      accession field.
       
      :var CharField attribute_name: name of the attribute
      :var IntegerField type: type of the attribute
      :var ManyToManyField accession_type: accession type linked to the attribute, 
                                           defined in the AccessionType model
    """
 
    attribute_name = models.CharField(max_length=100, blank=True, unique=True)
    type = models.IntegerField(choices=ATTRIBUTE_TYPES, blank=True)
    accession_type = models.ManyToManyField('AccessionType', through='AccessionAttributePosition', blank=True)

    def __str__(self):
        return self.attribute_name

    class Meta:
        ordering = ['attribute_name']
    
    def clean(self):
        acc_fields = Accession._meta.get_fields()
        acc_field_names = [f.name for f in acc_fields]
        acc_loc_fields = AccessionLocal._meta.get_fields()
        acc_loc_fields_names = [f.name for f in acc_loc_fields]
        clean_field_names = acc_field_names + acc_loc_fields_names
        
        if self.attribute_name.lower().replace(' ','_') in clean_field_names:
            raise ValidationError('A field with this name already exists for accessions.')
    

    def delete(self, *args, **kwargs):
        AccessionLocal = apps.get_model('accession','accessionlocal')
        AccessionAttributePosition = apps.get_model('accession','accessionattributeposition')
        
        types = AccessionAttributePosition.objects.filter(attribute=self.id).values_list('type', flat=True).distinct()
        if types:
            accessions = AccessionLocal.objects.filter(accession__type__in=types)
            for accession in accessions:
                accession.attributes_values.pop(str(self.id), None)
                accession.save()
                
            super(AccessionTypeAttribute, self).delete(*args, **kwargs)
        
        else:
            super(AccessionTypeAttribute, self).delete(*args, **kwargs)
                


class AccessionAttributePosition(models.Model):
    """
      The AccessionAttributePosition model gives information about the position of the attribute.
      It allows the movement of the attribute thanks to a drag and drop system and the memorization of the position of this attribute.
      
      :var ForeignKey attribute: name of the attribute, defined with the AccessionTypeAttribute model
      :var ForeignKey type: type of the accession which is linked to the attribute, defined with the AccessionType model
      :var IntegerField position: integer rendering the position number
    """
    
    attribute = models.ForeignKey('AccessionTypeAttribute', on_delete=models.CASCADE)
    type = models.ForeignKey('AccessionType', on_delete=models.CASCADE)
    position = models.IntegerField()
    
    class Meta:
        unique_together = ("type","position")


class Accession(models.Model):
    """
      The Accession model gives informations about the accessions.
       
      :var CharField name: name of the accession
      :var ForeignKey type: type of the accession, defined in the AccessionType model
      :var TextField breeder: breeder of the accession
    """
    name = models.CharField(max_length=100, unique=True, db_index=True)
    type = models.ForeignKey('AccessionType', db_index=True, on_delete=models.CASCADE)
    breeder = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ['name']


class AccessionLocal(models.Model):
    """
      The Accession model gives information about the local accessions.
       
      :var ForeignKey accession: reference to name and general informations about the local accession
      :var CountryField country: country on which the informations of this local accession are registered
      :var TextField inscription_date: inscription date of the local accession
      :var TextField maintainer: maintainer of the local accession
      :var TextField synonym: synonym(s) of the local accession
      :var TextField description: description of the local accession
      :var ManyToManyField projects: project(s) linked to the local accession
    """
    accession = models.ForeignKey('Accession', db_index=True, on_delete=models.CASCADE)
    country = CountryField()
    inscription_date = models.CharField(max_length=100)
    maintainer = models.CharField(max_length=100, blank=True, null=True)
    synonym = models.TextField(max_length=500, blank=True, null=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    attributes_values = JSONField(blank=True, null=True)
    
    projects = models.ManyToManyField('team.Project', blank = True)
    
    objects = AccessionManager() 
     
    def __str__(self):
        return '%s (%s)'%(self.accession, self.country)
     
    class Meta:
        unique_together=("accession","country")
        ordering = ['accession','country']
    
    def update_attributes(self, attributes, postdata):
        jsonvalues = {}
        for att in attributes:
            if att.attribute_name.lower().replace(' ','_') in postdata.keys():
                jsonvalues[att.id] = postdata[att.attribute_name.lower().replace(' ','_')]
            else:
                jsonvalues[att.id] = ''
        self.attributes_values = jsonvalues
    
    def delete(self, *args, **kwargs):
        # If the accession local is the last object attached to the accession, 
        # we delete the accession.
        Accession = apps.get_model('accession','accession')
        accessions_loc = AccessionLocal.objects.filter(accession=self.accession)
        
        if len(accessions_loc) == 1 and accessions_loc[0].id == self.id:
            accession = Accession.objects.get(id=self.accession.id)
            super(AccessionLocal, self).delete(*args, **kwargs)
            accession.delete()
        else:
            super(AccessionLocal, self).delete(*args, **kwargs)
        
