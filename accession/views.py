"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import ast
import csv
import json
import os

from _csv import Dialect
from django_countries import countries as dc_countries
from django import forms
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
from django.db import transaction
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views import View
from Levenshtein._levenshtein import distance

from accession.forms import AccessionTypeForm, AccessionLocalForm, AccessionForm, AccessionUploadForm,\
                            AccessionTypeAttributeForm, AccessionTypeSelectAttribute
from accession.models import AccessionType, Accession, AccessionLocal, AccessionTypeAttribute, AccessionAttributePosition
from commonfct.constants import filerenderer_headers
from commonfct.utils import get_fields, add_pagination, execute_function_in_thread, send_message_if_not_already_sent
from commonfct.genericviews import generic_create, generic_update, write_csv_from_viewer
from core_viewer.forms import DataviewProjectForm, DataviewAccessionTypeForm, DataviewCountryForm
from csv_io.views import CSVFactoryForTempFiles, CSVFactoryForStoredFiles
from team.models import Project, User
from team.views import LoginRequiredMixin, UserIsAdminMixin
from django.contrib.gis.geoip2.resources import Country



class AccessionTypeManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the Accession Type model. It allows the deletion and insertion of types based on the Accession Type Model.
      
      :var form form_type_class: the accession type form
      :var form form_attr_class: the descriptors form
      :var str template: html template for the rendering of the view
      :var queryset types: contains all the types from the database
      :var int number_all: contains the number of accession types of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
    """
    form_type_class = AccessionTypeForm
    form_attr_class = AccessionTypeSelectAttribute
    template = 'accession/accession_type.html'
    
    def get(self, request, *args, **kwargs):
        form_type = self.form_type_class()
        form_attr = self.form_attr_class()
        types = AccessionType.objects.all()
        number_all = len(types)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form_type)
        
        names.append('descriptors')
        labels.append('Descriptors')
        
        tag_fields = {'all':types,
                      'number_all':number_all,
                      'nb_per_page':nb_per_page,
                      'creation_mode':True,
                      'form_type':form_type,
                      'form_attr':form_attr,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "Accession Type Management"}
        
        
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        form_type = self.form_type_class(request.POST)
        form_attr = self.form_attr_class(request.POST)
        
        if form_type.is_valid() and form_attr.is_valid():
            type = form_type.save()
            
            position = 0
            for attribute in request.POST.getlist('descriptors'):
                attr = AccessionTypeAttribute.objects.get(id=attribute)
                att_pos = AccessionAttributePosition.objects.create(attribute=attr, type=type, position=position)
                att_pos.save()
                position += 1
            messages.add_message(request, messages.SUCCESS, "A new Accession Type named {0} has been created.".format(type))
            return redirect('accessiontype_management')
        
        else:
            messages.add_message(request, messages.ERROR, ' '.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form_type.errors.items() ]))
            messages.add_message(request, messages.ERROR, ' '.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form_attr.errors.items() ]))
            return redirect('accessiontype_management')


class AccessionTypeEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of Accession Type model.
      
      :var form form_type_class: the accession type form
      :var form form_attr_class: the descriptors form
      :var str template: html template for the rendering of the view
      :var queryset types: contains all the types from the database
      :var int number_all: contains the number of accession types of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
    """
    form_type_class = AccessionTypeForm
    form_attr_class = AccessionTypeSelectAttribute
    template = 'accession/accession_type.html'
    
    def get(self, request, *args, **kwargs):
        try:
            at = AccessionType.objects.get(id=kwargs['object_id'])
        except:
            messages.add_message(request, messages.ERROR, "This accession type doesn't exist in the database.")
            return redirect('accessiontype_management')
        
        form_type = self.form_type_class(initial={'name':at.name,
                                                  'description':at.description,
                                                  'category':at.category
                                                  })
        
        attributes = AccessionAttributePosition.objects.filter(type=at.id).values_list('attribute', flat=True)
        
        form_attr = self.form_attr_class(initial={'descriptors':attributes})
        
        #form_attr.fields['descriptors'].disabled = True
        
        types = AccessionType.objects.all()
        number_all = len(types)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form_type)
        
        names.append('descriptors')
        labels.append('Descriptors')
        
        tag_fields = {'all':types,
                      'hiddenid':at.id,
                      'number_all':number_all,
                      'nb_per_page':nb_per_page,
                      'creation_mode':False,
                      'form_type':form_type,
                      'form_attr':form_attr,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "Accession Type Edit"}
        
        
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        
        form_type = self.form_type_class(request.POST)
        form_attr = self.form_attr_class(request.POST)
        
        try:
            at = AccessionType.objects.get(id=kwargs['object_id'])
        except:
            messages.add_message(request, messages.ERROR, "This accession type doesn't exist in the database.")
            return redirect('accessiontype_management')
        
        try:
            at.name = request.POST.get('name')
            at.description = request.POST.get('description')
            at.category = request.POST.get('category')

            attributes = at.accessiontypeattribute_set.all()
            attributes = attributes.values_list('id', flat=True)
            new_attributes = request.POST.getlist('descriptors')
            
            update_attribute_list(at, attributes, new_attributes)
        
            at.save()
            
            messages.add_message(request, messages.SUCCESS, "The Accession Type {0} has been updated.".format(at))
            
        except Exception as e:
            messages.add_message(request, messages.ERROR, e)
        
        return redirect('accessiontype_management')

def update_attribute_list(at, attributes, new_attributes):
    """
      To update the attribute list in edit mode.
    """
    to_delete = []
    to_add = []
    
    for attr in attributes:
        if str(attr) not in new_attributes:
            to_delete.append(str(attr))
    
    for attr in new_attributes:
        if int(attr) not in attributes:
            to_add.append(attr)
    
    for attr in to_delete:
        position = AccessionAttributePosition.objects.get(attribute=attr, type=at)
        position.delete()
        
        
    attr_order = attributes.order_by('id')
    
    position = 0
    for attribute in attr_order:
        try:
            type_pos = AccessionAttributePosition.objects.get(type=at, position=position)
        except:
            type_pos = None
        
        if not type_pos:
            attr_pos = AccessionAttributePosition.objects.get(attribute=attribute, type=at)
            attr_pos.position = position
            attr_pos.save()
        position += 1
        
    for attr in to_add:
        a = AccessionTypeAttribute.objects.get(id=attr)
        attr_pos = AccessionAttributePosition.objects.create(attribute=a, type=at, position=position)
        attr_pos.save()
        position += 1
    
    accessions = AccessionLocal.objects.filter(accession__type=at)
    
    for accession in accessions:
        if accession.attributes_values == None:
            accession.attributes_values = {}
            
        for attr in to_delete:
            accession.attributes_values.pop(attr, None)
            
        for attr in to_add:
            accession.attributes_values[attr] = ''
        
        accession.save()


class AccessionTypeAttributeManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the creation of Accession Type Attributes model.
      
      :var form form_class: the accession type attribute form
      :var str template: html template for the rendering of the view
      :var queryset attributes: contains all the attributes from the database
      :var int number_all: contains the number of attributes of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
    """
    form_class = AccessionTypeAttributeForm
    template = 'accession/accession_attribute.html'
    
    def get(self, request, *args, **kwargs):
        form = self.form_class()
        attributes = AccessionTypeAttribute.objects.all()
        number_all = len(attributes)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)

        tag_fields = {'all':attributes,
                      'number_all':number_all,
                      'nb_per_page':nb_per_page,
                      'creation_mode':True,
                      'form':form,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "Accession Descriptors Management"}
        
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        request,template,tag_fields = generic_create(AccessionTypeAttribute, self.form_class, request, self.template,{}, "Accession Descriptors Management")
        return render(request,template,tag_fields)


class AccessionTypeAttributeEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of Accession Type Attributes model.
      
      :var form form_class: the accession type attribute form
      :var str template: html template for the rendering of the view
      :var queryset attributes: contains all the attributes from the database
      :var int number_all: contains the number of attributes of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
    """
    form_class = AccessionTypeAttributeForm
    template = 'accession/accession_attribute.html'
    
    def get(self, request, *args, **kwargs):
        try:
            attr = AccessionTypeAttribute.objects.get(id=kwargs['object_id'])
        except:
            messages.add_message(request, messages.ERROR, "This accession type descriptor doesn't exist in the database.")
            return redirect('accessiontypeattribute_management')
        
        form = self.form_class(initial={'attribute_name':attr.attribute_name,
                                        'type':attr.type
                                        })
        
        attributes = AccessionTypeAttribute.objects.all()
        number_all = len(attributes)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)

        tag_fields = {'all':attributes,
                      'hiddenid':attr.id,
                      'number_all':number_all,
                      'nb_per_page':nb_per_page,
                      'creation_mode':False,
                      'form':form,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "Accession Descriptors Edit"}
        
        return render(request, self.template, tag_fields)

    def post(self, request, *args, **kwargs):
        request,template,tag_fields = generic_update(AccessionTypeAttribute, self.form_class, request, self.template,{}, "Accession Descriptors Management")
        return redirect('accessiontypeattribute_management')

class AccessionManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the accession model. It allows the deletion and insertion of accessions based on the Accession model,
      but also the submission of a file containing accessions and their informations.
      The filters are applied to this view. The user can make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list of the fields the user can search with the refine search module
      :var form form_class: the accessions form based on the Accession model
      :var queryset accessions: list of all the projects from the database
      :var int number_all: number of all the accessions in the database
      :var int nb_per_page: number of data to display per page, initialized to 10
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class_name = AccessionForm
    form_class_acc = AccessionLocalForm
    file_form_class = AccessionUploadForm
    template = 'accession/accession_data.html'
    list_attributes=['name', 'breeder', 'country','inscription_date', 'maintainer', 'synonym', 'description', 'projects']
    
    def get(self, request, *args, **kwargs):
        try:
            accession_type = AccessionType.objects.get(id=kwargs['type_id'])
        except:
            messages.add_message(request, messages.ERROR, "This accession type doesn't exist in the database.")
            return redirect('accessiontype_management')
        
        attributes = accession_type.accessiontypeattribute_set.all()
        
        form_name = self.form_class_name()
        form_acc = self.form_class_acc(attributes=attributes)
        file_form = self.file_form_class()
        
        accessions = AccessionLocal.objects.filter(accession__in=Accession.objects.filter(type=accession_type)).distinct()
        set_accession_attributes(accessions, attributes)
        
        filtered_acc = accessions
        number_all = len(accessions)
        accessions = add_pagination(request, accessions)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form_acc)
        
        for attr in AccessionTypeAttribute.objects.filter(accession_type=accession_type):
            in_list = attr.attribute_name in self.list_attributes
            if in_list == False:
                self.list_attributes.append(attr.attribute_name)
        
        form_name.fields['type'].widget = forms.HiddenInput()
        form_name.fields['type'].initial = accession_type.id
        
        names.insert(0,'name')
        names.insert(1,'breeder')
        
        labels.insert(0,'Name')
        labels.insert(1,'Breeder')
        
#         names.remove(form['type'].name)
#         labels.remove(form['type'].label)
        
        tag_fields = {'all':accessions,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': str(accession_type.name) +" Accession Management",
                      'filetype':'Accession',
                      'typeid':accession_type.id,
                      'refine_search':True}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':accessions,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form_name':form_name,
                               'form_acc':form_acc,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            accessions_searched = AccessionLocal.objects.refine_search(or_and, dico_get, accession_type)
            accessions = accessions_searched.intersection(filtered_acc)
            accessions = accessions.order_by('accession_id')
            set_accession_attributes(accessions, attributes)
            
            number_all = len(accessions)
            accessions = add_pagination(request, accessions)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':accessions,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form_name':form_name,
                               'form_acc':form_acc,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
        
            return render(request,self.template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
             
            accessions_searched = AccessionLocal.objects.refine_search(or_and, dico_get, accession_type)

            #We only want to have accessions of the type considered
            accessions = accessions_searched.intersection(filtered_acc)
            accessions = accessions.order_by('accession_id')
            set_accession_attributes(accessions, attributes)
            
            number_all = len(accessions)
            list_exceptions = ['type','id','rawdata','varietyrepartition']
            download_not_empty = False
            thread_name = ''
            if accessions:
                thread_name = execute_function_in_thread(write_accession_csv_from_viewer, [accessions, "Result search", accession_type.id], {})
                download_not_empty = True
                
            tag_fields.update({'download_not_empty':download_not_empty,
                               'thread_name':thread_name})
            
            accessions = add_pagination(request, accessions)
        
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':accessions,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form_name':form_name,
                           'form_acc':form_acc,
                           'file_form':file_form,
                           'list_attributes':self.list_attributes}) 
        return render(request, self.template, tag_fields)
        
    def post(self, request, *args, **kwargs):
        accession_type = AccessionType.objects.get(id=kwargs['type_id'])
        
        attributes = accession_type.accessiontypeattribute_set.all()
        
        form_name = self.form_class_name(request.POST)
        form_acc = self.form_class_acc(data=request.POST, attributes=attributes)
        file_form = self.file_form_class(request.POST, request.FILES)
        
        accessions = AccessionLocal.objects.filter(accession__in=Accession.objects.filter(type=accession_type))
        set_accession_attributes(accessions, attributes)
        number_all = len(accessions)
        accessions = add_pagination(request, accessions)

        names, labels = get_fields(form_acc)
        
        for attr in AccessionTypeAttribute.objects.filter(accession_type=accession_type):
            in_list = attr.attribute_name in self.list_attributes
            if in_list == False:
                self.list_attributes.append(attr.attribute_name)
    
        form_name.fields['type'].widget = forms.HiddenInput()
        form_name.fields['type'].initial = accession_type.id
        
        names.insert(0,'name')
        names.insert(1,'breeder')
        
        labels.insert(0,'Name')
        labels.insert(1,'Breeder')
    
#         names.remove(form['type'].name)
#         labels.remove(form['type'].label)
        
        tag_fields = {'all':accessions,
                      'number_all':number_all,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': str(accession_type.name) +" Accession Management",
                      'form_name':form_name,
                      'form_acc':form_acc,
                      'file_form':file_form,
                      'creation_mode':True,
                      'refine_search':True,
                      'filetype':'Accession',
                      'typeid':accession_type.id,
                      'list_attributes':self.list_attributes}
        

        if form_acc.is_valid():
        
            if form_name.is_valid():
                acc_name = Accession.objects.create(name=request.POST['name'], type=accession_type, breeder=request.POST['breeder'])
                acc_name.save()
                messages.add_message(request, messages.SUCCESS, "The accession {0} has been created.".format(acc_name))
                
            else:
                try:
                    #To handle the case where the accession already exists in the database
                    acc_name = Accession.objects.get(name=request.POST['name'])
                    if acc_name.breeder == request.POST['breeder']:
                        form_name.is_valid = True
                    else:
                        messages.add_message(request, messages.ERROR, "The accession with the name {0} already exists in the database and is associated to the breeder {1}. Please change the breeder's name.".format(acc_name, acc_name.breeder))
                        return redirect('accession_management',type_id=accession_type.id)
                except:  
                    messages.add_message(request, messages.ERROR, ' '.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form_name.errors.items()]))
                    return redirect('accession_management',type_id=accession_type.id)
        
            create_accession_from_form(request, form_acc, acc_name, attributes)
            
            accessions = AccessionLocal.objects.filter(accession__in=Accession.objects.filter(type=accession_type))
            set_accession_attributes(accessions, attributes)
            number_all = len(accessions)
            accessions = add_pagination(request, accessions)
            
            tag_fields.update({'all':accessions,
                               'number_all':number_all})
            
            return render(request, self.template, tag_fields)
        
        elif file_form.is_valid():
            file = request.FILES['file']
            myfactory = CSVFactoryForTempFiles(file)
            myfactory.find_dialect()
            myfactory.read_file()
            file_dict = myfactory.get_file()
            
            headers_list = filerenderer_headers[9][1]
            
            missing = myfactory.check_headers_conformity(headers_list)
            
            if missing != []:
                messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                                     ". The required headers for this file are: "+format(', '.join(headers_list))+".")
                return redirect('accession_management',type_id=accession_type.id)
            
            if request.POST.get('submit'):
#                 create_accession_from_file(request, file_dict, accession_type)
                
                with open(settings.MEDIA_ROOT+'tmp/'+file.name, 'wb+') as destination:
                    for chunk in file.chunks():
                        destination.write(chunk)
                destination.close()
                
                return redirect('check_accession_names', type_id=accession_type.id, file_name=str(destination.name))
            
            elif request.POST.get('update'):
                update_accession_from_file(request, file_dict, accession_type)
                
            accessions = AccessionLocal.objects.filter(accession__in=Accession.objects.filter(type=accession_type))
            set_accession_attributes(accessions, attributes)
            number_all = len(accessions)
            accessions = add_pagination(request, accessions)
            tag_fields.update({'all':accessions,
                               'number_all':number_all})
            
            return render(request, self.template, tag_fields)
    
        else:
            messages.add_message(request, messages.ERROR, "An error occurred.")
            return render(request, self.template, tag_fields)
                      

def create_accession_from_form(request, form_acc, acc_name, attributes):
    """
       Function used to create a accession object from the form.
      :param form form_acc: form submitted by the user
      :param Accession acc_name: object which contains the name of the accession
      :param list attributes: attributes selected by the user
    """
    country = form_acc.cleaned_data['country']
    inscription_date = form_acc.cleaned_data['inscription_date']
    maintainer = form_acc.cleaned_data['maintainer']
    synonym = form_acc.cleaned_data['synonym']
    description = form_acc.cleaned_data['description']
    projects = Project.objects.filter(id__in=form_acc.cleaned_data['projects'])
    
    try:
        with transaction.atomic():   
            accession = AccessionLocal.objects.create(accession=acc_name,
                                                 country=country,
                                                 inscription_date=inscription_date,
                                                 maintainer=maintainer,
                                                 synonym=synonym,
                                                 description=description)
            accession.projects.add(*list(projects))
            accession.update_attributes(attributes, request.POST)
            accession.save()

    except Exception as e:
        messages.add_message(request, messages.ERROR, "Informations for this country ({0}) are already linked to this accession ({1}).".format(country, acc_name))
        return 
    
    messages.add_message(request, messages.SUCCESS, "The informations for the country {0} on the accession {1} have been created.".format(country,acc_name))

def get_or_create_accession(request, name, type, breeder):
    """
      Function to get the accession name object, or to create it if it doesn't exists.
      :param str name: name of the accession
      :param AccessionType type: type of the accession
      :param str breeder: breeder of the accession
    """
    try:
        acc_name = Accession.objects.get(name=name)
        return acc_name
    except:
        acc_name = Accession.objects.create(name=name, type=type, breeder=breeder)
        return acc_name

     
class CheckAccessionFromFile(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      :var str template: html template for the rendering of the view
      :var str title: title to display on the template
      :var dict report: dictionary containing the accessions of the file that are close of accessions names already in the database
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var bool download_not_empty: boolean allowing or not the downloading of the report file
    """
    template = 'accession/check_accession_names.html'
    title = 'Check the accessions'
        
    def get(self, request, *args, **kwargs):
        file_name = kwargs['file_name']
        type_id = kwargs['type_id']
        
        if type_id != 0:
            accession_type = AccessionType.objects.get(id=type_id)
        else:
            accession_type = 0
        
        path_head, path_tail = os.path.split(file_name)
        
        with open(file_name,'r', encoding='utf-8') as file:
            myfactory = CSVFactoryForStoredFiles(file)
            myfactory.find_dialect()
            myfactory.read_file()
        
        #The result is wrapped in a list to iterate multiple times on the DictReader
        file_dict = list(myfactory.get_file())
        
        #Coeff to specify the minimum distance we filtered on
        coeff = 0.3
        
        file_names = []
        for line in file_dict:
            file_names.append(line['Name'])
        
        db_names = Accession.objects.all().values_list('name', flat=True).distinct()
        
        report = {}
        no_test = []
         
        for f_name in file_names:
            #We can't test an accession if its length is less than 4 letters
            if len(f_name) < 4:
                no_test.append(f_name)
                report[f_name] = ['--']
            else:
                result = compare_accession_name_with_db(request, f_name, db_names, coeff, report)
                if result:
                    return result                            
        
        if report == {}:
            create_accession_from_file(request, file_dict, accession_type)
            
            #With the redirect the messages created in create or update accession
            #from file are lost. We call them back here
            for msg in messages.get_messages(request):
                messages.add_message(request, msg.level, msg.message)
            
            if accession_type != 0:
                return redirect('accession_management',type_id=accession_type.id)
            else:
                return redirect('accession_insert')
        
        else:
            filename = path_tail.split('.')[0] + "_report"
            dialect = Dialect(delimiter = ';')
            
            with open("/tmp/"+filename+".csv", 'w', encoding='utf-8') as csv_file:
                writer = csv.writer(csv_file, dialect)
                writer.writerow(["Accessions read from the file","Accessions already recorded from the database"])
                for acc, db_names in report.items():
                    row = [acc, ', '.join(db_names)]
                    writer.writerow(row)
            download_not_empty = True         
            
            messages.add_message(request, messages.WARNING, "The names of the accessions submitted in the file are close of some accessions' names already saved in the database.")
            messages.add_message(request, messages.WARNING, "Please check if your accessions are different of the existing ones. If one or several of your accessions already exists\
                                                             with another name, please remove them of your file and if needed, update the accessions informations.\
                                                             Else, you can submit your file to create new accessions.")
            if len(no_test) == 1:
                messages.add_message(request, messages.WARNING, "The accession {0} have less than 4 characters, it can't be tested because too many accessions could be close of it.".format(', '.join(no_test)))
            elif len(no_test) > 1:
                messages.add_message(request, messages.WARNING, "The accessions {0} have less than 4 characters, they can't be tested because too many accessions could be close of them.".format(', '.join(no_test)))
            
            tag_fields = {'title':self.title,
                          'report':report,
                          'file_dict':json.dumps(file_dict),
                          'acc_type':accession_type,
                          'download_not_empty':download_not_empty,
                          'filename':filename}
            return render(request, self.template, tag_fields)
        
    def post(self, request, *args, **kwargs):
        type_id = kwargs['type_id']
        
        if type_id != 0:
            accession_type = AccessionType.objects.get(id=type_id)
        else:
            accession_type = 0
            
        file_name = kwargs['file_name']
        
        if request.POST.get('submit'):
            file_dict = json.loads(request.POST.get('file_dict'))
            
            create_accession_from_file(request, file_dict, accession_type)
            os.remove(file_name)
            
            #With the redirect the messages created in create or update accession
            #from file are lost. We call them back here
            for msg in messages.get_messages(request):
                messages.add_message(request, msg.level, msg.message)
            
            if accession_type != 0:
                return redirect('accession_management',type_id=accession_type.id) 
            else:
                return redirect('accession_insert')
            
        elif request.POST.get('cancel'):
            os.remove(file_name)
            if accession_type != 0:
                return redirect('accession_management',type_id=accession_type.id)
            else:
                return redirect('accession_insert')
        

def compare_accession_name_with_db(request, f_name, db_names, coeff, report):
    """
      Comparison of accession name with all the name in the database in order to calculate the 
      distance between the names and see if the new name is included in already saved names.
      
      :param str f_name: new accession's name
      :param list db_names: list of the accessions name of the database
      :param float coeff: coeff on which Levenstein distance is computed
      :param AccessionType accession_type: accession type of the view we come from
      :param dict report: report of all the closest names
    """
    for db_name in db_names:
        dist = distance(f_name.lower(), db_name.lower())
        if float(dist) < len(f_name)*coeff:
            if f_name not in report.keys():
                report[f_name] = [db_name]
            else:
                report[f_name].append(db_name)
                   
        included = f_name in db_name if len(f_name) <= len(db_name) else db_name in f_name
        if included :
            if f_name in report.keys():
                if report[f_name][-1] != db_name: report[f_name].append(db_name)
            else:
                report[f_name] = [db_name]
    

def create_accession_from_file(request, file_dict, accession_type):
    """
      Function used to create accessions from a file uploaded by the user.
      :param dict file_dict: file on dict format returned by the CSVFactory
      :param AccessionType accession_type: accession type to link the created accession(s)
    """
    with transaction.atomic():      
        for line in file_dict:
            try:
                with transaction.atomic():  
                    if accession_type == "all" or accession_type == 0:
                        type = AccessionType.objects.get(name=line["Type"])
                    else:
                        type = accession_type
                        
                    if line["Country"] not in dict(dc_countries).keys():
                        raise Exception("The country code is unknown, please check https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes to find the ISO 3166 country's code.")
                    
                    acc_name = get_or_create_accession(request, line["Name"], type, line["Breeder"])
        
                    acc = AccessionLocal.objects.create(accession=acc_name,
                                                   country=line["Country"],
                                                   inscription_date=line["Inscription date"],
                                                   maintainer=line["Maintainer"],
                                                   synonym=line["Synonym"],
                                                   description=line["Description"])
                    
                    if line['Projects']:
                        project_list = [p.strip() for p in str(line['Projects']).split('|')]
                        projects = Project.objects.filter(name__in = project_list)
                        if len(project_list) != len(projects):
                            messages.add_message(request, messages.ERROR, "There is an error with a project in this list: "+ ', '.join(project_list)+".")
                            raise Exception
                        acc.projects.add(*list(projects))
                        
                    attributes = type.accessiontypeattribute_set.all()
                    attributes_names = attributes.values_list("attribute_name", flat=True)
                    
                    unknown_attr = []
                    headers_list = filerenderer_headers[9][1]
                    
#                     if attributes:
                    attr_data = {}
                    for field in line.keys():
                        if field in attributes_names:
                            attr_data[field.lower().replace(' ','_')] = line[field]
                        else:
                            if field not in headers_list:
                                unknown_attr.append(field)
                    
                    if attr_data:
                        acc.update_attributes(attributes, attr_data)
                        
                    acc.save()

                    if unknown_attr != []:
                        send_message_if_not_already_sent(request, "One or several headers in your file are not registered as descriptors: "+format(', '.join(unknown_attr))\
                                                         +". No informations have been saved for these columns.", 'warning')
    
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+", "+str(line["Country"])+").")
                continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")
    

def update_accession_from_file(request, file_dict, accession_type):
    """
      Function used to update accessions from a file uploaded by the user.
      :param dict file_dict: file on dict format returned by the CSVFactory
      :param AccessionType accession_type: accession type to link the created accession(s)
    """
    with transaction.atomic():        
        for line in file_dict:
            try:
                with transaction.atomic():
                    if accession_type == "all":
                        type = AccessionType.objects.get(name=line["Type"])
                    else:
                        type = accession_type
                    
                    acc_name = Accession.objects.get(name=line["Name"])
                    
                    acc = AccessionLocal.objects.get(accession=acc_name, country=line["Country"])
                    
                    acc.inscription_date=line["Inscription date"]
                    acc.maintainer=line["Maintainer"]
                    acc.synonym=line["Synonym"]
                    acc.description=line["Description"]
                    
                    if line['Projects']:
                        project_list = [p.strip() for p in str(line['Projects']).split('|')]
                        projects = Project.objects.filter(name__in = project_list)
                        if len(project_list) != len(projects):
                            messages.add_message(request, messages.ERROR, "There is an error with a project in this list: "+ ', '.join(project_list)+".")
                            raise Exception
                        acc.projects.clear()
                        acc.projects.add(*list(projects))
                        
                    else:
                        acc.projects.clear()
                        
                    attributes = type.accessiontypeattribute_set.all()
                    attributes_names = attributes.values_list("attribute_name", flat=True)
                    
                    unknown_attr = []
                    headers_list = filerenderer_headers[9][1]
                    
#                     if attributes:
                    attr_data = {}
                    for field in line.keys():
                        if field in attributes_names:
                            attr_data[field.lower().replace(' ','_')] = line[field]
                        else:
                            if field not in headers_list:
                                unknown_attr.append(field)
                    
                    if attr_data:
                        acc.update_attributes(attributes, attr_data)
                    
                    acc.save()
                    
                    if unknown_attr != []:
                        send_message_if_not_already_sent(request, "One or several headers in your file are not registered as descriptors: "+format(', '.join(unknown_attr))\
                                                         +". No informations have been saved for these columns.", 'warning')
        
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+", "+str(line["Country"])+").")
                    continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully uploaded and the accessions have been updated.")


def set_accession_attributes(accessions, attributes):
    """
      Get attribute's values for each accession to write in the table.
    """
    for accession in accessions:
            for attribute in attributes:
                if accession.attributes_values:
                    if type(accession.attributes_values) is dict:
                        attributes_values = accession.attributes_values
                    else:
                        attributes_values = json.loads(accession.attributes_values)
                        
                    if str(attribute.id) in attributes_values.keys():
                        try:
                            setattr(accession,attribute.attribute_name.lower().replace(' ','_'),attributes_values[str(attribute.id)])
                        except ValueError as e:
                            print(e)
                            setattr(accession,attribute.attribute_name.lower().replace(' ','_'),'')
                    else:
                        setattr(accession,attribute.attribute_name.lower().replace(' ','_'),'')
                else:
                    setattr(accession,attribute.attribute_name.lower().replace(' ','_'),'')
    
    
class AccessionEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of accession model. The user can still make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list of the fields the user can search with the refine search module
      :var form form_class: the accessions form based on the Accession model
      :var queryset accessions: list of all the accessions from the database
      :var int number_all: number of all the accessions in the database
      :var int nb_per_page: number of data to display per page, initialized to 10
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class_name = AccessionForm
    form_class_acc = AccessionLocalForm
    file_form_class = AccessionUploadForm
    template = 'accession/accession_data.html'
    list_attributes=['name', 'breeder', 'country','inscription_date', 'maintainer', 'synonym', 'description', 'projects']
    
    def get(self, request, *args, **kwargs):
        try:
            a = AccessionLocal.objects.get(id=kwargs['object_id'])
            accession_type = AccessionType.objects.get(id=a.accession.type.id)
        except:
            messages.add_message(request, messages.ERROR, "This accession doesn't exist in the database.")
            return redirect('accessiontype_management')
        
        attributes = accession_type.accessiontypeattribute_set.all()
        
        form_name = self.form_class_name(initial={'name':a.accession.name,
                                                  'breeder':a.accession.breeder})
        
        
        attributes_values = a.attributes_values
        
        data={'country':a.country,
              'inscription_date':a.inscription_date,
              'maintainer':a.maintainer,
              'synonym':a.synonym,
              'description':a.description,
              'projects':a.projects.all()
              }
        
        for attribute in attributes:
            if str(attribute.id) in attributes_values.keys():
                data[attribute.attribute_name.replace(' ','_').lower()] = attributes_values[str(attribute.id)]
        
        form_acc = self.form_class_acc(attributes=attributes, data=data)
        
        file_form = self.file_form_class()
        
        accessions = AccessionLocal.objects.filter(accession__in=Accession.objects.filter(type=accession_type)).distinct()
        set_accession_attributes(accessions, attributes)
        
        filtered_acc = accessions
        number_all = len(accessions)
        accessions = add_pagination(request, accessions)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form_acc)
        
        for attr in AccessionTypeAttribute.objects.filter(accession_type=accession_type):
            in_list = attr.attribute_name in self.list_attributes
            if in_list == False:
                self.list_attributes.append(attr.attribute_name)
        
        form_name.fields['type'].widget = forms.HiddenInput()
        form_name.fields['type'].initial = accession_type.id
        
        if a.country != 'FR':
            form_name.fields['name'].disabled = True
            form_name.fields['breeder'].disabled = True
        
        names.insert(0,'name')
        names.insert(1,'breeder')
        
        labels.insert(0,'Name')
        labels.insert(1,'Breeder')
        
#         names.remove(form['type'].name)
#         labels.remove(form['type'].label)
        
        tag_fields = {'all':accessions,
                      'hiddenid':a.id,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': str(accession_type.name) +" Accession Edit", 
                      'filetype':'Accession',
                      'typeid':accession_type.id,
                      'refine_search':True}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':accessions,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form_name':form_name,
                               'form_acc':form_acc,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
        
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            accessions_searched = AccessionLocal.objects.refine_search(or_and, dico_get, accession_type)
            accessions = accessions_searched.intersection(filtered_acc)
            accessions = accessions.order_by('accession_id')
            set_accession_attributes(accessions, attributes)
            
            number_all = len(accessions)
            accessions = add_pagination(request, accessions)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':accessions,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form_name':form_name,
                               'form_acc':form_acc,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
             
            accessions_searched = AccessionLocal.objects.refine_search(or_and, dico_get, accession_type)
            
            #We only want to have accessions of the type considered
            accessions = accessions_searched.intersection(filtered_acc)
            accessions = accessions.order_by('accession_id')
            set_accession_attributes(accessions, attributes)
            
            number_all = len(accessions)
            list_exceptions = ['type','id','rawdata','varietyrepartition']
            download_not_empty = False
            thread_name = ''
            if accessions:
                thread_name = execute_function_in_thread(write_accession_csv_from_viewer, [accessions, "Result search", accession_type.id], {})
                download_not_empty = True
                
            tag_fields.update({'download_not_empty':download_not_empty,
                               'thread_name':thread_name})
            
            accessions = add_pagination(request, accessions)
        
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':accessions,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':False,
                           'form_name':form_name,
                           'form_acc':form_acc,
                           'file_form':file_form,
                           'list_attributes':self.list_attributes})  
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        a = AccessionLocal.objects.get(id=kwargs['object_id'])
        accession_type = AccessionType.objects.get(id=a.accession.type.id)
        
        attributes = accession_type.accessiontypeattribute_set.all()
        
        form_name = self.form_class_name(request.POST)
        form_acc = self.form_class_acc(data=request.POST, attributes=attributes)
        file_form = self.file_form_class(request.POST, request.FILES)
        
        accessions = AccessionLocal.objects.filter(accession__in=Accession.objects.filter(type=accession_type))
        set_accession_attributes(accessions, attributes)
        number_all = len(accessions)
        accessions = add_pagination(request, accessions)
        
        names, labels = get_fields(form_acc)
        
        for attr in AccessionTypeAttribute.objects.filter(accession_type=accession_type):
            self.list_attributes.append(attr.attribute_name)
        
        form_name.fields['type'].widget = forms.HiddenInput()
        form_name.fields['type'].initial = accession_type.id
        
        if a.country != 'FR':
            form_name.fields['name'].disabled = True
            form_name.fields['breeder'].disabled = True
        
        names.insert(0,'name')
        names.insert(1,'breeder')
        
        labels.insert(0,'Name')
        labels.insert(1,'Breeder')
        
        tag_fields = {'all':accessions,
                      'number_all':number_all,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': str(accession_type.name) +" Accession Management",
                      'form_name':form_name,
                      'form_acc':form_acc,
                      'file_form':file_form,
                      'creation_mode':False,
                      'refine_search':True,
                      'filetype':'Accession',
                      'typeid':accession_type.id,
                      'list_attributes':self.list_attributes}
        
        if file_form.is_valid():
            messages.add_message(request, messages.ERROR, "You were in edit mode, please submit your file in management mode.")
            return redirect('accession_management',type_id=accession_type.id)
        
        if form_acc.is_valid():
            update_accession_from_form(request, form_acc, attributes)
        
        return redirect('accession_management',type_id=accession_type.id)

def update_accession_from_form(request, form_acc, attributes):
    """
      Function used to update an accession object from the form.
      :param form form_acc: form submitted by the user for the accession's informations
    """
    if "hiddenid" in request.POST.keys() :
        a = AccessionLocal.objects.get(id=request.POST['hiddenid'])
    else:
        messages.add_message(request, messages.ERROR, "An error as occurred, please try again.")
        return
    
    try:
        acc_name = Accession.objects.get(id=a.accession.id)
        projects = Project.objects.filter(id__in=form_acc.cleaned_data['projects'])
        
        if 'name' in request.POST.keys():
            acc_name.name = request.POST['name']
            acc_name.breeder = request.POST['breeder']
            acc_name.save()

        
        a.country = form_acc.cleaned_data['country']
        a.inscription_date = form_acc.cleaned_data['inscription_date']
        a.maintainer = form_acc.cleaned_data['maintainer']
        a.synonym = form_acc.cleaned_data['synonym']
        a.description = form_acc.cleaned_data['description']
        
        a.projects.clear()
        a.projects.add(*list(projects))
        
        a.update_attributes(attributes, request.POST)
        
        a.save()
        
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return 
    
    messages.add_message(request, messages.SUCCESS, "The accession {0} and it's informations for the country {1} have been updated.".format(acc_name, a.country))
    return 


def accession_filerenderer(type_id):
    """
      Filerenderer specific for accessions, to display in 
      separated columns the accession name, countries' informations and attributes' informations.
    """
    response = HttpResponse(content_type='text/csv') 
    writer = csv.writer(response,delimiter=';')
    headers = filerenderer_headers[9][1].copy()
    
    if type_id != 0:
        accessiontype = AccessionType.objects.get(id=int(type_id))
        filename = accessiontype.name.replace(" ","_")+'.csv'
        database = Accession.objects.filter(type=accessiontype).values_list('name').distinct().order_by('name')
        attribute_names = accessiontype.accessiontypeattribute_set.all()
        
        for attribute in attribute_names:
            headers.append(attribute.attribute_name)
        
    writer.writerow(headers)
    
    database_list = [i[0] for i in database]
    
    for entry_name in database_list:
        acc_name = Accession.objects.get(name=entry_name)
        accessions = AccessionLocal.objects.filter(accession=acc_name)
        for acc in accessions:
            line = [acc_name.name,
                    acc_name.breeder,
                    acc.country,
                    acc.inscription_date,
                    acc.maintainer,
                    acc.synonym,
                    acc.description]
            line.append(' | '.join([str(project) for project in acc.projects.filter()]))
            
            if type_id != 0 and attribute_names:
                line = add_attribute_values(line, acc, attribute_names)

            writer.writerow(line)
            
    response['Content-Disposition'] = 'attachment; filename=%s' %filename  
    writer = csv.writer(response)
    return response


class AccessionHeadersRenderer(View):
    """
      This view return a file containing the headers of an accession 
      file according to type's attributes.
    """
    def get(self, request, *args, **kwargs):
        type_id = kwargs['type_id']
        accession_type = AccessionType.objects.get(id=type_id)
        
        response = HttpResponse(content_type='text/csv') 
        writer = csv.writer(response,delimiter=';', quoting=csv.QUOTE_ALL)

        headers = filerenderer_headers[9][1].copy()
        
        attribute_names = accession_type.accessiontypeattribute_set.all()
        for attribute in attribute_names:
            headers.append(attribute.attribute_name)
            
        writer.writerow(headers)
        
        filename = accession_type.name.replace(" ","_")+'_headers.csv'
        
        response['Content-Disposition'] = 'attachment; filename=%s' %filename  
        writer = csv.writer(response)
        
        return response

def write_accession_csv_from_viewer(model_data, title, type_id):
    """
      Search results writer specific for accession, to display in separated columns 
      the accession name, countries' informations and attributes's informations.
      :param dict model_data: data filtered by the search parameters
      :param string title: title of the file
    """
    if type_id != 0:
        accessiontype = AccessionType.objects.get(id=int(type_id))
        attribute_names = accessiontype.accessiontypeattribute_set.all()
        list_col = filerenderer_headers[9][1].copy()
        
        for attribute in attribute_names:
            list_col.append(attribute.attribute_name)
    else:
        list_col = filerenderer_headers[0][1].copy()
    filename = title.lower().replace(" ","_")
    dialect = Dialect(delimiter = ';')
    
    with open("/tmp/"+filename+".csv", 'w', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, dialect)
        writer.writerow(list_col)
        
        for data in model_data:
            acc_name = data.accession
            
            if type_id != 0:
                row = [acc_name.name,
                       acc_name.breeder,
                       data.country,
                       data.inscription_date,
                       data.maintainer,
                       data.synonym,
                       data.description]
                row.append(' | '.join([str(project) for project in data.projects.filter()]))
            
                if attribute_names:
                    row = add_attribute_values(row, data, attribute_names)
            
            else:
                row = [acc_name.name,
                       acc_name.type,
                       acc_name.breeder,
                       data.country,
                       data.inscription_date,
                       data.maintainer,
                       data.synonym,
                       data.description]
                row.append(' | '.join([str(project) for project in data.projects.filter()]))
            
            writer.writerow(row)
            
    download_not_empty = True
    return download_not_empty


def add_attribute_values(line, accession, attribute_names):
    """
      Get accessions' attribute's values to write in the csv file.
    """
    if type(accession.attributes_values) is dict:
        attributes = accession.attributes_values
        for name in attribute_names:
            for id, value in attributes.items():
                if str(AccessionTypeAttribute.objects.get(id=int(id)).attribute_name) == str(name):
                    line.append(value)
    else:
        attributes = ast.literal_eval(accession.attributes_values)
        for name in attribute_names:
            for id, value in attributes.items():
                if AccessionTypeAttribute.objects.get(id=int(id)).attribute_name == name:
                    line.append(value)
    
    return line


class AccessionInsert(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View with form to upload a file that contains informations about accessions, 
      for all types of accessions. A column type is required in the csv file.
      
      :var str title: title displayed on the template
      :var str template: html template for the rendering of the view
      :var form form_class: the file submission form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var file file: file uploaded by the user in the form
      :var dict file_dict: dict version of the file, parsed by the CSVFactory
    """
    title = "Insert all types of accessions"
    template = 'accession/accession_insert.html'
    form_class = AccessionUploadForm
    
    def get(self, request, *args, **kwargs):
        form = self.form_class()
        
        tag_fields = {'title':self.title,
                      'filetype':"Accession_general",
                      'form':form
                      }
        
        return render(request, self.template, tag_fields) 
    
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        
        tag_fields = {'title':self.title,
                      'filetype':"Accession_general",
                      'form':form
                      }
        
        if form.is_valid():
            file = request.FILES['file']
            myfactory = CSVFactoryForTempFiles(file)
            myfactory.find_dialect()
            myfactory.read_file()
            file_dict = myfactory.get_file()
            
            headers_list = filerenderer_headers[0][1]
            
            missing = myfactory.check_headers_conformity(headers_list)
            
            if missing != []:
                messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                                     ". The required headers for this file are: "+format(', '.join(headers_list))+".")
                return redirect('accession_insert')

            if request.POST.get('submit'):

                with open(settings.MEDIA_ROOT+'tmp/'+file.name, 'wb+') as destination:
                    for chunk in file.chunks():
                        destination.write(chunk)
                destination.close()
                
                return redirect('check_accession_names', type_id=0, file_name=str(destination.name))
                
            elif request.POST.get('update'):
                update_accession_from_file(request, file_dict, "all")
                
            return render(request, self.template, tag_fields)
    
        else:
            messages.add_message(request, messages.ERROR, "An error occurred with your file.")
            return render(request, self.template, tag_fields)


class AccessionViewer(LoginRequiredMixin, View):
    """
      This view contains several forms to perform a query on accessions. The result of the query is displayed 
      in a table with export and refine search functionalities.
      
      :var str template: html template for the rendering of the view
      :var str title: title displayed on the template
      :var list list_attributes: list of the fields the user can search with the refine search module
      :var list list_exceptions: fields of the model that are not displayed
      :var list names: list containing the field names of the model
      :var list labels: list containing the field names of columns
      :var queryset accessions: queryset containing filtered accessions
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    template = 'dataview/accession_viewer.html'
    title = "Accession Viewer"
    list_attributes = ['name', 'type', 'breeder', 'country','inscription_date', 'maintainer', 'synonym', 'description', 'projects']
    list_exceptions = ['type','id','rawdata','varietyrepartition']
    
    names = ['name', 'type', 'breeder', 'country','inscription_date', 'maintainer', 'synonym', 'description', 'projects']
    labels = ['Accession card', 'Accession', 'Type', 'Breeder', 'Country', 'Inscription date', 'Maintainer', 'Synonym', 'Description', 'Projects']
     
    def get(self, request, *args, **kwargs):
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        
        if "nb_per_page" in request.GET and not "in_search" in request.GET:
            tag_fields = {'title': self.title,
                          'fields_name': self.names,
                          'fields_label': self.labels,
                          'n_page':2,
                          'list_attributes':self.list_attributes,
                          'filename':self.title.lower().replace(" ","_")
                          }
             
            projects = request.session['projects']
            types = request.session['types']
            countries = request.session['countries']
            acc_names = Accession.objects.filter(type__in=types)
            accessions = AccessionLocal.objects.filter(accession__in= acc_names, projects__in=projects, country__in=countries).distinct()
             
            number_all = len(accessions)
             
            accessions = add_pagination(request, accessions)
            download_not_empty = True
             
            tag_fields.update({'all': accessions,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'download_not_empty':download_not_empty,
                               'refine_search':True
                               })
             
            return render(request, self.template, tag_fields) 
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            tag_fields = {'title': self.title,
                          'fields_name': self.names,
                          'fields_label': self.labels,
                          'n_page':2,
                          'list_attributes':self.list_attributes,
                          'or_and':or_and,
                          'refine_search':True
                          }
            
            searched_accs = AccessionLocal.objects.viewer_refine_search(or_and, dico_get)
            
            projects = request.session['projects']
            types = request.session['types']
            countries = request.session['countries']
            
            acc_names = Accession.objects.filter(type__in=types)
            filtered_accs = AccessionLocal.objects.filter(accession__in= acc_names, projects__in=projects, country__in=countries).distinct()
            accessions = searched_accs.intersection(filtered_accs)
            accessions = accessions.order_by('accession_id')
            
            number_all = len(accessions)
            accessions = add_pagination(request, accessions)
            download_not_empty = True
            
            tag_fields.update({'all': accessions,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               })
             
            return render(request, self.template, tag_fields) 
        
        #Query in the refine search
        elif "or_and" in request.GET:
            dico_get={}
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
                    
            tag_fields = {'title': self.title,
                          'fields_name': self.names,
                          'fields_label': self.labels,
                          'n_page':2,
                          'list_attributes':self.list_attributes,
                          'or_and':or_and,
                          'refine_search':True
                          }
            
            searched_accs = AccessionLocal.objects.viewer_refine_search(or_and, dico_get)
            
            projects = request.session['projects']
            types = request.session['types']
            countries = request.session['countries']
            
            acc_names = Accession.objects.filter(type__in=types)
            filtered_accs = AccessionLocal.objects.filter(accession__in= acc_names, projects__in=projects, country__in=countries).distinct()
            accessions = searched_accs.intersection(filtered_accs)
            accessions = accessions.order_by('accession_id')
            
            number_all = len(accessions)
            download_not_empty = False
            thread_name = ''
            if accessions:
                thread_name = execute_function_in_thread(write_accession_csv_from_viewer, [accessions, "Result search", 0], {})
                download_not_empty = True
            
            accessions = add_pagination(request, accessions)
            
            tag_fields.update({'all': accessions,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'thread_name':thread_name
                               })
            
            return render(request, self.template, tag_fields)   
        
        formproject = DataviewProjectForm()

        username = request.user.login
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)

        formproject.fields['project'].queryset=projects.order_by('name')

        n_page = 0
        return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formproject':formproject})


    def post(self, request, *args, **kwargs):
        formproject = DataviewProjectForm(request.POST)
        #Get the actual page number
        n_page = int(request.POST['n_page'])
        nb_per_page = 50 
        
        if int(n_page) == 1:
            
            if formproject.is_valid():
                projects = request.POST.getlist('project')
                request.session['projects'] = projects
            else:
                return render(request,self.template,{'title':self.title, 'n_page':0, 'formproject':formproject})
            
            formcountry = DataviewCountryForm()
            formacctype = DataviewAccessionTypeForm()
            
            return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formcountry':formcountry, 'formacctype': formacctype})

        if int(n_page) == 2:
            
            formcountry = DataviewCountryForm(request.POST)
            formacctype = DataviewAccessionTypeForm(request.POST)

            if formcountry.is_valid() and formacctype.is_valid():
                countries = request.POST.getlist('country')
                types = request.POST.getlist('accession_type')
                request.session['countries'] = countries
                request.session['types'] = types
            else:
                return render(request,self.template,{'title':self.title, 'n_page':1, 'formcountry':formcountry, 'formacctype': formacctype})
            
            projects = request.session['projects']
            acc_names = Accession.objects.filter(type__in=types)
            accessions = AccessionLocal.objects.filter(accession__in= acc_names, projects__in=projects, country__in=countries).distinct()

            if not accessions:
                messages.add_message(request, messages.INFO, "There is no results for your query.")
                return render(request, self.template, {'title':self.title, 'n_page':n_page})
            
            number_all = len(accessions)
            
            #Export of the query
#             download_not_empty = write_accession_csv_from_viewer(accessions, self.title, 0)

            #Usage of a thread: the file is computed while the page is displayed
            thread_name = execute_function_in_thread(write_accession_csv_from_viewer, [accessions, self.title, 0], {})
            download_not_empty = True
            
            accessions = add_pagination(request, accessions)
            
            tag_fields = {'all': accessions,
                          'title': self.title,
                          'fields_name': self.names,
                          'fields_label': self.labels,
                          'n_page':n_page,
                          'list_attributes':self.list_attributes,
                          'download_not_empty':download_not_empty,
                          'thread_name':thread_name,
                          'filename':self.title.lower().replace(" ","_"),
                          'number_all':number_all,
                          'nb_per_page':nb_per_page,
                          'refine_search':True
                          }
            
            return render(request, self.template, tag_fields)

