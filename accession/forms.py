"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from django import forms

from django_countries.fields import CountryField
from django.contrib.admin.widgets import FilteredSelectMultiple

from django_countries import countries, Countries
from dal import autocomplete

from accession.models import AccessionLocal, Accession, AccessionType, AccessionTypeAttribute
from team.models import Project, User

class AccessionTypeForm(forms.ModelForm):
    """
      Form based on the AccessionType model managing data from this model
    """
    class Meta:
        model = AccessionType
        fields = '__all__'

class AccessionTypeAttributeForm(forms.ModelForm):
    """
      Form based on the AccessionTypeAttribute model managing data from this model
    """
    class Meta:
        model = AccessionTypeAttribute
        exclude = ['accession_type']

class AccessionForm(forms.ModelForm):
    """
      Form based on the Accession model, allowing the creation of new accessions' names into the database
    """
    class Meta:
        model = Accession
        fields = '__all__'
      
       
class AccessionLocalForm(forms.ModelForm):
    """
      Form based on the AccessionLocal model, allowing the creation of new local accessions into the database
    """
    class Meta:
        model = AccessionLocal
        exclude = ['accession', 'attributes_values']
        
    def __init__(self, *args, **kwargs):
        if 'attributes' in kwargs.keys():
            attributes = kwargs['attributes']
            if 'data' in kwargs.keys():
                if 'instance' in kwargs.keys():
                    super(AccessionLocalForm, self).__init__(data=kwargs['data'], instance=kwargs['instance'])
                else :
                    super(AccessionLocalForm, self).__init__(data=kwargs['data'])
            else :
                super(AccessionLocalForm, self).__init__()
             
            for attr in attributes:
                self.fields[attr.attribute_name.lower().replace(' ','_')] = forms.CharField(required = False)

        else :
            if 'data' in kwargs.keys():
                super(AccessionLocalForm, self).__init__(data=kwargs['data'])
            else :
                super(AccessionLocalForm, self).__init__()

class AccessionUploadForm(forms.Form):
    """
      Form used to upload a file containing informations on accessions.
    """
    file = forms.FileField(label='Select the file to upload ')

class AccessionTypeSelectAttribute(forms.Form):
    """
      Form used to select attributes when creating or updating an accession type.
    """
    descriptors = forms.ModelMultipleChoiceField(required=False,
                                                 queryset=AccessionTypeAttribute.objects.all(),
                                                 widget=autocomplete.ModelSelect2Multiple())