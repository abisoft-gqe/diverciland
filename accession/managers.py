"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import ast

from operator import ior, iand

from django.apps import apps
from django.db import models
from django.db.models import Q

class AccessionManager(models.Manager):
    """
      Manager for accession model.
    """
    def by_username(self,username):
        """
          Returns only the accessions from projects linked to this user 

          :param string username : username

          :var int username_id : user's id
          :var list projects : projects linked to the user
        """
        User = apps.get_model('team', 'user')
        Project = apps.get_model('team', 'project')
        
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        return self.filter(projects__in=projects).distinct()
    
    
    def refine_search(self, or_and, dico_get, accession_type):
        """
          Search in all accession according to some parameters

          :param string or_and : link between parameters (or, and)
          :param dict dico_get : data to search for
        """
        # Managers are called in accession.model
        # To avoid crossed imports we use apps to get objects from the model 
        Accession = apps.get_model('accession', 'accession')
        AccessionLocal = apps.get_model('accession', 'accessionlocal')
        AccessionType = apps.get_model('accession','accessiontype')
        AccessionTypeAttribute = apps.get_model('accession', 'accessiontypeattribute')
        Project = apps.get_model('team', 'project')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
            
        proj=[]
        query=Q()
        list_attr=[]
        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        query = myoperator(query, Q(projects__isnull=True))
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query = myoperator(query, Q(projects__in=proj))
                
                elif "type" in dico_type_data:
                    type=(AccessionType.objects.filter(name__icontains=dico_type_data['type']))
                    acc_names=(Accession.objects.filter(type__in=type))
                    query = myoperator(query, Q(accession__in=acc_names))
                    
                elif "name" in dico_type_data:
                    acc_names=(Accession.objects.filter(name__icontains=dico_type_data['name']))
                    query = myoperator(query, Q(accession__in=acc_names))
                
                elif "breeder" in dico_type_data:
                    acc_names=(Accession.objects.filter(breeder__icontains=dico_type_data['breeder']))
                    query = myoperator(query, Q(accession__in=acc_names))
                
                else:
                    for data_type, value in dico_type_data.items():
                        list_id_in=[]
                        for i in AccessionTypeAttribute.objects.filter(accession_type=accession_type):
                            if str(data_type) == str(i):
                                list_attr.append(str(i))
                        if data_type in list_attr:
                            id_attribute = str(AccessionTypeAttribute.objects.get(attribute_name=data_type, accession_type=accession_type).id)
                            for i in AccessionLocal.objects.filter(accession__in=Accession.objects.filter(type=accession_type)):
                                try:
                                    if id_attribute in i.attributes_values.keys():
                                        if value in ast.literal_eval(i.attributes_values)[id_attribute]:
                                            list_id_in.append(i.id)
                                except:
                                    try:
                                        if id_attribute in i.attributes_values.keys():
                                            if value in i.attributes_values[id_attribute]:
                                                list_id_in.append(i.id)
                                    except:
                                        i.attributes_values = ast.literal_eval(i.attributes_values)
                                        if id_attribute in i.attributes_values.keys():
                                            if value in i.attributes_values[id_attribute]:
                                                list_id_in.append(i.id)

                            query = myoperator(query, Q(id__in = list_id_in))
                
                        else:
                            var_search = str( data_type + "__icontains" )
                            query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))
        
        return AccessionLocal.objects.filter(query).distinct()
    
    
    def viewer_refine_search(self, or_and, dico_get):
        """
          Search in all accession according to some parameters (special for accession viewer)

          :param string or_and : link between parameters (or, and)
          :param dict dico_get : data to search for
        """
        # Managers are called in accession.model
        # To avoid crossed imports we use apps to get objects from the model 
        Accession = apps.get_model('accession', 'accession')
        AccessionLocal = apps.get_model('accession', 'accessionlocal')
        AccessionType = apps.get_model('accession','accessiontype')
        Project = apps.get_model('team', 'project')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
            
        proj=[]
        query=Q()
        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                var_search = str( data_type + "__icontains" )
                
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        query = myoperator(query, Q(projects__isnull=True))
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query = myoperator(query, Q(projects__in=proj))
                
                elif "type" in dico_type_data:
                    type=(AccessionType.objects.filter(name__icontains=dico_type_data['type']))
                    acc_names=(Accession.objects.filter(type__in=type))
                    query = myoperator(query, Q(accession__in=acc_names))
                    
                elif "name" in dico_type_data:
                    acc_names=(Accession.objects.filter(name__icontains=dico_type_data['name']))
                    query = myoperator(query, Q(accession__in=acc_names))
                
                elif "breeder" in dico_type_data:
                    acc_names=(Accession.objects.filter(breeder__icontains=dico_type_data['breeder']))
                    query = myoperator(query, Q(accession__in=acc_names))
                
                else:
                    query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))
        
        return AccessionLocal.objects.filter(query).distinct()
    
    
    
    
    
    
    
    
    
    
    