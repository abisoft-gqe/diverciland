"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from django.urls import path
from accession.views import AccessionTypeManagement, AccessionTypeEdit, AccessionManagement, AccessionEdit,\
                            AccessionViewer, AccessionInsert, AccessionTypeAttributeManagement, AccessionTypeAttributeEdit,\
                            AccessionHeadersRenderer, CheckAccessionFromFile

urlpatterns = [
    path('accessiontype/', AccessionTypeManagement.as_view(), name='accessiontype_management'),
    path('accessiontype/<int:object_id>/', AccessionTypeEdit.as_view(), name='accessiontype_edit'),
    path('accessiontypeattribute/', AccessionTypeAttributeManagement.as_view(), name='accessiontypeattribute_management'),
    path('accessiontypeattribute/<int:object_id>/', AccessionTypeAttributeEdit.as_view(), name='accessiontypeattribute_edit'),
    path('accession/type/<int:type_id>/', AccessionManagement.as_view(), name='accession_management'),
    path('accession/<int:object_id>/', AccessionEdit.as_view(), name='accession_edit'),
    path('checkaccessionnames/<int:type_id>/<path:file_name>/', CheckAccessionFromFile.as_view(), name='check_accession_names'),
    path('insert/', AccessionInsert.as_view(), name='accession_insert'),
    path('accessionviewer/', AccessionViewer.as_view(), name='accession_viewer'),
    
    path('accessionheadersrenderer/<int:type_id>/',AccessionHeadersRenderer.as_view(), name='accessionheadersrenderer')
]