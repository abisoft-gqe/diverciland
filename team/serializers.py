"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from rest_framework import serializers

from team.models import Project, Person, Institution

class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('name',)
        
        
class PersonSerializer(serializers.ModelSerializer):
#     def __init__(self, *args, **kwargs):
#         super(PersonSerializer, self).__init__(*args, **kwargs)
#         # change a widget attribute:
#         self.fields['note_on_person'].widget.attrs["cols"] = 20
#         self.fields['note_on_person'].widget.attrs["rows"] = 1
#         self.fields['institutions'].widget.attrs['style'] = 'width: 180px;height: 120px;'
#         self.fields['institutions'].queryset = Institution.objects.order_by('name')
#         
    class Meta:
        model = Person
        fields = '__all__'