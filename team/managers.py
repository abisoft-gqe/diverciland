"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from operator import ior, iand

from django.db import models
from django.contrib.auth.models import BaseUserManager
from django.utils import timezone
from django.apps import apps
from django.db.models import Q

class UserManager(BaseUserManager):
    """
        This manager allows the creation of an user
    """
    def create_user_with_person(self, username, password, person, **extra_fields ):
        """
            This function creates an user from a person previously created
        """
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        user = self.model(login=username, last_login=now, start_date=now, person=person, **extra_fields)
        user.set_password(password)
        user.save(using=self._db) 

        return user
    
    def create_user(self, username, password, first_name, last_name, **extra_fields):
        """
            This function creates a person if not created and calls the create_user_with_person function to create an user based on this person
        """
        person_class = apps.get_model('team', 'Person')
        person, created = person_class.objects.get_or_create(first_name=first_name, last_name=last_name)
        user = self.create_user_with_person(username, password, person, **extra_fields)
        return user
    
    def create_superuser(self, username, password, first_name, last_name, **extra_fields):
        """
            This function calls the create_superuser function and creates an user with all rights: it creates a superuser
        """
        user = self.create_user(username, password, first_name, last_name, **extra_fields)
        user.is_admin = True
        user.save(using=self._db)
        return user
    
    def refine_search(self, or_and, dico_get):
        """
            Search in all persons according to some parameters
    
            :param string or_and : link between parameters (or, and)
            :param dict dico_get : data to search for
        """
        User = apps.get_model('team', 'user')
        Person = apps.get_model('team', 'person')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
        
        query = Q()
        
        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                var_search = str( data_type + "__icontains" )
                
                if data_type == 'person':
                    query_person=Q()
                    query_person |= Q(last_name__icontains=dico_type_data[data_type])
                    query_person |= Q(first_name__icontains=dico_type_data[data_type])
                    query = myoperator(query, Q(person__in=Person.objects.filter(query_person)))
                else:
                    query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))
        
        return User.objects.filter(query).distinct()
        

class PersonManager(models.Manager):
    """
    Manager for person model.
    """
    def refine_search(self, or_and, dico_get):
        """
            Search in all persons according to some parameters
    
            :param string or_and : link between parameters (or, and)
            :param dict dico_get : data to search for
        """
        Person = apps.get_model('team', 'person')
        Institution = apps.get_model('team', 'institution')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
        
        query = Q()
        
        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                var_search = str( data_type + "__icontains" ) 
                var_search = var_search.replace(' ','_')
                
                if data_type == 'institutions':
                    query_inst = Q()
                    query_inst = myoperator(query_inst, Q(name__icontains=dico_type_data[data_type]))
                    query = myoperator(query, Q(institutions__in=Institution.objects.filter(query_inst)))
                else:
                    query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))
        
        return Person.objects.filter(query).distinct()
    
class InstitutionManager(models.Manager):
    """
    Manager for institution model.
    """
    def refine_search(self, or_and, dico_get):
        """
            Search in all institutions according to some parameters
    
            :param string or_and : link between parameters (or, and)
            :param dict dico_get : data to search for
        """
        Institution = apps.get_model('team', 'institution')
    
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
        
        query = Q()
        
        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                var_search = str( data_type + "__icontains" ) 
                var_search = var_search.replace(' ','_')
                query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))
        
        return Institution.objects.filter(query).distinct()
        
class ProjectManager(models.Manager):
    """
    Manager for project model.
    """
    def refine_search(self, or_and, dico_get):
        """
            Search in all projects according to some parameters
    
            :param string or_and : link between parameters (or, and)
            :param dict dico_get : data to search for
        """
        Project = apps.get_model('team', 'project')
        User = apps.get_model('team', 'user')
        Institution = apps.get_model('team', 'institution')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
        
        query = Q()

        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                var_search = str( data_type + "__icontains" ) 
                var_search = var_search.replace(' ','_')
                
                if data_type == "users":
                    query_user = Q()
                    query_user = myoperator(query_user, Q(login__icontains=dico_type_data[data_type]))
                    query = myoperator(query, Q(users__in=User.objects.filter(query_user)))
                elif data_type == "institutions":
                    query_inst = Q()
                    query_inst = myoperator(query_inst, Q(name__icontains=dico_type_data[data_type]))
                    query = myoperator(query, Q(institutions__in=Institution.objects.filter(query_inst)))
                else:
                    query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))
                
        return Project.objects.filter(query).distinct()


