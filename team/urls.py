"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from django.urls import path
from team.views import PersonManagement, PersonEdit, InstitutionManagement, InstitutionEdit,\
                       UserManagement, UserEdit, ProjectManagement, ProjectEdit, ProjectViewer,\
                       LinkData

urlpatterns = [
    #Admin
    path('institution/', InstitutionManagement.as_view(), name='institution_management'),
    path('institution/<int:object_id>/', InstitutionEdit.as_view(), name='institution_edit'),
    path('person/', PersonManagement.as_view(), name='person_management'),
    path('person/<int:object_id>/', PersonEdit.as_view(), name='person_edit'),
    path('user/', UserManagement.as_view(), name='user_management'),
    path('user/<int:object_id>/', UserEdit.as_view(), name='user_edit'),
    path('project/', ProjectManagement.as_view(), name='project_management'),
    path('project/<int:object_id>/', ProjectEdit.as_view(), name='project_edit'),
    path('link_data/', LinkData.as_view(), name='link_data'),
    
    #Dataview
    path('projectviewer/', ProjectViewer.as_view(), name='project_viewer'),
    
]