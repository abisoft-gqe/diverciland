"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from django.db import models
from django_countries.fields import CountryField
from django.contrib.auth.models import AbstractBaseUser
from django.conf import settings

from team.managers import UserManager, PersonManager, InstitutionManager, ProjectManager

class Institution(models.Model):
    """
        The Institution model gives information about the different attributes of an institution.
        Only the name of the institution is mandatory.
        
        
        :var CharField name: the name of the institution
        :var CharField acronym: the short name of the institution
        :var TextField address: the location of the institution
        :var CharField postal_code: the postal code of the institution
        :var CharField country: the country from where is the institution
        :var URLField web_site: the address where can be found the web site of the institution
        :var EmailField email_address: the email address of the institution
    
    """
    name = models.CharField(max_length=200, unique=True)
    acronym = models.CharField(max_length=50, blank=True, null=True)
    address = models.TextField(max_length=500, blank=True, null=True)
    postal_code = models.CharField(max_length=20, blank=True, null=True)
    city = models.CharField(max_length=200, blank=True, null=True)
    country = CountryField(blank=True, null=True)
    web_site = models.URLField(max_length=200, blank=True, null=True)
    email_address = models.EmailField(max_length=200, blank=True, null=True)

    objects = InstitutionManager()

    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ['name']

class Person(models.Model):
    """
        The Person model gives information about the person. Be careful, it is different of User.
        Only the first name and last name fields are mandatory.
        
        
        :var CharField first_name: the first name of the person
        :var CharField last_name: the last name of the person
        :var TextField initial: the initial (first letters of the first name and of the last name) of the person
        :var CharField title_of_person: the title of the person
        :var CharField other_language: the other language which might be spoken by the person
        :var CharField work_phone: the work phone number of the person
        :var CharField work_extension: the work extension of the person
        :var CharField fax_number: the fax number of the person
        :var EmailField email_address: the email address of the person
        :var TextField note_on_person: description of the person
        :var ManyToManyField institutions: institution where the person is from
    
    """
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    initial = models.CharField(max_length=25,blank=True, null=True)
    title_of_person = models.CharField(max_length=25, blank=True, null=True)
    other_language = models.CharField(max_length=25, blank=True, null=True)
    work_phone = models.CharField(max_length=25, blank=True, null=True)
    work_extension = models.CharField(max_length=25, blank=True, null=True)
    fax_number = models.CharField(max_length=25, blank=True, null=True)
    email_address = models.EmailField(max_length=200, blank=True, null=True)
    note_on_person = models.TextField(max_length=200, blank=True, null=True)
    institutions = models.ManyToManyField('Institution', blank=True)
    
    objects = PersonManager()
    
    def __str__(self):
        return '%s %s' %(self.first_name, self.last_name)
    
    class Meta:
        unique_together=("first_name","last_name")
        ordering = ['first_name','last_name']

        
class User(AbstractBaseUser):
    """
        The User model gives information about the user. A user is a person with access to the database. This model also manages the rights granted to a user.
        
        
        :var CharField person: the real name of the user
        :var CharField login: the login of the user
        :var DateField start_date: the date when the account of the user is activated
        :var DateField end_date: the date when the account of the user is closed
        :var BooleanField is_admin: check if the user is admin
    
    """
    person = models.OneToOneField('Person', on_delete=models.CASCADE)
    login = models.CharField(max_length=50, unique=True)
    start_date = models.DateField(auto_now_add=True)
    end_date = models.DateField(blank=True, null=True)
    is_admin = models.BooleanField(default=False)
    
    objects = UserManager()
    
    USERNAME_FIELD = 'login'

    def get_full_name(self):
        return "{0} {1}".format(self.person.first_name,self.person.last_name)
    
    def get_short_name(self):
        return self.person.initialOrOtherName

    def is_superuser(self):
        return self.is_admin
    
    def is_admin_user(self):
        return self.is_admin
    
    class Meta:
        ordering = ['login']

class Project(models.Model):
    """
        The Project model gives information about the project. Users linked to a project 
        have access to any data belonging to this project.
        
        
        :var CharField name: the name of the project
        :var CharField authors: the authors who made the project
        :var DateField start_date: the beginning date of the project
        :var DateField end_date: the ending date of the project
        :var TextField funding: funding of the project
        :var TextField description: the description of the project
        :var ManyToManyField users: the users who have rights on the project 

    """
    name = models.CharField(max_length=200, unique=True)
    authors = models.CharField(max_length=200)
    start_date = models.DateField()
    end_date = models.DateField(blank=True, null=True)
    funding = models.TextField(max_length=500, blank=True, null=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    
    users = models.ManyToManyField(settings.AUTH_USER_MODEL)
    institutions = models.ManyToManyField('Institution')
    
    objects = ProjectManager()

    def __str__(self):
        return self.name    

    class Meta:
        ordering = ['name']
