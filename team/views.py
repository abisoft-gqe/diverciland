"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import ast

from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import render, redirect
from django.views import View

from team.forms import PersonForm, InstitutionForm, PersonInfosUserForm, UserCreateForm, UserUpdateForm, ProjectForm
from team.models import User, Project, Person, Institution
from commonfct.genericviews import generic_create, generic_update, write_csv_from_viewer
from commonfct.utils import get_fields, add_pagination

class UserIsAdminMixin(UserPassesTestMixin):
    """ This class overload the UserPassesTestMixin and test if the user is admin. 
        Allows to restrict access to some views only to admin users. """
    def test_func(self):
        return self.request.user.is_admin_user()

class Authentication(View):
    """
        View allowing the authentication of a user.
        "home" is True so that the home tab is colored in orange and the user knows which tab he is on.
        
        :var string username: username gotten from the request.POST
        :var string password: password gotten from the request.POST
        :var model user: using the function authenticate from django, this variable gets an User object
        :var model personname: the name of the person authenticated
        :var int person_id: the id of the person authenticated
        :var queryset projects: the projects which the user have access to
    """
    
    def get(self, request, *args, **kwargs):
        #In get we only display the authentication form
        return render(request,'team/login.html',{'form':AuthenticationForm()})
    
    def post(self, request, *args, **kwargs):
        #We retrieve the username and password from post request
        username = request.POST['username']
        password = request.POST['password']
        #We use the django's authenticate function to retrieve a User object
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active: #we test if the user's status is active
                login(request, user) #if yes he we logged him
                personname = User.objects.get(login=username).person
                person_id = User.objects.get(login=username).person_id
                projects = list(Project.objects.filter(users=person_id))
                return render(request,'team/home.html',{'projects':projects,'personname':personname,'home':True})
            else:
                #The authentication form is returned with an error message
                return render(request,'team/login.html',{'form':AuthenticationForm(data=request.POST),'message':'Authentication failed !'})
        else:
            #The authentication form is returned with an error message
            return render(request,'team/login.html',{'form':AuthenticationForm(data=request.POST),'message':'Authentication failed !'})

class Home(LoginRequiredMixin, View):
    """
       View rendering the home page.
       "home" is True so that the home tab is colored in orange and the user knows which tab he is on.
    """
    def get(self, request, *args, **kwargs):
        return render(request,'team/home.html')
    
    
class UserProfile(LoginRequiredMixin, View):
    """
      View listing the projects which the user has access to.
      "user_profile" is True so that the user profile tab is orange and the user knows which tab he is on.
      
      :var string login: get the login of the user
      :var model personname: model of the person authenticated
      :var int person_id: id of the person authenticated
      :var queryset projects: if the person is a superuser, he has access to all the projects and all of them are displayed. Else, only the projects he as access to are displayed and contained in this variable
    """
    def get(self, request, *args, **kwargs):
        
        login = request.user.login
        personname = User.objects.get(login=login).person
        user_id = User.objects.get(login=login).id
        projects = list(Project.objects.filter(users=user_id))
        
        return render(request,'team/user_profile.html',{'projects':projects,'personname':personname})
        
class PageNotAvailable(LoginRequiredMixin, View):
    """
      View returning a special template for the views that are not available yet.
    """
    def get(self, request, *args, **kwargs):
        return render(request,'common/page_not_available.html',{})
        
class PersonManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the person model. It allows the deletion and insertion of persons based on the Person Model.
      The filters are applied to this view. The user can make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list containing the names of the fields which can be searched with the refine search 
      :var form form_class: the person form
      :var queryset persons: contains all the persons of the database
      :var int number_all: contains the number of persons of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = PersonForm
    template = 'team/users_generic.html'
    list_attributes=['first name', 'last name', 'initial','work phone', 'email address', 'institutions']
    
    def get(self, request, *args, **kwargs):
        form = self.form_class()
        persons = Person.objects.all()
        number_all = len(persons)
        persons = add_pagination(request,persons)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        tag_fields = {'all':persons,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "Person Management", 
                      'refine_search':True}
         
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':persons,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'list_attributes':self.list_attributes})
 
            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            persons = Person.objects.refine_search(or_and, dico_get)
             
            number_all = len(persons)
            persons = add_pagination(request,persons)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':persons,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'list_attributes':self.list_attributes})
 
            return render(request,self.template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
             
            persons = Person.objects.refine_search(or_and, dico_get)
             
            number_all = len(persons)
            list_exceptions = ['user','id']
            download_not_empty = False
            if persons:
                download_not_empty = write_csv_from_viewer(persons, list_exceptions, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            persons = add_pagination(request,persons)
        
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':persons,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           'list_attributes':self.list_attributes})  
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        request,template,tag_fields = generic_create(Person, self.form_class, request, self.template,{'refine_search':True,
                                                                                                      'list_attributes':self.list_attributes}, "Person Management")
        return render(request,template,tag_fields)

class PersonEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of person model. The user can still make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list containing the names of the fields which can be searched with the refine search 
      :var form form_class: the person form
      :var queryset persons: contains all the persons of the database
      :var int number_all: contains the number of persons of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = PersonForm
    template = 'team/users_generic.html'
    list_attributes=['first name', 'last name', 'initial']
    
    def get(self, request, *args, **kwargs):
        try:
            p = Person.objects.get(id=kwargs['object_id'])
        except :
            messages.add_message(request, messages.ERROR, "This person doesn't exist in the database.")
            return redirect('person_management')
        
        form = self.form_class(initial={'first_name':p.first_name,
                                        'last_name':p.last_name,
                                        'initial':p.initial,
                                        'title_of_person':p.title_of_person,
                                        'other_language':p.other_language,
                                        'work_phone':p.work_phone,
                                        'work_extension':p.work_extension,
                                        'fax_number':p.fax_number,
                                        'email_address':p.email_address,
                                        'note_on_person':p.note_on_person,
                                        'institutions':p.institutions.all()
                                        })
        
        persons = Person.objects.all()
        number_all = len(persons)
        persons = add_pagination(request,persons)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        tag_fields = {'all':persons,
                      'hiddenid':p.id,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "Person Edit", 
                      'refine_search':True}
         
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':persons,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'list_attributes':self.list_attributes})
 
            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            persons = Person.objects.refine_search(or_and, dico_get)
             
            number_all = len(persons)
            persons = add_pagination(request,persons)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':persons,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'list_attributes':self.list_attributes})
 
            return render(request,self.template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
             
            persons = Person.objects.refine_search(or_and, dico_get)
             
            number_all = len(persons)
            list_exceptions = ['user','id']
            download_not_empty = False
            if persons:
                download_not_empty = write_csv_from_viewer(persons, list_exceptions, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            persons = add_pagination(request,persons)
            
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':persons,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':False,
                           'form':form,
                           'list_attributes':self.list_attributes})  
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        request,template,tag_fields = generic_update(Person, self.form_class, request, self.template,{'refine_search':True,
                                                                                                      'list_attributes':self.list_attributes}, "Person Management")
        return redirect('person_management')


class InstitutionManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the institution model. It allows the deletion and insertion of institutions based on the Institution Model.
      The filters are applied to this view. The user can make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list of the fields the user can search with the refine search module  
      :var form form_class: the institution form based on the Institution model
      :var queryset institutions: list of all the institutions from the database
      :var int number_all: number of all the institutions in the database
      :var int nb_per_page: number of data to display per page, initialized to 10
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = InstitutionForm
    template = 'team/users_generic.html'
    list_attributes = ['name', 'acronym', 'address', 'postal code', 'city', 'country', 'web site', 'email address']
    
    def get(self, request, *args, **kwargs):
        form = self.form_class()
        institutions = Institution.objects.all()
        number_all = len(institutions)
        institutions = add_pagination(request,institutions)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        tag_fields = {'all':institutions,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "Institution Management",
                      'refine_search':True}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':institutions,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'list_attributes':self.list_attributes})

            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            institutions = Institution.objects.refine_search(or_and, dico_get)
            
            number_all = len(institutions)
            institutions = add_pagination(request,institutions)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':institutions,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'list_attributes':self.list_attributes})

            return render(request,self.template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
            
            institutions = Institution.objects.refine_search(or_and, dico_get)
            
            number_all = len(institutions)
            list_exceptions = ['id', 'person', 'project','experiment']
            download_not_empty = False
            if institutions:
                download_not_empty = write_csv_from_viewer(institutions, list_exceptions, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            institutions = add_pagination(request,institutions)

        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':institutions,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           'list_attributes':self.list_attributes})  
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        request,template,tag_fields = generic_create(Institution, self.form_class, request, self.template,{'refine_search':True,
                                                                                                           'list_attributes':self.list_attributes}, "Institution Management")
        return render(request,template,tag_fields)
    
class InstitutionEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of institution model. The user can still make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list of the fields the user can search with the refine search module  
      :var form form_class: the institution form based on the Institution model
      :var queryset institutions: list of all the institutions from the database
      :var int number_all: number of all the institutions in the database
      :var int nb_per_page: number of data to display per page, initialized to 10
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = InstitutionForm
    template = 'team/users_generic.html'
    list_attributes = ['name', 'acronym', 'address', 'postal code', 'city', 'country', 'web site', 'email address']
    
    def get(self, request, *args, **kwargs):
        try:
            i = Institution.objects.get(id=kwargs['object_id'])
        except :
            messages.add_message(request, messages.ERROR, "This institution doesn't exist in the database.")
            return redirect('institution_management')
        
        form = self.form_class(initial={'name':i.name,
                                        'acronym':i.acronym,
                                        'address':i.address,
                                        'postal_code':i.postal_code,
                                        'city':i.city,
                                        'country':i.country,
                                        'web_site':i.web_site,
                                        'email_address':i.email_address
                                        })
        
        institutions = Institution.objects.all()
        number_all = len(institutions)
        institutions = add_pagination(request,institutions)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        tag_fields = {'all':institutions,
                      'hiddenid':i.id,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "Institution Edit",
                      'refine_search':True}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':institutions,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'list_attributes':self.list_attributes})

            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            institutions = Institution.objects.refine_search(or_and, dico_get)
            
            number_all = len(institutions)
            institutions = add_pagination(request,institutions)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':institutions,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'list_attributes':self.list_attributes})

            return render(request,self.template,tag_fields)
            
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
            
            institutions = Institution.objects.refine_search(or_and, dico_get)
            
            number_all = len(institutions)
            list_exceptions = ['id', 'person', 'project','experiment']
            download_not_empty = False
            if institutions:
                download_not_empty = write_csv_from_viewer(institutions, list_exceptions, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            institutions = add_pagination(request,institutions)

        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':institutions,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':False,
                           'form':form,
                           'list_attributes':self.list_attributes})  
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        request,template,tag_fields = generic_update(Institution, self.form_class, request, self.template,{'refine_search':True,
                                                                                                           'list_attributes':self.list_attributes}, "Institution Management")
        return redirect('institution_management')
    
    
class UserManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the user model. It allows the deletion and insertion of users based on the User Model.
      A user account can be link to a new person or to an existing person if it is not already linked to a user account.
      Thanks to the User model and this view, superusers are created.
    
      :var str template: html template for the rendering of the view
      :var list list_attributes: list of the fields the user can search with the refine search module
      :var form form_class_person: the person form based on the Person model
      :var form form_class_user: the user form based on the User model
      :var queryset users: list of all the users from the database
      :var int number_all: number of all the users in the database
      :var int nb_per_page: number of data to display per page, initialized to 10
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class_person = PersonInfosUserForm
    form_class_user = UserCreateForm
    template = 'team/user_management.html'
    list_attributes = ['login', 'person', 'is_admin']
    
    def get(self, request, *args, **kwargs):
        form_person = self.form_class_person()
        form_user = self.form_class_user()
        users = User.objects.all()
        number_all = len(users)
        users = add_pagination(request,users)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form_user)
        
        #To have person field in the table between login and is_admin (not in the form)
        names.insert(1,'person')
        labels.insert(1,'Person')
        
        names.remove(form_user['password'].name)
        labels.remove(form_user['password'].label)
        
        tag_fields = {'all':users,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "User Management",
                      'refine_search':True}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':users,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form_person':form_person,
                               'form_user':form_user,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            users = User.objects.refine_search(or_and, dico_get)
            
            number_all = len(users)
            users = add_pagination(request,users)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':users,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form_person':form_person,
                               'form_user':form_user,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
    
            users = User.objects.refine_search(or_and, dico_get)
            
            number_all = len(users)
            list_exceptions = ['project','start_date','password','end_date','id','last_login','rawdata']
            download_not_empty = False
            if users:
                download_not_empty = write_csv_from_viewer(users, list_exceptions, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            users = add_pagination(request,users)
            
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':users,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form_person':form_person,
                           'form_user':form_user,
                           'list_attributes':self.list_attributes})
        
        return render(request,self.template,tag_fields)
    
    def post(self, request, *args, **kwargs):
        form_person = self.form_class_person(data=request.POST)
        form_user = self.form_class_user(data=request.POST)
        
        if form_person.is_valid():
            person = Person.objects.create(first_name=request.POST['first_name'], last_name=request.POST['last_name'])
            messages.add_message(request, messages.SUCCESS, "A new Person named {0} has been created.".format(person))
        else:
            try :
                #To handle the case where the person exists but is not linked to a user account
                person = Person.objects.get(first_name=request.POST['first_name'], last_name=request.POST['last_name'])
                #if the person exists but is not linked to a user account the form is valid
                if not User.objects.filter(person=person):
                    form_person.is_valid = True
                else:
                    messages.add_message(request, messages.ERROR, "This person is already linked to a user account.")
                    return redirect('user_management')
            except:  
                messages.add_message(request, messages.ERROR, ' '.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form_person.errors.items()]))
                return redirect('user_management')
        
        if form_user.is_valid():
            username = request.POST['login']
            password = request.POST['password']
            
            extrafields = {}
            field = 'is_admin'
            
            #Checkboxes management : if the checkbox is not checked, there is no references of the field in request.POST
            if field in request.POST.keys() and request.POST[field] == 'on':
                extrafields[field] = True
            else :
                extrafields[field] = False
                    
            new_user = User.objects.create_user_with_person(username, password, person, **extrafields)
            messages.add_message(request, messages.SUCCESS, "A new User named {0} has been created.".format(new_user))
            return redirect('user_management')
        else :
            messages.add_message(request, messages.ERROR, ' '.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form_user.errors.items()]))
            return redirect('user_management')
        
class UserEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of user model. The user can still make a refined request on the table.
      The edition allows the modification of the name and last name of the person.
      The password can't be edited. To create a new password, you have to use the python shell of the application. (user.set_password())
    
      :var str template: html template for the rendering of the view
      :var list list_attributes: list of the fields the user can search with the refine search module
      :var form form_class_person: the person form based on the Person model
      :var form form_class_user: the user form based on the User model
      :var queryset users: list of all the users from the database
      :var int number_all: number of all the users in the database
      :var int nb_per_page: number of data to display per page, initialized to 10
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
      :var dict tag_fields: dictionary containing the parameters passed to the template
    """
    form_class_person = PersonInfosUserForm
    form_class_user = UserUpdateForm
    template = 'team/user_management.html'
    list_attributes = ['login', 'person', 'is_admin']
    
    def get(self, request, *args, **kwargs):
        try:
            u = User.objects.get(id=kwargs['object_id'])
            p = Person.objects.get(id=u.person.id)
        except :
            messages.add_message(request, messages.ERROR, "This user doesn't exist in the database.")
            return redirect('user_management')
        
        form_person = self.form_class_person(initial={'first_name':p.first_name,
                                                      'last_name':p.last_name})
        
        form_user = self.form_class_user(initial={'login':u.login,
                                                  'person':u.person,
                                                  'is_admin':u.is_admin
                                                  })
        
        users = User.objects.all()
        number_all = len(users)
        users = add_pagination(request,users)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form_user)
        
        #To have person field in the table between login and is_admin (not in the form)
        names.insert(1,'person')
        labels.insert(1,'Person')
        
        tag_fields = {'all':users,
                      'hiddenid':u.id,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "User Edit",
                      'refine_search':True}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':users,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form_person':form_person,
                               'form_user':form_user,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
    
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            users = User.objects.refine_search(or_and, dico_get)
            
            number_all = len(users)
            users = add_pagination(request,users)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':users,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form_person':form_person,
                               'form_user':form_user,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
    
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
    
            users = User.objects.refine_search(or_and, dico_get)
            
            number_all = len(users)
            list_exceptions = ['project','start_date','password','end_date','id','last_login','rawdata']
            download_not_empty = False
            if users:
                download_not_empty = write_csv_from_viewer(users, list_exceptions, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            users = add_pagination(request,users)
            
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':users,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':False,
                           'form_person':form_person,
                           'form_user':form_user,
                           'list_attributes':self.list_attributes})
        
        return render(request,self.template,tag_fields)
    
    def post(self, request, *args, **kwargs):
        if "hiddenid" in request.POST.keys() :
            u = User.objects.get(id=request.POST['hiddenid'])
            p = Person.objects.get(id=u.person.id)
            form_user = self.form_class_user(instance=u, data=request.POST)
            form_person = self.form_class_person(instance=p, data=request.POST)
            
            if form_person.is_valid() and form_user.is_valid():
                form_person.save()
                messages.add_message(request, messages.SUCCESS, "The Person named {0} has been updated.".format(p))
                
                form_user.save()
                messages.add_message(request, messages.SUCCESS, "The User named {0} has been updated.".format(u))
                
            else:
                messages.add_message(request, messages.ERROR, ' '.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form_person.errors.items() ]))
                messages.add_message(request, messages.ERROR, ' '.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form_user.errors.items() ]))
                
            return redirect('user_management')
        
        else :
            messages.add_message(request, messages.ERROR, "An error as occurred, please try again.")
            return redirect('user_management')

class ProjectManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the projects model. It allows the deletion and insertion of projects based on the Project model.
      The filters are applied to this view. The user can make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list of the fields the user can search with the refine search module
      :var form form_class: the projects form based on the Project model
      :var queryset projects: list of all the projects from the database
      :var int number_all: number of all the projects in the database
      :var int nb_per_page: number of data to display per page, initialized to 10
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = ProjectForm
    template = 'team/projects_generic.html'
    list_attributes = ['name', 'authors', 'start date', 'end date', 'funding', 'description', 'users', 'institutions']

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        projects = Project.objects.all()
        number_all = len(projects)
        projects = add_pagination(request,projects)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        tag_fields = {'all':projects,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "Project Management", 
                      'filetype':'Project',
                      'refine_search':True}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':projects,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            projects = Project.objects.refine_search(or_and, dico_get)
            
            number_all = len(projects)
            projects = add_pagination(request,projects)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':projects,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera

            projects = Project.objects.refine_search(or_and, dico_get)
            
            number_all = len(projects)
            list_exceptions = ['id', 'accessionlocal','trait','locus','experiment']
            download_not_empty = False
            if projects:
                download_not_empty = write_csv_from_viewer(projects, list_exceptions, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            projects = add_pagination(request,projects)

        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':projects,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           'list_attributes':self.list_attributes})
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        request,template,tag_fields = generic_create(Project, self.form_class, request, self.template,{'refine_search':True,
                                                                                                       'list_attributes':self.list_attributes},"Project Management")
        return render(request,template,tag_fields)


class ProjectEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of project model. The user can still make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list of the fields the user can search with the refine search module
      :var form form_class: the projects form based on the Project model
      :var queryset projects: list of all the projects of the database
      :var int number_all: number of all the projects of the database
      :var int nb_per_page: number of data to display per page, initialized to 10
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = ProjectForm
    template = 'team/projects_generic.html'
    list_attributes = ['name', 'authors', 'start date', 'end date', 'funding', 'description', 'users', 'institutions']
    
    def get(self, request, *args, **kwargs):
        try:
            p = Project.objects.get(id=kwargs['object_id'])
        except :
            messages.add_message(request, messages.ERROR, "This project doesn't exist in the database.")
            return redirect('project_management')
        
        form = self.form_class(initial={'name':p.name,
                                        'authors':p.authors,
                                        'start_date':p.start_date,
                                        'end_date':p.end_date,
                                        'funding':p.funding,
                                        'description':p.description,
                                        'users':p.users.all(),
                                        'institutions':p.institutions.all()
                                        })
        
        projects = Project.objects.all()
        number_all = len(projects)
        projects = add_pagination(request,projects)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        tag_fields = {'all':projects,
                      'hiddenid':p.id,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "Project Edit", 
                      'refine_search':True}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':projects,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            projects = Project.objects.refine_search(or_and, dico_get)
            
            number_all = len(projects)
            projects = add_pagination(request,projects)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':projects,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
            
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera

            projects = Project.objects.refine_search(or_and, dico_get)
            
            number_all = len(projects)
            list_exceptions = ['id', 'accessionlocal','trait','locus','experiment']
            download_not_empty = False
            if projects:
                download_not_empty = write_csv_from_viewer(projects, list_exceptions, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            projects = add_pagination(request,projects)

        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':projects,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':False,
                           'form':form,
                           'list_attributes':self.list_attributes})
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        request,template,tag_fields = generic_update(Project, self.form_class, request, self.template,{'refine_search':True,
                                                                                                       'list_attributes':self.list_attributes},"Project Management")
        return redirect('project_management')
   
class LinkData(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      This view allows the link between data and projects. The users can link users, accessions, seedlots, samples, genotypingIDs, experiments, environments, traits
      and classification data to the project(s) they want.
      A table with data already linked to the existing projects can be displayed. 
      Data can be linked by uploading a file containing the data or they can be added one by one thanks to the form.
    """
    def get(self, request, *args, **kwargs):
        return redirect('page_not_available')
    
class ProjectViewer(LoginRequiredMixin, View):
    """
      This view list all the existing projects saved in the database.
      
      :var str template: html template for the rendering of the view
    """
    template = 'dataview/project_viewer.html'
    title = 'Project Viewer'
    list_exceptions = ['accessionlocal','id','trait','locus','experiment']
    
    def get(self, request, *args, **kwargs):
        username = request.user.login
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        
        projects = add_pagination(request, projects)
        
        names = ['name', 'authors', 'start_date','end_date','funding', 'description', 'users', 'institutions']
        labels = ['Project card', 'Project', 'Authors', 'Start date', 'End date', 'Funding', 'Description', 'Users', 'institutions']
        
        download_not_empty = False
        if projects:
            download_not_empty = write_csv_from_viewer(projects, self.list_exceptions, self.title)
        else:
            messages.add_message(request, messages.INFO, "There is no results for your query.")
        
        tag_fields = {'all': projects,
                      'title': self.title,
                      'fields_name': names,
                      'fields_label': labels,
                      'download_not_empty':download_not_empty,
                      'filename':self.title.lower().replace(" ","_")
                      }
        
        
        return render(request, self.template, tag_fields)
    