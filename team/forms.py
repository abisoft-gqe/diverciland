"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from django import forms

from team.models import Person, Institution, User, Project

class PersonForm(forms.ModelForm):
    """
        Form based on the Person model to create persons
    """
    def __init__(self, *args, **kwargs):
        super(PersonForm, self).__init__(*args, **kwargs)
        # change a widget attribute:
        self.fields['note_on_person'].widget.attrs["cols"] = 20
        self.fields['note_on_person'].widget.attrs["rows"] = 1
    
    class Meta:
        fields = '__all__'
        model = Person
        
class InstitutionForm(forms.ModelForm):
    """
        Form based on the Institution model to create institutions
    """
    class Meta:
        fields = '__all__'
        model = Institution

class PersonInfosUserForm(forms.ModelForm):
    """
        Form containing person info for User creation
    """
    class Meta:
        model = Person
        fields = ['first_name','last_name']

class UserCreateForm(forms.ModelForm):
    """
        Form based on the User model to create users
    """
    class Meta:
        model = User
        fields = ['login','password','is_admin']
        
class UserUpdateForm(forms.ModelForm):
    """
        Form based on the User model to update users
    """
    class Meta:
        model = User
        fields = ['login','is_admin']
        
class ProjectForm(forms.ModelForm):   
    """
        Form based on the Project model to create projects
    """
    def __init__(self, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)
        self.fields['users'].widget.attrs['style'] = 'width: 180px;height: 100px;'
        
    class Meta:
        model = Project
        widgets = {'start_date': forms.TextInput(attrs = {'class':'datePicker'}),
                   'end_date': forms.TextInput(attrs = {'class':'datePicker'}),
                   'funding': forms.Textarea(attrs={'rows':'2', 'cols':'40'})
                   }
        fields = '__all__'