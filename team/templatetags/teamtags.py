"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import ast

from django import template
from django.apps import apps
from django.contrib.contenttypes.models import ContentType
from django.db.models.fields.files import FieldFile

register = template.Library()


@register.filter
def to_class_name(value):
    return ContentType.objects.get_for_model(value).model

@register.filter
def to_app_name(value):
    return ContentType.objects.get_for_model(value).app_label

@register.filter
def to_formated_name(value):
    return value.replace('_', ' ').capitalize()

@register.simple_tag
def showattribute(value, attribute, app_label, model_name):
    attr_value = None

    try:
        attr_value = value.__getattribute__(attribute.lower().replace(' ','_'))
    except Exception:
        model=apps.get_model(app_label, model_name)
        model_type_attribute = apps.get_model(app_label, str(model_name+'TypeAttribute'))
        try:
            for i in ast.literal_eval(value.attributes_values):
                if model_type_attribute.objects.get(id=int(i)).attribute_name.replace(' ','_').lower()==attribute.lower():
                    attr_value=ast.literal_eval(model.objects.get(name=value).attributes_values)[i]
        except:
            for i in value.attributes_values:
                if model_type_attribute.objects.get(id=i).attribute_name.replace(' ','_').lower()==attribute.lower():
                    attr_value=model.objects.get(name=value).attributes_values[i]
            
    if type(attr_value) is str :
        return attr_value
    elif attr_value is None :
        return ''
    elif type(attr_value) is FieldFile:
        link = "<a href=\"/filerender/{0}\">{1}</a>".format(attr_value.url,attr_value.url.split('/')[-1])
        return link
    else :
        try :
            msg = ''
            for att in attr_value.all() :
                msg += str(att)+', '
            return msg[:-2]
        except :
            try :
                if type(attr_value) is int :
                    try :
                        method_to_call = 'get_{0}_display'.format(attribute)
                        mymethod = getattr(value, method_to_call)
                        return mymethod()
                    except AttributeError:
                        return str(attr_value)
                else :
                    return str(attr_value)      
            except :
                return "Unexpected value" 
            
@register.filter
def is_list(value):
    if type(value) is list:
        return True
    else:
        return False

@register.filter
def is_str(value):
    if isinstance(value, str):
        return True
    else:
        return False

@register.filter  
def display_empty_field(value):
    if value == "":
        return '-'
    else:
        return value
