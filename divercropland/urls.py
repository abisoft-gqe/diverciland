"""DiverCILand URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from django.conf import settings
from django.conf.urls import include
from django.conf.urls.static import static

from commonfct.filerenderer import FileRenderer, HeadersRenderer
from commonfct.genericviews import return_csv, generic_delete, generic_edit, generic_delete_all,\
                                   check_thread_activity
from commonfct.views import GlobalSearch, GlobalSearchAutocomplete
from team.views import Home, Authentication, UserProfile, PageNotAvailable

urlpatterns = [
    path('', Authentication.as_view(), name='login'),
    path('home/', Home.as_view(), name='home'),
    path('logout/', auth_views.LogoutView.as_view(next_page=settings.ROOT_URL), name='logout'),
    path('userprofile/', UserProfile.as_view(), name='user_profile'),
    path('not_available/', PageNotAvailable.as_view(), name='page_not_available'),
    path('team/', include('team.urls')),
    path('accession/', include('accession.urls')),
    path('', include('core_viewer.urls')),
    path('phenotyping/', include('phenotyping.urls')),
    path('landscape/', include('landscape.urls')),
    path('genotyping/', include('genotyping.urls')),
    
    #Search
    path('globalsearch/', GlobalSearch.as_view(), name='globalsearch'),
    path('search-autocomplete/', GlobalSearchAutocomplete.as_view(), name='search-autocomplete'),

    #Generic functions
    path('delete/<str:app>/<str:model>/<int:object_id>/', generic_delete, name='generic_delete'),
    path('edit/<str:app>/<str:model>/<int:object_id>/', generic_edit, name='generic_edit'),
    #path('deleteall/<str:app>/<str:model>/<str:id_str>/', generic_delete_all, name='delete_all'),
    path('deleteall/<str:app>/<str:model>/', generic_delete_all, name='delete_all'),
    path('checkthread/',check_thread_activity,name='check_thread'),
    
    #URLs for generic functions used everywhere in the app
    path('return_csv/<filename>/', return_csv, name='return_csv'),
    path('filerenderer/<str:filetype>/<int:type_id>/',FileRenderer.as_view(), name='filerenderer'),
    path('headersrenderer/<str:filetype>/',HeadersRenderer.as_view(), name='headersrenderer')
    
    # Uncomment the next line to enable the admin:
    #path('admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

        # For django versions before 2.0:
        # url(r'^__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns