"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import os

from django.apps import apps
from django.contrib.postgres.fields import JSONField
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.core.exceptions import ValidationError
from django.dispatch import receiver

from django.db import models

from genotyping.managers import LocusManager, ExperimentManager
from commonfct.constants import ATTRIBUTE_TYPES, EXPERIMENT_CATEGORY

class LocusType(models.Model):
    """
      The LocusType model gives information about the type of locus.
      
      :var CharField name: name of the locus type
      :var TextField description: description of the locus type
    """
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    positioned = models.BooleanField()
    
    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ['name']
    
    def attribute_set(self):
        return self.locustypeattribute_set


class LocusTypeAttribute(models.Model):
    """
      The LocusTypeAttribute model gives informations about the attributes
      linked to the locus type.
      
      :var CharField attribute_name: name of the attribute
      :var IntegerField type: type of the attribute
      :var ManyToManyField locus_type: accession type linked to the attribute, 
                                       defined in the LocusType model
    """
    attribute_name = models.CharField(max_length=100, blank=True, unique=True)
    type = models.IntegerField(choices=ATTRIBUTE_TYPES, blank=True)
    locus_type = models.ManyToManyField('LocusType', through='LocusAttributePosition', blank=True)
    
    def __str__(self):
        return self.attribute_name
    
    class Meta:
        ordering = ['attribute_name']
    
    def clean(self):
        loc_fields = Locus._meta.get_fields()
        loc_field_names = [f.name for f in loc_fields]
        pos_fields = Position._meta.get_fields()
        pos_field_names = [f.name for f in pos_fields]
        clean_field_names = loc_field_names + pos_field_names
        
        if self.attribute_name.lower().replace(' ','_') in clean_field_names:
            raise ValidationError('A field with this name already exists for locus.')
    
    def delete(self, *args, **kwargs):
        Locus = apps.get_model('genotyping','locus')
        LocusAttributePosition = apps.get_model('genotyping','locusattributeposition')
        
        types = LocusAttributePosition.objects.filter(attribute=self.id).values_list('type', flat=True).distinct()
        if types:
            locus = Locus.objects.filter(type__in=types)
            for loc in locus:
                loc.attributes_values.pop(str(self.id), None)
                loc.save()
                
            super(LocusTypeAttribute, self).delete(*args, **kwargs)
        
        else:
            super(LocusTypeAttribute, self).delete(*args, **kwargs)
            

class LocusAttributePosition(models.Model):
    """
      The LocusAttributePosition model gives information about the position of the attribute.
      It allows the movement of the attribute thanks to a drag and drop system and the memorization of the position of this attribute.
      
      :var ForeignKey attribute: name of the attribute, defined with the LocusTypeAttribute model
      :var ForeignKey type: type of the locus which is linked to the attribute, defined with the LocusType model
      :var IntegerField position: integer rendering the position number
    """
    attribute = models.ForeignKey('LocusTypeAttribute', on_delete=models.CASCADE)
    type = models.ForeignKey('LocusType', on_delete=models.CASCADE)
    position = models.IntegerField()
    
    class Meta:
        unique_together = ("type","position")


class Locus(models.Model):
    """
      The Locus model gives informations about the locus.
      
      :var CharField name: name of the locus
      :var ForeignKey type: type of the locus, defined in the LocusType model
      :var TextField comments: comments on the locus
      :var JSONField attributes_values: values of the locus type's attributes
      :var ManyToManyField projects: Link with the projects
    """
    name = models.CharField(max_length=100, unique=True, db_index=True)
    type = models.ForeignKey('LocusType', db_index=True, on_delete=models.CASCADE)
    comments = models.TextField(max_length=500, blank=True, null=True)
    attributes_values = JSONField(blank=True, null=True)

    projects = models.ManyToManyField('team.Project', blank = True)
    
    objects = LocusManager()

    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ['name']

    def update_attributes(self, attributes, postdata):
        jsonvalues = {}
        for att in attributes:
            if att.attribute_name.lower().replace(' ','_') in postdata.keys():
                jsonvalues[att.id] = postdata[att.attribute_name.lower().replace(' ','_')]
            else:
                jsonvalues[att.id] = ''
        self.attributes_values = jsonvalues


class Position(models.Model):
    """
      The Position model gives informations on the position of the locus according to the genome version
      :var IntegerField genome_version: genome version of the locus' informations
      :var CharField chromosome: chromosome on which the locus is
      :var IntegerField position: position of the locus on the chromosome
      :var ForeignKey locus: locus to which belongs position informations
    """
    genome_version = models.CharField(max_length=100, db_index=True)
    chromosome = models.CharField(max_length=100, blank=True, null=True, db_index=True)
    position = models.IntegerField(blank=True, null=True)
    locus = models.ForeignKey('genotyping.Locus', on_delete=models.CASCADE)

    class Meta:
        unique_together=("locus", "genome_version")
        

class GenotypingValue(models.Model):
    """
      The GenotypingValue model allows to store genotyping data.
      :var ForeignKey accession: accession on which the value has been measured.
      :var ForeignKey locus: locus for which the value has been measured.
      :var ForeignKey experiment: Experiment in which the value has been measured
      :var CharField value: genotyping value.
      :var FloatField allelic_frequency: allelic frequency.
    """
    accession = models.ForeignKey('accession.Accession', on_delete=models.CASCADE)
    locus = models.ForeignKey('genotyping.Locus', on_delete=models.CASCADE)
    experiment = models.ForeignKey('genotyping.Experiment', on_delete=models.CASCADE)
    value = models.CharField(max_length=100)
    allelic_frequency = models.FloatField(default='1')
 
 
    class Meta:
        unique_together=("accession", "locus", "experiment")
#         ordering = ['accession', 'locus', 'experiment']

fs = FileSystemStorage(location=settings.MEDIA_ROOT)

class Experiment(models.Model):
    """
      The Experiment model gives informations about the locus.
      
      :var CharField name: name of the experiment
      :var DateField date: date of the experiment
      :var TextField comments: comments on the experiment
      :var ManyToManyField projects: Link with the projects
    """
    name = models.CharField(max_length=100, unique=True)
    institution = models.ForeignKey('team.Institution', on_delete=models.CASCADE)
    date = models.DateField()
    category = models.IntegerField(choices=EXPERIMENT_CATEGORY)
    phenotyping_file = models.FileField(storage=fs, upload_to='postulation_file/', blank=True)
    genotyping_file = models.FileField(storage=fs, upload_to='postulation_file/', blank=True)
    isolat_file = models.FileField(storage=fs, upload_to='postulation_file/', blank=True)
    comments = models.TextField(blank=True, null=True)
    
    projects = models.ManyToManyField('team.Project',blank=True)
    
    objects = ExperimentManager()
    
    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ['name']
        

@receiver(models.signals.post_delete, sender=Experiment)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
      Deletes files of filesystem
      when corresponding Experiment object is deleted.
    """
    if instance.phenotyping_file:
        if os.path.isfile(instance.phenotyping_file.path):
            os.remove(instance.phenotyping_file.path)
    
    if instance.genotyping_file:
        if os.path.isfile(instance.genotyping_file.path):
            os.remove(instance.genotyping_file.path)

    if instance.isolat_file:
        if os.path.isfile(instance.isolat_file.path):
            os.remove(instance.isolat_file.path)
            
