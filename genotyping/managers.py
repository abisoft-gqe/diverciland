"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import ast

from operator import ior, iand

from django.apps import apps
from django.db import models
from django.db.models import Q

from team.models import User, Project
from commonfct.constants import experiment_category

class LocusManager(models.Manager):
    """
      Manager for Locus model.
    """
    def refine_search(self, or_and, dico_get, locus_type):
        """
          Search in all locus according to some parameters

          :param string or_and : link between parameters (or, and)
          :param dict dico_get : data to search for
        """
        # Managers are called in locus.model
        # To avoid crossed imports we use apps to get objects from the model
        Locus = apps.get_model('genotyping','locus')
        LocusTypeAttribute = apps.get_model('genotyping', 'locustypeattribute')
        Position = apps.get_model('genotyping', 'position')
        Project = apps.get_model('team', 'project')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
        
        proj=[]
        query=Q()
        list_attr=[]
        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        query = myoperator(query, Q(projects__isnull=True))
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query = myoperator(query, Q(projects__in=proj))
                
                elif "genome version" in dico_type_data:
                    locus_id=(Position.objects.filter(genome_version__icontains=dico_type_data['genome version']).values_list('locus', flat=True))
                    query = myoperator(query, Q(id__in=locus_id))
                
                elif "chromosome" in dico_type_data:
                    locus_id=(Position.objects.filter(chromosome__icontains=dico_type_data['chromosome']).values_list('locus', flat=True))
                    query = myoperator(query, Q(id__in=locus_id))
                    
                elif "position" in dico_type_data:
                    locus_id=(Position.objects.filter(position__icontains=dico_type_data['position']).values_list('locus', flat=True))
                    query = myoperator(query, Q(id__in=locus_id))
                   
                else:
                    for data_type, value in dico_type_data.items():
                        list_id_in=[]
                        for i in LocusTypeAttribute.objects.filter(locus_type=locus_type):
                            if str(data_type) == str(i):
                                list_attr.append(str(i))
                        if data_type in list_attr:
                            id_attribute = str(LocusTypeAttribute.objects.get(attribute_name=data_type, locus_type=locus_type).id)
                            for i in Locus.objects.filter(type=locus_type):
                                try:
                                    if id_attribute in i.attributes_values.keys():
                                        if value in ast.literal_eval(i.attributes_values)[id_attribute]:
                                            list_id_in.append(i.id)
                                except:
                                    try:
                                        if id_attribute in i.attributes_values.keys():
                                            if value in i.attributes_values[id_attribute]:
                                                list_id_in.append(i.id)
                                    except:
                                        i.attributes_values = ast.literal_eval(i.attributes_values)
                                        if id_attribute in i.attributes_values.keys():
                                            if value in i.attributes_values[id_attribute]:
                                                list_id_in.append(i.id)
        
                            query = myoperator(query, Q(id__in = list_id_in))
                
                        else:
                            var_search = str( data_type + "__icontains" )
                            query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))

        return Locus.objects.filter(query).distinct()



class ExperimentManager(models.Manager):
    """
      Manager for experiment model.
    """
    def by_username(self,username):
        """
          Returns only the experiments from projects linked to this user 

          :param string username : username

          :var int username_id : user's id
          :var list projects : projects linked to the user
        """
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        return self.filter(projects__in=projects).distinct()
        
        
    def refine_search(self, or_and, dico_get):
        """
          Search in all experiments according to some parameters
    
          :param string or_and : link between parameters (or, and)
          :param dict dico_get : data to search for
        """
        # Managers are called in accession.model
        # To avoid crossed imports we use apps to get objects from the model 
        Experiment = apps.get_model('genotyping', 'experiment')
        Project = apps.get_model('team', 'project')
        Institution = apps.get_model('team','institution')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
            
        proj=[]
        query=Q()
        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                var_search = str( data_type + "__icontains" )
                
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        query = myoperator(query, Q(projects__isnull=True))
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query = myoperator(query, Q(projects__in=proj))
                
                elif "institution" in dico_type_data:
                    inst =(Institution.objects.filter(name__icontains=dico_type_data['institution']))
                    query = myoperator(query, Q(institution__in=inst))
                
                elif "category" in dico_type_data:
                    list_c = []
                    for key, category in experiment_category.items():
                        if dico_type_data[data_type].lower() in category.lower():
                            list_c.append(key)
                    query = myoperator(query, Q(category__in=list_c))

                else:
                    query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))
                    
        return Experiment.objects.filter(query).distinct()
        
    
    
    