"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from django import template

from genotyping.models import LocusAttributePosition, LocusTypeAttribute, Position
register = template.Library()

@register.simple_tag
def get_loc_descriptors(type):
    descriptors = LocusAttributePosition.objects.filter(type=type).values_list('attribute__attribute_name', flat=True)
    return ', '.join(descriptors)


@register.filter
def get_loc_descriptor_type(name):
    attributes_types = {1:"Short Text",
                        2:"Long Text",
                        3:"Number",
                        4:"URL"}
    try:
        descriptor = LocusTypeAttribute.objects.get(attribute_name=name)
        dtype = attributes_types[descriptor.type]
    except:
        dtype = None
    return dtype


@register.simple_tag
def get_position_values(locus, attr):
    try:
        position = Position.objects.get(locus=locus.id)
    except:
        return ''
    
    if attr == 'genome_version':
        return position.genome_version
    elif attr == 'position':
        return position.position
    elif attr == 'chromosome':
        return position.chromosome
    else:
        return ''