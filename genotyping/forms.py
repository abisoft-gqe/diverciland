"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from dal import autocomplete

from django import forms

from genotyping.models import Locus, LocusType, LocusTypeAttribute, Experiment, Position

class LocusTypeForm(forms.ModelForm):
    """
      Form based on the LocusType model managing data from this model.
    """
    class Meta:
        model = LocusType
        fields = '__all__'
        
class LocusTypeAttributeForm(forms.ModelForm):
    """
      Form based on the LocusTypeAttribute model managing data from this model
    """
    class Meta:
        model = LocusTypeAttribute
        exclude = ['locus_type']
        
        
class LocusTypeSelectAttribute(forms.Form):
    """
      Form used to select attributes when creating or updating a locus type.
    """
    descriptors = forms.ModelMultipleChoiceField(required=False,
                                                 queryset=LocusTypeAttribute.objects.all(),
                                                 widget=autocomplete.ModelSelect2Multiple())
    
    
class LocusForm(forms.ModelForm):
    """
      Form based on the Locus model, allowing the creation of new locus into the database.
    """
    class Meta:
        model = Locus
        exclude = ['attributes_values']
    
    def __init__(self, *args, **kwargs):
        if 'attributes' in kwargs.keys():
            attributes = kwargs['attributes']
            if 'data' in kwargs.keys():
                if 'instance' in kwargs.keys():
                    super(LocusForm, self).__init__(data=kwargs['data'], instance=kwargs['instance'])
                else:
                    super(LocusForm, self).__init__(data=kwargs['data'])
            else :
                super(LocusForm, self).__init__()
              
            for attr in attributes:
                self.fields[attr.attribute_name.lower().replace(' ','_')] = forms.CharField(required = False)
          
        else:
            if 'data' in kwargs.keys():
                super(LocusForm, self).__init__(data=kwargs['data'])
            else :
                super(LocusForm, self).__init__()



class PositionForm(forms.ModelForm):
    """
      Form based on the position model, to get locus position informations.
    """
    class Meta:
        model = Position
        exclude = ['locus']
        widgets = {'position': forms.NumberInput(attrs = {'class': 'form_number_input'})}

class UploadForm(forms.Form):
    """
      Form used to upload a file.
    """
    file = forms.FileField(label='Select the file to upload ')
    

class ExperimentForm(forms.ModelForm):
    """
      Form based on the Experiment model to create experiments.
    """
    class Meta:
        fields = '__all__'
        model = Experiment
        widgets = {'date': forms.TextInput(attrs = {'class':'datePicker'})}


class PostulationDataUpload(forms.Form):
    """
      Form used to upload postulation data.
    """
    experiment = forms.ModelChoiceField(required=True, queryset=Experiment.objects.filter(category=1))
    file = forms.FileField(label='Select the file to upload ')


FORMAT = [(1, 'Matrix'),
          (2, 'Flat matrix')]
class GenotypingDataUpload(forms.Form):
    """
      Form used to upload genotyping and postulation data.
    """
    experiment = forms.ModelChoiceField(required=True, queryset=Experiment.objects.all())
    file_format = forms.ChoiceField(required=True,
                                    widget=forms.RadioSelect,
                                    choices=FORMAT)
    file = forms.FileField(label='Select the file to upload ')
    
