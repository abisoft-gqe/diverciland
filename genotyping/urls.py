"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from django.urls import path
from genotyping.views import LocusTypeManagement, LocusTypeEdit, LocusTypeAttributeManagement,\
                             LocusTypeAttributeEdit, LocusManagement, LocusEdit,\
                             ExperimentManagement, ExperimentEdit, LocusHeadersRenderer,\
                             GenotypingDataInsert, GenotypingMatrixTemplateFileRenderer,\
                             GenotypingViewer

urlpatterns = [
    path('locustype/', LocusTypeManagement.as_view(), name='locustype_management'),
    path('locustype/<int:object_id>/', LocusTypeEdit.as_view(), name='locustype_edit'),
    path('locustypeattribute/', LocusTypeAttributeManagement.as_view(), name='locustypeattribute_management'),
    path('locustypeattribute/<int:object_id>/', LocusTypeAttributeEdit.as_view(), name='locustypeattribute_edit'),
    path('locus/type/<int:type_id>/', LocusManagement.as_view(), name='locus_management'),
    path('locus/<int:object_id>/', LocusEdit.as_view(), name='locus_edit'),
    path('experiment/', ExperimentManagement.as_view(), name='experiment_management'),
    path('experiment/<int:object_id>/', ExperimentEdit.as_view(), name='experiment_edit'),
    path('experimentdata/insert/', GenotypingDataInsert.as_view(), name='genotypingdata_insert'),
    path('experimentdataviewer/', GenotypingViewer.as_view(), name='experimentdata_viewer'),
    
    path('locusheadersrenderer/<int:type_id>/',LocusHeadersRenderer.as_view(), name='locusheadersrenderer'),
    path('matrixfilerenderer/',GenotypingMatrixTemplateFileRenderer.as_view(), name='matrixfilerenderer')
]