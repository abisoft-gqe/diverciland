"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import ast
import csv
import json
import os
import pandas

from _csv import Dialect
from django import forms
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import transaction
from django.db.models import Count
from django.db.models.fields.files import FieldFile
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views import View

from accession.models import Accession, AccessionLocal
from commonfct.constants import filerenderer_headers, experiment_category
from commonfct.utils import get_fields, add_pagination, execute_function_in_thread
from commonfct.genericviews import generic_create, generic_update, write_csv_from_viewer
from core_viewer.forms import DataviewProjectForm, DataviewAccessionListForm, DataviewLocusPositionForm,\
                              DataviewLocusFileUploadForm, DataviewExperimentListForm
from csv_io.views import CSVFactoryForTempFiles, CSVFactoryForMatrix
from genotyping.forms import LocusTypeForm, LocusTypeAttributeForm, LocusTypeSelectAttribute,\
                             LocusForm, UploadForm, ExperimentForm, PositionForm,\
                             PostulationDataUpload, GenotypingDataUpload
from genotyping.models import LocusType, LocusTypeAttribute, LocusAttributePosition, Locus,\
                              Experiment, Position, GenotypingValue
from team.models import Project, User
from team.views import LoginRequiredMixin, UserIsAdminMixin


class LocusTypeManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the Locus Type model. It allows the deletion and insertion of types based on the Locus Type Model.
      
      :var form form_type_class: the locus type form
      :var form form_attr_class: the descriptors form
      :var str template: html template for the rendering of the view
      :var queryset types: contains all the types from the database
      :var int number_all: contains the number of locus types of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
    """
    
    form_type_class = LocusTypeForm
    form_attr_class = LocusTypeSelectAttribute
    template = 'genotyping/locus_type.html'
    
    def get(self, request, *args, **kwargs):
        form_type = self.form_type_class()
        form_attr = self.form_attr_class()
        types = LocusType.objects.all()
        number_all = len(types)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form_type)
        
        names.append('descriptors')
        labels.append('Descriptors')
        
        tag_fields = {'all':types,
                      'number_all':number_all,
                      'nb_per_page':nb_per_page,
                      'creation_mode':True,
                      'form_type':form_type,
                      'form_attr':form_attr,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "Locus Type Management"}
        
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        form_type = self.form_type_class(request.POST)
        form_attr = self.form_attr_class(request.POST)
        
        if form_type.is_valid() and form_attr.is_valid():
            type = form_type.save()
            
            position = 0
            for attribute in request.POST.getlist('descriptors'):
                attr = LocusTypeAttribute.objects.get(id=attribute)
                att_pos = LocusAttributePosition.objects.create(attribute=attr, type=type, position=position)
                att_pos.save()
                position += 1
            messages.add_message(request, messages.SUCCESS, "A new Locus Type named {0} has been created.".format(type))
            return redirect('locustype_management')
        
        else:
            messages.add_message(request, messages.ERROR, "An error occurred.")
            return render(request, self.template, {})
        

class LocusTypeEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of Locus Type model.
      
      :var form form_type_class: the locus type form
      :var form form_attr_class: the descriptors form
      :var str template: html template for the rendering of the view
      :var queryset types: contains all the types from the database
      :var int number_all: contains the number of locus types of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
    """
    form_type_class = LocusTypeForm
    form_attr_class = LocusTypeSelectAttribute
    template = 'genotyping/locus_type.html'

    def get(self, request, *args, **kwargs):
        try:
            lt = LocusType.objects.get(id=kwargs['object_id'])
        except:
            messages.add_message(request, messages.ERROR, "This locus type doesn't exist in the database.")
            return redirect('locustype_management')
        
        form_type = self.form_type_class(initial={'name':lt.name,
                                                  'description':lt.description,
                                                  'positioned':lt.positioned
                                                  })
        
        attributes = LocusAttributePosition.objects.filter(type=lt.id).values_list('attribute', flat=True)
        
        form_attr = self.form_attr_class(initial={'descriptors':attributes})
        
        types = LocusType.objects.all()
        number_all = len(types)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form_type)
        
        names.append('descriptors')
        labels.append('Descriptors')
        
        tag_fields = {'all':types,
                      'hiddenid':lt.id,
                      'number_all':number_all,
                      'nb_per_page':nb_per_page,
                      'creation_mode':False,
                      'form_type':form_type,
                      'form_attr':form_attr,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "Locus Type Edit"}
        
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        
        form_type = self.form_type_class(request.POST)
        form_attr = self.form_attr_class(request.POST)
        
        try:
            lt = LocusType.objects.get(id=kwargs['object_id'])
        except:
            messages.add_message(request, messages.ERROR, "This locus type doesn't exist in the database.")
            return redirect('locustype_management')
        
        try:
            positioned = lt.positioned

            lt.name = request.POST.get('name')
            lt.description = request.POST.get('description')
            lt.positioned = True if request.POST.get('positioned') else False
            
            attributes = lt.locustypeattribute_set.all()
            attributes = attributes.values_list('id', flat=True)
            new_attributes = request.POST.getlist('descriptors')

            update_attribute_list(lt, attributes, new_attributes)
            
            lt.save()
            
            messages.add_message(request, messages.SUCCESS, "The Locus Type {0} has been updated.".format(lt))

        except Exception as e:
            messages.add_message(request, messages.ERROR, e)
        
        #We delete the position objects linked to the locus
        if positioned == True and lt.positioned == False:
            locus = Locus.objects.filter(type=lt)
            for loc in locus:
                try:
                    position = Position.objects.get(locus=loc)
                    position.delete()
                except ObjectDoesNotExist as e:
                    # If there is no object, there is nothing to delete
                    pass
        
        #We create all the position objects linked to the existing locus
        if positioned == False and lt.positioned == True:
            locus = Locus.objects.filter(type=lt)
            for loc in locus:
                position = Position.objects.create(locus=loc, chromosome=0, position=0)
                position.save()
                    
        return redirect('locustype_management')


#TODO: A regrouper avec la meme fonction dans le module accession, mais comment ?
def update_attribute_list(lt, attributes, new_attributes):
    """
      To update the attribute list in edit mode.
    """
    to_delete = []
    to_add = []
    
    for attr in attributes:
        if str(attr) not in new_attributes:
            to_delete.append(str(attr))
    
    for attr in new_attributes:
        if int(attr) not in attributes:
            to_add.append(attr)
    
    for attr in to_delete:
        position = LocusAttributePosition.objects.get(attribute=attr, type=lt)
        position.delete()
        
    attr_order = attributes.order_by('id')
    
    position = 0
    for attribute in attr_order:
        try:
            type_pos = LocusAttributePosition.objects.get(type=lt, position=position)
        except:
            type_pos = None
        
        if not type_pos:
            attr_pos = LocusAttributePosition.objects.get(attribute=attribute, type=lt)
            attr_pos.position = position
            attr_pos.save()
        position += 1
        
    for attr in to_add:
        a = LocusTypeAttribute.objects.get(id=attr)
        attr_pos = LocusAttributePosition.objects.create(attribute=a, type=lt, position=position)
        attr_pos.save()
        position += 1
    
    locus = Locus.objects.filter(type=lt)
    
    for loc in locus:
        if loc.attributes_values == None:
            loc.attributes_values = {}
        
        for attr in to_delete:
            loc.attributes_values.pop(attr, None)
            
        for attr in to_add:
            loc.attributes_values[attr] = ''
        
        loc.save()
        
class LocusTypeAttributeManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of Locus Type Attributes model.
      
      :var form form_class: the locus type attribute form
      :var str template: html template for the rendering of the view
      :var queryset types: contains all the attributes from the database
      :var int number_all: contains the number of attributes of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
    """
    form_class = LocusTypeAttributeForm
    template = 'genotyping/genotyping_attribute.html'
    
    def get(self, request, *args, **kwargs):
        form = self.form_class()
        attributes = LocusTypeAttribute.objects.all()
        number_all = len(attributes)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        
        tag_fields = {'all':attributes,
                      'number_all':number_all,
                      'nb_per_page':nb_per_page,
                      'creation_mode':True,
                      'form':form,
                      'fields_name':names,
                      'fields_label':labels,
                      'title':"Locus Descriptors Management"}
        
        return render(request, self.template, tag_fields)

    def post(self, request, *args, **kwargs):
        request,template,tag_fields = generic_create(LocusTypeAttribute, self.form_class, request, self.template,{}, "Locus Descriptors Management")
        return render(request,template,tag_fields)


class LocusTypeAttributeEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of Locus Type Attributes model.
      
      :var form form_class: the locus type attribute form
      :var str template: html template for the rendering of the view
      :var queryset types: contains all the attributes from the database
      :var int number_all: contains the number of attributes of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
    """
    form_class = LocusTypeAttributeForm
    template = 'genotyping/genotyping_attribute.html'
    
    def get(self, request, *args, **kwargs):
        try:
            attr = LocusTypeAttribute.objects.get(id=kwargs['object_id'])
        except:
            messages.add_message(request, messages.ERROR, "This locus type descriptor doesn't exist in the database.")
            return redirect('locustypeattribute_management')
        
        form = self.form_class(initial={'attribute_name':attr.attribute_name,
                                        'type':attr.type
                                        })
        
        attributes = LocusTypeAttribute.objects.all()
        number_all = len(attributes)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        
        tag_fields = {'all':attributes,
                      'hiddenid':attr.id,
                      'number_all':number_all,
                      'nb_per_page':nb_per_page,
                      'creation_mode':False,
                      'form':form,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "Locus Descriptors Edit"}
        
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        request,template,tag_fields = generic_update(LocusTypeAttribute, self.form_class, request, self.template,{}, "Locus Descriptors Management")
        return redirect('locustypeattribute_management')



class LocusManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the locus model. It allows the deletion and insertion of locus based on the Locus model,
      but also the submission of a file containing locus and their informations.
      The filters are applied to this view. The user can make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list of the fields the user can search with the refine search module
      :var form form_class_loc: the locus form based on the Locus model
      :var form file_form_class: the form to submit a file
      :var queryset locus: list of all the locus from the database
      :var int number_all: number of all the locus in the database
      :var int nb_per_page: number of data to display per page, initialized to 10
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class_loc = LocusForm
    form_class_pos = PositionForm
    file_form_class = UploadForm
    template = 'genotyping/locus_data.html'
    list_attributes = ['name', 'comments', 'projects']
    
    def get(self, request, *args, **kwargs):
        try:
            locus_type = LocusType.objects.get(id=kwargs['type_id'])
        except:
            messages.add_message(request, messages.ERROR, "This locus type doesn't exist in the database.")
            return redirect('locustype_management')
        
        attributes = locus_type.locustypeattribute_set.all()
        
        form_loc = self.form_class_loc(attributes=attributes)
        file_form = self.file_form_class()
        
        form_pos = self.form_class_pos() if locus_type.positioned == True else None
        
        locus = Locus.objects.filter(type=locus_type).distinct()
        set_locus_attributes(locus, attributes)
        
#         nb_pos = Position.objects.filter(locus__in=locus).distinct().annotate(count=Count('chromosome'))
#         for pos in nb_pos:
#             print(pos.count)
        
        filtered_loc = locus
        number_all = len(locus)
        locus = add_pagination(request, locus)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form_loc)
        
        for attr in LocusTypeAttribute.objects.filter(locus_type=locus_type):
            in_list = attr.attribute_name in self.list_attributes
            if in_list == False:
                self.list_attributes.append(attr.attribute_name)
        
        if form_pos:
            names.extend([form_pos['genome_version'].name,form_pos['chromosome'].name,form_pos['position'].name])
            labels.extend([form_pos['genome_version'].label,form_pos['chromosome'].label,form_pos['position'].label])
            if 'chromosome' not in self.list_attributes:
                self.list_attributes.extend(['genome version', 'chromosome', 'position'])
        
        form_loc.fields['type'].widget = forms.HiddenInput()
        form_loc.fields['type'].initial = locus_type.id
        
        names.remove(form_loc['type'].name)
        labels.remove(form_loc['type'].label)
        
        tag_fields = {'all':locus,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': str(locus_type.name) +" Locus Management",
                      'filetype':'Locus',
                      'typeid':locus_type.id,
                      'refine_search':True}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':locus,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form_loc':form_loc,
                               'file_form':file_form,
                               'form_pos':form_pos,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            locus_searched = Locus.objects.refine_search(or_and, dico_get, locus_type)
            locus = locus_searched.intersection(filtered_loc)
            locus = locus.order_by('name')
            set_locus_attributes(locus, attributes)
            
            number_all = len(locus)
            locus = add_pagination(request, locus)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':locus,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form_loc':form_loc,
                               'file_form':file_form,
                               'form_pos':form_pos,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
             
            locus_searched = Locus.objects.refine_search(or_and, dico_get, locus_type)
            
            #We only want to have accessions of the type considered
            locus = locus_searched.intersection(filtered_loc)
            locus = locus.order_by('name')
            set_locus_attributes(locus, attributes)
            
            number_all = len(locus)
            list_exceptions = ['type','id']
            download_not_empty = False
            thread_name = ''
            if locus:
                thread_name = execute_function_in_thread(write_locus_csv_from_viewer, [locus, "Result search", locus_type.id], {})
                download_not_empty = True
                
            tag_fields.update({'download_not_empty':download_not_empty,
                               'thread_name':thread_name})
            
            locus = add_pagination(request, locus)
            
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':locus,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form_loc':form_loc,
                           'file_form':file_form,
                           'form_pos':form_pos,
                           'list_attributes':self.list_attributes}) 
        return render(request, self.template, tag_fields)
    
    
    def post(self, request, *args, **kwargs):
        locus_type = LocusType.objects.get(id=kwargs['type_id'])
        
        attributes = locus_type.locustypeattribute_set.all()
        
        form_loc = self.form_class_loc(data=request.POST, attributes=attributes)
        file_form = self.file_form_class(request.POST, request.FILES)
        
        form_pos = self.form_class_pos() if locus_type.positioned == True else None
        
        locus = Locus.objects.filter(type=locus_type)
        set_locus_attributes(locus, attributes)
        number_all = len(locus)
        locus = add_pagination(request, locus)
        
        names, labels = get_fields(form_loc)
        
        for attr in LocusTypeAttribute.objects.filter(locus_type=locus_type):
            in_list = attr.attribute_name in self.list_attributes
            if in_list == False:
                self.list_attributes.append(attr.attribute_name)
                
        if form_pos:
            names.extend([form_pos['genome_version'].name,form_pos['chromosome'].name,form_pos['position'].name])
            labels.extend([form_pos['genome_version'].label,form_pos['chromosome'].label,form_pos['position'].label])
            if 'chromosome' not in self.list_attributes:
                self.list_attributes.extend(['genome version', 'chromosome', 'position'])
                
        form_loc.fields['type'].widget = forms.HiddenInput()
        form_loc.fields['type'].initial = locus_type.id
        
        names.remove(form_loc['type'].name)
        labels.remove(form_loc['type'].label)
        
        tag_fields = {'all':locus,
                      'number_all':number_all,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': str(locus_type.name) +" Locus Management",
                      'form_loc':form_loc,
                      'file_form':file_form,
                      'form_pos':form_pos,
                      'creation_mode':True,
                      'refine_search':True,
                      'filetype':'Locus',
                      'typeid':locus_type.id,
                      'list_attributes':self.list_attributes}
        
        if form_loc.is_valid():
            create_locus_from_form(request, form_loc, form_pos, locus_type, attributes)        
            
            locus = Locus.objects.filter(type=locus_type)
            set_locus_attributes(locus, attributes)
            number_all = len(locus)
            locus = add_pagination(request, locus)
            
            tag_fields.update({'all':locus,
                               'number_all':number_all})
            
            return render(request, self.template, tag_fields)
        
        elif file_form.is_valid():
            file = request.FILES['file']
            myfactory = CSVFactoryForTempFiles(file)
            myfactory.find_dialect()
            myfactory.read_file()
            file_dict = myfactory.get_file()
            
            headers_list = filerenderer_headers[12][1]
            
            missing = myfactory.check_headers_conformity(headers_list)
            
            if missing != []:
                messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                                     ". The required headers for this file are: "+format(', '.join(headers_list))+".")
                return redirect('locus_management',type_id=locus_type.id)
            
            if request.POST.get('submit'):
                create_locus_from_file(request, file_dict, locus_type)
            elif request.POST.get('update'):
                update_locus_from_file(request, file_dict, locus_type)
                
            locus = Locus.objects.filter(type=locus_type)
            set_locus_attributes(locus, attributes)
            number_all = len(locus)
            locus = add_pagination(request, locus)
            tag_fields.update({'all':locus,
                               'number_all':number_all})
            
            return render(request, self.template, tag_fields)
    
        else:
            messages.add_message(request, messages.ERROR, "An error occurred.")
            return render(request, self.template, tag_fields)



def create_locus_from_form(request, form_loc, form_pos, locus_type, attributes):
    """
      Function used to create a locus object from the form.
      :param form form_loc: form submitted by the user
      :param list attributes: attributes selected by the user
    """
    name = form_loc.cleaned_data['name']
    comments = form_loc.cleaned_data['comments']
    projects = Project.objects.filter(id__in=form_loc.cleaned_data['projects'])
    
    try:
        with transaction.atomic():
            locus = Locus.objects.create(name=name,
                                         type=locus_type,
                                         comments=comments)
            
            locus.projects.add(*list(projects))
            locus.update_attributes(attributes, request.POST)
            locus.save()
            
            if form_pos:
                genome_version = '' if request.POST.get('genome_version') == '' else request.POST.get('genome_version')
                chromosome = '' if request.POST.get('chromosome') == '' else request.POST.get('chromosome')
                position = 0 if request.POST.get('position') == '' else request.POST.get('position')
                position = Position.objects.create(locus=locus, genome_version=genome_version, chromosome=chromosome, position=position)
                position.save()
            
    except Exception as e:
        messages.add_message(request, messages.ERROR, "A locus named {0} already exists.".format(name))
        return 

    messages.add_message(request, messages.SUCCESS, "A new Locus named {0} has been created.".format(name))


def create_locus_from_file(request, file_dict, locus_type):
    """
      Function used to create locus from a file uploaded by the user.
      :param dict file_dict: file on dict format returned by the CSVFactory
      :param LocusType locus_type: locus type to link the created locus
    """
    with transaction.atomic():
        for line in file_dict:
            try:
                with transaction.atomic():
                    locus = Locus.objects.create(name=line["Name"],
                                                 type=locus_type,
                                                 comments=line["Comments"])
                    
                    if line["Projects"]:
                        project_list = [p.strip() for p in str(line['Projects']).split('|')]
                        projects = Project.objects.filter(name__in = project_list)
                        if len(project_list) != len(projects):
                            messages.add_message(request, messages.ERROR, "There is an error with a project in this list: "+ ', '.join(project_list)+".")
                            raise Exception
                        locus.projects.add(*list(projects))
                    
                    attributes = locus_type.locustypeattribute_set.all()
                    attributes_names = attributes.values_list("attribute_name", flat=True)
                    
                    if attributes:
                        attr_data = {}
                        for field in line.keys():
                            if field.lower() in attributes_names:
                                attr_data[field.lower()] = line[field]
                        
                        locus.update_attributes(attributes, attr_data)
                    
                    locus.save()
                    
                    if locus_type.positioned == True:
                        genome_version = '' if line['Genome version'] == '' else line['Genome version']
                        chromosome = '' if line['Chromosome'] == '' else line['Chromosome']
                        position = 0 if line['Position'] == '' else line['Position']
                        position = Position.objects.create(locus=locus, genome_version=genome_version, chromosome=chromosome, position=position)
                        position.save()
                
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+line["Name"]+").")
                    continue
        
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")


def update_locus_from_file(request, file_dict, locus_type):
    """
      Function used to update locus from a file uploaded by the user.
      :param dict file_dict: file on dict format returned by the CSVFactory
      :param LocusType locus_type: locus type to link the created locus
    """
    with transaction.atomic():       
        for line in file_dict:
            try:
                with transaction.atomic(): 
                    locus = Locus.objects.get(name=line["Name"])
                    
                    locus.comments=line["Comments"]
                    
                    if line['Projects']:
                        project_list = [p.strip() for p in str(line['Projects']).split('|')]
                        projects = Project.objects.filter(name__in = project_list)
                        if len(project_list) != len(projects):
                            messages.add_message(request, messages.ERROR, "There is an error with a project in this list: "+ ', '.join(project_list)+".")
                            raise Exception
                        locus.projects.clear()
                        locus.projects.add(*list(projects))
                        
                    else:
                        locus.projects.clear()
                        
                    attributes = locus_type.locustypeattribute_set.all()
                    attributes_names = attributes.values_list("attribute_name", flat=True)
                    
                    if attributes:
                        attr_data = {}
                        for field in line.keys():
                            if field.lower() in attributes_names:
                                attr_data[field.lower()] = line[field]
                        
                        locus.update_attributes(attributes, attr_data)
                    
                    locus.save()
                    
                    if locus_type.positioned == True:
                        position, result = Position.objects.get_or_create(locus=locus, defaults={'genome_version':'',
                                                                                                 'chromosome':'',
                                                                                                 'position':0})
                        position.genome_version = '' if line['Genome version'] == '' else line['Genome version']
                        position.chromosome = '' if line['Chromosome'] == '' else line['Chromosome']
                        position.position = 0 if line['Position'] == '' else line['Position']
                        position.save()
        
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+line["Name"]+").")
                    continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully uploaded and the locus have been updated.")


def set_locus_attributes(locus, attributes):
    """
      Get attribute's values for each locus to write in the table.
    """
    for loc in locus:
        for attribute in attributes:
            if loc.attributes_values:
                if type(loc.attributes_values) is dict:
                    attributes_values = loc.attributes_values
                else:
                    attributes_values = json.loads(loc.attributes_values)
                
                if str(attribute.id) in attributes_values.keys():
                    try:
                        setattr(loc, attribute.attribute_name.lower().replace(' ','_'),attributes_values[str(attribute.id)])
                    except ValueError as e:
                        print(e)
                        setattr(loc,attribute.attribute_name.lower().replace(' ','_'),'')
                else:
                    setattr(loc,attribute.attribute_name.lower().replace(' ','_'),'')
            else:
                setattr(loc,attribute.attribute_name.lower().replace(' ','_'),'')



class LocusEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of locus model. The user can still make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list of the fields the user can search with the refine search module
      :var form form_class_loc: the locus form based on the Locus model
      :var form file_form_class: the form to submit a file
      :var queryset locus: list of all the locus from the database
      :var int number_all: number of all the locus in the database
      :var int nb_per_page: number of data to display per page, initialized to 10
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class_loc = LocusForm
    form_class_pos = PositionForm
    file_form_class = UploadForm
    template = 'genotyping/locus_data.html'
    list_attributes = ['name', 'comments', 'projects']
    
    def get(self, request, *args, **kwargs):
        try:
            l = Locus.objects.get(id=kwargs['object_id'])
            locus_type = LocusType.objects.get(id=l.type.id)
        except:
            messages.add_message(request, messages.ERROR, "This locus doesn't exist in the database.")
            return redirect('locustype_management')
        
        attributes = locus_type.locustypeattribute_set.all()
        
        attributes_values = l.attributes_values
        
        data = {'name':l.name,
                'type':l.type.id,
                'comments':l.comments,
                'projects':l.projects.all()
                }
        
        for attribute in attributes:
            if str(attribute.id) in attributes_values.keys():
                data[attribute.attribute_name.replace(' ','_').lower()] = attributes_values[str(attribute.id)]
        
        form_loc = self.form_class_loc(attributes=attributes, data=data, instance=l)

        file_form = self.file_form_class()
        
        if locus_type.positioned == True:
            p, result = Position.objects.get_or_create(locus=l, defaults={'genome_version':'',
                                                                          'chromosome':'',
                                                                          'position':0})
                
            form_pos = self.form_class_pos(initial={'genome_version':p.genome_version,
                                                    'chromosome':p.chromosome,
                                                    'position':p.position})
        else:
            form_pos = None
        
        locus = Locus.objects.filter(type=locus_type).distinct()
        set_locus_attributes(locus, attributes)

        filtered_loc = locus
        number_all = len(locus)
        locus = add_pagination(request, locus)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form_loc)
        
        for attr in LocusTypeAttribute.objects.filter(locus_type=locus_type):
            in_list = attr.attribute_name in self.list_attributes
            if in_list == False:
                self.list_attributes.append(attr.attribute_name)
        
        if form_pos:
            names.extend([form_pos['genome_version'].name,form_pos['chromosome'].name,form_pos['position'].name])
            labels.extend([form_pos['genome_version'].label,form_pos['chromosome'].label,form_pos['position'].label])
            if 'chromosome' not in self.list_attributes:
                self.list_attributes.extend(['genome version','chromosome', 'position'])

        form_loc.fields['type'].widget = forms.HiddenInput()
        form_loc.fields['type'].initial = locus_type.id
        
        names.remove(form_loc['type'].name)
        labels.remove(form_loc['type'].label)
        
        tag_fields = {'all':locus,
                      'hiddenid':l.id,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': str(locus_type.name) +" Locus Edit",
                      'filetype':'Locus',
                      'typeid':locus_type.id,
                      'refine_search':True}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':locus,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                                'form_loc':form_loc,
                               'file_form':file_form,
                               'form_pos':form_pos,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            locus_searched = Locus.objects.refine_search(or_and, dico_get, locus_type)
            locus = locus_searched.intersection(filtered_loc)
            locus = locus.order_by('name')
            set_locus_attributes(locus, attributes)
            
            number_all = len(locus)
            locus = add_pagination(request, locus)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':locus,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                                'form_loc':form_loc,
                               'file_form':file_form,
                               'form_pos':form_pos,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
            
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
        
            locus_searched = Locus.objects.refine_search(or_and, dico_get, locus_type)
            
            #We only want to have accessions of the type considered
            locus = locus_searched.intersection(filtered_loc)
            locus = locus.order_by('name')
            set_locus_attributes(locus, attributes)
            
            number_all = len(locus)
            list_exceptions = ['type','id']
            download_not_empty = False
            thread_name = ''
            if locus:
                thread_name = execute_function_in_thread(write_locus_csv_from_viewer, [locus, "Result search", locus_type.id], {})
                download_not_empty = True
                
            tag_fields.update({'download_not_empty':download_not_empty,
                               'thread_name':thread_name})
            
            locus = add_pagination(request, locus)
            
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':locus,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':False,
                           'form_loc':form_loc,
                           'file_form':file_form,
                           'form_pos':form_pos,
                           'list_attributes':self.list_attributes}) 
        return render(request, self.template, tag_fields)
        
    def post(self, request, *args, **kwargs):
        l = Locus.objects.get(id=kwargs['object_id'])
        locus_type = LocusType.objects.get(id=l.type.id)

        attributes = locus_type.locustypeattribute_set.all()
        
        form_loc = self.form_class_loc(data=request.POST, attributes=attributes)
        file_form = self.file_form_class(request.POST, request.FILES)

        form_pos = self.form_class_pos(request.POST) if locus_type.positioned == True else None

        locus = Locus.objects.filter(type=locus_type)
        set_locus_attributes(locus, attributes)
        number_all = len(locus)
        locus = add_pagination(request, locus)
        
        names, labels = get_fields(form_loc)
        
        for attr in LocusTypeAttribute.objects.filter(locus_type=locus_type):
            self.list_attributes.append(attr.attribute_name)
            
        if form_pos:
            names.extend([form_pos['genome_version'].name,form_pos['chromosome'].name,form_pos['position'].name])
            labels.extend([form_pos['genome_version'].label,form_pos['chromosome'].label,form_pos['position'].label])
            self.list_attributes.extend(['genome version', 'chromosome', 'position'])

        form_loc.fields['type'].widget = forms.HiddenInput()
        form_loc.fields['type'].initial = locus_type.id
        
        names.remove(form_loc['type'].name)
        labels.remove(form_loc['type'].label)
        
        tag_fields = {'all':locus,
                      'number_all':number_all,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': str(locus_type.name) +" Locus Management",
                      'form_loc':form_loc,
                      'file_form':file_form,
                      'form_pos':form_pos,
                      'creation_mode':False,
                      'refine_search':True,
                      'filetype':'Locus',
                      'typeid':locus_type.id,
                      'list_attributes':self.list_attributes}

        if file_form.is_valid():
            messages.add_message(request, messages.ERROR, "You were in edit mode, please submit your file in management mode.")
            return redirect('locus_management',type_id=locus_type.id)
        
        update_locus_from_form(request, form_loc, form_pos, attributes)
    
        return redirect('locus_management',type_id=locus_type.id)



def update_locus_from_form(request, form_loc, form_pos, attributes):
    """
      Function used to update a locus object from the form.
      :param form form_loc: form submitted by the user for the locus' informations
    """
    if "hiddenid" in request.POST.keys() :
        l = Locus.objects.get(id=request.POST['hiddenid'])
    else:
        messages.add_message(request, messages.ERROR, "An error as occurred, please try again.")
        return
    
    try:
        projects = Project.objects.filter(id__in=request.POST.getlist('projects'))
         
        l.name = request.POST.get('name')
        l.comments = request.POST.get('comments')
        
        l.projects.clear()
        l.projects.add(*list(projects))
        
        l.update_attributes(attributes, request.POST)
        
        l.save()
        
        if form_pos:
            p = Position.objects.get(locus=l)
            p.genome_version = '' if request.POST.get('genome_version') == '' else request.POST.get('genome_version')
            p.chromosome = '' if request.POST.get('chromosome') == '' else request.POST.get('chromosome')
            p.position = 0 if request.POST.get('position') == '' else request.POST.get('position')
            p.save()
        
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return 
    
    messages.add_message(request, messages.SUCCESS, "The locus named {0} has been updated.".format(l.name))
    return 


def locus_filerenderer(type_id):
    """
      Filerenderer specific for locus, to display in 
      separated columns the locus and attributes' informations.
    """
    response = HttpResponse(content_type='text/csv') 
    writer = csv.writer(response,delimiter=';')
    headers = filerenderer_headers[12][1].copy()
    
    locustype = LocusType.objects.get(id=int(type_id))
    filename = locustype.name.replace(" ","_")+'.csv'
    database = Locus.objects.filter(type=locustype).values_list('name').distinct().order_by('name')
    attribute_names = locustype.locustypeattribute_set.all()
    
    for attribute in attribute_names:
        headers.append(attribute.attribute_name)
        
    if locustype.positioned == True:
        headers.extend(["Genome version", "Chromosome", "Position"])
    
    writer.writerow(headers)
    
    database_list = [i[0] for i in database]
    
    for entry_name in database_list:
        locus = Locus.objects.get(name=entry_name)
        
        line = [locus.name,
                locus.comments]
        
        line.append(' | '.join([str(project) for project in locus.projects.filter()]))
        
        if type_id != 0 and attribute_names:
                line = add_attribute_values(line, locus, attribute_names)
        
        if locustype.positioned == True:
            try:
                position = Position.objects.get(locus=locus)
                line.append(position.genome_version)
                line.append(position.chromosome)
                line.append(position.position)
            except:
                line.append('')
                line.append('')
                line.append('')
        
        writer.writerow(line)
        
    response['Content-Disposition'] = 'attachment; filename=%s' %filename  
    writer = csv.writer(response)
    return response
    

class LocusHeadersRenderer(View):
    """
      This view return a file containing the headers of a locus file according 
      to type's attributes and positions if locus are positioned.
    """
    def get(self, request, *args, **kwargs):
        
        type_id = kwargs['type_id']
        locus_type = LocusType.objects.get(id=type_id)
        
        response = HttpResponse(content_type='text/csv') 
        writer = csv.writer(response,delimiter=';', quoting=csv.QUOTE_ALL)
        
        headers = filerenderer_headers[12][1].copy()
        
        attribute_names = locus_type.locustypeattribute_set.all()
        for attribute in attribute_names:
            headers.append(attribute.attribute_name)
            
        if locus_type.positioned == True:
            headers.extend(["Genome version", "Chromosome", "Position"])
            
        writer.writerow(headers)
        
        filename = locus_type.name.replace(" ","_")+'_headers.csv'
        
        response['Content-Disposition'] = 'attachment; filename=%s' %filename  
        writer = csv.writer(response)
        
        return response


def write_locus_csv_from_viewer(model_data, title, type_id):
    """
      Search results writer specific for locus, to display in separated columns
      the locus and attributes' informations.
      :param dict model_data: data filtered by the search parameters
      :param string title: title of the file
    """
    locustype = LocusType.objects.get(id=int(type_id))
    attribute_names = locustype.locustypeattribute_set.all()
    list_col = filerenderer_headers[12][1].copy()
    
    for attribute in attribute_names:
        list_col.append(attribute.attribute_name)
        
    if locustype.positioned == True:
        list_col.extend(["Genome version", "Chromosome", "Position"])
    
    filename = title.lower().replace(" ","_")
    dialect = Dialect(delimiter = ';')
    
    with open("/tmp/"+filename+".csv", 'w', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, dialect)
        writer.writerow(list_col)
        
        for data in model_data:
            row = [data.name,
                   data.comments]
            row.append(' | '.join([str(project) for project in data.projects.filter()]))
            
            if attribute_names:
                row = add_attribute_values(row, data, attribute_names)
            
            if locustype.positioned == True:
                try:
                    position = Position.objects.get(locus=data)
                    row.append(position.genome_version)
                    row.append(position.chromosome)
                    row.append(position.position)
                except:
                    row.append('')
                    row.append('')
                    row.append('')
                    
            writer.writerow(row)
    
    download_not_empty = True
    return download_not_empty


#TODO: A regrouper avec la meme fonction dans le module accession, mais comment ?
def add_attribute_values(line, locus, attribute_names):
    """
      Get locus' attribute's values to write in the csv file.
    """
    if type(locus.attributes_values) is dict:
        attributes = locus.attributes_values
        for name in attribute_names:
            for id, value in attributes.items():
                if str(LocusTypeAttribute.objects.get(id=int(id)).attribute_name) == str(name):
                    line.append(value)
    else:
        attributes = ast.literal_eval(locus.attributes_values)
        for name in attribute_names:
            for id, value in attributes.items():
                if LocusTypeAttribute.objects.get(id=int(id)).attribute_name == name:
                    line.append(value)
    
    return line
    
    

class ExperimentManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the experiment model. It allows the deletion and insertion of experiment based on the Experiment Model.
      The filters are applied to this view. The user can make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list containing the names of the fields which can be searched with the refine search 
      :var form form_class: the experiment form
      :var queryset traits: contains all the experiments of the database
      :var int number_all: contains the number of experiments of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = ExperimentForm
    template = 'genotyping/genotyping_experiment.html'
    title = "Experiment Management"
    list_attributes=['name', 'institution', 'date', 'category', 'comments', 'projects']
    
    def get(self, request, *args, **kwargs):
        form = self.form_class()
        experiments = Experiment.objects.all()
        number_all = len(experiments)
        experiments = add_pagination(request, experiments)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        tag_fields = {'all':experiments,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': self.title, 
                      'filetype':'Experiment',
                      'refine_search':True}

        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':experiments,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'list_attributes':self.list_attributes})
 
            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            experiments = Experiment.objects.refine_search(or_and, dico_get)
            
            number_all = len(experiments)
            experiments = add_pagination(request, experiments)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':experiments,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'list_attributes':self.list_attributes})
 
            return render(request,self.template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
             
            experiments = Experiment.objects.refine_search(or_and, dico_get)
            
            number_all = len(experiments)
            download_not_empty = False
            if experiments:
                download_not_empty = write_experiment_csv_from_viewer(experiments, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            experiments = add_pagination(request, experiments)
        
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':experiments,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           'list_attributes':self.list_attributes})  
        return render(request, self.template, tag_fields)
        
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        
        experiments = Experiment.objects.all()
        number_all = len(experiments)
        experiments = add_pagination(request, experiments)
        
        names, labels = get_fields(form)
        
        tag_fields = {'all':experiments,
                      'number_all':number_all,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': self.title,
                      'form':form,
                      'creation_mode':True,
                      'refine_search':True,
                      'filetype':'Experiment',
                      'list_attributes':self.list_attributes}
        
        if form.is_valid():
            request,template,tag_fields = generic_create(Experiment, self.form_class, request, self.template,{'refine_search':True,
                                                                                                              'filetype':'Experiment',
                                                                                                              'list_attributes':self.list_attributes}, self.title)
            return render(request,template,tag_fields) 
            
        
        else:
            messages.add_message(request, messages.ERROR, "An error occurred.")
            return render(request, self.template, tag_fields)


class ExperimentEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of experiment model. The user can still make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list containing the names of the fields which can be searched with the refine search 
      :var form form_class: the experiment form
      :var queryset traits: contains all the experiments of the database
      :var int number_all: contains the number of experiments of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = ExperimentForm
    template = 'genotyping/genotyping_experiment.html'
    title = "Experiment Management"
    list_attributes=['name', 'institution', 'date', 'comments', 'projects']
    
    def get(self, request, *args, **kwargs):
        try:
            e = Experiment.objects.get(id=kwargs['object_id'])
        except:
            messages.add_message(request, messages.ERROR, "This experiment doesn't exist in the database.")
            return redirect('experiment_management')
        
        form = self.form_class(initial={'name':e.name,
                                        'institution':e.institution,
                                        'date':e.date,
                                        'category':e.category,
                                        'phenotyping_file':e.phenotyping_file,
                                        'genotyping_file':e.genotyping_file,
                                        'isolat_file':e.isolat_file,
                                        'comments':e.comments,
                                        'projects':e.projects.all()
                                        })
        
        experiments = Experiment.objects.all()
        number_all = len(experiments)
        experiments = add_pagination(request, experiments)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        tag_fields = {'all':experiments,
                      'hiddenid':e.id,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': self.title, 
                      'filetype':'Experiment',
                      'refine_search':True}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':experiments,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'list_attributes':self.list_attributes})
 
            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            experiments = Experiment.objects.refine_search(or_and, dico_get)
            
            number_all = len(experiments)
            experiments = add_pagination(request, experiments)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':experiments,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
             
            experiments = Experiment.objects.refine_search(or_and, dico_get)
            
            number_all = len(experiments)
            download_not_empty = False
            if experiments:
                download_not_empty = write_experiment_csv_from_viewer(experiments, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            experiments = add_pagination(request, experiments)
        
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':experiments,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':False,
                           'form':form,
                           'list_attributes':self.list_attributes})  
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        update_experiment_from_form(request, self.form_class)
        
        return redirect('experiment_management')

def update_experiment_from_form(request, form_class):
    """
      Function used to update an experiment object from the form.
      :param form form_class: experiment form class
    """
    
    if "hiddenid" in request.POST.keys() :
        e = Experiment.objects.get(id=request.POST['hiddenid'])
    else:
        messages.add_message(request, messages.ERROR, "An error as occurred, please try again.")
        return
    
    try:
        old_pheno = e.phenotyping_file.path if e.phenotyping_file else None
        old_geno = e.genotyping_file.path if e.genotyping_file else None
        old_isolat = e.isolat_file.path if e.isolat_file else None
        
        form = form_class(request.POST, request.FILES, instance=e)
        
        if form.is_valid():
            files_to_delete = manage_postulation_files(form, e, old_pheno, old_geno, old_isolat)
             
            form.save()
            
            #When the form is saved, the files are deleted
            for file in files_to_delete:
                os.remove(file)
             
        else:
            messages.add_message(request, messages.ERROR, ' '.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ]))
      
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return
    
    messages.add_message(request, messages.SUCCESS, "The experiment named {0} has been updated.".format(e.name))
    return 

def manage_postulation_files(form, e, old_pheno, old_geno, old_isolat):
    """
      Manage the deletion of the files on the server according to 
      the submitted and deleted files on the form.
    """
    files_to_delete = []
    
    if form.cleaned_data['category'] != 1:
        if old_pheno:
            files_to_delete.append(old_pheno)
            e.phenotyping_file = ''
        if old_geno:
            files_to_delete.append(old_geno)
            e.genotyping_file = ''
        if old_isolat:
            files_to_delete.append(old_isolat)
            e.isolat_file = ''
      
    if old_pheno and (type(form.cleaned_data['phenotyping_file']) == InMemoryUploadedFile or form.cleaned_data['phenotyping_file'] == False):
        files_to_delete.append(old_pheno)
              
    if old_geno and (type(form.cleaned_data['genotyping_file']) == InMemoryUploadedFile or form.cleaned_data['genotyping_file'] == False):
        files_to_delete.append(old_geno)
      
    if old_isolat and (type(form.cleaned_data['isolat_file']) == InMemoryUploadedFile or form.cleaned_data['isolat_file'] == False):
        files_to_delete.append(old_isolat)
    
    return files_to_delete

def experiment_filerenderer():
    """
      Filerenderer specific for experiment, to display the category 
      of the experiment.
    """
    response = HttpResponse(content_type='text/csv') 
    writer = csv.writer(response,delimiter=';')
    headers = filerenderer_headers[10][1].copy()
    
    filename = 'Experiment.csv'
    database = Experiment.objects.all().values_list('name').distinct().order_by('name')
    
    writer.writerow(headers)
    
    database_list = [i[0] for i in database]
    
    for entry_name in database_list:
        exp = Experiment.objects.get(name=entry_name)
        
        line = [exp.name,
                exp.institution,
                exp.date,
                experiment_category[exp.category],
                exp.comments]
        line.append(' | '.join([str(project) for project in exp.projects.filter()]))
        
        writer.writerow(line)
        
    response['Content-Disposition'] = 'attachment; filename=%s' %filename  
    writer = csv.writer(response)
    return response


def write_experiment_csv_from_viewer(model_data, title):
    """
      Search results writer specific for experiment, to display the category 
      of the experiment.
      :param dict model_data: data filtered by the search parameters
      :param string title: title of the file
    """
    list_col = filerenderer_headers[10][1].copy()
    
    filename = title.lower().replace(" ","_")
    dialect = Dialect(delimiter = ';')

    with open("/tmp/"+filename+".csv", 'w', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, dialect)
        writer.writerow(list_col)
        
        for data in model_data:
            row = [data.name,
                   data.institution,
                   data.date,
                   experiment_category[data.category],
                   data.comments]
            row.append(' | '.join([str(project) for project in data.projects.filter()]))

            writer.writerow(row)
    
    download_not_empty = True
    return download_not_empty


class GenotypingDataInsert(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View displaying a form to insert genotyping data files.
      :var str title: title displayed on the template
      :var str template: html template for the rendering of the view
      :var form form_class: the file submission form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var file file: file uploaded by the user in the form
      :var dict file_dict: dict version of the file, parsed by the CSVFactory
    """
    title = "Insert Experiment Data"
    template = 'genotyping/insert_genotyping_data.html'
    form_class = GenotypingDataUpload
    
    def get(self, request, *args, **kwargs):
        form = self.form_class()
        
        tag_fields = {'title':self.title,
                      'filetype':"Flat_matrix",
                      'form':form
                      }
        
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        
        tag_fields = {'title':self.title,
                      'filetype':"Flat_matrix",
                      'form':form
                      }
        
        if form.is_valid():
            file = request.FILES['file']
            exp_name = form.cleaned_data['experiment']
            
            if form.cleaned_data['file_format'] == "1":
                myfactory = CSVFactoryForMatrix(file)
                myfactory.find_dialect()
                myfactory.read_file()
                file_dict = myfactory.get_file()
                
                label = myfactory.get_first_label()
                
                create_genotyping_data_from_matrix_file(request, file_dict, label, exp_name)
                return render(request, self.template, tag_fields)
                
            elif form.cleaned_data['file_format'] == "2":
                myfactory = CSVFactoryForTempFiles(file)
                myfactory.find_dialect()
                myfactory.read_file()
                file_dict = myfactory.get_file()
                
                headers_list = filerenderer_headers[11][1]
                
                missing = myfactory.check_headers_conformity(headers_list)
                
                if missing != []:
                    messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                                         ". The required headers for this file are: "+format(', '.join(headers_list))+".")
                    return redirect('genotypingdata_insert')
                 
                create_genotyping_data_from_flat_file(request, file_dict, exp_name)
                return render(request, self.template, tag_fields)
            
            else:
                messages.add_message(request, messages.ERROR, "An error occurred with your file.")
                return render(request, self.template, tag_fields)
        
        else:
            messages.add_message(request, messages.ERROR, "An error occurred with your file.")
            return render(request, self.template, tag_fields)


def create_genotyping_data_from_matrix_file(request, file_dict, label, exp_name):
    """
      Parse the FileDict given by the CSVFactory and insert data into the database.
      :param FileDict file_dict: dict version of the file, parsed by the CSVFactory.
      :param str label: name of the first cell of the file, key for the accession's name in the dict.
      :param str exp_name: name of the selected experiment.
    """
    try:
        experiment = Experiment.objects.get(name=exp_name)
    except Exception as e:
        messages.add_message(request, messages.ERROR, "The experiment is not valid, please try again.")
        return
    
    with transaction.atomic():
        for line in file_dict:
            try:
                acc_name = line[label]
                accession = Accession.objects.get(name=acc_name)
                del line[label]
                
                for locus_name in line.keys():
                    try:
                        with transaction.atomic():
                            locus = Locus.objects.get(name=locus_name)
                            geno_value = GenotypingValue.objects.create(accession=accession, locus=locus, experiment=experiment, value=line[locus_name])
                            geno_value.save()
     
                    except Exception as e:
                        if str(e) != "":
                            messages.add_message(request, messages.ERROR, str(e)+" (at line: "+acc_name+", column: "+locus_name+").")
                            continue              
                    
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+acc_name+").")
                    continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")


def create_genotyping_data_from_flat_file(request, file_dict, exp_name):
    """
      Parse the FileDict given by the CSVFactory and insert data into the database.
      :param FileDict file_dict: dict version of the file, parsed by the CSVFactory.
      :param str exp_name: name of the selected experiment.
    """
    try:
        experiment = Experiment.objects.get(name=exp_name)
    except Exception as e:
        messages.add_message(request, messages.ERROR, "The experiment is not valid, please try again.")
        return
    
    with transaction.atomic():
        for line in file_dict:
            try:
                with transaction.atomic(): 
                    accession = Accession.objects.get(name=line['Accession'])
                    locus = Locus.objects.get(name=line['Locus'])
                    value = line['Allele_value']
                    allelic_freq = line['Allelic_frequency'] if 'Allelic_frequency' in line.keys() else 1
                    geno_value = GenotypingValue.objects.create(accession=accession, locus=locus, experiment=experiment, value=value, allelic_frequency=allelic_freq)
                    geno_value.save()
        
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+line['Accession']+", "+line['Locus']+").")
                    continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")

class GenotypingMatrixTemplateFileRenderer(View):
    """
      View to render a matrix file template.
    """
    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv') 
        writer = csv.writer(response,delimiter=';', quoting=csv.QUOTE_ALL)
        
        headers = ['Name', 'Locus1', 'Locus2', 'Locus3', 'Locus4', '...']
        
        writer.writerow(headers)
        
        writer.writerow(['Accession1', '', '', '', ''])
        writer.writerow(['Accession2', '', '', '', ''])
        writer.writerow(['Accession3', '', '', '', ''])
        writer.writerow(['Accession4', '', '', '', ''])
        writer.writerow(['...', '', '', '', ''])
        
        filename = 'Matrix_file_template.csv'
        
        response['Content-Disposition'] = 'attachment; filename=%s' %filename  
        writer = csv.writer(response)
        
        return response
    


class GenotypingViewer(LoginRequiredMixin, View):
    """
    """
    template = 'dataview/genotyping_viewer.html'
    title = "Experiment data Viewer"
    
    def get(self, request, *args, **kwargs):
        formproject = DataviewProjectForm()
        username = request.user.login
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)

        formproject.fields['project'].queryset=projects.order_by('name')

        n_page = 0
        return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formproject':formproject})


    def post(self, request, *args, **kwargs):
        formproject = DataviewProjectForm(request.POST)
        
        #Get the actual page number
        n_page = int(request.POST['n_page'])
        
        if int(n_page) == 1:
            if formproject.is_valid():
                projects = request.POST.getlist('project')
                request.session['projects'] = projects
            else:
                return render(request,self.template,{'title':self.title, 'n_page':0, 'formproject':formproject})
            
            formacc = DataviewAccessionListForm()
            formloc = DataviewLocusPositionForm()
            formlocfile = DataviewLocusFileUploadForm()
            formexp = DataviewExperimentListForm()
            
            accessions_names = AccessionLocal.objects.filter(projects__in=projects).values('accession').distinct()
            projects_acc = Accession.objects.filter(id__in=accessions_names).distinct()
            
            projects_exp = Experiment.objects.filter(projects__in=projects).distinct()
            
            projects_locus = Locus.objects.filter(projects__in=projects).distinct()
            
            genome_versions = Position.objects.filter(locus__in=projects_locus).values('genome_version').distinct()
            chromosomes = Position.objects.filter(locus__in=projects_locus).values('chromosome').distinct()
            
            version_choices = []
            chromosome_choices = []
            
            for version in genome_versions:
                version_choices.append([version['genome_version'],version['genome_version']])
            for chr in chromosomes:
                chromosome_choices.append([chr['chromosome'],chr['chromosome']])
            
            formacc.fields['accession'].queryset = projects_acc
            
            formexp.fields['experiment'].queryset = projects_exp
            
            formloc.fields['genome_version'].choices = version_choices
            formloc.fields['chromosome'].choices = chromosome_choices
            
            return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formacc':formacc,
                                                 'formloc':formloc, 'formlocfile':formlocfile, 'formexp':formexp})
            
        
        if int(n_page) == 2:
            
            formacc = DataviewAccessionListForm(request.POST)
            formloc = DataviewLocusPositionForm(request.POST)
            formlocfile = DataviewLocusFileUploadForm(request.POST, request.FILES)
            formexp = DataviewExperimentListForm(request.POST)
            
            if formacc.is_valid() and formexp.is_valid():
                accessions = request.POST.getlist('accession')
                experiments = request.POST.getlist('experiments')
                request.session['accessions'] = accessions
                request.session['experiments'] = experiments
                
            else:
                return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formacc':formacc,
                                                     'formloc':formloc, 'formlocfile':formlocfile, 'formexp':formexp})
            
            file = None
            locus_list = None
            print(request.POST)
            
            if request.FILES:
                file = request.FILES['file']
            elif 'locus' in request.POST.keys():
                locus_list = request.POST['locus']
            else:
                if formloc.is_valid():
                    genome_version = request.POST.get('genome_version') if 'genome_version' in request.POST else None
                    chromosome = request.POST.get('chromosome') if 'chromosome' in request.POST else None
                    pos_min = request.POST.get('pos_min') if 'pos_min' in request.POST else None
                    pos_max = request.POST.get('pos_max') if 'pos_max' in request.POST else None
                
                else:
                    return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formacc':formacc,
                                                         'formloc':formloc, 'formlocfile':formlocfile, 'formexp':formexp})


            if not file and not locus_list:
                print("on est dans not file and not list")
                resubmit = False
                if not genome_version:
                    messages.add_message(request, messages.ERROR, "Please give a value for genome version.")
                    resubmit = True
                
                if not chromosome:
                    messages.add_message(request, messages.ERROR, "Please give a value for the chromosome.")
                    resubmit = True
                    
                if not pos_min:
                    messages.add_message(request, messages.ERROR, "Please give a value for the minimal position.")
                    resubmit = True
                    
                if not pos_max:
                    messages.add_message(request, messages.ERROR, "Please give a value for the maximal position.")
                    resubmit = True
                
                if resubmit:
                    return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formacc':formacc,
                                                         'formloc':formloc, 'formlocfile':formlocfile, 'formexp':formexp})
                
                
                locus_init = Locus.objects.filter(projects__in=request.session['projects'])
                loc_positions = Position.objects.filter(locus__in=locus_init, genome_version=genome_version, chromosome=chromosome, position__gte=pos_min, position__lte=pos_max).values_list('locus', flat=True)
                locus = Locus.objects.filter(id__in=loc_positions).distinct()
                locus_names = locus.values_list('name', flat=True)
                
                print("========================locus")
                print(locus_init)
                print(Position.objects.filter(locus__in=locus_init, genome_version=genome_version, chromosome=chromosome, position__gte=pos_min, position__lte=pos_max))
                print(loc_positions)
                print(locus)
                print(locus_names)
            
            elif locus_list:
                print("on est dans locus list")
                locus = Locus.objects.filter(projects__in=request.session['projects'], id__in=locus_list).distinct()
                locus_names = locus.values_list('name', flat=True)
            
            #Parsing du fichier ici
            else:
                print("on est dans file")
                myfactory = CSVFactoryForTempFiles(file)
                myfactory.find_dialect()
                myfactory.read_file()
                file_dict = myfactory.get_file()
                
                loc_name_list = []
                for line in file_dict:
                    loc_name_list.append(line)
                
                locus = Locus.objects.filter(projects__in=request.session['projects'], name__in=loc_name_list).distinct()
                locus_names = locus.values_list('name', flat=True)
                
            geno_values = GenotypingValue.objects.filter(locus__in=locus, accession__in=accessions, experiment__in=experiments)
            accessions_list = geno_values.values_list('accession', flat=True)
            accessions_final = Accession.objects.filter(id__in=accessions_list)
            
            locus_list = geno_values.values_list('locus', flat=True)
            locus_final = Locus.objects.filter(id__in=locus_list)
            
            print(accessions_final)
            
            initial_rows = ["Genome version","Chromosome","Position"]
            initial_cols = [""]
            
            for acc in accessions_final:
                initial_rows.append(acc.name)
            
#             for val in geno_values:
#                 initial_rows.append()

            csv_list=[]
            
            for locus in locus_final:
                geno = geno_values.filter(locus=locus)
                loc_pos = Position.objects.get(locus=locus)
                
                for i in range(len(geno)):
                    version_dict = {'':'Genome Version',locus:loc_pos.genome_version}
                    chromosome_dict = {'':'Chromosome',locus:loc_pos.chromosome}
                    position_dict = {'':'Position',locus:loc_pos.position}
                    csv_list.append(version_dict)
                    csv_list.append(chromosome_dict)
                    csv_list.append(position_dict)










