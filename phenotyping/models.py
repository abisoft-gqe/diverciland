"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import os

from django.db import models
from django_countries.fields import CountryField
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.files.storage import FileSystemStorage
from django.dispatch import receiver

from commonfct.constants import TRAIT_CATEGORY
from phenotyping.managers import TraitManager, MethodManager


fs = FileSystemStorage(location=settings.MEDIA_ROOT)

class Trait(models.Model):
    """
      The Trait model gives information about the traits.
      
      :var CharField name: name of the trait
      :var CharField plot_abbreviation: abbreviation of the trait to display on plots
      :var CharField unit: unit of measurement of the trait
      :var TextField comments: comments about the trait 
      :var ManyToManyField projects: project(s) linked to the traits
    """
    name = models.CharField(max_length = 100, unique = True)
    plot_abbreviation = models.CharField(max_length = 20, blank=True, null=True)
    unit = models.CharField(max_length = 100, blank=True, null=True)
    category = models.IntegerField(choices=TRAIT_CATEGORY)
    scale_file = models.FileField(storage=fs, upload_to='scale_file/', blank=True)
    comments = models.TextField(max_length=500, blank=True, null=True)
    
    projects = models.ManyToManyField('team.Project')
    
    objects = TraitManager()
    
    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ['name']


class Method(models.Model):
    """
        The Method model gives information about the method.
        
        :var CharField name: the name of the method
        :var TextField description: the description of the method
    """
    name = models.CharField(max_length=200, unique=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    
    objects = MethodManager()
    
    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ['name']


DATA_TYPES = ((1,'Catalogue data'),
              (2,'Trial data'),
              )

class RawData(models.Model):
    """
        The RawData model gives informations on data inserted in the database.
        
        :var TextField raw_data: value of the data
        :var IntegerField type: type of the data
        :var DateField date: date of measurement of the data
        :var CountryField country: country of measurement of the data
        :var ForeignKey accession: accession on which the data have been measured
        :var ForeignKey trait: trait linked to the data
        :var ForeignKey method: method of measurement of the data
        :var ManyToManyField projects: projects in which the data can be showed
        :var DateField submit_date: date where the data have been submitted
        :var DateField last_modification_date: date where the data have been last modified
        :var ForeignKey last_modification_user: user who modified the data
    """
    raw_data = models.TextField(blank=True,null=True)
    elaborated_data = models.TextField(blank=True,null=True)
    type = models.IntegerField(choices=DATA_TYPES)
    year = models.IntegerField()
    country = CountryField()
    
    accession = models.ForeignKey('accession.Accession', db_index=True, on_delete=models.CASCADE)
    trait = models.ForeignKey('Trait', db_index=True, on_delete=models.CASCADE)
    method = models.ForeignKey('Method', blank=True, null=True, on_delete=models.CASCADE)
    
    submit_date = models.DateField(auto_now_add=True)
    last_modification_date = models.DateField(auto_now=True)
    last_modification_user = models.ForeignKey('team.User', blank=True, null=True, on_delete=models.SET_NULL)
    
    class Meta:
        unique_together=("type", "year", "country", "accession", "trait")
    
    def __str__(self):
        return self.raw_data


@receiver(models.signals.post_delete, sender=Trait)
def auto_delete_scale_file_on_delete(sender, instance, **kwargs):
    """
      Delete file of filesystem when
      corresponding Trait object is deleted
    """
    if instance.scale_file:
        if os.path.isfile(instance.scale_file.path):
            os.remove(instance.scale_file.path)
