"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import datetime

from dal import autocomplete
from django import forms

from accession.models import AccessionLocal, Accession
from django_countries.fields import CountryField
from django.forms import ModelForm
from django.forms.widgets import NumberInput

from phenotyping.models import Trait, Method, RawData


class TraitForm(ModelForm):
    """
      Form based on the Trait model to create traits
    """
    class Meta:
        fields = '__all__'
        model = Trait

class TraitUploadForm(forms.Form):
    """
      Form used to upload a file to create or update traits.
    """
    file = forms.FileField(label='Select the file to upload ')


class MethodForm(ModelForm):
    """
      Form based on the Method model to create traits
    """
    class Meta:
        fields = '__all__'
        model = Method

class RawDataForm(ModelForm):
    """
      Form based on the RawData model to edit raw data.
    """
    class Meta:
        model = RawData
        fields = ['raw_data','year', 'accession', 'country', 'trait','method']
        widgets = {'raw_data':forms.Textarea(attrs={'rows':'1', 'cols':'13'}),
                   'year': forms.TextInput(attrs={'disabled': True}),
                   'accession': forms.TextInput(attrs={'disabled': True}),
                   'country': forms.TextInput(attrs={'disabled': True}),
                   'trait': forms.TextInput(attrs={'disabled': True}),
                   'method': forms.TextInput(attrs={'disabled': True})}

class CatalogueDataUploadForm(forms.Form):
    """
      Form used to upload a file containing catalogue data 
      (several traits on one year).
    """
    country = CountryField(blank_label='Choose a Country').formfield()
    file = forms.FileField(label='Select the file to upload ')
    

class CatalogueDataUploadUpdateForm(forms.Form):
    """
      Form used to upload a file containing catalogue data 
      updates on several years for one trait.
    """
    country = CountryField(blank_label='Choose a Country').formfield()
    trait = forms.ModelChoiceField(required=True,
                                   queryset=Trait.objects.all(),
                                   empty_label="Choose a Trait")
    method = forms.ModelChoiceField(required=False,
                                   queryset=Method.objects.all(),
                                   empty_label="No method")
    file = forms.FileField(label='Select the file to upload ')


class AccessionAutocompleteForm(forms.Form):
    """
      Form to select one accession with its name (autocomplete).
    """
    accession = forms.ModelChoiceField(required=True,
                                       queryset=Accession.objects.all(),
                                       widget=autocomplete.ModelSelect2())

