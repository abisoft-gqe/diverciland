"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from django.urls import path
from phenotyping.views import TraitManagement, TraitEdit, MethodManagement, MethodEdit,\
                              CatalogueDataInsert, CatalogueDataViewer, CatalogueDataCardViewer,\
                              CatalogueDataEdit, GetRawDataEdition,\
                              ScaleTemplateFileRenderer

urlpatterns = [
    path('trait/', TraitManagement.as_view(), name='trait_management'),
    path('trait/<int:object_id>/', TraitEdit.as_view(), name='trait_edit'),
    path('method/', MethodManagement.as_view(), name='method_management'),
    path('method/<int:object_id>/', MethodEdit.as_view(), name='method_edit'),
    path('cataloguedata/insert/', CatalogueDataInsert.as_view(), name='catalogue_data_insert'),
    path('cataloguedata/edit/', CatalogueDataEdit.as_view(), name='catalogue_data_edit'),
    path('cataloguedata/edit/<int:id>/', GetRawDataEdition.as_view(), name='raw_data_edition'),
    path('cataloguedataviewer/', CatalogueDataViewer.as_view(), name='catalogue_data_viewer'),
    path('cataloguedatacardviewer/<int:accession_id>/<str:trait_name>/', CatalogueDataCardViewer.as_view(), name='catalogue_data_card_viewer'),
    
    path('scalefilerenderer/',ScaleTemplateFileRenderer.as_view(), name='scalefilerenderer')
]