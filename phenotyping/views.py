"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import ast
import operator
import csv
import colorcet as cc
import json
import copy
import folium
import os

from _csv import Dialect
from django_countries import countries
from bokeh.embed import components
from bokeh.models import Title
from bokeh.plotting import figure
from bokeh.palettes import magma
from django.contrib import messages
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import transaction, IntegrityError
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.views import View

from accession.models import AccessionLocal, Accession
from commonfct.constants import filerenderer_headers, trait_category
from commonfct.utils import get_fields, add_pagination, get_dict_key_from_value,\
                            send_message_if_not_already_sent
from commonfct.genericviews import generic_create, generic_update, write_csv_from_viewer
from csv_io.views import CSVFactoryForTempFiles
from core_viewer.forms import DataviewProjectForm, DataviewAccessionMultAutocompleteForm,\
                              DataviewTraitListForm, DataviewCountryFormList
from phenotyping.forms import TraitForm, TraitUploadForm, MethodForm, CatalogueDataUploadForm,\
                              CatalogueDataUploadUpdateForm, AccessionAutocompleteForm,\
                              RawDataForm
from phenotyping.models import Trait, Method, RawData
from remote_forms.forms import RemoteForm
from team.models import Project, User
from team.views import LoginRequiredMixin, UserIsAdminMixin

class TraitManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the trait model. It allows the deletion and insertion of traits based on the Trait Model.
      The filters are applied to this view. The user can make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list containing the names of the fields which can be searched with the refine search 
      :var form form_class: the trait form
      :var queryset traits: contains all the traits of the database
      :var int number_all: contains the number of traits of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = TraitForm
    file_form_class = TraitUploadForm
    template = 'phenotyping/phenotyping_trait.html'
    title = "Trait Management"
    list_attributes=['name', 'plot_abbreviation', 'unit', 'category', 'comments', 'projects']
    
    def get(self, request, *args, **kwargs):
        form = self.form_class()
        file_form = self.file_form_class()
        traits = Trait.objects.all()
        number_all = len(traits)
        traits = add_pagination(request, traits)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        tag_fields = {'all':traits,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': self.title, 
                      'filetype':'Trait',
                      'refine_search':True}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':traits,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
 
            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            traits = Trait.objects.refine_search(or_and, dico_get)
            
            number_all = len(traits)
            traits = add_pagination(request, traits)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':traits,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
 
            return render(request,self.template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
             
            traits = Trait.objects.refine_search(or_and, dico_get)
            
            number_all = len(traits)
            list_exceptions = ['id','rawdata']
            download_not_empty = False
            if traits:
                download_not_empty = write_trait_csv_from_viewer(traits, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            traits = add_pagination(request, traits)
        
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':traits,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           'file_form':file_form,
                           'list_attributes':self.list_attributes})  
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        file_form = self.file_form_class(request.POST, request.FILES)
        
        traits = Trait.objects.all()
        number_all = len(traits)
        traits = add_pagination(request, traits)
        
        names, labels = get_fields(form)
        
        tag_fields = {'all':traits,
                      'number_all':number_all,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': self.title,
                      'form':form,
                      'file_form':file_form,
                      'creation_mode':True,
                      'refine_search':True,
                      'filetype':'Trait',
                      'list_attributes':self.list_attributes}
        
        if form.is_valid():
            try:
                trait = form.save()
             
            except Exception as e:
                messages.add_message(request, messages.ERROR, str(e))
                return redirect('trait_management')
            
            if trait.scale_file:
                myfactory = CSVFactoryForTempFiles(trait.scale_file)
                myfactory.find_dialect()
                myfactory.read_file()
                 
                headers = myfactory.get_headers()
                if "Common Scale" not in headers or "Label" not in headers:
                    os.remove(trait.scale_file.path)
                    trait.scale_file = None
                    trait.save()
                    messages.add_message(request, messages.ERROR, "An important header is missing of your file. It must contain: 'Common scale' and 'Label'.")
                    messages.add_message(request, messages.ERROR, "Please complete and resubmit the file by editing the trait.")
             
            traits = Trait.objects.all()
            number_all = len(traits)
            traits = add_pagination(request, traits)
             
            tag_fields.update({'all':traits,
                               'number_all':number_all,
                               'form':form,
                               'object':trait,
                               'creation_mode':True})
             
            messages.add_message(request, messages.SUCCESS, "A new Trait named {0} has been created.".format(trait))
            return render(request,self.template,tag_fields)
        
        elif file_form.is_valid():
            file = request.FILES['file']
            myfactory = CSVFactoryForTempFiles(file)
            myfactory.find_dialect()
            myfactory.read_file()
            file_dict = myfactory.get_file()

            headers_list = filerenderer_headers[2][1]
            
            missing = myfactory.check_headers_conformity(headers_list)
            
            if missing != []:
                messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                                     ". The required headers for this file are: "+format(', '.join(headers_list))+".")
                return redirect('trait_management')

            if request.POST.get('submit'):
                create_trait_from_file(request, file_dict)
            elif request.POST.get('update'):
                update_trait_from_file(request, file_dict)
            
            traits = Trait.objects.all()
            number_all = len(traits)
            traits = add_pagination(request, traits)
            tag_fields.update({'all':traits,
                               'number_all':number_all})
            
            return render(request, self.template, tag_fields)
    
        else:
            messages.add_message(request, messages.ERROR, "An error occurred.")
            return render(request, self.template, tag_fields)


def create_trait_from_file(request, file_dict):
    """
      Function used to create traits from a file uploaded by the user.
      :param dict file_dict: file on dict format returned by the CSVFactory
    """
    with transaction.atomic():       
        for line in file_dict:
            try:
                with transaction.atomic(): 
                    category = get_dict_key_from_value(trait_category, line["Category"])
                    if type(category) != int:
                        raise Exception("This category doesn't exists, please use a category of the following list: {0}.".format(', '.join(trait_category.values())))
                    
                    trait = Trait.objects.create(name=line["Name"],
                                                 plot_abbreviation=line["Plot abbreviation"],
                                                 unit=line["Unit"],
                                                 category=category,
                                                 comments=line["Comments"]
                                                 )
                    
                    if line['Projects']:
                        project_list = [p.strip() for p in str(line['Projects']).split('|')]
                        projects = Project.objects.filter(name__in = project_list)
                        if len(project_list) != len(projects):
                            messages.add_message(request, messages.ERROR, "There is an error with a project in this list: "+ ', '.join(project_list)+".")
                            raise Exception
                        trait.projects.add(*list(projects))
                
                    trait.save()
                
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                    continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")



def update_trait_from_file(request, file_dict):
    """
      Function used to update traits from a file uploaded by the user.
      :param dict file_dict: file on dict format returned by the CSVFactory
    """
    with transaction.atomic():       
        for line in file_dict:
            try:
                with transaction.atomic(): 
                    trait = Trait.objects.get(name=line["Name"])
                    
                    category = get_dict_key_from_value(trait_category, line["Category"])
                    if type(category) != int:
                        raise Exception("This category doesn't exists, please use a category of the following list: {0}.".format(', '.join(trait_category.values())))
                    
                    trait.plot_abbreviation=line["Plot abbreviation"]
                    trait.unit=line["Unit"]
                    trait.category=category
                    trait.comments=line["Comments"]
                    
                    if line['Projects']:
                        project_list = [p.strip() for p in str(line['Projects']).split('|')]
                        projects = Project.objects.filter(name__in = project_list)
                        if len(project_list) != len(projects):
                            messages.add_message(request, messages.ERROR, "There is an error with a project in this list: "+ ', '.join(project_list)+".")
                            raise Exception
                        trait.projects.clear()
                        trait.projects.add(*list(projects))
        
                    else:
                        trait.projects.clear()
                    
                    trait.save()
                
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                    continue
        
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully uploaded and the traits have been updated.")


class TraitEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of trait model. The user can still make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list containing the names of the fields which can be searched with the refine search 
      :var form form_class: the trait form
      :var queryset traits: contains all the traits of the database
      :var int number_all: contains the number of traits of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = TraitForm
    file_form_class = TraitUploadForm
    template = 'phenotyping/phenotyping_trait.html'
    title = "Trait Edit"
    list_attributes=['name', 'plot_abbreviation', 'unit', 'category', 'comments', 'projects']
    
    def get(self, request, *args, **kwargs):
        try:
            t = Trait.objects.get(id=kwargs['object_id'])
        except:
            messages.add_message(request, messages.ERROR, "This trait doesn't exist in the database.")
            return redirect('trait_management')
    
        form = self.form_class(initial={'name':t.name,
                                        'plot_abbreviation':t.plot_abbreviation,
                                        'unit':t.unit,
                                        'category':t.category,
                                        'scale_file':t.scale_file,
                                        'comments':t.comments,
                                        'projects':t.projects.all()
                                        })
        
        file_form = self.file_form_class()
        
        traits = Trait.objects.all()
        number_all = len(traits)
        traits = add_pagination(request, traits)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        tag_fields = {'all':traits,
                      'hiddenid':t.id,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': self.title, 
                      'filetype':'Trait',
                      'refine_search':True}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':traits,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
 
            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            traits = Trait.objects.refine_search(or_and, dico_get)
            
            number_all = len(traits)
            traits = add_pagination(request, traits)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':traits,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
 
            return render(request,self.template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
             
            traits = Trait.objects.refine_search(or_and, dico_get)
            
            number_all = len(traits)
            list_exceptions = ['id','rawdata']
            download_not_empty = False
            if traits:
                download_not_empty = write_trait_csv_from_viewer(traits, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            traits = add_pagination(request, traits)
            
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':traits,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':False,
                           'form':form,
                           'file_form':file_form,
                           'list_attributes':self.list_attributes})  
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        file_form = self.file_form_class(request.POST, request.FILES)
        
        if file_form.is_valid():
            messages.add_message(request, messages.ERROR, "You were in edit mode, please submit your file in management mode.")
            return redirect('trait_management')
        
        update_trait_from_form(request, self.form_class)
        return redirect('trait_management')
    
def update_trait_from_form(request, form_class):
    """
      Function used to update a trait object from the form.
      :param form form_class: experiment form class
    """
    if "hiddenid" in request.POST.keys() :
        t = Trait.objects.get(id=request.POST['hiddenid'])
    else:
        messages.add_message(request, messages.ERROR, "An error as occurred, please try again.")
        return
    
    try:
        old_scale = t.scale_file.path if t.scale_file else None
        form = form_class(request.POST, request.FILES, instance=t)
        
        if form.is_valid():
            to_delete = False
            if old_scale and (type(form.cleaned_data['scale_file']) == InMemoryUploadedFile or form.cleaned_data['scale_file'] == False):
                to_delete = True
            
            form.save()
            
            if to_delete:
                os.remove(old_scale)
        
            # If there is data already linked to the trait, we store the conversions
            if t.scale_file:
                delete_file = False
                with transaction.atomic():
                    calculate_estimated_data_from_scale_file(request, t)
                    
                    #To cancel all the transactions if there is an error message
                    for msg in messages.get_messages(request):
                        messages.add_message(request, msg.level, msg.message)
                        if msg.level == 40:
                            transaction.set_rollback(True)
                            delete_file = True
                
                # If there is an error in the file, we delete it, and delete elaborated 
                # data linked to the former file if there was some
                if delete_file:
                    os.remove(t.scale_file.path)
                    t.scale_file = None
                    t.save()
                    messages.add_message(request, messages.ERROR,"Please complete and resubmit the file by editing the trait.")
                    empty_elaborated_data(request, t)
                
                else:
                    messages.add_message(request, messages.WARNING, "If the database contains existing data for this trait, conversions have been made according to the new scale file.")
            
            # If the file is deleted, we delete elaborated data linked to the file
            else:
                empty_elaborated_data(request, t)

        else:
            raise Exception(' '.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items()]))
#             messages.add_message(request, messages.ERROR, ' '.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ]))
            
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return
    
    messages.add_message(request, messages.SUCCESS, "The trait named {0} has been updated.".format(t.name))
    return 


def get_country_values_from_scale_file(request, scale_file):
    """
      To get a dict containing the values of the scale file 
      and the countries that are present in the file
      :param String scale_file: path of the file
    """
    myfactory = CSVFactoryForTempFiles(scale_file)
    myfactory.find_dialect()
    myfactory.read_file()
    file_dict = myfactory.get_file()
    
    headers = myfactory.get_headers()
    
    if "Common Scale" not in headers or "Label" not in headers:
        messages.add_message(request, messages.ERROR, "An important header is missing of your file. It must contain: 'Common scale' and 'Label'.")
    
    countries = headers.copy()
    
    del countries[0]
    
    country_values = {}
            
    for country in countries:
        country_values[country] = {'NA':'NA'}
    
    for line in file_dict:
        for country in country_values.keys():
            for var in line[country].split('|'):
                country_values[country][var.strip().replace(',','.')] = line["Common Scale"]
                        
    print(country_values)
    
    return countries, country_values
    
def get_common_scale_values(country_values):
    """
      To get common scale values from the file dict object 
      read by the csv factory.
      :param FileDict country_values: file read by the csv factory
    """
    cs_values = []
    for country in country_values.keys():
        for value in country_values[country].values():
            if value not in cs_values:
                cs_values.append(value)
    
    cs_values.sort()
    return cs_values

def calculate_estimated_data_from_scale_file(request, trait):
    """
      Function to calculate or re-calculate data 
      when a new scale file is submitted.
      :param Trait trait: Trait for which there is a scale file
    """
    try:
        countries, country_values = get_country_values_from_scale_file(request, trait.scale_file)
        
        raw_data = RawData.objects.filter(trait=trait, country__in=countries)
        
        for data in raw_data:
            if data.raw_data.strip() in country_values[data.country].keys():
                data.elaborated_data = country_values[data.country][data.raw_data.strip()]
                data.save()
                 
            else:
                send_message_if_not_already_sent(request,"An existing catalogue data stored in the database couldn't be updated according to your scale file. There is no common scale value for the value {0} for the country {1}.".format(data.raw_data, data.country),'error')

    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return


def empty_elaborated_data(request, trait):
    """
      Function used to empty elaborated data stored
      in database when a scale file is removed.
    """
    raw_data = RawData.objects.filter(trait=trait)
    
    display_msg = False
    for data in raw_data:
        if data.elaborated_data:
            display_msg = True
            data.elaborated_data = None
            data.save()
            
    if display_msg:
        messages.add_message(request, messages.WARNING,"Data conversions linked to the file have been deleted")


class ScaleTemplateFileRenderer(View):
    """
      View to render a scale file template.
    """
    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv')
        writer = csv.writer(response,delimiter=';', quoting=csv.QUOTE_ALL)
        
        headers = ['Common Scale', 'Label', 'FR', 'UK', 'DE', '...']
        
        writer.writerow(headers)
        
        writer.writerow(['5', 'Resistant', '9', '', '', ''])
        writer.writerow(['4', '', '8 | 7', '', '', ''])
        writer.writerow(['3', 'Moderately susceptible', '', '', '', ''])
        writer.writerow(['2', '', '', '', '', ''])
        writer.writerow(['1', 'Susceptible', '', '', '', ''])
        
        filename = 'Scale_file_template.csv'
        
        response['Content-Disposition'] = 'attachment; filename=%s' %filename  
        writer = csv.writer(response)
        
        return response


def trait_filerenderer():
    """
      Filerenderer specific for trait, to display 
      the category of the trait.
    """
    response = HttpResponse(content_type='text/csv') 
    writer = csv.writer(response,delimiter=';')
    headers = filerenderer_headers[2][1].copy()
    
    filename = 'Trait.csv'
    database = Trait.objects.all().values_list('name').distinct().order_by('name')

    writer.writerow(headers)
    
    database_list = [i[0] for i in database]
    
    for entry_name in database_list:
        trait = Trait.objects.get(name=entry_name)
        
        line = [trait.name,
                trait.plot_abbreviation,
                trait.unit,
                trait_category[trait.category],
                trait.comments]
        line.append(' | '.join([str(project) for project in trait.projects.filter()]))
        
        writer.writerow(line)
        
    response['Content-Disposition'] = 'attachment; filename=%s' %filename  
    writer = csv.writer(response)
    return response


def write_trait_csv_from_viewer(model_data, title):
    """
      Search result writer specific for trait, to display the category
      of the trait.
      :param dict model_data: data filtered by the search parameters
      :param string title: title of the file
    """
    list_col = filerenderer_headers[2][1].copy()
    
    filename = title.lower().replace(" ","_")
    dialect = Dialect(delimiter = ';')
    
    with open("/tmp/"+filename+".csv", 'w', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, dialect)
        writer.writerow(list_col)
        
        for data in model_data:
            row = [data.name,
                   data.plot_abbreviation,
                   data.unit,
                   trait_category[data.category],
                   data.comments]
            row.append(' | '.join([str(project) for project in data.projects.filter()]))
            
            writer.writerow(row)
            
    download_not_empty = True
    return download_not_empty


class MethodManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the method model. It allows the deletion and insertion of methods based on the Method Model.
      The filters are applied to this view. The user can make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list containing the names of the fields which can be searched with the refine search 
      :var form form_class: the method form
      :var queryset methods: contains all the methods of the database
      :var int number_all: contains the number of methods of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = MethodForm
    template = 'phenotyping/phenotyping_generic.html'
    title = "Method Management"
    list_attributes=['name', 'description']

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        methods = Method.objects.all()
        number_all = len(methods)
        methods = add_pagination(request, methods)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        tag_fields = {'all':methods,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': self.title, 
                      'filetype':'Method',
                      'refine_search':True}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':methods,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'list_attributes':self.list_attributes})
 
            return render(request,self.template,tag_fields)

        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            methods = Method.objects.refine_search(or_and, dico_get)

            number_all = len(methods)
            methods = add_pagination(request, methods)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':methods,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'list_attributes':self.list_attributes})
 
            return render(request,self.template,tag_fields)

        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
             
            methods = Method.objects.refine_search(or_and, dico_get)

            number_all = len(methods)
            list_exceptions = ['id','rawdata']
            download_not_empty = False
            if methods:
                download_not_empty = write_csv_from_viewer(methods, list_exceptions, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            methods = add_pagination(request, methods)
        
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':methods,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           'list_attributes':self.list_attributes})  
        return render(request, self.template, tag_fields)

    def post(self, request, *args, **kwargs):
        request,template,tag_fields = generic_create(Method, self.form_class, request, self.template,{'refine_search':True,
                                                                                                      'list_attributes':self.list_attributes}, self.title)
        return render(request,template,tag_fields)
    

class MethodEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of method model. The user can still make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list containing the names of the fields which can be searched with the refine search 
      :var form form_class: the method form
      :var queryset methods: contains all the methods of the database
      :var int number_all: contains the number of methods of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = MethodForm
    template = 'phenotyping/phenotyping_generic.html'
    title = "Method Edit"
    list_attributes=['name', 'description']

    def get(self, request, *args, **kwargs):
        try:
            m = Method.objects.get(id=kwargs['object_id'])
        except :
            messages.add_message(request, messages.ERROR, "This method doesn't exist in the database.")
            return redirect('method_management')   

        form = self.form_class(initial={'name':m.name,
                                        'description':m.description
                                        })

        methods = Method.objects.all()
        number_all = len(methods)
        methods = add_pagination(request, methods)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        tag_fields = {'all':methods,
                      'hiddenid':m.id,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': self.title, 
                      'filetype':'Method',
                      'refine_search':True}

        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':methods,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'list_attributes':self.list_attributes})

            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            methods = Method.objects.refine_search(or_and, dico_get)

            number_all = len(methods)
            methods = add_pagination(request, methods)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':methods,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'list_attributes':self.list_attributes})

            return render(request,self.template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
             
            methods = Method.objects.refine_search(or_and, dico_get)

            number_all = len(methods)
            list_exceptions = ['id','rawdata']
            download_not_empty = False
            if methods:
                download_not_empty = write_csv_from_viewer(methods, list_exceptions, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            methods = add_pagination(request, methods)
        
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':methods,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':False,
                           'form':form,
                           'list_attributes':self.list_attributes})  
        return render(request, self.template, tag_fields)

    def post(self, request, *args, **kwargs):
        request,template,tag_fields = generic_update(Method, self.form_class, request, self.template,{'refine_search':True,
                                                                                                      'list_attributes':self.list_attributes}, self.title)
        return render(request,template,tag_fields)
    

class CatalogueDataInsert(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View with form to upload a file that contains catalogue data, 
      for accessions. The user must select a country and a year.
      
      :var str title: title displayed on the template
      :var str template: html template for the rendering of the view
      :var form form_class: the file submission form
      :var form formupdate_class: the update file submission form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var file file: file uploaded by the user in the form
      :var dict file_dict: dict version of the file, parsed by the CSVFactory
    """
    title = "Insert catalogue data"
    template = 'phenotyping/catalogue_data_insert.html'
    form_class = CatalogueDataUploadForm
    formupdate_class = CatalogueDataUploadUpdateForm
    
    def get(self, request, *args, **kwargs):
        form = self.form_class()
        formupdate = self.formupdate_class()
        
        tag_fields = {'title':self.title,
                      'filetype':"Catalogue_data",
                      'filetype_update':"Catalogue_data_update",
                      'form':form,
                      'formupdate':formupdate
                      }
        
        return render(request, self.template, tag_fields) 

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        formupdate = self.formupdate_class(request.POST, request.FILES)

        tag_fields = {'title':self.title,
                      'filetype':"Catalogue_data",
                      'filetype_update':"Catalogue_data_update",
                      'form':form,
                      'formupdate':formupdate
                      }

        if form.is_valid() and "trait" not in request.POST.keys():
            process_catalogue_data_file(request)
            return render(request, self.template, tag_fields)
        
        elif formupdate.is_valid():
            process_catalogue_data_update_file(request)
            return render(request, self.template, tag_fields)      
        
        else:
            messages.add_message(request, messages.ERROR, "An error occurred with your file.")
            return render(request, self.template, tag_fields)



def process_headers_catalogue_data_file(header, traits, methods):
    """
      This function return a dict of method and traits names.
      
      :param str header: column header to split
      :param dict traits: dict which contains all traits specified in the headers
      :param dict method: dict which contains all methods specified in the headers
    """
    if '|' in header:
        trait_name, method_name = header.strip().split('|')
    else:
        trait_name = header.strip()
        method_name = ''
    traits[header] = Trait.objects.get(name=trait_name.strip())
    if method_name:
        methods[header] = Method.objects.get(name=method_name.strip())


def add_rawdata_additional_keys(request, line, header, rawdata, methods):
    """
    This function add the Foreign Keys and ManyToMany Keys that could be empty to rawdata object.
    :param dict line: line of the file in a dict format
    :param str header: header of the processed column of the file
    :param RawData rawdata: rawdata object that will be created
    :param dict methods: dict which contains all methods specified in the headers
    """
    if header in methods.keys():
        rawdata.method = methods[header]
    
        
def process_catalogue_data_file(request):
    """
      This function process the submitted file and create the rawdata objects.
      If there is an error an error message is added to the request to be 
      displayed on the template.
    """
    country = request.POST.get('country')
    file = request.FILES['file']
    
    username = request.user.login
    user=User.objects.get(login=username)
    
    myfactory = CSVFactoryForTempFiles(file)
    myfactory.find_dialect()
    myfactory.read_file()
    file_dict = myfactory.get_file()
    
    #Copy the list in headers to not change the original list
    headers = myfactory.get_headers().copy()
    
    if "Accession" in headers and "Year" in headers:
        headers.remove("Accession")
        headers.remove("Year")
    else:
        messages.add_message(request, messages.ERROR, "Wrong file format: Your file doesn't contain the mandatory headers.")
        return
    
    traits = {}
    methods = {}
    scale = {}
    
    for header in headers:
        try:
            process_headers_catalogue_data_file(header, traits, methods)
    
        except Exception as e:
            messages.add_message(request, messages.ERROR, str(e)+" (In the header: "+header+").")

    for trait in traits.values():
        if trait.scale_file:
            countries, country_values = get_country_values_from_scale_file(request, trait.scale_file)
            if country in countries:
                scale[trait] = country_values
            else:
                scale[trait] = None
                messages.add_message(request, messages.WARNING, "The country you selected is not present in the scale conversion file of the trait {0}, the data couldn't be converted.". format(trait))
        else:
            scale[trait] = None
            messages.add_message(request, messages.WARNING, "There is no scale conversion file for the trait {0}, the data couldn't be converted.". format(trait))

    with transaction.atomic():
        for line in file_dict:
            try:
                acc = AccessionLocal.objects.get(accession=Accession.objects.get(name=line["Accession"]), country=country)
                for header in headers:
                    try:
                        with transaction.atomic():
                            if line["Year"] == '':
                                raise ValueError('There is no given year for the accession {0}.'.format(line["Accession"]))
                            
                            line[header] = line[header].strip().replace(',','.')
                            
                            rawdata = RawData.objects.create(raw_data=line[header],
                                                             type=1,
                                                             year=line["Year"],
                                                             country=country,
                                                             accession=acc.accession,
                                                             trait=traits[header],
                                                             last_modification_user=user
                                                             )
        
                            add_rawdata_additional_keys(request, line, header, rawdata, methods)
                            
                            if scale[traits[header]]:
                                if line[header] in scale[traits[header]][country]:
                                    rawdata.elaborated_data = scale[traits[header]][country][line[header]]
                                
                                else:
                                    send_message_if_not_already_sent(request,"A value couldn't be converted according to your scale conversion file. There is no common scale value for the value: {0} (for the trait {1})".format(line[header], traits[header]),'warning')
                            rawdata.save()
                            
                    except Exception as e:
                        if str(e) != "":
                            messages.add_message(request, messages.ERROR, str(e)+" (at line: "+line["Accession"]+", column: "+header+").")
                        continue
            
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+line["Accession"]+").")
                continue
            
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")
    
def process_catalogue_data_update_file(request):
    """
      This function process the submitted file and create the rawdata objects.
      If there is an error an error message is added to the request to be 
      displayed on the template.
    """
    country = request.POST.get('country')
    trait = request.POST.get('trait')
    file = request.FILES['file']

    username = request.user.login
    user=User.objects.get(login=username)
    
    myfactory = CSVFactoryForTempFiles(file)
    myfactory.find_dialect()
    myfactory.read_file()
    file_dict = myfactory.get_file()
    
    #Copy the list in headers to not change the original list
    headers = myfactory.get_headers().copy()
    
    if "Accession" in headers:
        headers.remove("Accession")
    else:
        messages.add_message(request, messages.ERROR, "Wrong file format: Your file doesn't contain the mandatory headers.")
        return
    
    if '' in headers:
        messages.add_message(request, messages.ERROR, "Wrong file format: A year is missing in your file.")
        return
    
    try:
        trait = Trait.objects.get(id=request.POST.get('trait'))
        
        method = ""
        if request.POST.get('method'):
            method = Method.objects.get(id=request.POST.get('method'))
    
    except Exception as e:
        messages.add_message(request, messages.ERROR, "An error occurred, please try again.")
        return
    
    country_values = {}
    
    if trait.scale_file:
        countries, country_values = get_country_values_from_scale_file(request, trait.scale_file)
        if country not in countries:
            messages.add_message(request, messages.WARNING, "The country you selected is not present in the scale conversion file of the trait {0}, the data couldn't be converted.". format(trait))
            country_values = {}
    else:
        messages.add_message(request, messages.WARNING, "There is no scale conversion file for the trait {0}, the data couldn't be converted.". format(trait))
    
    list_objs = []
    with transaction.atomic():
        for line in file_dict:
            try:
                acc = AccessionLocal.objects.get(accession=Accession.objects.get(name=line["Accession"]), country=country)
                for header in headers: 
                    try:
                        with transaction.atomic():
                            if line[header] != '':
                                
                                line[header] = line[header].strip().replace(',','.')
                                
                                r_data = RawData(raw_data=line[header],
                                                 type=1,
                                                 year=header,
                                                 country=country,
                                                 accession=acc.accession,
                                                 trait=trait,
                                                 last_modification_user=user)
                                
                                if method:
                                    r_data.method = method
                                    
                                if country_values:
                                    if line[header] in country_values[country]:
                                        r_data.elaborated_data = country_values[country][line[header]]
                                    else:
                                        send_message_if_not_already_sent(request,"A value couldn't be converted according to your scale conversion file. There is no common scale value for the value: {0}".format(line[header]),'warning')
                
                                list_objs.append(r_data)                        
                    
                    except Exception as e:
                        if str(e) != "":
                            messages.add_message(request, messages.ERROR, str(e)+" (at line: "+line["Accession"]+", column: "+header+").")
                        continue
            
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+line["Accession"]+").")
                continue
            
            
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return
        
        try:
            rawdata = RawData.objects.bulk_create(list_objs)
            
        except Exception as e:
            messages.add_message(request, messages.ERROR, str(e))
            return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")


class CatalogueDataEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View displaying a table of all catalogue grades for an accession on a country.
      :var str title: title displayed on the template
      :var str template: html template for the rendering of the view
    """
    template = 'phenotyping/catalogue_data_edit.html'
    title = "Catalogue data Edit"
    
    def get(self, request, *args, **kwargs):
        formacc = AccessionAutocompleteForm()
        formcountry = DataviewCountryFormList()
        username = request.user.login
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)

        accessions_initial = AccessionLocal.objects.filter(projects__in=projects).values_list('accession').distinct()
        raw_data = RawData.objects.filter(accession__in=accessions_initial)
        accessions = Accession.objects.filter(id__in=raw_data.values_list('accession', flat=True)).distinct()
        
        formacc.fields['accession'].queryset = accessions
        
        n_page = 0
        
        return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formacc':formacc, 'formcountry':formcountry})

    def post(self, request, *args, **kwargs):
        formacc = AccessionAutocompleteForm(request.POST)
        formcountry = DataviewCountryFormList(request.POST)
        
        n_page = int(request.POST['n_page'])
        
        if int(n_page) == 1:
        
            if formacc.is_valid():
                acc_id = request.POST.get('accession')
                country = request.POST.get('country')
            else:
                return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formacc':formacc, 'formcountry':formcountry})
            
            try:
                accession = Accession.objects.get(id=acc_id)
                #accession = AccessionLocal.objects.get(accession=accession, country=country)
            except:
                messages.add_message(request, messages.INFO, "There is no data to display.")
                return render(request, self.template, {'n_page':n_page})
            
            title = "All catalogue data for the accession "+accession.name+" in "+dict(countries)[country]
            
            tag_fields = {'title':title,
                          'n_page':n_page}
            
            
            raw_data = RawData.objects.filter(accession=accession, country=country)
            
            if not raw_data:
                messages.add_message(request, messages.INFO, "There is no data to display.")
                render(request, self.template, {})
            
            years = {}
            traits = {}
            
            for data in raw_data:
                if data.year not in years.keys():
                    years[data.year] = 1
                if data.trait not in traits.keys():
                    traits[data.trait] = 1
            
            list_years = list(years.keys())
            list_years.sort()
            list_traits = Trait.objects.filter(name__in=traits).order_by('name')
            
            fields_label = ["Trait"]+list_years
            data = {}
            
            for trait in list_traits:
                data[trait.name] = []
                for year in list_years:
                    try:
                        rawdata = RawData.objects.get(year=year, country=country, accession=accession, trait=trait)
                        data[trait.name].append(rawdata)
                    except RawData.DoesNotExist:
                        data[trait.name].append('')
                
            
            tag_fields.update({'fields_label':fields_label,
                               'data':data})
            
            return render(request, self.template, tag_fields)


class GetRawDataEdition(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View returning a form to edit a catalogue grade. The form is created 
      and then serialized via the function RemoteForm. In POST the modification
      of the grade is saved
    """
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            try:
                raw_data = RawData.objects.get(id=kwargs['id'])
            except:
                return Http404()
            
            form = RawDataForm(instance=raw_data)
#             print(form)
            remote_form = RemoteForm(form)
            return HttpResponse(json.dumps({'form': json.dumps(remote_form.as_dict())}))
        
        else:
            return redirect('home')
    
    def post(self, request, *args, **kwargs):
        raw_data = RawData.objects.get(id=kwargs['id'])
        form = RawDataForm(copy.copy(request.POST), instance=raw_data)
        
        if form.is_valid():
            if request.POST.get('raw_data').strip() != '':
                updated_rawdata = form.save(commit=False)
                
                username = request.user.login
                user = User.objects.get(login=username)
                
                updated_rawdata.raw_data = request.POST.get('raw_data').replace(',','.')
                updated_rawdata.last_modification_user = user
                
                updated_rawdata.save()
                data = serializers.serialize("json", [updated_rawdata])
                return HttpResponse(json.dumps({'data':data}))
            else:
                remote_form = RemoteForm(form)
                remote_form_dict = remote_form.as_dict()
                return HttpResponse(json.dumps({'form': json.dumps(remote_form_dict), 'message': 'Incorrect value for raw data.'}))
        
        else:
            remote_form = RemoteForm(form)
            remote_form_dict = remote_form.as_dict()
            return HttpResponse(json.dumps({'form': json.dumps(remote_form_dict), 'message': 'There is an error in the form.'}))


class CatalogueDataViewer(LoginRequiredMixin, View):
    template = "dataview/catalogue_data_viewer.html"
    title = "Catalogue data Viewer"
    
    def get(self, request, *args, **kwargs):
        
        formproject = DataviewProjectForm()
        username = request.user.login
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)

        formproject.fields['project'].queryset=projects.order_by('name')

        n_page = 0
        return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formproject':formproject})
    
    
    def post(self, request, *args, **kwargs):
        formproject = DataviewProjectForm(request.POST)
        #Get the actual page number
        n_page = int(request.POST['n_page'])
        
        if int(n_page) == 1:
            if formproject.is_valid():
                projects = request.POST.getlist('project')
                request.session['projects'] = projects
            else:
                return render(request,self.template,{'title':self.title, 'n_page':0, 'formproject':formproject})

            accessions_initial = AccessionLocal.objects.filter(projects__in=request.session['projects']).values_list('accession').distinct()
            traits = Trait.objects.filter(projects__in=request.session['projects']).distinct()
            
            raw_data = RawData.objects.filter(accession__in=accessions_initial, trait__in=traits)
            accessions = Accession.objects.filter(id__in=raw_data.values_list('accession', flat=True)).distinct()
            traits = Trait.objects.filter(id__in=raw_data.values_list('trait', flat=True)).distinct()
            
            formcountry = DataviewCountryFormList()
            
            formacc = DataviewAccessionMultAutocompleteForm()
            formacc.fields['accession'].queryset = accessions
            
            formtrait = DataviewTraitListForm()
            formtrait.fields['trait'].queryset = traits
            
            return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formcountry':formcountry, 'formacc':formacc, 'formtrait':formtrait})

        if int(n_page) == 2:
            
            formcountry = DataviewCountryFormList(request.POST)
            formacc = DataviewAccessionMultAutocompleteForm(request.POST)
            formtrait = DataviewTraitListForm(request.POST)
            
            if formcountry.is_valid() and formacc.is_valid() and formtrait.is_valid():
                country = request.POST.get('country')
                accessions = request.POST.getlist('accession')
                trait = request.POST.get('trait')
                request.session['country'] = country
                request.session['accessions'] = accessions
                request.session['trait'] = trait
            else:
                return render(request,self.template,{'title':self.title, 'n_page':1, 'formcountry':formcountry, 'formacc':formacc, 'formtrait':formtrait})

            trait = Trait.objects.get(id=trait)
            
            final_years_list = []
            final_grades_list = []
            acc_names = []
            
            for accession in accessions:
                list_years = []
                list_grades = []
                try:
                    accession = Accession.objects.get(id=accession)
                    rawdata = RawData.objects.filter(type=1, country=country, accession=accession, trait=trait).order_by('year')
                    
                    for data in rawdata:
                        list_years.append(data.year)
                        if ',' in data.raw_data:
                            raw_data_edit = data.raw_data.replace(',','.')
                            list_grades.append(raw_data_edit)
                        else:
                            list_grades.append(data.raw_data)
    
                    acc_names.append(accession.name)
                    final_years_list.append(list_years)
                    final_grades_list.append(list_grades)
                
                except:
                    messages.add_message(request, messages.INFO, "The accession {0} have no saved informations on this country in the database.".format(Accession.objects.get(id=accession)))
                    
            if acc_names:
                
                plot_abbreviation = '' if trait.plot_abbreviation == None else str(trait.plot_abbreviation)  
                unit = '' if trait.unit == None else " ("+str(trait.unit)+")"
                
                #Setup graph plot
                plot = figure(x_axis_label='Years', y_axis_label=plot_abbreviation+unit, plot_width=960, plot_height=600)
                
                #To start the y axis at 0
                plot.y_range.start = 0
                
                plot.add_layout(Title(text="Scale for "+str(trait.name)+" trait vs. years in "+dict(countries)[country], align="center"), "below")
    
                #Plot line(s)
                for (color, legend, year, grade) in zip(cc.b_glasbey_hv[:len(acc_names)], acc_names, final_years_list, final_grades_list):
                    plot.line(year, grade, color=color, legend_label=legend, line_width=1.5)
                    plot.circle(year, grade, fill_color=color, line_color=color, size=6)
    
                plot.legend.title = "Accession"
                
                #Store components
                script, div = components(plot)
                
                tag_fields = {'title': self.title,
                              'n_page':n_page,
                              'script':script,
                              'div':div
                              }
                
                return render(request, self.template, tag_fields)
            
            else:
                messages.add_message(request, messages.INFO, "There is no data to display.")
                return render(request, self.template, {'n_page':n_page})


class CatalogueDataCardViewer(LoginRequiredMixin, View):
    """
      View to display evolution of grades of one trait for one accession.
    """
    #template = 'dataview/catalogue_data_card_viewer.html'
    
    def get(self, request, *args, **kwargs):
        
        username = request.user.login
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        
        try:
            accession = AccessionLocal.objects.get(id=kwargs['accession_id'])
            trait = Trait.objects.get(name=kwargs['trait_name'])
            
        except (AccessionLocal.DoesNotExist, Trait.DoesNotExist):
            messages.add_message(request, messages.ERROR, "This data doesn't exist in the database or you don't have the rights to visualize it.")
            return Http404()
            #return render(request, self.template, {})
        
            
        rawdata = RawData.objects.filter(type=1, country=accession.country, accession=accession.accession, trait=trait).order_by('year')
        
        list_years = []
        list_grades = []
        
        for data in rawdata:
            list_years.append(data.year)
            if ',' in data.raw_data:
                raw_data_edit = data.raw_data.replace(',','.')
                list_grades.append(raw_data_edit)
            else:
                list_grades.append(data.raw_data)
        
        #Name of the trait on the graph     
        plot_abbreviation = '' if trait.plot_abbreviation == None else str(trait.plot_abbreviation)  
        unit = '' if trait.unit == None else " ("+str(trait.unit)+")"
        
        #Setup graph plot
        plot = figure(x_axis_label='Years', y_axis_label=plot_abbreviation+unit, plot_width=650, plot_height=425)
        
        #To start the y axis at 0
        plot.y_range.start = 0
        
        plot.add_layout(Title(text="Scale for "+str(trait.name)+" trait vs. years in "+dict(countries)[accession.country], align="center"), "below")

        #Plot line(s)
        plot.line(list_years, list_grades, color=cc.b_glasbey_hv[0], legend_label=accession.accession.name, line_width=1.5)
        plot.circle(list_years, list_grades, fill_color=cc.b_glasbey_hv[0], line_color=cc.b_glasbey_hv[0], size=6)

        plot.legend.title = "Accession"
        
        #Store components
        script, div = components(plot)
        
        tag_fields = {'script':script,
                      'div':div
                      }
        
        return HttpResponse(json.dumps({'script': json.dumps(script), 'div': json.dumps(div)}))

