"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from operator import ior, iand

from django.apps import apps
from django.db import models
from django.db.models import Q

from commonfct.constants import trait_category
from team.models import User, Project

class TraitManager(models.Manager):
    """
      Manager for trait model.
    """
    def by_username(self,username):
        """
          Returns only the traits from projects linked to this user 

          :param string username : username

          :var int username_id : user's id
          :var list projects : projects linked to the user
        """
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        return self.filter(projects__in=projects).distinct()
    
    
    def refine_search(self, or_and, dico_get):
        """
          Search in all traits according to some parameters
    
          :param string or_and : link between parameters (or, and)
          :param dict dico_get : data to search for
        """
        # Managers are called in accession.model
        # To avoid crossed imports we use apps to get objects from the model 
        Trait = apps.get_model('phenotyping', 'trait')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
            
        proj=[]
        query=Q()
        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                var_search = str( data_type + "__icontains" )
                
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        query = myoperator(query, Q(projects__isnull=True))
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query = myoperator(query, Q(projects__in=proj))
                
                elif "category" in dico_type_data:
                    list_c = []  
                    for key, category in trait_category.items():
                        if dico_type_data[data_type].lower() in category.lower():
                            list_c.append(key)
                    query = myoperator(query, Q(category__in=list_c))
                else:
                    query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))
                    
        return Trait.objects.filter(query).distinct()



class MethodManager(models.Manager):
    """
      Manager for method model.
    """
    def refine_search(self, or_and, dico_get):
        """
          Search in all methods according to some parameters
    
          :param string or_and : link between parameters (or, and)
          :param dict dico_get : data to search for
        """
        # Managers are called in accession.model
        # To avoid crossed imports we use apps to get objects from the model 
        Method = apps.get_model('phenotyping', 'method')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
            
        proj=[]
        query=Q()
        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                var_search = str( data_type + "__icontains" )
                query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))
                    
        return Method.objects.filter(query).distinct()


