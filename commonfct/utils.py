"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from threading import Thread
from django.apps import apps
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def get_fields(form):
    """
    get_fields est une fonction qui permet à partir d'un formulaire de récupérer le nom
    des champs du Model, ainsi que les labels qui leur sont associés
    """
    fields_name = []
    fields_label = []
    
    for field in form.fields.keys():
        fields_name.append(field)
        fields_label.append(form[field].label)
        
    return fields_name, fields_label

def get_model_from_name(classname):
    """Warning : this function doesn't work if two classes have the same name in two different apps"""
    for model in apps.get_models():
        if model._meta.model_name == classname.lower() :
            return model
    raise Exception("No model found for Class name : {0}".format(classname))


def get_formated_model_name(model_object):
    """
      This function returns the formated name of a model (for example to display in messages).
    """
    model_name = model_object.__class__.__name__
    for model in apps.get_models():
        if model._meta.model_name == model_name.lower():
            return model._meta.verbose_name.capitalize()

def add_pagination(request, objects):
    """ Add pagination to a queryset of objects
        Base of 50 objects by page. """
    if 'nb_per_page' in request.GET.keys():
        nb_per_page = request.GET.get('nb_per_page')
        
        if nb_per_page == 'all':
            nb_per_page = len(objects) if len(objects) != 0 else 50
    else:
        nb_per_page = 50
    
    paginator = Paginator(objects, nb_per_page)
    page = request.GET.get('page')
    try:
        pag_objects = paginator.page(page)
    except PageNotAnInteger:
        # If page number is not an integer then page one
        pag_objects = paginator.page(1)
    except EmptyPage:
        # invalid page number show the last
        pag_objects = paginator.page(paginator.num_pages)
        
    return pag_objects


def clean_session(request, keys):
    """
      Clean the session by deleting infos stored with their keys.
    """
    for key in keys:
        if key in request.session.keys():
            del request.session[key]

def execute_function_in_thread(function, args, kwargs):
    """
      To execute a funtion in a thread (writing of csv files for example)
      :param Function function: Function to execute
      :param list args: List of arguments of the function
      :param dict kwargs: Dict of function kwargs
    """
    t = Thread(target = function, args=args, kwargs=kwargs)
    t.daemon = True
    t.start()
    
    return t.name


def get_dict_key_from_value(my_dict, val):
    """
      To get a dict key from a value.
      :param dict my_dict
      :param str val
    """
    for key, value in my_dict.items():
        if val == value: 
            return key
         
    return "key doesn't exist"

            
def send_message_if_not_already_sent(request, message, message_level):
    """
      Check if a message already exists in the message 
      framework to sent it only once
    """
    levels = {'success':messages.SUCCESS,
              'warning':messages.WARNING,
              'error':messages.ERROR}
    
    messages_content = [msg.message for msg in list(messages.get_messages(request)) if msg.level_tag == message_level]
    if message not in messages_content:
        messages.add_message(request, levels[message_level], message)




        