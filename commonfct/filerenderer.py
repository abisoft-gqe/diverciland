"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import csv

import accession
import landscape
import genotyping
import phenotyping

from django.http import HttpResponse
from django.views import View

from accession.models import AccessionType, Accession, AccessionLocal
from commonfct.constants import filerenderer_headers
from commonfct.utils import get_model_from_name
from genotyping.models import Locus, Experiment
from landscape.models import AnnualArea, VarietyRepartition
from phenotyping.models import Trait

class FileRenderer(View):
    """
      This view return a file containing all the data of a model.
    """
    def get(self, request, *args, **kwargs):
        
        filetype = kwargs['filetype']
        type_id = kwargs['type_id']
        
        Model = get_model_from_name(filetype)
        print(Model)
        if Model == AnnualArea:
            response = landscape.views.annual_area_filerenderer()
            return response
        elif Model == VarietyRepartition:
            response = landscape.views.repartition_filerenderer()
            return response
        elif Model == Accession:
            response = accession.views.accession_filerenderer(type_id)
            return response
        elif Model == Locus:
            response = genotyping.views.locus_filerenderer(type_id)
            return response
        elif Model == Experiment:
            response = genotyping.views.experiment_filerenderer()
            return response
        elif Model == Trait:
            response = phenotyping.views.trait_filerenderer()
            return response
        
        response = HttpResponse(content_type='text/csv') 
        writer = csv.writer(response,delimiter=';')
        headers = get_filerenderer_headers(filetype)
        filename = None
        
        filename = filetype+'.csv'

        database = Model.objects.all().values_list('name').distinct().order_by('name')
        
        if type_id != 0:
            accessiontype = AccessionType.objects.get(id=int(type_id))
            filename = accessiontype.name.replace(" ","_")+'.csv'
            database = Model.objects.filter(type=accessiontype).values_list('name').distinct().order_by('name')
        
        writer.writerow(headers)
        
        fields_names = []
        for csv_field in headers:
            field_name = csv_field.lower() #Les majuscules deviennent des minuscules
            field_name = field_name.replace(" ","_") #underscores à la place des espaces
            fields_names.append(field_name)
            
        database_list = [i[0] for i in database]
        
        for entry_name in database_list:
            if type_id != 0:
                entry_values = Model.objects.filter(name=entry_name, type=accessiontype)
            else:
                entry_values = Model.objects.filter(name=entry_name)
            for entry_value in entry_values:
                line = []
                fetched_values = [getattr(entry_value, field_name) for field_name in fields_names] #On récupere la vaeur de chacun des champs statiques
                fields_types = [Model._meta.get_field(field_name).get_internal_type() for field_name in fields_names]
                for k, field_type in zip(range(len(fetched_values)), fields_types):
                    if field_type == 'ManyToManyField':
                        line.append(' | '.join([str(i) for i in fetched_values[k].filter()]))
                    else:
                        if fetched_values[k]==None:
                            fetched_values[k]=""
                        line.append(fetched_values[k])
                
                writer.writerow(line)

        response['Content-Disposition'] = 'attachment; filename=%s' %filename  
        writer = csv.writer(response)
        return response


class HeadersRenderer(View):
    """
      This view return a file containing the headers of a file type.
    """
    def get(self, request, *args, **kwargs):
        
        filetype = kwargs['filetype']
        
        response = HttpResponse(content_type='text/csv') 
        writer = csv.writer(response,delimiter=';', quoting=csv.QUOTE_ALL)
        headers = get_filerenderer_headers(filetype)
        writer.writerow(headers)
        
        filename = filetype+'_headers.csv'

        response['Content-Disposition'] = 'attachment; filename=%s' %filename  
        writer = csv.writer(response)
        
        return response

def get_filerenderer_headers(filetype):
    """
      This function give the headers for the csv file returned by FileRenderer.
      Headers are specific according to the filetype
      
      :param str filetype: Name of the model
      
      :returns: headers
      :rtype list
    """
    headers = []
    if filetype == "Accession_general":
        headers.extend(filerenderer_headers[0][1])
    elif filetype == "Project":
        headers.extend(filerenderer_headers[1][1])
    elif filetype == "Trait":
        headers.extend(filerenderer_headers[2][1])
    elif filetype == "Method":
        headers.extend(filerenderer_headers[3][1])
    elif filetype == "Catalogue_data":
        headers.extend(filerenderer_headers[4][1])
    elif filetype == "Catalogue_data_update":
        headers.extend(filerenderer_headers[5][1])
    elif filetype == "LandUnit":
        headers.extend(filerenderer_headers[6][1])
    
    # Headers of filerenderers in separated functions
    elif filetype == "AnnualArea":
        headers.extend(filerenderer_headers[7][1])
    elif filetype == "VarietyRepartition":
        headers.extend(filerenderer_headers[8][1])
    elif filetype == "Accession":
        headers.extend(filerenderer_headers[9][1])
    elif filetype == "Experiment":
        headers.extend(filerenderer_headers[10][1])
    elif filetype == "Flat_matrix":
        headers.extend(filerenderer_headers[11][1])
    return headers
    

