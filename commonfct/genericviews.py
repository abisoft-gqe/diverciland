"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
import csv
import json
import threading

from _csv import Dialect
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.db.models.query import QuerySet
from django.forms.models import model_to_dict
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError, Http404,\
                        JsonResponse
from django.shortcuts import render, redirect

from commonfct.utils import get_model_from_name, get_fields, add_pagination, get_formated_model_name
from team.models import Person
from team.templatetags.teamtags import to_class_name

def return_csv(request, filename):
    tmpfile=open('/tmp/'+filename+'.csv','rb')
    response=HttpResponse(tmpfile.read(),content_type="application/vnd.ms-excel")
    response['Content-Disposition']='attachment; filename='+filename+'.csv'
    return response


def write_csv_from_viewer(model_data, list_exceptions, title):
    """
      Function allowing the writing of the csv file from export in CSV in viewer.
      
      :param model model_data: model corresponding to the data required 
      :param list list_exceptions: list containing the model fields which can't be displayed in the csv file
      :param str title: name of the file returned
      
      :var list list_col: headers of the csv file
      :var dict dico: model_data turned into a dictionary
      :var list row: list containing the elements of each row to be written in the csv file
    """
    list_col_init = [f.name for f in model_data[0]._meta.get_fields()]
    for element_to_remove in list_exceptions:
        list_col_init.remove(element_to_remove)
        
    # We capitalize the headers and replace the _ by a space so 
    # that the headers matches the keys to upload the file.
    list_col = [col.replace("_"," ").capitalize() for col in list_col_init]
    filename = title.lower().replace(" ","_")
    dialect = Dialect(delimiter = ';')
    
    with open("/tmp/"+filename+".csv", 'w', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, dialect)
        writer.writerow(list_col)
        for i in model_data:
            #print(i)
            row = []
            dico = process_search_result(model_to_dict(i))
            #print(dico)
            for col in list_col:
                if type(dico[col]) is list:
                    dico[col] = ' | '.join([str(o) for o in dico[col]])
                row.append(dico[col])
            writer.writerow(row) 
    download_not_empty = True
    return download_not_empty


def process_search_result(dico_init):
    dico = {}
    # We capitalize the headers and replace the _ by a space so 
    # that the headers matches the keys to upload the file.
    for key in dico_init.keys():
        dico[key.replace("_"," ").capitalize()] = dico_init[key]
    
    for key, value in dico.items():
        if not dico[key]:
            dico[key]=""
        elif isinstance(dico[key], QuerySet):
            list_qs = []
            for queryset_i in dico[key]:
                try:
                    list_qs.append(queryset_i.name)
                except:
                    list_qs.append(queryset_i.login)
            dico[key]=', '.join(list_qs)
    return dico
    


def generic_create(MyModel, MyModelForm, request, template, adds, page_title):   
    """ The Model class in parameters allows to retrieve all the objects of the Model. 
    The corresponding form is also created. """
    allobjects = MyModel.objects.all()
    form = MyModelForm()
    
    """ A list of the fields that describes the model are retrieved from the form.
    fields_name = fields' names
    fields_label : what should be displayed """

    fields_name, fields_label = get_fields(form) 
    
    allobjects = add_pagination(request, allobjects)
    
    tag_fields = {'all':allobjects,
                  'fields_name':fields_name,
                  'fields_label':fields_label,
                  'title':page_title}
    tag_fields.update(adds)
    
    if request.method == "GET":
        tag_fields.update({'form':form,'creation_mode':True})
        return render(request,template,tag_fields)
    
    elif request.method == "POST":
        #If in creation mode, the form is saved and the object is created
        form = MyModelForm(request.POST, request.FILES)
        if 'error_field' in adds:
            messages.add_message(request, messages.ERROR, adds['error_field'])
            if MyModel.objects.get(name=adds['name']):
                MyModel.objects.get(name=adds['name']).delete()
            tag_fields.update({'form':form,
                               'creation_mode':True,
                               })
            return request,template,tag_fields
        
        if form.is_valid() :
            newobject = form.save()
            #The objects are reloaded to add the new one
            allobjects = MyModel.objects.all()
            number_all = len(allobjects)
            allobjects = add_pagination(request, allobjects)
            tag_fields.update({'all':allobjects,
                               'number_all':number_all,
                               'form':form,
                               'object':newobject,
                               'creation_mode':True})
            messages.add_message(request, messages.SUCCESS, "A new {0} named {1} has been created.".format(get_formated_model_name(newobject), newobject))
            return request,template,tag_fields
        
        else :
            #If the form is not valid, errors will be displayed
            messages.add_message(request, messages.ERROR, ' '.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items()]))
            tag_fields.update({'form':form,
                               'creation_mode':True,
                               })
            return request,template,tag_fields
        

def generic_update(MyModel, MyModelForm, request, template, adds, page_title):
    """ The Model class in parameters allows to retrieve all the objects of the Model. 
    The corresponding form is also created. """
    allobjects = MyModel.objects.all()
    number_all = len(allobjects)
    form = MyModelForm()
    
    """ A list of the fields that describes the model are retrieved from the form.
    fields_name = fields' names
    fields_label : what should be displayed """

    fields_name, fields_label = get_fields(form) 
    
    allobjects = add_pagination(request, allobjects)
    
    tag_fields = {'all':allobjects,
                  'fields_name':fields_name,
                  'fields_label':fields_label,
                  'number_all':number_all,
                  'title':page_title}
    tag_fields.update(adds)
    
    if request.method == "GET":
        tag_fields.update({'form':form,'creation_mode':True})
        return render(request,template,tag_fields)
    
    elif request.method == "POST":
        #The existing object is retrieved and the form fonctionnalities are used to update it
        if 'hiddenid' in request.POST.keys():
            instance = MyModel.objects.get(id=request.POST['hiddenid'])
        else:
            messages.add_message(request, messages.ERROR, "An error as occurred, please try again.")
            return request,template,tag_fields
        
        form = MyModelForm(instance=instance, data=request.POST)
        if form.is_valid():
            form.save()
            tag_fields.update({'form':form,
                               'creation_mode':False,
                               'object':instance,
                               'hiddenid':instance.id})
            messages.add_message(request, messages.SUCCESS, "The {0} named {1} has been updated.".format(get_formated_model_name(instance).capitalize(), instance))
            return request,template,tag_fields
        
        else :
            #An error message is displayed
            messages.add_message(request, messages.ERROR, ' '.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ]))
            tag_fields.update({'form':form,
                               'creation_mode':False,
                               'hiddenid':instance.id})
            return request,template,tag_fields
    

def generic_delete(request, app, model, object_id):
    """
      This function allows the deletion of an object.
      :param string app: name of the model
      :param string model: name of the app
      :param int object_id: id of the object to delete
      
      :returns: redirect on the previous page
    """
    if request.method == "GET":
        try :
            MyModel = get_model_from_name(model)
            #print(MyModel)
            instance = MyModel.objects.get(id=object_id)
            #print(instance)
            instance.delete()
            
        except Exception as e:
            messages.add_message(request, messages.ERROR, e)
            
        else:
            messages.add_message(request, messages.SUCCESS, "The {0} named {1} has been deleted from database.".format(get_formated_model_name(instance), instance))
        
        return redirect(request.META.get('HTTP_REFERER'))


def generic_edit(request, app, model, object_id):
    """
      This function redirect to the edition page of the object.
      :param string app: name of the app
      :param string model: name of he model
      :param int object_id: id of the object to edit
      
      :returns: redirect on the edition page
    """
    if request.method == "GET":
        return redirect('/'+app+'/'+model+'/'+str(object_id)+'/')
        


#def generic_delete_all(request, app, model, id_str):
def generic_delete_all(request, app, model):
    """
      This function allows the deletion of several objects.
      :param string app: name of the model
      :param string model: name of the app
      :param string id_str: ids of the objects to delete in str, delimited by '-'
      
      :returns: redirect on the previous page
    """
#     if request.method == "GET":
#         id_list = id_str.split('-')
#         try :
#             MyModel = get_model_from_name(model)
#             
#             names = []
#             for id in id_list:
#                 instance = MyModel.objects.get(id=id)
#                 names.append(str(instance))
#                 instance.delete()
#                 
#         except Exception as e:
#             messages.add_message(request, messages.ERROR, e)
#             
#         else:
#             if len(names) == 1:
#                 messages.add_message(request, messages.SUCCESS, "The {0} named {1} has been deleted from database.".format(get_formated_model_name(instance), ", ".join(names)))
#             else:
#                 messages.add_message(request, messages.SUCCESS, "The {0}s named {1} have been deleted from database.".format(get_formated_model_name(instance), ", ".join(names)))
#                 
#         return redirect(request.META.get('HTTP_REFERER'))
    
    if request.method == 'POST':
        id_str = request.POST.get('id_str_final')
        id_list = id_str.split('-')
        
        try :
            MyModel = get_model_from_name(model)
            
            names = []
            for id in id_list:
                instance = MyModel.objects.get(id=id)
                names.append(str(instance))
                instance.delete()
                
        except Exception as e:
            messages.add_message(request, messages.ERROR, e)
            
        else:
            if len(names) == 1:
                messages.add_message(request, messages.SUCCESS, "The {0} named {1} has been deleted from database.".format(get_formated_model_name(instance), ", ".join(names)))
            else:
                messages.add_message(request, messages.SUCCESS, "The {0}s named {1} have been deleted from database.".format(get_formated_model_name(instance), ", ".join(names)))
        
        return JsonResponse({
            'url': request.META.get('HTTP_REFERER'),
        })
        

def check_thread_activity(request):
    """
      Check if a thread is active and 
      return True or False
    """
    thread_name = request.POST.get('thread_name')
    status = ''
#     print(thread_name)
    for thread in threading.enumerate():
#         print(thread)
        if thread_name == str(thread.name):
            status = thread.is_alive()
            return JsonResponse({
                'status': status,
            })
    
    if not status:
        return JsonResponse({
                'status': False,
        })

