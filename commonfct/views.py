"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from dal import autocomplete
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import redirect
from django.views import View

from commonfct.forms import GlobalSearchForm 
from team.models import User, Project
from accession.models import AccessionLocal, Accession
from phenotyping.models import Trait
from genotyping.models import Experiment

class GlobalSearch(LoginRequiredMixin, View):
    """
        Redirects to the searched object datacard

        :var form form : search form
        :var int content_type_id : content_type's id
        :var string content_type : object's type
        :var int object_id : object's id
        :var Object obj : searched object
        :var string link : datacard url of this specific object
        :var string modelName : obj's type
        :var string modelNameUrl : obj's type in lower case
    """
    form_class = GlobalSearchForm
    
    def get(self, request, *args, **kwargs):
        return redirect('home')

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)

        if form.is_valid():
            content_type_id = request.POST['search'].split('-')[0]
            content_type = ContentType.objects.get(id=content_type_id)
            object_id = request.POST['search'].split('-')[1]
            
            model_name = content_type.model_class()
            obj=model_name.objects.get(id__iexact=object_id)
            model_name_url = str(model_name).lower().split('.')[2][:-2]
            
            return redirect('data_card',model=model_name_url,object_id=str(obj.id))
            

class GlobalSearchAutocomplete(autocomplete.Select2QuerySetSequenceView):
    
    def get_queryset(self):
        """
        Creates a list of accessions, seedlots and samples corresponding to the search, the user can access every object from this list
    
        :var string username : username
        :var list qs_project : projects linked to this user and containing the searched char
        """
    
        username = self.request.user.login
        username_id=User.objects.get(login=username).id

        
        qs_project = Project.objects.filter(users=username_id)
        filtered_accessions = AccessionLocal.objects.by_username(username).values('accession')
        qs_accession = Accession.objects.filter(id__in=filtered_accessions)
        qs_trait = Trait.objects.by_username(username)
        qs_experiment = Experiment.objects.by_username(username)
            
        if self.q:
            qs_project = qs_project.filter(name__icontains =self.q)
            qs_accession = qs_accession.filter(name__icontains = self.q)
            qs_trait = qs_trait.filter(name__icontains = self.q)
            qs_experiment = qs_experiment.filter(name__icontains = self.q)
        
        return autocomplete.QuerySetSequence(
            qs_project,
            qs_accession,
            qs_trait,
            qs_experiment
        )  
        
            