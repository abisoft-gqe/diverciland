"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import dal_queryset_sequence
import dal_select2_queryset_sequence

from dal import autocomplete
from django import forms
from django.utils.translation import ugettext as _

from accession.models import AccessionLocal, Accession
from team.models import Project
from phenotyping.models import Trait
from genotyping.models import Experiment


class GlobalSearchForm(forms.Form):
    """
    This class represents a form for a global search.
    
    :var queryset search : contains all objects from Projects
    """
    
    search = dal_queryset_sequence.fields.QuerySetSequenceModelField(label=_("Search in database"),
                                                                     queryset=autocomplete.QuerySetSequence(Project.objects.all(),
                                                                                                            Accession.objects.all(),
                                                                                                            Trait.objects.all(),
                                                                                                            Experiment.objects.all(),
                                                                                                            ),
                                                                     #required=False,
                                                                     widget=dal_select2_queryset_sequence.widgets.QuerySetSequenceSelect2(url='search-autocomplete', 
                                                                                                                                          attrs={'style': 'width:100px'})
                                                                     )
    