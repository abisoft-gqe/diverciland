"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

""" Models constants """
ATTRIBUTE_TYPES = ((1,"Short Text"),
                   (2,"Long Text"),
                   (3,"Number"),
                   (4,"URL")
                   )


LANDSCAPE_DATA_TYPES = ((1, 'Cultivated'),
                        (2, 'Multiplied'))

landscape_data_types = {1:'Cultivated',
                        2:'Multiplied'}

LANDSCAPE_UNITS = ((1, 'Hectare'),
                   (2, 'Tonne'),
                   (3, 'Kilogram'),
                   (4, 'Quintal'))

landscape_units = {1:'Hectare',
                   2:'Tonne',
                   3:'Kilogram',
                   4:'Quintal'}

LANDSCAPE_STATUS = ((1,'Raw data'),
                    (2,'Calculated raw data'),
                    (3,'Database computed'),
                    (4,'Automatically generated'))

landscape_status = {1:'Raw data',
                    2:'Calculated raw data',
                    3:'Database computed',
                    4:'Automatically generated'}

EXPERIMENT_CATEGORY = ((1,'Postulation'),
                       (2,'Genotyping'))

experiment_category = {1:'Postulation',
                       2:'Genotyping'}

TRAIT_CATEGORY = ((1,'Continuous'),
                  (2,'Discontinuous'),
                  (3,'Other'))

trait_category = {1:'Continuous',
                  2:'Discontinuous',
                  3:'Other'}



""" View constants """

filerenderer_headers = (('Accession_general', ['Name', 'Type', 'Breeder', 'Country', 'Inscription date', 'Maintainer', 'Synonym', 'Description', 'Projects']),
                        ('Project', ['Name', 'Authors', 'Start date', 'End date', 'Funding', 'Description', 'Users', 'Institutions']),
                        ('Trait', ['Name','Plot abbreviation', 'Unit', 'Category', 'Comments', 'Projects']),
                        ('Method', ['Name','Description']),
                        ('Catalogue_data', ['Accession','Year','Trait | Method','Trait2','...']),
                        ('Catalogue_data_update', ['Accession','Year1','Year2','...']),
                        ('LandUnit', ['Name','Country','NUTS','Code']),
                        ('AnnualArea', ['Country', 'Land unit', 'Year', 'Deployment', 'Unit', 'Description']),
                        ('VarietyRepartition', ['Country', 'Land unit', 'Year', 'Accession', 'Percentage', 'Deployment', 'Unit', 'Tags']),
                        ('Accession', ['Name', 'Breeder', 'Country', 'Inscription date', 'Maintainer', 'Synonym', 'Description', 'Projects']),
                        ('Experiment', ['Name', 'Institution', 'Date', 'Category', 'Comments', 'Projects']),
                        ('Flat_matrix', ['Accession', 'Locus', 'Allele_value', 'Allelic_frequency']),
                        ('Locus', ['Name', 'Comments', 'Projects']))




