"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from django.urls import path

from core_viewer.views import DataCard, AccessionLocalAutocomplete, AccessionAutocomplete, LandUnitAutocomplete

urlpatterns = [
    path('datacard/<str:model>/<int:object_id>/', DataCard.as_view(), name='data_card'),
    path('datacard/<str:model>/<int:object_id>/<str:country>/', DataCard.as_view(), name='data_card'),
    
    path('accessionlocal-autocomplete/', AccessionLocalAutocomplete.as_view(), name='accessionlocal-autocomplete'),
    path('accession-autocomplete/', AccessionAutocomplete.as_view(), name='accession-autocomplete'),
    path('landunit-automplete/', LandUnitAutocomplete.as_view(), name='landunit-autocomplete')
]