"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import datetime

from django import forms

from django_countries.fields import CountryField
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.forms.widgets import NumberInput

from django_countries import countries, Countries
from dal import autocomplete

from accession.models import AccessionLocal, AccessionType, Accession
from team.models import Project, User
from genotyping.models import Experiment, Position, Locus
from landscape.models import LandUnit
from phenotyping.models import Trait

class DataviewProjectForm(forms.Form):
    """
      Dataview form to select the projects for which data will be displayed.
    """
    project = forms.ModelMultipleChoiceField(queryset=Project.objects.all().order_by('name'),
                                             widget=FilteredSelectMultiple("Project", False, attrs={'rows':'100','size':'15'}),
                                             required=True)
    
class DataviewAccessionTypeForm(forms.Form):
    """
      Dataview form to select the types of accessions for which data will be displayed.
    """
    accession_type = forms.ModelMultipleChoiceField(queryset=AccessionType.objects.all().order_by('name'),
                                                    widget=FilteredSelectMultiple("Accession",False,attrs={'rows':'100','size':'7'}),
                                                    required=True)


class DataviewCountryForm(forms.Form):
    """
      Dataview form to select the countries for which data will be displayed.
    """
    country = forms.MultipleChoiceField(
        choices=countries,
        widget=FilteredSelectMultiple("countries", False, attrs={'rows':'100','size':'8'}),
        required=False
    )

class DataviewCountryFormList(forms.Form):
    """
      Dataview form to select a country in a list for which data will be displayed.
    """
    country = CountryField(blank_label='Choose a Country').formfield()


class DataviewAccessionForm(forms.Form):
    """
      Dataview form to select accession(s) in a list for which data will be displayed.
    """
    accession = forms.ModelMultipleChoiceField(queryset=AccessionLocal.objects.all().order_by('name'),
                                               widget=FilteredSelectMultiple("Accession",False,attrs={'rows':'100','size':'13'}),
                                               required=True)
    
class DataviewAccessionFormNotR(forms.Form):
    """
      Dataview form to select accession(s) in a list for which data will be displayed.
    """
    accession = forms.ModelMultipleChoiceField(queryset=AccessionLocal.objects.all().order_by('name'),
                                               widget=FilteredSelectMultiple("Accession",False,attrs={'rows':'100','size':'13'}),
                                               required=False)

class DataviewAccessionMultAutocompleteForm(forms.Form):
    """
      Dataview form to select accessions with their names (multiple autocomplete)
    """
    
#     def __init__(self, *args, **kwargs):
#         if 'qs' in kwargs.keys():
#             qs = kwargs.pop('qs')
#             super(DataviewAccessionMultAutocompleteForm, self).__init__(*args, **kwargs)
#             self.fields['accession'].queryset = qs
#         else:
#             super(DataviewAccessionMultAutocompleteForm, self).__init__(*args, **kwargs)
    
    accession = forms.ModelMultipleChoiceField(required=True,
                                               queryset=Accession.objects.all(),
                                               widget=autocomplete.ModelSelect2Multiple())
    
    
class DataviewAccessionMultAutocompleteFormNotR(forms.Form):
    """
      Dataview form to select accessions with their names (multiple autocomplete)
    """
    accession = forms.ModelMultipleChoiceField(required=False,
                                               queryset=AccessionLocal.objects.all(),
                                               widget=autocomplete.ModelSelect2Multiple())

class DataviewTraitListForm(forms.Form):
    """
      Dataview form to select a trait in a list.
    """
    trait = forms.ModelChoiceField(required=True,
                                   queryset=Trait.objects.all(),
                                   empty_label="Choose a Trait")

countries_list = []
for code, name in list(countries):
    countries_list.append([code, name])
countries_list = [('','Choose a Country')] + countries_list

class DataviewCountryFormListOnChange(forms.Form):
    """
      Dataview form to select a country in a list for which data will be displayed.
    """
    country = forms.ChoiceField(required=True,
                                widget=forms.Select(attrs={'onchange': 'submit();'}),
                                choices=countries_list,
                                label=False)



# Genotyping viewer  
  
class DataviewAccessionListForm(forms.Form):
    """
      Dataview form to select multiple accessions in a list.
    """
    accession = forms.ModelMultipleChoiceField(queryset=Accession.objects.all().order_by('name'),
                                                widget=FilteredSelectMultiple("Accession",False,attrs={'rows':'100','size':'20'}),
                                                required=True)

# class DataviewLocusPositionForm(forms.Form):
#     """
#       Dataview form to get locus according to their positions on the genome.
#     """
#     genome_version = forms.CharField(required = False, label='Enter genome version')
#     chromosome = forms.CharField(required = False, label='Enter chromosome')
#     pos_min = forms.IntegerField(required = False, label='Enter minimal position',
#                                  widget=forms.NumberInput(attrs = {'class': 'form_number_input'}))
#     pos_max = forms.IntegerField(required = False, label='Enter maximal position',
#                                  widget=forms.NumberInput(attrs = {'class': 'form_number_input'}))
    
def get_genome_version_choices():
    genome_versions = Position.objects.values('genome_version').distinct()
    GENOME_V_CHOICES = []
    for version in genome_versions:
        GENOME_V_CHOICES.append([version['genome_version'],version['genome_version']])
    return GENOME_V_CHOICES
    
def get_chromosome_choices():
    chromosomes = Position.objects.values('chromosome').distinct()
    CHROMOSOME_CHOICES = []
    for chr in chromosomes:
        CHROMOSOME_CHOICES.append([chr['chromosome'],chr['chromosome']])
    return CHROMOSOME_CHOICES


class DataviewLocusPositionForm(forms.Form):
    """
      Dataview form to get locus according to their positions on the genome.
    """
    
    genome_version = forms.ChoiceField(required=False,
                                       choices=get_genome_version_choices,
                                       label='Select genome version')
    chromosome = forms.ChoiceField(required=False,
                                   choices=get_chromosome_choices,
                                   label='Select chromosome')
    
    pos_min = forms.IntegerField(required = False, label='Enter minimal position',
                                 widget=forms.NumberInput(attrs = {'class': 'form_number_input'}))
    pos_max = forms.IntegerField(required = False, label='Enter maximal position',
                                 widget=forms.NumberInput(attrs = {'class': 'form_number_input'}))


class DataviewLocusFileUploadForm(forms.Form):
    """
      Dataview form to upload a file containing a list of loci.
    """
    file = forms.FileField(label='Select the file to upload', required=False)
    locus = forms.ModelMultipleChoiceField(required=False,
                                           queryset=Locus.objects.all(),
                                           widget=autocomplete.ModelSelect2Multiple())

class DataviewExperimentListForm(forms.Form):
    """
      Dataview form to select multiple experiments in a list.
    """
    experiment = forms.ModelMultipleChoiceField(queryset=Experiment.objects.all().order_by('name'),
                                                widget=FilteredSelectMultiple("Experiment",False,attrs={'rows':'100','size':'20'}),
                                                required=True)

class DataviewDisplayNAData(forms.Form):
    """
      Dataview form to check if user wants to see NA data
    """
    na_data = forms.BooleanField(required=False, label="Display NA data")
