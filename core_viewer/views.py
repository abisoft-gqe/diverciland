"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from dal import autocomplete
from django_countries import countries as dj_countries

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views import View

from commonfct.utils import get_model_from_name
from core_viewer.forms import DataviewCountryFormListOnChange
from team.models import User, Project
from accession.models import AccessionLocal, Accession
from landscape.models import LandUnit
from phenotyping.models import RawData, Trait
from genotyping.models import Experiment

class DataCard(LoginRequiredMixin, View):
    """
      This view show a card with the data of one object.
      
      :var str template: html template for the rendering of the view
    """
    template="dataview/data_card.html"
    
    def get(self, request, *args, **kwargs):
        username = request.user.login
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        
        model_name = kwargs['model']
        object_id = kwargs['object_id']
        
        Model = get_model_from_name(kwargs['model'])
        
#         print(model_name)
        
        try :
            if model_name == "project":
                object = Model.objects.get(id=object_id)
                
            elif model_name == "accessionlocal" or model_name == "accession":
                model_name = "accession"
                if 'country' in kwargs.keys():
                    object = AccessionLocal.objects.filter(accession=object_id, country=kwargs['country'], projects__in=projects).first()
                else:
                    object = AccessionLocal.objects.filter(accession=object_id, projects__in=projects).first()

            else:
                object = Model.objects.filter(id=object_id, projects__in=projects).first()
                
        except :
            messages.add_message(request, messages.ERROR, "This "+model_name+" doesn't exist in the database or you don't have the rights to visualize this data.")
            return render(request, self.template)
        
        list_exceptions = get_list_exceptions(model_name)
        field_names = [f.name for f in object._meta.get_fields()]
        
        for element_to_remove in list_exceptions:
            field_names.remove(element_to_remove)
        
        name = object.accession.name if model_name == 'accession' else object.name 
        
        tag_fields = {'model':model_name.capitalize(),  
                      'name':name,
                      'object':object,
                      'field_names':field_names
                      }
        
        add_additional_data(model_name, object, tag_fields, projects)
        
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        model_name = kwargs['model']
        object_id = kwargs['object_id']

        if model_name == 'accession':
            formcountry = DataviewCountryFormListOnChange(request.POST)
            
            if formcountry.is_valid():
                country = request.POST.get('country')
            else:
                messages.add_message(request, messages.ERROR, "There is no informations for this accession on the selected country.")
                return redirect('data_card',model=model_name, object_id=object_id)
    
            acc_name = Accession.objects.get(id=object_id)
            
            return redirect('data_card',model=model_name, object_id=acc_name.id, country=country)

        else:
            messages.add_message(request, messages.ERROR, "This "+model_name+" doesn't exist in the database or you don't have the rights to visualize this data.")
            return render(request, self.template)

def get_list_exceptions(model_name):
    """
      Return the list of the fields that will not be showed in the datacard for each models.
      
      :param str model_name: the name of the model
      
      :returns: list_exceptions
      :rtype list
    """
    list_exceptions = []
    
    if model_name == 'project':
        list_exceptions = ['id', 'accessionlocal', 'trait', 'locus', 'experiment']
    elif model_name == 'accession':
        list_exceptions = ['id', 'accession', 'attributes_values']
    elif model_name == 'trait':
        list_exceptions = ['id','rawdata','scale_file']
    elif model_name == 'experiment':
        list_exceptions = ['id', 'phenotyping_file', 'genotyping_file', 'isolat_file', 'genotypingvalue']
    
    return list_exceptions

def add_additional_data(model_name, object, tag_fields, projects):
    """
      Get additional data for specific models.
    """
    if model_name == 'accession':
        legend = "Catalogue data"
        table_labels = ["Trait","Grade(s)"]
        acc_name_fields = ['name','type','breeder']
        user_traits = Trait.objects.filter(projects__in=projects)
        rawdata = RawData.objects.filter(accession=object.accession, country=object.country, type=1, trait__in=user_traits)
        traits = {}
        for data in rawdata:
            get_catalogue_data(data, traits)
        
        attribute_values = {}
        get_attribute_data(object, attribute_values)
        
        formcountry = DataviewCountryFormListOnChange()
        countries = AccessionLocal.objects.filter(accession=object.accession).values_list('country', flat=True)
        
        countries_list = []
        for country in countries:
            if AccessionLocal.objects.filter(accession=object.accession, country=country, projects__in=projects):
                countries_list.append([country, dict(dj_countries)[country]])
        
        formcountry.fields['country'].choices = countries_list
        formcountry.fields['country'].initial = object.country
        
        tag_fields.update({'acc_name_field_names':acc_name_fields,
                           'formcountry':formcountry,
                           'legend':legend,
                           'table_labels':table_labels,
                           'additional_data':traits,
                           'attributes':attribute_values})
        
        
    if model_name == 'project':
        legend = "Experiments linked to this project"
        table_labels = ["Name"]
        experiments = Experiment.objects.filter(projects=object)
        
        tag_fields.update({'legend':legend,
                           'table_labels':table_labels,
                           'additional_data':experiments})
        
    return tag_fields

def get_catalogue_data(data, traits):
    """
      This function add the rawdata and traits linked to the accession in a dict.
      :param RawData data: rawdata linked to the accession
      :param dict traits: dict which contains the oldest and most recent rawdata for each trait
    """
    if str(data.trait) not in traits.keys():
        traits[str(data.trait)] = data
    else:
        if type(traits[str(data.trait)]) is list:
            list_data = traits[str(data.trait)]
            if data.year < list_data[1].year:
                list_data[1] = data
            elif data.year > list_data[0].year:
                list_data[0] = data
        else:
            list_data = [traits[str(data.trait)], traits[str(data.trait)]]
            if data.year < traits[str(data.trait)].year:
                list_data[1] = data
            elif data.year > traits[str(data.trait)].year:
                list_data[0] = data
        traits[str(data.trait)] = list_data
        
def get_attribute_data(object, attribute_values):
    """
      Get the attributes values of the object in a dict.
    """
    accession_type = object.accession.type
    attributes = accession_type.accessiontypeattribute_set.all()
    
    values = object.attributes_values
    
    for attribute in attributes:
        if str(attribute.id) in values.keys():
                attribute_values[attribute.attribute_name] = values[str(attribute.id)]

class AccessionLocalAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return AccessionLocal.objects.none()
        
        username = None
        if self.request.user.is_authenticated:
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
        qs = AccessionLocal.objects.filter(projects__in=projects)
        
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs

class AccessionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Accession.objects.none()
        
        username = None
        if self.request.user.is_authenticated:
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
        accessions = AccessionLocal.objects.filter(projects__in=projects).values_list('accession',flat=True)
        
        if accessions:
            qs = Accession.objects.filter(id__in=list(accessions))
            
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs

class LandUnitAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return LandUnit.objects.none()
    
        qs = LandUnit.objects.all()
        
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs



