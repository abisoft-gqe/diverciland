"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import ast
import branca
import csv
import datetime
import colorcet as cc
import folium
import json
import operator
import os
import pandas as pd
import time
import threading

from django_countries import countries
from bokeh.embed import components, file_html
from bokeh.io import export_svgs, export_png
from bokeh.layouts import row, column
from bokeh.models import Title, Legend, LegendItem, Range1d, Div
from bokeh.plotting import figure, output_file, show
from bokeh.palettes import magma, linear_palette, Magma256
from bokeh.transform import cumsum
from bokeh.resources import INLINE
from _csv import Dialect
from django_countries import countries as dc_countries
from django.db import transaction
from django.db.models import Count, Max, Min, Q
from django.conf import settings
from django.contrib import messages
from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views import View
from math import pi
from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.firefox.options import Options

from accession.models import AccessionLocal, Accession
from commonfct.constants import landscape_data_types, landscape_units, filerenderer_headers
from commonfct.genericviews import generic_create, generic_update, write_csv_from_viewer
from commonfct.utils import get_fields, add_pagination, execute_function_in_thread, get_dict_key_from_value,\
                            send_message_if_not_already_sent
from core_viewer.forms import DataviewProjectForm, DataviewAccessionFormNotR, DataviewCountryFormList,\
                              DataviewAccessionMultAutocompleteFormNotR, DataviewTraitListForm, DataviewDisplayNAData
from csv_io.views import CSVFactoryForTempFiles
from landscape.models import LandUnit, AnnualArea, VarietyRepartition, Tag, LandscapeRawData
from landscape.forms import LandUnitForm, LandUnitUploadForm, AnnualAreaForm, AnnualAreaUploadForm,\
                            VarietyRepartitionForm, VarietyRepartitionUploadForm, DataviewCountryFormListOnChange,\
                            DataviewYearForm, DataviewLandUnitListForm, DataviewYearListForm, DataviewAccessionLifeCycleForm,\
                            DataviewMapYearListForm, TagForm
from landscape.signals import multiplications_updated, estimated_data_submitted, repartition_data_submitted
from phenotyping.models import Trait, RawData
from phenotyping.views import get_country_values_from_scale_file, get_common_scale_values
from team.models import Project, User
from team.views import LoginRequiredMixin, UserIsAdminMixin


class LandUnitManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the land unit model. It allows the deletion and insertion of land units based on the Trait Model.
      The filters are applied to this view. The user can make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list containing the names of the fields which can be searched with the refine search 
      :var form form_class: the land unit form
      :var queryset land_units: contains all the land_units of the database
      :var int number_all: contains the number of land_units of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = LandUnitForm
    file_form_class = LandUnitUploadForm
    template = 'landscape/landscape_generic.html'
    title = "Land Unit Management"
    list_attributes = ['name', 'country', 'nuts', 'code']
    
    def get(self, request, *args, **kwargs):
        form = self.form_class()
        file_form = self.file_form_class()
        land_units = LandUnit.objects.all()
        number_all = len(land_units)
        land_units = add_pagination(request,land_units)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        tag_fields = {'all':land_units,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': self.title, 
                      'filetype':'LandUnit',
                      'refine_search':True}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':land_units,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            land_units = LandUnit.objects.refine_search(or_and, dico_get)
            
            number_all = len(land_units)
            land_units = add_pagination(request,land_units)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':land_units,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
             
            land_units = LandUnit.objects.refine_search(or_and, dico_get)
            
            number_all = len(land_units)
            list_exceptions = ['id','annualarea']
            download_not_empty = False
            if land_units:
                download_not_empty = write_csv_from_viewer(land_units, list_exceptions, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            land_units = add_pagination(request,land_units)
        
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':land_units,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           'file_form':file_form,
                           'list_attributes':self.list_attributes})  
        return render(request, self.template, tag_fields)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        file_form = self.file_form_class(request.POST, request.FILES)
        
        land_units = LandUnit.objects.all()
        number_all = len(land_units)
        land_units = add_pagination(request,land_units)
        
        names, labels = get_fields(form)

        tag_fields = {'all':land_units,
                      'number_all':number_all,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': self.title,
                      'form':form,
                      'file_form':file_form,
                      'creation_mode':True,
                      'refine_search':True,
                      'filetype':'LandUnit',
                      'list_attributes':self.list_attributes}
        
        if form.is_valid():
            request,template,tag_fields = generic_create(LandUnit, self.form_class, request, self.template,{'file_form':file_form,
                                                                                                            'refine_search':True,
                                                                                                            'filetype':'LandUnit',
                                                                                                            'list_attributes':self.list_attributes}, self.title)
            return render(request,template,tag_fields)

        elif file_form.is_valid():
            file = request.FILES['file']
            myfactory = CSVFactoryForTempFiles(file)
            myfactory.find_dialect()
            myfactory.read_file()
            file_dict = myfactory.get_file()

            headers_list = filerenderer_headers[6][1]
            
            missing = myfactory.check_headers_conformity(headers_list)
            
            if missing != []:
                messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                                     ". The required headers for this file are: "+format(', '.join(headers_list))+".")
                return redirect('land_unit_management')

            if request.POST.get('submit'):
                create_land_unit_from_file(request, file_dict)
            elif request.POST.get('update'):
                update_land_unit_from_file(request, file_dict)
                
            land_units = LandUnit.objects.all()
            number_all = len(land_units)
            tag_fields.update({'all':land_units,
                               'number_all':number_all})
            
            return render(request, self.template, tag_fields)
    
        else:
            messages.add_message(request, messages.ERROR, "An error occurred.")
            return render(request, self.template, tag_fields)



def create_land_unit_from_file(request, file_dict):
    """
      Function used to create land_units from a file uploaded by the user.
      :param dict file_dict: file on dict format returned by the CSVFactory
    """
    with transaction.atomic():        
        for line in file_dict:
            try:
                with transaction.atomic():
                    if "NUTS" not in line.keys():
                        line["NUTS"] = line["Nuts"]
                        
                    if "NUTS" in line["NUTS"]:
                        nuts = line["NUTS"][4:].strip()
                    else:
                        nuts = line["NUTS"]
                        
                    if line["Country"] not in dict(dc_countries).keys():
                        raise Exception("The country code is unknown, please check https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes to find the ISO 3166 country's code.")
                        
                    land_unit = LandUnit.objects.create(name=line["Name"],
                                                        country=line["Country"],
                                                        nuts=nuts,
                                                        code=line["Code"]
                                                        )
    
                    land_unit.save()

            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+", "+str(line["Country"])+").")
                    continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")


def update_land_unit_from_file(request, file_dict):
    """
      Function used to update land_units from a file uploaded by the user.
      :param dict file_dict: file on dict format returned by the CSVFactory
    """
    with transaction.atomic():    
        for line in file_dict:
            try:
                with transaction.atomic():    
                    land_unit = LandUnit.objects.get(name=line["Name"], country=line["Country"])
                    
                    if "NUTS" not in line.keys():
                        line["NUTS"] = line["Nuts"]
                    
                    if "NUTS" in line["NUTS"]:
                        nuts = line["NUTS"][4:].strip()
                    else:
                        nuts = line["NUTS"]
                    
                    land_unit.nuts = nuts
                    land_unit.code = line["Code"]
                    
                    land_unit.save()
        
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+", "+str(line["Country"])+").")
                    continue
        
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully uploaded and the land units have been updated.")


class LandUnitEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of land unit model. The user can make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list containing the names of the fields which can be searched with the refine search 
      :var form form_class: the land unit form
      :var queryset land_units: contains all the land_units of the database
      :var int number_all: contains the number of land_units of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = LandUnitForm
    file_form_class = LandUnitUploadForm
    template = 'landscape/landscape_generic.html'
    title = "Land Unit Edit"
    list_attributes = ['name', 'country', 'nuts', 'code']
    
    def get(self, request, *args, **kwargs):
        try:
            lu = LandUnit.objects.get(id=kwargs['object_id'])
        except:
            messages.add_message(request, messages.ERROR, "This land unit doesn't exist in the database.")
            return redirect('land_unit_management')

        form = self.form_class(initial={'name':lu.name,
                                        'country':lu.country,
                                        'nuts':lu.nuts,
                                        'code':lu.code
                                        })
        
        file_form = self.file_form_class()
        
        land_units = LandUnit.objects.all()
        number_all = len(land_units)
        land_units = add_pagination(request,land_units)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        tag_fields = {'all':land_units,
                      'hiddenid':lu.id,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': self.title, 
                      'filetype':'LandUnit',
                      'refine_search':True}

        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':land_units,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
 
            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            land_units = LandUnit.objects.refine_search(or_and, dico_get)

            number_all = len(land_units)
            land_units = add_pagination(request,land_units)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':land_units,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
 
            return render(request,self.template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
             
            land_units = LandUnit.objects.refine_search(or_and, dico_get)

            number_all = len(land_units)
            list_exceptions = ['id','annualarea']
            download_not_empty = False
            if land_units:
                download_not_empty = write_csv_from_viewer(land_units, list_exceptions, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            land_units = add_pagination(request,land_units)
            
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':land_units,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':False,
                           'form':form,
                           'file_form':file_form,
                           'list_attributes':self.list_attributes})  
        return render(request, self.template, tag_fields)

    def post(self, request, *args, **kwargs):
        file_form = self.file_form_class(request.POST, request.FILES)
        
        if file_form.is_valid():
            messages.add_message(request, messages.ERROR, "You were in edit mode, please submit your file in management mode.")
            return redirect('land_unit_management')
        
        request,template,tag_fields = generic_update(LandUnit, self.form_class, request, self.template,{'refine_search':True,
                                                                                                        'list_attributes':self.list_attributes}, self.title)
        return redirect('land_unit_management')

class TagManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the creation of Tags.
      
      :var form form_class: the tag form
      :var str template: html template for the rendering of the view
      :var queryset tags: contains all the tags from the database
      :var int number_all: contains the number of tags of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
    """
    form_class = TagForm
    template = 'landscape/landscape_generic.html'
    
    def get(self, request, *args, **kwargs):
        form = self.form_class()
        tags = Tag.objects.all()
        number_all = len(tags)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        
        tag_fields = {'all':tags,
                      'number_all':number_all,
                      'nb_per_page':nb_per_page,
                      'creation_mode':True,
                      'form':form,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "Tag Management"}
        
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        request,template,tag_fields = generic_create(Tag, self.form_class, request, self.template,{}, "Tag Management")
        return render(request,template,tag_fields)

class TagEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of Tags.
      
      :var form form_class: the tag form
      :var str template: html template for the rendering of the view
      :var queryset tags: contains all the tags from the database
      :var int number_all: contains the number of tags of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
    """
    form_class = TagForm
    template = 'landscape/landscape_generic.html'
    
    def get(self, request, *args, **kwargs):
        try:
            t = Tag.objects.get(id=kwargs['object_id'])
        except:
            messages.add_message(request, messages.ERROR, "This tag doesn't exist in the database.")
            return redirect('tag_management')
        
        form = self.form_class(initial={'name':t.name,
                                        'description':t.description
                                        })
        
        tags = Tag.objects.all()
        number_all = len(tags)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        
        tag_fields = {'all':tags,
                      'hiddenid':t.id,
                      'number_all':number_all,
                      'nb_per_page':nb_per_page,
                      'creation_mode':False,
                      'form':form,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': "Tag Edit"}
        
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        request,template,tag_fields = generic_update(Tag, self.form_class, request, self.template,{}, "Tag Management")
        return redirect('tag_management')

def set_annual_area_tags(annual_areas, tags):
    """
      Get tags values for each annual area to write in the table.
    """
    for area in annual_areas:
        for tag in tags:
            try:
                raw_data = LandscapeRawData.objects.get(annual_area=area, tag=tag)
                setattr(area, tag.name.lower().replace(' ','_'), raw_data.value)
            except:
                setattr(area, tag.name.lower().replace(' ','_'), '')
        

class AnnualAreaManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the annual area model. It allows the deletion and insertion of annual areas based on the AnnualArea Model.
      The filters are applied to this view. The user can make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list containing the names of the fields which can be searched with the refine search 
      :var form form_class: the annual area form
      :var queryset annual_areas: contains all the annual areas of the database
      :var int number_all: contains the number of annual areas of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = AnnualAreaForm
    file_form_class = AnnualAreaUploadForm
    template = 'landscape/landscape_annual_area.html'
    title = "Annual Area Management"
    list_attributes = ['land unit', 'year', 'data type', 'deployment', 'unit', 'description']
    
    def get(self, request, *args, **kwargs):
        annual_areas = AnnualArea.objects.all()
        
        raw_data = LandscapeRawData.objects.filter(annual_area__in=annual_areas)
        tags_id = raw_data.values("tag").distinct()
        tags = Tag.objects.filter(id__in=tags_id)
        set_annual_area_tags(annual_areas, tags)
        
        form = self.form_class(tags=tags)
        form.fields['select_tags'].queryset = Tag.objects.exclude(id__in=tags)
        file_form = self.file_form_class()
        
        number_all = len(annual_areas)
        annual_areas = add_pagination(request, annual_areas)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        names.remove('select_tags')
        labels.remove('Select tags')
        names.insert(2,'data_type')
        labels.insert(2,'Data type')
        
        for tag in tags:
            in_list = tag.name.lower() in self.list_attributes
            if in_list == False:
                self.list_attributes.append(tag.name.lower())
            
        
        tag_fields = {'all':annual_areas,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': self.title,
                      'filetype':'AnnualArea',
                      'refine_search':True}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':annual_areas,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            annual_areas = AnnualArea.objects.refine_search(or_and, dico_get)
            set_annual_area_tags(annual_areas, tags)
            
            number_all = len(annual_areas)
            annual_areas = add_pagination(request, annual_areas)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':annual_areas,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
             
            annual_areas = AnnualArea.objects.refine_search(or_and, dico_get)
            set_annual_area_tags(annual_areas, tags)
            
            number_all = len(annual_areas)
            list_exceptions = ['id','varietyrepartition']
            download_not_empty = False
            thread_name = ''
            if annual_areas:
                thread_name = execute_function_in_thread(write_annual_area_csv_from_viewer, [annual_areas, "Result search"], {})
                download_not_empty = True
            
            tag_fields.update({'download_not_empty':download_not_empty,
                               'thread_name':thread_name})
            
            
            
            annual_areas = add_pagination(request, annual_areas)
        
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':annual_areas,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           'file_form':file_form,
                           'list_attributes':self.list_attributes})  
        return render(request, self.template, tag_fields)

    def post(self, request, *args, **kwargs):
        annual_areas = AnnualArea.objects.all()
        
        raw_data = LandscapeRawData.objects.filter(annual_area__in=annual_areas)
        tags_id = raw_data.values("tag").distinct()
        tags = Tag.objects.filter(id__in=tags_id)
        set_annual_area_tags(annual_areas, tags)
        
        form = self.form_class(tags=tags, data=request.POST)
        form.fields['select_tags'].queryset = Tag.objects.exclude(id__in=tags)
        file_form = self.file_form_class(request.POST, request.FILES)
        
        number_all = len(annual_areas)
        annual_areas = add_pagination(request, annual_areas)
        
        names, labels = get_fields(form)
        names.remove('select_tags')
        labels.remove('Select tags')
        names.insert(2,'data_type')
        labels.insert(2,'Data type')
        
        for tag in tags:
            in_list = tag.name.lower() in self.list_attributes
            if in_list == False:
                self.list_attributes.append(tag.name.lower())
            
        
        tag_fields = {'all':annual_areas,
                      'number_all':number_all,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': self.title,
                      'form':form,
                      'file_form':file_form,
                      'creation_mode':True,
                      'refine_search':True,
                      'filetype':'AnnualArea',
                      'list_attributes':self.list_attributes}
        
        if form.is_valid():
#             annual_area = form.save()
            try:
                with transaction.atomic():
                    annual_area = AnnualArea.objects.create(land_unit=form.cleaned_data['land_unit'],
                                                            year=form.cleaned_data['year'],
                                                            data_type=1,
                                                            deployment=form.cleaned_data['deployment'],
                                                            unit=form.cleaned_data['unit'],
                                                            description=form.cleaned_data['description'])
            except Exception as e:
                messages.add_message(request, messages.ERROR, str(e))
                return redirect('annual_area_management')
            
            create_or_update_tag_data(request, annual_area)

            annual_areas = AnnualArea.objects.all()
         
            raw_data = LandscapeRawData.objects.filter(annual_area__in=annual_areas)
            tags_id = raw_data.values("tag").distinct()
            tags = Tag.objects.filter(id__in=tags_id)
            set_annual_area_tags(annual_areas, tags)
            
            form = self.form_class(tags=tags)
            form.fields['select_tags'].queryset = Tag.objects.exclude(id__in=tags)
            file_form = self.file_form_class()
            
            number_all = len(annual_areas)
            annual_areas = add_pagination(request, annual_areas)
            
            names, labels = get_fields(form)
            names.remove('select_tags')
            labels.remove('Select tags')
            names.insert(2,'data_type')
            labels.insert(2,'Data type')
             
            tag_fields.update({'all':annual_areas,
                               'number_all':number_all,
                               'form':form,
                               'file_form':file_form,
                               'object':annual_area,
                               'creation_mode':True,
                               'fields_name':names,
                               'fields_label':labels})
             
            messages.add_message(request, messages.SUCCESS, "A new Annual Area named {0} has been created.".format(annual_area))
             
            return render(request, self.template, tag_fields)
            
        
        elif file_form.is_valid():
            file = request.FILES['file']
            myfactory = CSVFactoryForTempFiles(file)
            myfactory.find_dialect()
            myfactory.read_file()
            file_dict = myfactory.get_file()
            
            headers_list = filerenderer_headers[7][1]
            
            missing = myfactory.check_headers_conformity(headers_list)
            
            if missing != []:
                messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                                     ". The required headers for this file are: "+format(', '.join(headers_list))+".")
                return redirect('annual_area_management')
            
            if request.POST.get('submit'):
                create_annual_area_from_file(request, file_dict)
            elif request.POST.get('update'):
                update_annual_area_from_file(request, file_dict)
            
            annual_areas = AnnualArea.objects.all()
            raw_data = LandscapeRawData.objects.filter(annual_area__in=annual_areas)
            tags_id = raw_data.values("tag").distinct()
            tags = Tag.objects.filter(id__in=tags_id)
            set_annual_area_tags(annual_areas, tags)
            
            form = self.form_class(tags=tags)
            form.fields['select_tags'].queryset = Tag.objects.exclude(id__in=tags)
            
            number_all = len(annual_areas)
            annual_areas = add_pagination(request, annual_areas)
            
            names, labels = get_fields(form)
            names.remove('select_tags')
            labels.remove('Select tags')
            names.insert(2,'data_type')
            labels.insert(2,'Data type')
            
            tag_fields.update({'all':annual_areas,
                               'number_all':number_all,
                               'form':form,
                               'fields_name':names,
                               'fields_label':labels})
        
            return render(request, self.template, tag_fields)
        
        else:
            messages.add_message(request, messages.ERROR, "An error occurred.")
            return render(request, self.template, tag_fields)
    
def create_or_update_tag_data(request, annual_area):
    """
      Function used to create or update landscape raw data objects according 
      to the tags values when an annual area object is created or updated.
    """
    tags = Tag.objects.all()
    for tag in tags:
        if tag.name.lower().replace(' ','_') in request.POST.keys():
            value = request.POST[tag.name.lower().replace(' ','_')]
            raw_data_q = LandscapeRawData.objects.filter(tag=tag, annual_area=annual_area).distinct()
            
            if raw_data_q and value != '':
                l_raw_data = raw_data_q[0]
                l_raw_data.value = value
                l_raw_data.save()
            
            elif raw_data_q and value == '':
                l_raw_data = raw_data_q[0]
                l_raw_data.delete()
                
            elif not raw_data_q and value != '':
                l_raw_data = LandscapeRawData.objects.create(value=value, tag=tag, annual_area=annual_area)
                l_raw_data.save()


def create_annual_area_from_file(request, file_dict):
    """
      Function used to create annual areas from a file uploaded by the user.
      :param dict file_dict: file on dict format returned by the CSVFactory
    """
    with transaction.atomic():       
        for line in file_dict:
            try:
                with transaction.atomic(): 
                    land_unit = LandUnit.objects.get(name=line["Land unit"], country=line["Country"])
                    
                    key_unit = get_dict_key_from_value(landscape_units, line["Unit"]) 
                    if type(key_unit) != int:
                        raise Exception("This unit doesn't exists, please use a unit of the following list: {0}.".format(', '.join(landscape_units.values())))
                    
                    deployment = float(line["Deployment"].replace(',','.')) if line["Deployment"] else None
                    
                    annual_area = AnnualArea.objects.create(land_unit=land_unit,
                                                            year=line["Year"],
                                                            data_type=1,
                                                            deployment=deployment,
                                                            unit=key_unit,
                                                            description=line["Description"]
                                                            )
                    
                    annual_area.save()
                    
                    headers_list = filerenderer_headers[7][1]
                    unknown_tag = []
                    
                    tag_names = Tag.objects.all().values_list("name", flat=True)

                    for field in line.keys():
                        if field in tag_names:
                            if line[field] != '':
                                tag = Tag.objects.get(name=field)
                                l_raw_data = LandscapeRawData.objects.create(value=line[field], tag=tag, annual_area=annual_area)
                                l_raw_data.save()
                        else:
                            if field not in headers_list:
                                unknown_tag.append(field)
                                
                    if unknown_tag != []:
                        send_message_if_not_already_sent(request, "One or several headers in your file are not registered as tags: "+format(', '.join(unknown_tag))\
                                                         +". No informations have been saved for these columns.", 'warning')
                
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Land unit"])+" ("+str(line["Country"])+"), "+str(line["Year"])+").")
                    continue
        
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")


def update_annual_area_from_file(request, file_dict):
    """
      Function used to update annual areas from a file uploaded by the user.
      :param dict file_dict: file on dict format returned by the CSVFactory
    """
    with transaction.atomic():        
        for line in file_dict:
            try:
                with transaction.atomic():
                    land_unit = LandUnit.objects.get(name=line["Land unit"], country=line["Country"])
                    
                    key_unit = get_dict_key_from_value(landscape_units, line["Unit"]) 
                    if type(key_unit) != int:
                        raise Exception("This unit doesn't exists, please use a unit of the following list: {0}.".format(', '.join(landscape_units.values())))
                    
                    deployment = float(line["Deployment"].replace(',','.')) if line["Deployment"] else None
                    
                    annual_area = AnnualArea.objects.get(land_unit=land_unit, year=line["Year"], data_type=1)
                    
                    if annual_area.deployment != float(deployment):
                        data_update = update_variety_repartitions(request, annual_area, deployment)
                        if data_update == 1:
                            raise Exception("An error occurred.")
                    
                    annual_area.deployment = deployment
                    annual_area.unit = key_unit
                    annual_area.description=line['Description']
    
                    annual_area.save()
                    
                    headers_list = filerenderer_headers[7][1]
                    unknown_tag = []
                    
                    tag_names = Tag.objects.all().values_list("name", flat=True)
                    
                    for field in line.keys():
                        if field in tag_names:
                            if line[field] != '':
                                tag = Tag.objects.get(name=field)
    
                                raw_data, result = LandscapeRawData.objects.get_or_create(tag=tag, annual_area=annual_area, defaults={'value':line[field],
                                                                                                                                      'tag':tag,
                                                                                                                                      'annual_area':annual_area})
                                if result == False:
                                    raw_data.value=line[field]
                                      
                                raw_data.save()
                                
                        else:
                            if field not in headers_list:
                                unknown_tag.append(field)
                                
                    if unknown_tag != []:
                        send_message_if_not_already_sent(request, "One or several headers in your file are not registered as tags: "+format(', '.join(unknown_tag))\
                                                         +". No informations have been saved for these columns.", 'warning')
    
            except LandscapeRawData.DoesNotExist as e:
                messages.add_message(request, messages.ERROR, "There is no value for the Tag {0} for the Annual Area: {1}. It can't be updated.".format(tag, annual_area))
                continue
            
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Land unit"])+" ("+str(line["Country"])+"), "+str(line["Year"])+").")
                    continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return
    
        messages.add_message(request, messages.WARNING, "The variety repartition objects linked to these annual areas were updated according to the new deployment value (for the values computed by the database).")
        messages.add_message(request, messages.WARNING, "Don't forget to update your raw data if needed.")             
    
    messages.add_message(request, messages.SUCCESS, "The file has been successfully uploaded and the annual areas have been updated.")

def update_variety_repartitions(request, annual_area, new_value):
    """
      Function to update variety repartition objects values when 
      an annual area deployment value is updated by the user
    """
    repartitions = VarietyRepartition.objects.filter(annual_area=annual_area, data_type=annual_area.data_type, perc_status__in=[1,3], depl_status__in=[1,3])
    
    total_depl = 0
    for rep in repartitions:
        if rep.depl_status == 1:
            total_depl += rep.deployment

    if total_depl == 0 or total_depl <= new_value:
        for rep in repartitions:
            if (rep.perc_status == 1 and rep.depl_status == 3) or (rep.perc_status == 3 and rep.depl_status == 3):
                rep.deployment = (rep.percentage / 100.0) * float(new_value)
                rep.save()
             
            elif rep.perc_status == 3 and rep.depl_status == 1:
                rep.percentage = (rep.deployment / float(new_value)) * 100.0
                rep.save()
                
                if rep.data_type == 2:
                    update_estimated_variety_repartitions(rep, annual_area)

        return 0
    
    elif total_depl > new_value:
        messages.add_message(request, messages.ERROR, "The new annual area deployment value is inferior to the sum of the variety repartition raw deployments values associated to this annual area. Please first update the variety repartitions deployment values.")
        return 1

def update_estimated_variety_repartitions(rep, annual_area):
    """
      Function to update percentage and deployment value when a deployment value 
      is updated for an annual area and the percentage of a multiplied 
      data is modified. We report the modification on the estimated data 
      percentage and deployment.
    """
    next_year = annual_area.year + 1
    next_annual_areas = AnnualArea.objects.filter(land_unit=annual_area.land_unit, year=next_year, data_type="1").distinct()
    
    if next_annual_areas :
        next_annual_area = next_annual_areas[0]
        
        if next_annual_area.deployment != 0:
            estimated_reps = VarietyRepartition.objects.filter(annual_area=next_annual_area, accession=rep.accession, data_type="1", perc_status=3, depl_status=3).distinct()
            
            if estimated_reps:
                estimated_rep = estimated_reps[0]
                new_deployment = (rep.percentage / 100.0) * next_annual_area.deployment
                new_percentage = rep.percentage
                
                estimated_rep.percentage = new_percentage
                estimated_rep.deployment = new_deployment
                
                estimated_rep.save()


class AnnualAreaEdit(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the edition of annual area model. The user can make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list containing the names of the fields which can be searched with the refine search 
      :var form form_class: the annual area form
      :var queryset annual_areas: contains all the annual areas of the database
      :var int number_all: contains the number of annual areas of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = AnnualAreaForm
    file_form_class = AnnualAreaUploadForm
    template = 'landscape/landscape_annual_area.html'
    title = "Annual Area Edit"
    list_attributes = ['land unit', 'year', 'data type', 'deployment', 'unit', 'description']
    
    def get(self, request, *args, **kwargs):
        try:
            a = AnnualArea.objects.get(id=kwargs['object_id'])
        except:
            messages.add_message(request, messages.ERROR, "This annual area doesn't exist in the database.")
            return redirect('annual_area_management')
        
        annual_areas = AnnualArea.objects.all()
        
        raw_data = LandscapeRawData.objects.filter(annual_area__in=annual_areas)
        tags_id = raw_data.values("tag").distinct()
        tags = Tag.objects.filter(id__in=tags_id)
        set_annual_area_tags(annual_areas, tags)
        
        data = {'land_unit':a.land_unit.id,
                'year':a.year,
                'deployment':a.deployment,
                'unit':a.unit,
                'description':a.description}
        
        for tag in tags:
            raw_data = LandscapeRawData.objects.filter(annual_area=a, tag=tag).distinct()
            if raw_data:
                data[tag.name.lower().replace(' ','_')] = raw_data[0].value
        
        
        form = self.form_class(tags=tags, data=data, instance=a)
        form.fields['select_tags'].queryset = Tag.objects.exclude(id__in=tags)
        
        file_form = self.file_form_class()
        
        
        number_all = len(annual_areas)
        annual_areas = add_pagination(request, annual_areas)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        names.remove('select_tags')
        labels.remove('Select tags')
        names.insert(2,'data_type')
        labels.insert(2,'Data type')
        
        for tag in tags:
            in_list = tag.name.lower() in self.list_attributes
            if in_list == False:
                self.list_attributes.append(tag.name.lower())
        
        tag_fields = {'all':annual_areas,
                      'hiddenid':a.id,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': self.title,
                      'filetype':'AnnualArea',
                      'refine_search':True}

        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':annual_areas,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)

        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            annual_areas = AnnualArea.objects.refine_search(or_and, dico_get)
            set_annual_area_tags(annual_areas, tags)

            number_all = len(annual_areas)
            annual_areas = add_pagination(request, annual_areas)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':annual_areas,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)

        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
             
            annual_areas = AnnualArea.objects.refine_search(or_and, dico_get)
            set_annual_area_tags(annual_areas, tags)

            number_all = len(annual_areas)
            list_exceptions = ['id','varietyrepartition']
            download_not_empty = False
            thread_name = ''
            if annual_areas:
                thread_name = execute_function_in_thread(write_annual_area_csv_from_viewer, [annual_areas, "Result search"], {})
                download_not_empty = True
                
            tag_fields.update({'download_not_empty':download_not_empty,
                               'thread_name':thread_name})
            
            annual_areas = add_pagination(request, annual_areas)
        
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':annual_areas,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':False,
                           'form':form,
                           'file_form':file_form,
                           'list_attributes':self.list_attributes})  
        return render(request, self.template, tag_fields)

    def post(self, request, *args, **kwargs):
        annual_areas = AnnualArea.objects.all()
        
        raw_data = LandscapeRawData.objects.filter(annual_area__in=annual_areas)
        tags_id = raw_data.values("tag").distinct()
        tags = Tag.objects.filter(id__in=tags_id)
        
        file_form = self.file_form_class(request.POST, request.FILES)

        if file_form.is_valid():
            messages.add_message(request, messages.ERROR, "You were in edit mode, please submit your file in management mode.")
            return redirect('annual_area_management')
        
        if 'hiddenid' in request.POST.keys():
            aa = AnnualArea.objects.get(id=request.POST['hiddenid'])
        else:
            messages.add_message(request, messages.ERROR, "An error as occurred, please try again.")
            return redirect('annual_area_management')
        
        form = AnnualAreaForm(tags=tags, instance=aa, data=request.POST)
        aa_depl = aa.deployment
        
        if form.is_valid():
            deployment = form.cleaned_data['deployment']
            if aa_depl != float(deployment):
                data_update = update_variety_repartitions(request, aa, deployment)
                if data_update == 1:
                    return redirect('annual_area_management')
                else:
                    messages.add_message(request, messages.WARNING, "The variety repartition objects linked to this annual area were updated according to the new deployment value (for the values computed by the database).")
                    messages.add_message(request, messages.WARNING, "Don't forget to update your raw data if needed.")
            
            form.save()
            create_or_update_tag_data(request, aa)
            messages.add_message(request, messages.SUCCESS, "The Annual Area named {0} has been updated.".format(aa))
        
        else :
            #An error message is displayed
            messages.add_message(request, messages.ERROR, ' '.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ]))
        
        return redirect('annual_area_management')


def annual_area_filerenderer():
    """
      Filerenderer specific for annual area, to display in separated columns 
      the land unit and the country.
    """
    response = HttpResponse(content_type='text/csv') 
    writer = csv.writer(response,delimiter=';')
    headers = filerenderer_headers[7][1].copy()
    headers.insert(3, 'Data type')
    filename = 'AnnualArea.csv'
    
    raw_data = LandscapeRawData.objects.filter(annual_area__in=AnnualArea.objects.all())
    tags_id = raw_data.values("tag").distinct()
    tags = Tag.objects.filter(id__in=tags_id)
    
    for tag in tags:
        headers.append(tag.name)
    
    database = AnnualArea.objects.all().values_list('land_unit').distinct().order_by('land_unit')
    writer.writerow(headers)
    
    database_list = [i[0] for i in database]
        
    for entry_name in database_list:
        annual_areas = AnnualArea.objects.filter(land_unit=entry_name)
        for annual_area in annual_areas:
            line = [annual_area.land_unit.country, 
                    annual_area.land_unit.name, 
                    annual_area.year,
                    landscape_data_types[annual_area.data_type],
                    annual_area.deployment,
                    landscape_units[annual_area.unit],
                    annual_area.description]
        
            for tag in tags:
                try:
                    raw_data = LandscapeRawData.objects.get(tag=tag, annual_area=annual_area)
                    line.append(raw_data)
                except:
                    line.append('')
        
            writer.writerow(line)
    
    response['Content-Disposition'] = 'attachment; filename=%s' %filename  
    writer = csv.writer(response)
    return response

class AnnualAreaHeadersRenderer(View):
    """
      This view return a file containing the headers of an accession 
      file with the selected tags.
    """
    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv') 
        writer = csv.writer(response,delimiter=';', quoting=csv.QUOTE_ALL)
        
        headers = filerenderer_headers[7][1].copy()
        
        raw_data = LandscapeRawData.objects.filter(annual_area__in=AnnualArea.objects.all())
        tags_id = raw_data.values("tag").distinct()
        tags = Tag.objects.filter(id__in=tags_id)
        
        for tag in tags:
            headers.append(tag.name)
            
        writer.writerow(headers)
        
        filename = 'AnnualArea_headers.csv'
        
        response['Content-Disposition'] = 'attachment; filename=%s' %filename  
        writer = csv.writer(response)
        
        return response

def write_annual_area_csv_from_viewer(model_data, title):
    """
      Search results writer specific for annual area, to display in separated columns 
      the land unit and country.
      :param dict model_data: data filtered by the search parameters
      :param string title: title of the file
    """
    list_col = filerenderer_headers[7][1].copy()
    list_col.insert(3, 'Data type')
    
    raw_data = LandscapeRawData.objects.filter(annual_area__in=model_data)
    tags_id = raw_data.values("tag").distinct()
    tags = Tag.objects.filter(id__in=tags_id)
    
    for tag in tags:
        list_col.append(tag.name)
    
    filename = title.lower().replace(" ","_")
    dialect = Dialect(delimiter = ';')
    
    with open("/tmp/"+filename+".csv", 'w', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, dialect)
        writer.writerow(list_col)
        
        for data in model_data:
            row = [data.land_unit.country, 
                   data.land_unit.name, 
                   data.year,
                   landscape_data_types[data.data_type],
                   data.deployment,
                   landscape_units[data.unit],
                   data.description]
            
            for tag in tags:
                try:
                    raw_data = LandscapeRawData.objects.get(tag=tag, annual_area=data)
                    row.append(raw_data)
                except:
                    row.append('')
            
            writer.writerow(row)
    download_not_empty = True
    return download_not_empty
        
        
        
class VarietyRepartitionManagement(LoginRequiredMixin, UserIsAdminMixin, View):
    """
      View managing the variety repartition model. It allows the deletion and insertion of variety repartitions based on the VarietyRepartition Model.
      The filters are applied to this view. The user can make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var str template: html template for the rendering of the view
      :var list list_attributes: list containing the names of the fields which can be searched with the refine search 
      :var form form_class: the annual area form
      :var queryset repartitions: contains all the variety repartitions of the database
      :var int number_all: contains the number of variety repartitions of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list names: variable containing the field names of the model
      :var list labels: variable containing the field names of the form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var dict dico_get: dictionary containing the data obtained with the GET request method
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form_class = VarietyRepartitionForm
    file_form_class = VarietyRepartitionUploadForm
    template = 'landscape/landscape_repartition.html'
    title = "Variety Repartition Management"
    list_attributes = ['land unit', 'year', 'accession', 'data type', 'percentage', 'deployment', 'unit', 'tags']
    
    def get(self, request, *args, **kwargs):
        form = self.form_class()
        file_form = self.file_form_class()
        repartitions = VarietyRepartition.objects.all()
        number_all = len(repartitions)
        repartitions = add_pagination(request,repartitions)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)

        tag_fields = {'all':repartitions,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': self.title,
                      'filetype':'VarietyRepartition',
                      'refine_search':True}
    
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':repartitions,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)

        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            repartitions = VarietyRepartition.objects.refine_search(or_and, dico_get)
            
            number_all = len(repartitions)
            repartitions = add_pagination(request,repartitions)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':repartitions,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'file_form':file_form,
                               'list_attributes':self.list_attributes})
            
            return render(request,self.template,tag_fields)

        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
             
            repartitions = VarietyRepartition.objects.refine_search(or_and, dico_get)
            
            number_all = len(repartitions)
            list_exceptions = ['id']
            download_not_empty = False
            thread_name = ''
            if repartitions:
                thread_name = execute_function_in_thread(write_repartition_csv_from_viewer, [repartitions, "Result search"], {})
                download_not_empty = True
                
            tag_fields.update({'download_not_empty':download_not_empty,
                               'thread_name':thread_name})
            
            repartitions = add_pagination(request,repartitions)

        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':repartitions,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           'file_form':file_form,
                           'list_attributes':self.list_attributes})  
        return render(request, self.template, tag_fields)
    
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        file_form = self.file_form_class(request.POST, request.FILES)
        
        repartitions = VarietyRepartition.objects.all()
        number_all = len(repartitions)
        repartitions = add_pagination(request,repartitions)
        
        names, labels = get_fields(form)
        
        tag_fields = {'all':repartitions,
                      'number_all':number_all,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': self.title,
                      'form':form,
                      'file_form':file_form,
                      'creation_mode':True,
                      'refine_search':True,
                      'filetype':'Trait',
                      'list_attributes':self.list_attributes}
        
        if form.is_valid():
            
            create_repartition_from_form(request, form)
            
            repartitions = VarietyRepartition.objects.all()
            number_all = len(repartitions)
            repartitions = add_pagination(request,repartitions)
            tag_fields.update({'all':repartitions,
                               'number_all':number_all})

            return render(request, self.template, tag_fields)
        
        elif file_form.is_valid():
            file = request.FILES['file']
            data_type = file_form.cleaned_data['file_data_type']
            is_calculated = file_form.cleaned_data['is_calculated']
            calculate_data = file_form.cleaned_data['calculate_data']
            myfactory = CSVFactoryForTempFiles(file)
            myfactory.find_dialect()
            myfactory.read_file()
            file_dict = myfactory.get_file()
            
            headers_list = filerenderer_headers[8][1]
            
            missing = myfactory.check_headers_conformity(headers_list)
            
            if missing != []:
                messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                                     ". The required headers for this file are: "+format(', '.join(headers_list))+".")
                return redirect('repartition_management')

            if request.POST.get('submit'):
                create_repartition_from_file(request, file_dict, data_type, is_calculated, calculate_data)
            elif request.POST.get('update'):
                update_repartition_from_file(request, file_dict, data_type, is_calculated, calculate_data)
                
            repartitions = VarietyRepartition.objects.all()
            number_all = len(repartitions)
            repartitions = add_pagination(request,repartitions)
            tag_fields.update({'all':repartitions,
                               'number_all':number_all})

            return render(request, self.template, tag_fields)
        
        else:
            messages.add_message(request, messages.ERROR, "An error occurred.")
            return render(request, self.template, tag_fields)


def create_repartition_from_form(request, form):
    """
      Function used to create a variety repartition object from the form.
      :param form form: form submitted by the user
    """
    land_unit = form.cleaned_data['land_unit']
    year = form.cleaned_data['year']
    data_type = form.cleaned_data['data_type']
    created = False
    
    try:
        annual_area = AnnualArea.objects.get(land_unit=land_unit, year=year, data_type=data_type)
    except:
        if data_type == '2':
            annual_area = AnnualArea.objects.create(land_unit=land_unit, year=year, data_type=data_type, deployment=0, unit=1)
            annual_area.save()
            created = True
        else:
            messages.add_message(request, messages.ERROR, "There is no Annual Area corresponding to your query in the database.")
            return 
    
    accession = form.cleaned_data['accession']
    
    try:
        accession = AccessionLocal.objects.get(accession=Accession.objects.get(name=accession), country=annual_area.land_unit.country)
    except:
        messages.add_message(request, messages.ERROR, "There is no Accession's informations for the accession {0} in the country {1} in the database.".format(accession, annual_area.land_unit.country))
        return 
    
    percentage = form.cleaned_data['percentage']
    deployment = form.cleaned_data['deployment']
    unit = form.cleaned_data['unit']
    tags = Tag.objects.filter(id__in=form.cleaned_data['tags'])
    
    try:
        with transaction.atomic():
            percentage, deployment, perc_status, depl_status = check_percentage_and_deployment_for_form(request, annual_area, percentage, deployment, data_type)
    
            repartition = VarietyRepartition.objects.create(annual_area=annual_area,
                                                            accession=accession.accession,
                                                            data_type=data_type,
                                                            percentage=percentage,
                                                            perc_status=perc_status,
                                                            deployment=deployment,
                                                            depl_status=depl_status,
                                                            unit=unit)
            
            repartition.tags.add(*list(tags))
            
            repartition.save()
        
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        if created:
            annual_area.delete()
        return 
    
    if data_type == '2':
        annual_area.unit = unit
        annual_area.save()
        multiplications_updated.send(sender=VarietyRepartition, aa_ids=[annual_area.id])
        
    repartition_data_submitted.send(sender=VarietyRepartition, aa_ids=[annual_area.id])
        
    messages.add_message(request, messages.SUCCESS, "A new Variety repartition for the accession {0} on land unit {1} in {2} has been created.".format(accession.accession.name, str(land_unit), year))


def check_percentage_and_deployment_for_form(request, annual_area, percentage, deployment, data_type):
    """
      Function used to check if the percentages and areas submitted by the users are correlated, or to compute the missing one.
      (area from the percentage and the annual area or conversely).
      :param int data_type: represent the type of the data uploaded: cultivated or multiplied
      :param AnnualArea annual_area: used to obtain the annual area for the land unit
      :param float percentage: percentage (existing or not) to check or to compute from other data
      :param float area: area (existing or not) to check or to compute from other data
    """
    
    db_area = float(annual_area.deployment) if annual_area.deployment else 0
    
    if data_type == '1':
        if db_area == 0:
            messages.add_message(request, messages.ERROR, "There is no annual area deployment value for this land unit: "+str(annual_area.land_unit))
            raise Exception('An error as occurred.')
        
        if deployment and deployment > db_area:
            messages.add_message(request, messages.ERROR, "The submitted area can't be superior to the annual area deployment for the land unit: "+format(db_area, '.2f'))
            raise Exception('An error as occurred.')
        
        if percentage and not deployment:
            deployment = (percentage / 100.0) * db_area
            perc_status = 1
            depl_status = 3
            
        elif deployment and not percentage:
            percentage = (deployment / db_area) * 100.0
            perc_status = 3
            depl_status = 1
        
        elif percentage and deployment:
            perc_status = 1
            depl_status = 1
    
    #If multiplied data, the annual area value is not checked because equal to 0 and 
    #percentage or deployment are not calculated
    else:
        if percentage and deployment:
            perc_status = 1
            depl_status = 1
            
        elif percentage and not deployment:
            messages.add_message(request, messages.ERROR, "For multiplied data you must submit a deployment value and not only a percentage value.")
            raise Exception('An error as occurred.')
            
        elif deployment and not percentage:
            percentage = 0
            perc_status = 3
            depl_status = 1
    
    return percentage, deployment, perc_status, depl_status


def create_repartition_from_file(request, file_dict, data_type, is_calculated, calculate_data):
    """
      Function used to create variety repartitions from a file uploaded by the user.
      :param dict file_dict: file on dict format returned by the CSVFactory
      :param int data_type: represents the type of the data uploaded: cultivated or multiplied
      :param boolean is_calculated: if the data is calculated raw data
      :param boolean calculate_data: if the user wants to calculate year n+1 cultivated data
    """

    i = 0
    aa_ids = []
    rep_ids = []
    with transaction.atomic():     
        for line in file_dict:
            try:
                with transaction.atomic(): 
                    if i%1000 == 0:
                        print(i)
                    
                    land_unit = LandUnit.objects.get(name=line["Land unit"], country=line["Country"])
                    if data_type == '1':
                        result = None
                        annual_area = AnnualArea.objects.get(land_unit=land_unit, year=line["Year"], data_type=data_type)
                    else:
                        annual_area, result = AnnualArea.objects.get_or_create(land_unit=land_unit, year=line["Year"], data_type=data_type, defaults={'land_unit':land_unit,
                                                                                                                                                      'year':line["Year"],
                                                                                                                                                      'data_type':data_type,
                                                                                                                                                      'deployment':0,
                                                                                                                                                      'unit':1})
                        annual_area.save()
                    
                    
                    accession = AccessionLocal.objects.get(accession=Accession.objects.get(name=line["Accession"]), country=line["Country"])
                    
                    percentage = float(line['Percentage'].replace(',','.')) if line['Percentage'] else None
                    deployment = float(line['Deployment'].replace(',','.')) if line['Deployment'] else None
                    
                    if annual_area.id not in aa_ids:
                        aa_ids.append(annual_area.id)
                    
                    if line["Unit"] != "":
                        key_unit = get_dict_key_from_value(landscape_units, line["Unit"]) 
                        if type(key_unit) != int:
                            raise Exception("This unit doesn't exists, please use a unit of the following list: {0}.".format(', '.join(landscape_units.values())))
                    else:
                        if data_type == '1':
                            key_unit = annual_area.unit
                            send_message_if_not_already_sent(request, 'The unit was not specified for, at least, one line in your file, the unit of the annual area has been used.', 'warning')
                        else:
                            raise Exception("For multiplied data you must specify a unit of the following list: {0}.".format(', '.join(landscape_units.values())))
    
                    if data_type == '2':
                        annual_area.unit = key_unit
                        annual_area.save()
    
                    if annual_area.unit != key_unit:
                        raise Exception("The unit of the corresponding annual area is different of the unit in the file, please correct your file. ({0}, {1})".format(landscape_units[annual_area.unit], line["Unit"]))
        
                    if deployment != None and deployment == 0.0:
                        messages.add_message(request, messages.WARNING, "A deployment value equal to 0 was submitted at line: "+line["Land unit"]+" ("+line["Country"]+"), "+line["Accession"]+", "+line["Year"]+".")
                        continue
        
                    if percentage != None and percentage == 0.0:
                        messages.add_message(request, messages.WARNING, "A percentage value equal to 0 was submitted at line: "+line["Land unit"]+" ("+line["Country"]+"), "+line["Accession"]+", "+line["Year"]+".")
                        continue
        
                    percentage, deployment, perc_status, depl_status, rep_ids = process_create_variety_repartition_data(request, data_type, annual_area, percentage, deployment, is_calculated, calculate_data, accession, rep_ids)
                    
                    tags = []
                    if line["Tags"] != '':
                        for tag_name in line["Tags"].split(','):
                            tags.append(Tag.objects.get(name=tag_name.strip()))
                    
                    repartition = VarietyRepartition.objects.create(annual_area=annual_area,
                                                                    accession=accession.accession,
                                                                    data_type=data_type,
                                                                    percentage=percentage,
                                                                    perc_status=perc_status,
                                                                    deployment=deployment,
                                                                    depl_status=depl_status,
                                                                    unit=key_unit)
                    
                    repartition.tags.add(*list(tags))
        
                    repartition.save()
                    i+=1
             
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+line["Land unit"]+" ("+line["Country"]+"), "+line["Accession"]+", "+line["Year"]+").")
                    if result:
                        aa_ids.remove(annual_area.id)
                        annual_area.delete()
                    i+=1
                    continue
 
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    if data_type == '2':
        multiplications_updated.send(sender=VarietyRepartition, aa_ids=aa_ids)
        if rep_ids:
            estimated_data_submitted.send(sender=VarietyRepartition, rep_ids=rep_ids)
            
    repartition_data_submitted.send(sender=VarietyRepartition, aa_ids=aa_ids)

    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")
    
    

def process_create_variety_repartition_data(request, data_type, annual_area, percentage, deployment, is_calculated, calculate_data, accession, rep_ids):
    """
      Check and process data according to submitted parameters in order to create variety repartition data.
      :param int data_type: represents the type of the data uploaded: cultivated or multiplied
      :param AnnualArea annual_area: annual area to link to the data
      :param float percentage: value of the percentage
      :param float deployment: value of the deployment
      :param boolean is_calculated: if the data is calculated raw data
      :param boolean calculate_data: if the user wants to calculate year n+1 cultivated data
      :param Accession accession: accession to link to the data
    """
    db_area = float(annual_area.deployment) if annual_area.deployment else 0
    
    if data_type == '1':
        if db_area == 0:
            messages.add_message(request, messages.ERROR, "There is no annual area deployment value for this land unit: "+str(annual_area.land_unit))
            raise Exception('An error as occurred.')
    
        if deployment and deployment > db_area:
            messages.add_message(request, messages.ERROR, "The submitted area can't be superior to the annual area deployment for the land unit: "+format(db_area, '.2f'))
            raise Exception('An error as occurred.')
        
        if deployment != None and deployment < 0.0:
            messages.add_message(request, messages.ERROR, "The submitted area can't be less than 0: "+format(deployment, '.2f'))
            raise Exception('An error as occurred.')
    
        if percentage != None and (percentage < 0.0 or percentage > 100.0):
            messages.add_message(request, messages.ERROR, "The submitted value for percentage is not valid: "+format(percentage, '.2f'))
            raise Exception('An error as occurred.')
    
    
        #For Cultivated data or Multiplied data
        #Data not already calculated
        if not is_calculated:
            if percentage and not deployment:
                deployment = (percentage / 100.0) * db_area
                perc_status = 1
                depl_status = 3
                
            elif deployment and not percentage:
                percentage = (deployment / db_area) * 100.0
                perc_status = 3
                depl_status = 1
            
            elif percentage and deployment:
                perc_status = 1
                depl_status = 1
        
        #Data already calculated, we don't calculate missing data (except percentages)
        else:
            #If the deployments are submitted we calculate the missing percentages for the dataview
            if deployment and not percentage:
                percentage = (deployment / db_area) * 100.0
                perc_status = 3
                
            else:
                perc_status = 2
                
            depl_status = 2
    
    else:
        if deployment != None and deployment < 0.0:
            messages.add_message(request, messages.ERROR, "The submitted area can't be less than 0: "+format(deployment, '.2f'))
            raise Exception('An error as occurred.')
    
        if percentage != None and (percentage < 0.0 or percentage > 100.0):
            messages.add_message(request, messages.ERROR, "The submitted value for percentage is not valid: "+format(percentage, '.2f'))
            raise Exception('An error as occurred.')
        
        if not is_calculated:
            if percentage and deployment:
                perc_status = 1
                depl_status = 1
            
            elif percentage and not deployment:
                messages.add_message(request, messages.ERROR, "For multiplied data you must submit a deployment value and not only a percentage value.")
                raise Exception('An error as occurred.')
                
            elif deployment and not percentage:
                percentage = 0
                perc_status = 3
                depl_status = 1
                
        else:
            if deployment and not percentage:
                percentage = 0
                perc_status = 3
            else:
                perc_status = 2
                
            depl_status = 2
    
    #Multiplied data: The n+1 year cultivated data is calculated from n year multiplied data
    if data_type == "2" and calculate_data:
        if is_calculated:
            messages.add_message(request, messages.WARNING, "It is not allowed to calculate estimated cultivated data for year n+1 from calculated raw multiplied data. These data haven't been created.")
        
        else:
            next_year = int(annual_area.year) + 1
            next_annual_area = AnnualArea.objects.get(land_unit=annual_area.land_unit, year=next_year, data_type="1")
            
            
            if next_annual_area.deployment == 0:
                messages.add_message(request, messages.ERROR, "There is no annual area deployment value for this land unit: "+str(annual_area.land_unit)+" for the year "+str(next_year))
                raise Exception('An error as occurred.')
            
#             new_deployment = (percentage / 100.0) * next_annual_area.deployment
#             new_percentage = percentage
            
            estimated_rep = VarietyRepartition.objects.create(annual_area=next_annual_area,
                                                              accession=accession.accession,
                                                              data_type="1",
                                                              percentage=0,#new_percentage,
                                                              perc_status=3,
                                                              deployment=0,#new_deployment,
                                                              depl_status=3,
                                                              unit=next_annual_area.unit)
            estimated_rep.save()
            rep_ids.append(estimated_rep.id)
            
    return percentage, deployment, perc_status, depl_status, rep_ids


def update_repartition_from_file(request, file_dict, data_type, is_calculated, calculate_data):
    """
      Function used to update variety repartitions from a file uploaded by the user.
      :param dict file_dict: file on dict format returned by the CSVFactory
      :param int data_type: represent the type of the data uploaded: cultivated or multiplied
      :param boolean is_calculated: if the data is calculated raw data
      :param boolean calculate_data: if the user wants to calculate year n+1 cultivated data
    """
    aa_ids = []
    rep_ids = []
    with transaction.atomic():      
        for line in file_dict:
            try:
                with transaction.atomic():  
                    land_unit = LandUnit.objects.get(name=line["Land unit"], country=line["Country"])
                    annual_area = AnnualArea.objects.get(land_unit=land_unit, year=line["Year"], data_type=data_type)
                    accession = AccessionLocal.objects.get(accession=Accession.objects.get(name=line["Accession"]), country=line["Country"])
                    
                    percentage = float(line['Percentage'].replace(',','.')) if line['Percentage'] else None
                    deployment = float(line['Deployment'].replace(',','.')) if line['Deployment'] else None
                    
                    if annual_area.id not in aa_ids:
                        aa_ids.append(annual_area.id)
                    
                    if line["Unit"] != "":
                        key_unit = get_dict_key_from_value(landscape_units, line["Unit"]) 
                        if type(key_unit) != int:
                            raise Exception("This unit doesn't exists, please use a unit of the following list: {0}.".format(', '.join(landscape_units.values())))
                    else:
                        key_unit = annual_area.unit
                        send_message_if_not_already_sent(request, 'The unit was not specified for, at least, one line in your file, the unit of the annual area has been used.', 'warning')
    
                    if annual_area.unit != key_unit:
                        raise Exception("The unit of the corresponding annual area is different of the unit in the file, please correct your file. ({0}, {1})".format(landscape_units[annual_area.unit], line["Unit"]))
                    
                    
                    if deployment != None and deployment == 0.0:
                        messages.add_message(request, messages.WARNING, "A deployment value equal to 0 was submitted at line: "+line["Land unit"]+" ("+line["Country"]+"), "+line["Accession"]+", "+line["Year"]+").")
                        continue
                    
                    if percentage != None and percentage == 0.0:
                        messages.add_message(request, messages.WARNING, "A percentage value equal to 0 was submitted at line: "+line["Land unit"]+" ("+line["Country"]+"), "+line["Accession"]+", "+line["Year"]+").")
                        continue
                    
                    repartition, percentage, deployment, perc_status, depl_status, rep_ids = process_update_variety_repartition_data(request, data_type, annual_area, percentage, deployment, is_calculated, calculate_data, accession, rep_ids)
                    
                    tags = []
                    if line["Tags"] != '':
                        for tag_name in line["Tags"].split(','):
                            tags.append(Tag.objects.get(name=tag_name.strip()))
                    
                    repartition.percentage = percentage
                    repartition.deployment = deployment
                    repartition.unit = key_unit
                    repartition.perc_status = perc_status
                    repartition.depl_status = depl_status
                    
                    repartition.tags.clear()
                    repartition.tags.add(*list(tags))
                    
                    repartition.save()
                  
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+line["Land unit"]+" ("+line["Country"]+"), "+line["Accession"]+", "+line["Year"]+").")
                    continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return
       
    if data_type == '2':
        multiplications_updated.send(sender=VarietyRepartition, aa_ids=aa_ids)
        if rep_ids:
            estimated_data_submitted.send(sender=VarietyRepartition, rep_ids=rep_ids)
            
    repartition_data_submitted.send(sender=VarietyRepartition, aa_ids=aa_ids)

    messages.add_message(request, messages.SUCCESS, "The file has been successfully uploaded and the variety repartitions have been updated.")


def process_update_variety_repartition_data(request, data_type, annual_area, percentage, deployment, is_calculated, calculate_data, accession, rep_ids):
    """
      Check and process data according to submitted parameters in order to update variety repartition data.
      :param int data_type: represents the type of the data uploaded: cultivated or multiplied
      :param AnnualArea annual_area: annual area to link to the data
      :param float percentage: value of the percentage
      :param float deployment: value of the deployment
      :param boolean is_calculated: if the data is calculated raw data
      :param boolean calculate_data: if the user wants to calculate year n+1 cultivated data
      :param Accession accession: accession to link to the data
    """
    db_area = float(annual_area.deployment) if annual_area.deployment else 0
    
    if db_area == 0:
        messages.add_message(request, messages.ERROR, "There is no annual area deployment value for this land unit: "+str(annual_area.land_unit))
        raise Exception('An error as occurred.')

    if data_type == '1' and deployment and deployment > db_area:
        messages.add_message(request, messages.ERROR, "The submitted area can't be superior to the annual area deployment for the land unit: "+format(db_area, '.2f'))
        raise Exception('An error as occurred.')
    
    if deployment != None and deployment < 0.0:
        messages.add_message(request, messages.ERROR, "The submitted area can't be less than 0: "+format(deployment, '.2f'))
        raise Exception('An error as occurred.')
    
    if percentage != None and (percentage < 0.0 or percentage > 100.0):
        messages.add_message(request, messages.ERROR, "The submitted value for percentage is not valid: "+format(percentage, '.2f'))
        raise Exception('An error as occurred.')
    
    if not is_calculated:
        #Get repartition object
        repartitions = VarietyRepartition.objects.filter(annual_area=annual_area, accession=accession.accession,data_type=data_type, perc_status__in=[1,3], depl_status__in=[1,3]).distinct()
        repartitions = repartitions.exclude(perc_status=3, depl_status=3)
        
        if not repartitions:
            messages.add_message(request, messages.ERROR, "The object you want to update doesn't exists, at line : {0} ({1}), {2}, {3}".format(annual_area.land_unit, annual_area.land_unit.country, accession.accession, annual_area.year))
            raise Exception('An error as occurred.')
        
        repartition = repartitions[0]
        
        if percentage and not deployment:
            deployment = (percentage / 100.0) * db_area
            perc_status = 1
            depl_status = 3
            
        elif deployment and not percentage:
            percentage = (deployment / db_area) * 100.0
            perc_status = 3
            depl_status = 1
        
        elif percentage and deployment:
            perc_status = 1
            depl_status = 1
        
    else:
        #Get repartition object
        repartitions = VarietyRepartition.objects.filter(annual_area=annual_area, accession=accession.accession,data_type=data_type, perc_status=2, depl_status=2).distinct()
        if not repartitions:
            messages.add_message(request, messages.ERROR, "The object you want to update doesn't exists, at line : {0} ({1}), {2}, {3}".format(annual_area.land_unit, annual_area.land_unit.country, accession.accession, annual_area.year))
            raise Exception('An error as occurred.')
        
        repartition = repartitions[0]
        
        #If the deployments are submitted we calculate the missing percentages for the dataview
        if deployment and not percentage:
            percentage = (deployment / db_area) * 100.0
            perc_status = 3
            
        else:
            perc_status = 2
            
        depl_status = 2
    
    #Multiplied data: The n+1 year cultivated data is re-calculated from n year multiplied data
    if data_type == "2" and calculate_data:
        if is_calculated:
            messages.add_message(request, messages.WARNING, "It is not allowed to calculate estimated cultivated data for year n+1 from calculated raw multiplied data. These data haven't been created or updated.")
        
        else:
            next_year = annual_area.year + 1
            next_annual_area = AnnualArea.objects.get(land_unit=annual_area.land_unit, year=next_year, data_type="1")
            
            
            if next_annual_area.deployment == 0:
                messages.add_message(request, messages.ERROR, "There is no annual area deployment value for this land unit: "+str(annual_area.land_unit)+" for the year "+str(next_year))
                raise Exception('An error as occurred.')
            
#             new_deployment = (percentage / 100.0) * next_annual_area.deployment
#             new_percentage = percentage
            
            #If the calculated data doesn't exists, we create it
            estimated_rep, result = VarietyRepartition.objects.get_or_create(annual_area=next_annual_area, accession=accession.accession, data_type="1", perc_status=3, depl_status=3, defaults={'annual_area':next_annual_area,
                                                                                                                                                                                                 'accession':accession.accession,
                                                                                                                                                                                                 'data_type':1,
                                                                                                                                                                                                 'percentage':0,#new_percentage,
                                                                                                                                                                                                 'perc_status':3,
                                                                                                                                                                                                 'deployment':0,#new_deployment,
                                                                                                                                                                                                 'depl_status':3,
                                                                                                                                                                                                 'unit':next_annual_area.unit})
            
            if not result:
                estimated_rep.percentage = 0#new_percentage
                estimated_rep.deployment = 0#new_deployment
                estimated_rep.unit = next_annual_area.unit
            
            estimated_rep.save()
            rep_ids.append(estimated_rep.id)
            
    #Multiplied data, if calculate data not checked, we check if estimated data already exists
    #to update it, but if it doesn't we don't create it.
    if data_type == "2" and not calculate_data:
        next_year = annual_area.year + 1
        next_annual_areas = AnnualArea.objects.filter(land_unit=annual_area.land_unit, year=next_year, data_type="1").distinct()
        
        if next_annual_areas :
            next_annual_area = next_annual_areas[0]
            if next_annual_area.deployment != 0:
                estimated_reps = VarietyRepartition.objects.filter(annual_area=next_annual_area, accession=accession.accession, data_type="1", perc_status=3, depl_status=3).distinct()
                
                if estimated_reps:
                    estimated_rep = estimated_reps[0]
#                     new_deployment = (percentage / 100.0) * next_annual_area.deployment
#                     new_percentage = percentage
                
                    estimated_rep.percentage = 0#new_percentage
                    estimated_rep.deployment = 0#new_deployment
                    estimated_rep.unit = next_annual_area.unit
                    
                    estimated_rep.save()
                    rep_ids.append(estimated_rep.id)
    
    return repartition, percentage, deployment, perc_status, depl_status, rep_ids
        
    

#Delete the possibility to edit variety repartition data (users meeting 26-09-19)
# class VarietyRepartitionEdit(LoginRequiredMixin, UserIsAdminMixin, View):
#     """
#       View managing the edition of the variety repartition model. The user can make a refined request on the table.
#       The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
#       
#       :var str template: html template for the rendering of the view
#       :var list list_attributes: list containing the names of the fields which can be searched with the refine search 
#       :var form form_class: the annual area form
#       :var queryset repartitions: contains all the variety repartitions of the database
#       :var int number_all: contains the number of variety repartitions of the database and is used, mostly, for the pagination
#       :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
#       :var list names: variable containing the field names of the model
#       :var list labels: variable containing the field names of the form
#       :var dict tag_fields: dictionary containing the parameters passed to the template
#       :var dict dico_get: dictionary containing the data obtained with the GET request method
#       :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
#     """
#     form_class = VarietyRepartitionForm
#     file_form_class = VarietyRepartitionUploadForm
#     template = 'landscape/landscape_repartition.html'
#     title = "Variety Repartition Edit"
#     list_attributes = ['land unit', 'year', 'accession', 'data type', 'percentage', 'area', 'quantity']
# 
#     def get(self, request, *args, **kwargs):
#         try:
#             r = VarietyRepartition.objects.get(id=kwargs['object_id'])
#         except:
#             messages.add_message(request, messages.ERROR, "This variety repartition doesn't exist in the database.")
#             return redirect('repartition_management')
# 
#         form = self.form_class(initial={'land_unit':r.annual_area.land_unit,
#                                         'year':r.annual_area.year,
#                                         'accession':r.accession,
#                                         'data_type':r.data_type,
#                                         'percentage':r.percentage,
#                                         'area':r.area,
#                                         'quantity':r.quantity,
#                                         })
# 
#         file_form = self.file_form_class()
# 
#         repartitions = VarietyRepartition.objects.all()
#         number_all = len(repartitions)
#         repartitions = add_pagination(request,repartitions)
#         nb_per_page = 10
#         names, labels = get_fields(form)
#         tag_fields = {'all':repartitions,
#                       'hiddenid':r.id,
#                       'fields_name':names,
#                       'fields_label':labels,
#                       'title': self.title,
#                       'filetype':'VarietyRepartition',
#                       'refine_search':True}
#     
#         dico_get={}
#         or_and = None
#         if "no_search" in request.GET:
#             tag_fields.update({'search_result':dico_get,
#                                'or_and':or_and,
#                                'all':repartitions,
#                                'number_all':number_all,
#                                'nb_per_page':nb_per_page,
#                                'creation_mode':False,
#                                'form':form,
#                                'file_form':file_form,
#                                'list_attributes':self.list_attributes})
#             
#             return render(request,self.template,tag_fields)
# 
#         elif "or_and" in request.GET:
#             or_and=request.GET['or_and']
#             for i in range(1,10):
#                 data="data"+str(i)
#                 text_data="text_data"+str(i)
#                 if data in request.GET and text_data in request.GET:
#                     dico_get[data]={request.GET[data]:request.GET[text_data]}
#              
#             repartitions = VarietyRepartition.objects.refine_search(or_and, dico_get)
#             
#             number_all = len(repartitions)
#             repartitions = add_pagination(request,repartitions)
#             list_exceptions = ['id']
#             download_not_empty = False
#             if repartitions:
#                 download_not_empty = write_repartition_csv_from_viewer(repartitions, "Result search")
#             tag_fields.update({'download_not_empty':download_not_empty})
# 
#         tag_fields.update({'search_result':dico_get,
#                            'or_and':or_and,
#                            'all':repartitions,
#                            'number_all':number_all,
#                            'nb_per_page':nb_per_page,
#                            'creation_mode':False,
#                            'form':form,
#                            'file_form':file_form,
#                            'list_attributes':self.list_attributes})  
#         return render(request, self.template, tag_fields)
# 
#     def post(self, request, *args, **kwargs):
#         form = self.form_class(request.POST)
#         file_form = self.file_form_class(request.POST, request.FILES)
#         
#         repartitions = VarietyRepartition.objects.all()
#         number_all = len(repartitions)
#         
#         names, labels = get_fields(form)
#         
#         tag_fields = {'all':repartitions,
#                       'number_all':number_all,
#                       'fields_name':names,
#                       'fields_label':labels,
#                       'title': self.title,
#                       'form':form,
#                       'file_form':file_form,
#                       'creation_mode':False,
#                       'refine_search':True,
#                       'filetype':'Trait',
#                       'list_attributes':self.list_attributes}
#         
#         if file_form.is_valid():
#             messages.add_message(request, messages.ERROR, "You were in edit mode, please submit your file in management mode.")
#             return redirect('repartition_management')
# 
#         if form.is_valid():
#             update_repartition_from_form(request, form)
# 
#         return redirect('repartition_management')

# def update_repartition_from_form(request, form):
#     """
#       Function used to update a variety repartition object from the form.
#       :param form form: form submitted by the user
#     """
#     if "hiddenid" in request.POST.keys() :
#         r = VarietyRepartition.objects.get(id=request.POST['hiddenid'])
#     else:
#         messages.add_message(request, messages.ERROR, "An error as occurred, please try again.")
#         return
# 
#     
#     land_unit = form.cleaned_data['land_unit']
#     year = form.cleaned_data['year']
#     
#     try:
#         annual_area = AnnualArea.objects.get(land_unit=land_unit, year=year)
#     except:
#         messages.add_message(request, messages.ERROR, "There is no Annual Area corresponding to your query in the database.")
#         return 
#     
#     
#     data_type = str(form.cleaned_data['data_type'])
#     percentage = form.cleaned_data['percentage']
#     area = form.cleaned_data['area']
#     
#     percentage, area = check_percentage_and_area(request, data_type, annual_area, percentage, area)
#     
#     try:
#         r.annual_area = annual_area
#         r.accession = form.cleaned_data['accession']
#         r.data_type = data_type
#         r.percentage = percentage
#         r.area = area
#         r.quantity = form.cleaned_data['quantity']
#     
#         r.save()
#         
#     except Exception as e:
#         messages.add_message(request, messages.ERROR, e)
#         return 
#     
#     messages.add_message(request, messages.SUCCESS, "The {0} has been updated.".format(r))
#     return 

def repartition_filerenderer():
    """
      Filerenderer specific for variety repartition, to display in 
      separated columns the country, land unit and year.
    """
    response = HttpResponse(content_type='text/csv') 
    writer = csv.writer(response,delimiter=';')
    headers = filerenderer_headers[8][1].copy()
    headers.insert(4, 'Data type')
    
    filename = 'VarietyRepartition.csv'
    
    database = VarietyRepartition.objects.all().values_list('annual_area').distinct().order_by('annual_area')
    writer.writerow(headers)
    
    database_list = [i[0] for i in database]
        
    for entry_name in database_list:
        repartitions = VarietyRepartition.objects.filter(annual_area=entry_name)
        for repartition in repartitions:
            annual_area = repartition.annual_area
            line = [annual_area.land_unit.country,
                    annual_area.land_unit.name,
                    annual_area.year,
                    repartition.accession,
                    landscape_data_types[repartition.data_type],
                    repartition.percentage,
                    repartition.deployment,
                    landscape_units[repartition.unit]]
            line.append(', '.join([str(tag) for tag in repartition.tags.filter()]))
            
            writer.writerow(line)
    
    response['Content-Disposition'] = 'attachment; filename=%s' %filename  
    writer = csv.writer(response)
    return response

def write_repartition_csv_from_viewer(model_data, title):
    """
      Search results writer specific for variety repartition, to display in separated columns 
      the land unit, country and year.
      :param dict model_data: data filtered by the search parameters
      :param string title: title of the file
    """
    list_col = filerenderer_headers[8][1].copy()
    list_col.insert(4, 'Data type')
    
    filename = title.lower().replace(" ","_")
    dialect = Dialect(delimiter = ';')
    
    with open("/tmp/"+filename+".csv", 'w', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, dialect)
        writer.writerow(list_col)
        
        for data in model_data:
            annual_area = data.annual_area
            row = [annual_area.land_unit.country,
                   annual_area.land_unit.name,
                   annual_area.year,
                   data.accession,
                   landscape_data_types[data.data_type],
                   data.percentage,
                   data.deployment,
                   landscape_units[data.unit]]
            row.append(', '.join([str(tag) for tag in data.tags.filter()]))
            writer.writerow(row)
    download_not_empty = True
    return download_not_empty



class LifeCycleViewer(LoginRequiredMixin, View):
    """
      This view allows the user to visualize life cycles of accessions. The first part of the view is a form 
      used to select the projects in which browse the data, the country for which there is repartition data, 
      and then the accessions and years to visualize.
      The result is a plot that represents the percentages of cultivated acreages vs. years.
      
      :var str template: html template for the rendering of the view
      :var str title: title displayed on the template
      :var dict tag_fields: dictionary containing the parameters passed to the template
    """
    template = 'dataview/life_cycle_viewer.html'
    title = "Life Cycle Viewer"
    
    def get(self, request, *args, **kwargs):
        
        formproject = DataviewProjectForm()
        username = request.user.login
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)

        formproject.fields['project'].queryset=projects.order_by('name')

        n_page = 0
        return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formproject':formproject})

   
    def post(self, request, *args, **kwargs):
        formproject = DataviewProjectForm(request.POST)
        #Get the actual page number
        n_page = int(request.POST['n_page'])
        
        if int(n_page) == 1:
            
            if formproject.is_valid():
                projects = request.POST.getlist('project')
                request.session['projects'] = projects
            else:
                return render(request,self.template,{'title':self.title, 'n_page':0, 'formproject':formproject})
            
            formcountry = DataviewCountryFormList()
            
            return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formcountry':formcountry})
        
        if int(n_page) == 2:
            
            formcountry = DataviewCountryFormList(request.POST)
            
            if formcountry.is_valid():
                country = request.POST.get('country')
                request.session['country'] = country
            else:
                return render(request,self.template,{'title':self.title, 'n_page':1, 'formcountry':formcountry})

            formacc = DataviewAccessionLifeCycleForm()
            formyear = DataviewYearForm()
            
            #We select the lowest (max) level of NUTS available for the country
            land_units = LandUnit.objects.filter(country=country)
#             print(land_units)
            nuts_min = land_units.aggregate(Min('nuts'))
#             print(nuts_min)
            
            #We check for which nuts there is repartition data
            for nuts_value in range(nuts_min["nuts__min"], 4):
                #The accessions are filtered by projects
                accessions = AccessionLocal.objects.filter(projects__in=request.session['projects'], country=country).values_list('accession').distinct()
                annual_areas = AnnualArea.objects.filter(land_unit__in=LandUnit.objects.filter(country=country, nuts=nuts_value), data_type=1)
                repartitions = VarietyRepartition.objects.filter(annual_area__in=annual_areas, accession__in=accessions, data_type=1).distinct()
#                 print(repartitions)
                if repartitions:
                    nuts_min["nuts__min"] = nuts_value
                    break
            
                    
            
            if repartitions:
                nb_rep_acc, year_choices = get_year_and_accession_list(repartitions)
                
                acc_order = []
                for acc in nb_rep_acc:
                    if acc['id__count'] > 1:
                        #acc_order.append(AccessionLocal.objects.get(id=acc['accession']).accession.id)
                        acc_order.append(acc['accession'])

                clauses = ' '.join(['WHEN id=%s THEN %s' % (id, i) for i, id in enumerate(acc_order)])
                ordering = 'CASE %s END' % clauses
                queryset = Accession.objects.filter(id__in=acc_order).extra(select={'ordering': ordering}, order_by=('ordering',))
                
                request.session['acc_order'] = acc_order
                request.session['year_choices'] = year_choices
                request.session['nuts_min'] = nuts_min
                
                formacc.fields['accession_search'].queryset = queryset
                formyear.fields['year'].choices = year_choices
                
            else:
                formacc.fields['accession_search'].queryset = Accession.objects.none()
                formyear.fields['year'].choices = []
                
            return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formacc':formacc, 'formyear':formyear})
        
        if int(n_page) == 3:
            acc_order = request.session['acc_order']
            
            formacc = DataviewAccessionLifeCycleForm(request.POST)
            formyear = DataviewYearForm(request.POST)
            
            if formacc.is_valid() and formyear.is_valid():
                accessions = request.POST.getlist('accession_search')
                years = request.POST.getlist('year')
                request.session['accessions'] = accessions
                request.session['years'] = years
            else:
                clauses = ' '.join(['WHEN id=%s THEN %s' % (id, i) for i, id in enumerate(acc_order)])
                ordering = 'CASE %s END' % clauses
                queryset = Accession.objects.filter(id__in=acc_order).extra(select={'ordering': ordering}, order_by=('ordering',))
                
                formacc.fields['accession_search'].queryset = queryset
                formyear.fields['year'].choices = request.session['year_choices']
                return render(request,self.template,{'title':self.title, 'n_page':2, 'formacc':formacc, 'formyear': formyear})
            
            country = request.session['country']
            nuts_min = request.session['nuts_min']


            acc_names, final_years_list, final_perc_list = get_life_cycle_values(accessions, country, years, nuts_min)

            #Setup graph plot
            plot = figure(x_axis_label='Years', y_axis_label='Cultivated acreages (%)', plot_width=960, plot_height=600)
            
            #To start the y axis at 0
            plot.y_range.start = 0
            
            plot.add_layout(Title(text="Percentages of cultivated acreages vs. years in "+dict(countries)[country], align="center"), "below")
            
            #Plot line(s)
            for (color, legend, year, perc) in zip(cc.b_glasbey_hv[:len(acc_names)], acc_names, final_years_list, final_perc_list):
                plot.line(year, perc, color=color, legend_label=legend, line_width=1.5)
                
            plot.legend.title = "Accession"
            
            #Store components
            script, div = components(plot)
            
            tag_fields = {'title':self.title,
                          'n_page':n_page,
                          'script':script,
                          'div':div
                          }

            return render(request, self.template, tag_fields)

def get_year_list(repartitions):
    """
      This function returns a list of years according to the minimum 
      and maximum years for which there is repartition data.
      
      :param queryset repartitions: repartitions data
    """
    min_year = 2019
    max_year = 1900
    for repartition in repartitions:
        if repartition.annual_area.year < min_year:
            min_year = repartition.annual_area.year
        elif repartition.annual_area.year > max_year:
            max_year = repartition.annual_area.year
     
    year_choices = []
    for year in range(min_year,max_year+1):
        year_choices.append([year, year])
    
    return year_choices


def get_year_and_accession_list(repartitions):
    """
      This function returns a list of years according to the minimum 
      and maximum years for which there is repartition data, and a 
      queryset containing the number of repartitions data for each accession.
      
      :param queryset repartitions: repartitions data
    """
    #Get accessions and quantity of data for each accession
    count_rep = repartitions.values("accession").annotate(Count("id")).order_by('-id__count')
    
    #Get min and max year of the queryset
    max_year = repartitions.order_by('-annual_area__year')[0].annual_area.year
    min_year = repartitions.order_by('annual_area__year')[0].annual_area.year
    
    year_choices = []
    for year in range(min_year,max_year+1):
        year_choices.append([year, year])
    
    return count_rep, year_choices

def get_life_cycle_values(accessions, country, years, nuts_min):
    """
      This function returns the lists of lists containing the coordinates to draw data in the plot.
      
      :param list accessions: list of ids of the selected accessions
      :param list years: list of the selected years
      
      :returns: acc_names, final_years_list, final_perc_list
    """
    final_years_list = []
    final_perc_list = []
    acc_names = []

    #To parse only the accessions that contains data in the next step
    all_annual_areas = AnnualArea.objects.filter(land_unit__in=LandUnit.objects.filter(country=country, nuts=nuts_min["nuts__min"]), year__in=years)
    filtered_accessions = []
    for accession in accessions:
        accession = AccessionLocal.objects.select_related("accession").get(accession=accession, country=country)
        all_repartitions = VarietyRepartition.objects.filter(data_type=1, accession=accession.accession, annual_area__in=all_annual_areas)
        if all_repartitions:
            filtered_accessions.append(accession)

    for accession in filtered_accessions:
        list_years = []
        list_perc = []
        for year in years:
            annual_areas = AnnualArea.objects.filter(land_unit__in=LandUnit.objects.filter(country=country, nuts=nuts_min["nuts__min"]), year=year)
            repartitions = VarietyRepartition.objects.select_related('accession', 'annual_area').filter(data_type=1, accession=accession.accession, annual_area__in=annual_areas)
            repartitions = validate_queryset_repartition_data(repartitions)
            percentage = 0
            
            if repartitions:
                for rep in repartitions:
                    percentage += rep.percentage
                percentage = percentage/len(repartitions)

                list_years.append(year)
                list_perc.append(percentage)
        #If there is only one data the line can't be traced, we don't save the accession's name and the value
        if len(list_perc) != 0 and len(list_perc) != 1:
            final_years_list.append(list_years)
            final_perc_list.append(list_perc)
            acc_names.append(accession.accession.name)
    
    return acc_names, final_years_list, final_perc_list


def validate_queryset_repartition_data(repartitions):
    """
      Function used to keep only one value for repartition data.
      As two cultivated data area are allowed, this function 
      keeps the raw data and delete the estimated 
      data value of the queryset.
    """
    ids_to_remove = []
    for rep in repartitions:
        query = repartitions.filter(annual_area=rep.annual_area, accession=rep.accession, data_type=1)
        if len(query) > 1:
            estimated = query.filter(perc_status=3, depl_status=3)
            if estimated: 
                if estimated[0].id not in ids_to_remove:
                    ids_to_remove.append(estimated[0].id)
                
    repartitions = repartitions.exclude(id__in=ids_to_remove)
    
    return repartitions
            
class VarietyRepartitionViewer(LoginRequiredMixin, View):
    """
      This view allows the user to visualize repartition of accessions on a year in a land unit. 
      The first part of the view is a form used to select the projects in which browse the data, 
      and next the country, land unit and year to display repartition data.
      The result is two pie charts and a table that represents the percentages of cultivated acreages 
      of each accession and type of accession.
      
      :var str template: html template for the rendering of the view
      :var str title: title displayed on the template
      :var list list_attributes: list of attributes for the refine search
      :var list labels: labels of the columns of the table
      :var dict tag_fields: dictionary containing the parameters passed to the template
    """
    template = 'dataview/repartition_viewer.html'
    title = "Variety Repartition Viewer"
    list_attributes=['accession','type','repartition']
    labels = ['Accession','Type','Repartition (%)']
    
    def get(self, request, *args, **kwargs):
        
        #Query in the refine search
        if "or_and" in request.GET:
            dico_get={}
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}            
        
        
            searched_reps = VarietyRepartition.objects.viewer_refine_search(or_and, dico_get)
            
            land_unit = LandUnit.objects.get(id__in=request.session['land_unit'])
            projects = request.session['projects']
            year = request.session['year']
            
            tag_fields = {'title': self.title,
                          'n_page':4,
                          'land_unit_id':land_unit.id,
                          'year':year,
                          'list_attributes':self.list_attributes,
                          'or_and':or_and,
                          'refine_search':True
                          }
            
            annual_areas = AnnualArea.objects.filter(land_unit=land_unit.id, year=year) 
            accessions = AccessionLocal.objects.filter(projects__in=projects, country=request.session['country'][0]).values_list('accession')
            filtered_repartitions = VarietyRepartition.objects.filter(data_type=1, annual_area__in=annual_areas, accession__in=accessions).distinct()
            filtered_repartitions = validate_queryset_repartition_data(filtered_repartitions)
        
            repartitions = searched_reps.intersection(filtered_repartitions)
        
            rep_data = {}
            
            for repartition in repartitions:
                if repartition.accession not in rep_data.keys():
                    rep_data[repartition.accession] = repartition
                else:
                    print("on est dans else: c'est mauvais")
                    
                    
            #Figures
            #rep_p_data, type_data = process_repartitions(repartitions, False)
            rep_p_data, type_data = process_repartitions(filtered_repartitions, False)
            
            #Varieties pie chart        
            script_rep, div_rep, size = variety_pie_chart(rep_p_data, "Repartition of cultivated acreages (%) by varieties: "+str(land_unit)+" in "+str(year), "Accession", 950)
            
            #Can be useful for FRB database but not for RustWatch
            #Types pie chart
            #script_type, div_type, size = variety_pie_chart(type_data, "Repartition of cultivated acreages (%) by varieties types: "+str(land_unit)+" in "+str(year), "Type", size)
                    
            download_not_empty = False
            if repartitions:
                download_not_empty = write_repartition_viewer_csv(rep_data, "Result search")
                
            tag_fields.update({'script_rep':script_rep,
                               #'script_type':script_type,
                               'div_rep':div_rep,
                               #'div_type':div_type,
                               'fields_label':self.labels,
                               'rep_data': rep_data,
                               'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               })
            
            return render(request, self.template, tag_fields) 
        
        formproject = DataviewProjectForm()
        username = request.user.login
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)

        formproject.fields['project'].queryset=projects.order_by('name')

        n_page = 0
        return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formproject':formproject})

   
    def post(self, request, *args, **kwargs):
        formproject = DataviewProjectForm(request.POST)
        #Get the actual page number
        n_page = int(request.POST['n_page'])
    
        if int(n_page) == 1:
            
            if formproject.is_valid():
                projects = request.POST.getlist('project')
                request.session['projects'] = projects
            else:
                return render(request,self.template,{'title':self.title, 'n_page':0, 'formproject':formproject})
            
            formcountry = DataviewCountryFormListOnChange()
            
            formlandunit = DataviewLandUnitListForm()
            formlandunit.fields['land_unit'].disabled = True
            
            formyear = DataviewYearListForm()
            formyear.fields['year'].disabled = True
            
            
            return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formcountry':formcountry, 
                                                 'formlandunit':formlandunit, 'formyear':formyear})
    
        if int(n_page) == 2:
            
            formcountry = DataviewCountryFormListOnChange(request.POST)
            
            formlandunit = DataviewLandUnitListForm()
            
            formyear = DataviewYearListForm()
            formyear.fields['year'].disabled = True
            
            if formcountry.is_valid():
                country = request.POST.getlist('country')
                request.session['country'] = country
            else:
                formlandunit.fields['land_unit'].disabled = True
                return render(request,self.template,{'title':self.title, 'n_page':1, 'formcountry':formcountry, 
                                                     'formlandunit':formlandunit, 'formyear':formyear})

            
            land_units = LandUnit.objects.filter(country__in=country)
            
            formcountry.fields['country'].initial = country[0]
            formcountry.fields['country'].disabled = True
            
            formlandunit.fields['land_unit'].queryset = land_units

            return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formcountry':formcountry, 
                                                 'formlandunit':formlandunit, 'formyear':formyear})
        
        if int(n_page) == 3:
            
            formcountry = DataviewCountryFormListOnChange()
            formcountry.fields['country'].initial = request.session['country'][0]
            formcountry.fields['country'].disabled = True
            
            formlandunit = DataviewLandUnitListForm(request.POST)
            
            formyear = DataviewYearListForm()
            
            if formlandunit.is_valid():
                land_unit = request.POST.getlist('land_unit')
                request.session['land_unit'] = land_unit
            else:
                formyear.fields['year'].disabled = True
                return render(request,self.template,{'title':self.title, 'n_page':2, 'formcountry':formcountry,
                                                     'formlandunit':formlandunit, 'formyear':formyear})
            
            
            years = []
            annual_areas = AnnualArea.objects.filter(land_unit__in=land_unit)
            repartitions = VarietyRepartition.objects.filter(annual_area__in=annual_areas, data_type=1).distinct()
            
            list_years = repartitions.values('annual_area__year').order_by('annual_area__year').distinct()

            for year in list_years:
                years.append([year['annual_area__year'],year['annual_area__year']])

            formlandunit.fields['land_unit'].initial = land_unit[0]
            formlandunit.fields['land_unit'].disabled = True
            
            formyear.fields['year'].choices = [('','Choose a Year')] + years
            
            return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formcountry':formcountry, 
                                                 'formlandunit':formlandunit, 'formyear':formyear})
        
        if int(n_page) == 4:
            
            formcountry = DataviewCountryFormListOnChange()
            formcountry.fields['country'].initial = request.session['country'][0]
            formcountry.fields['country'].disabled = True
            
            formlandunit = DataviewLandUnitListForm()
            formlandunit.fields['land_unit'].initial = request.session['land_unit'][0]
            formlandunit.fields['land_unit'].disabled = True
            
            formyear = DataviewYearListForm(request.POST)
            
            if formyear.is_valid():
                year = request.POST.getlist('year')[0]
                request.session['year'] = year
            else:
                return render(request,self.template,{'title':self.title, 'n_page':3, 'formcountry':formcountry, 
                                                     'formlandunit':formlandunit,'formyear': formyear})
            
            
            land_unit = LandUnit.objects.get(id__in=request.session['land_unit'])
            projects = request.session['projects']
            
            title = self.title + ": "+str(land_unit)+" in "+year
            
            tag_fields = {'title': title,
                          'land_unit_id':land_unit.id,
                          'year':year,
                          'n_page':n_page,
                          }
            
            annual_areas = AnnualArea.objects.filter(land_unit=land_unit.id, year=year) 
            accessions = AccessionLocal.objects.filter(projects__in=projects, country=request.session['country'][0]).values_list('accession')
            repartitions = VarietyRepartition.objects.select_related('accession','accession__type','annual_area').filter(data_type=1, annual_area__in=annual_areas, accession__in=accessions).distinct()
            repartitions = validate_queryset_repartition_data(repartitions)
            
            if not repartitions:
                messages.add_message(request, messages.INFO, "There is no data to display.")
                render(request, self.template, {})
            
            rep_p_data, type_data, rep_data = process_repartitions(repartitions, True)
            
            #Varieties pie chart        
            script_rep, div_rep, size = variety_pie_chart(rep_p_data, "Repartition of cultivated acreages (%) by varieties: "+str(land_unit)+" in "+str(year), "Accession", 950)
            
            #Can be useful for FRB database but not for RustWatch
            #Types pie chart
            #script_type, div_type, size = variety_pie_chart(type_data, "Repartition of cultivated acreages (%) by varieties types: "+str(land_unit)+" in "+str(year), "Type", size)
            
            #Export of the query
            download_not_empty = write_repartition_viewer_csv(rep_data, title)
            
            tag_fields.update({'script_rep':script_rep,
                               #'script_type':script_type,
                               'div_rep':div_rep,
                               #'div_type':div_type,
                               'fields_label':self.labels,
                               'rep_data': rep_data,
                               'list_attributes':self.list_attributes,
                               'download_not_empty':download_not_empty,
                               'filename':title.lower().replace(" ","_"),
                               'refine_search':True
                              })
            
            
            return render(request, self.template, tag_fields)


def process_repartitions(repartitions, table):
    """
    This functions returns two dictionary containing data to display on pie charts.
    If the boolean table is True, this function returns a third dictionary 
    containing data to display in the table.
    
    :param dict repartitions: Queryset to create the dictionaries
    :param boolean table: to specify if the third dictionary must be returned
    """
    rep_data = {}
    rep_p_data = {}
    type_data = {}
    total = 0
    
    for repartition in repartitions:
        if repartition.accession.name not in rep_p_data.keys():
            if table:
                rep_data[repartition.accession] = repartition
            rep_p_data[repartition.accession.name] = repartition.percentage
            total += repartition.percentage
        else:
            print("on est dans else: c'est mauvais")
#             if table:
#                 rep_data[repartition.accession] += repartition.percentage
#             rep_p_data[repartition.accession.name] += repartition.percentage
        if repartition.accession.type.name not in type_data.keys():
            type_data[repartition.accession.type.name] = repartition.percentage
        else:
            type_data[repartition.accession.type.name] += repartition.percentage  

    if total < 100:
        rep_p_data['NA'] = 100 - total
        rep_data['NA'] = 100 - total

    if table:
        return rep_p_data, type_data, rep_data
    else:
        return rep_p_data, type_data

def variety_pie_chart(pie_c_data, title, legend, size):
    """
      This function returns the pie chart representation of the dictionary 
      given in parameters.
      
      :param dict pie_c_data: dictionary to transform in pie chart
      :param str title: title of the pie chart
      :param str legend: legend of the pie chart
      :param int size: size of the pie chart
    """
    data = pd.Series(pie_c_data).reset_index(name='value').rename(columns={'index':'accessions'}).sort_values(by='value', ascending=False)
        
    data['angle'] = data['value']/data['value'].sum() * 2*pi

    data['color'] = linear_palette(cc.glasbey_cool, len(pie_c_data))
    
#     if size == 0:
#         size = 500
#     
#         if len(pie_c_data.keys()) * 27 > size:
#             size = len(pie_c_data.keys()) * 27
    
    
    plot = figure(plot_height=size,plot_width=size, title=title, tooltips="@accessions: @value", x_range=(-0.5, 1.0))
    plot.wedge(x=0, y=1, radius=0.4,
               start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
               line_color="white", fill_color='color', legend_field='accessions', source=data)
    
    plot.axis.axis_label=None
    plot.axis.visible=False
    plot.grid.grid_line_color = None
    
    plot.legend.title = legend
    plot.legend.label_text_font_size = '9pt'
    
    script, div = components(plot)
    
    return script, div, size


def write_repartition_viewer_csv(model_data, title):
    """
      Results writer specific for variety repartition viewer to display 
      the different informations. Used for general export and refine search export.
      :param dict model_data: data initially displayed or filtered by the search parameters
      :param string title: title of the file
    """
    list_col = ['Accession','Type','Repartition (%)']
    filename = title.lower().replace(" ","_")
    dialect = Dialect(delimiter = ';')
    
    with open("/tmp/"+filename+".csv", 'w', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, dialect)
        writer.writerow(list_col)
        
        perc_data = {}
        for key, value in model_data.items():
            if isinstance(key, str):
                perc_data[key] = value
            else:
                perc_data[key] = value.percentage
            
        # We sort the model data to have the repartition percentage in descending order (gives back a list)
        #data = sorted(model_data.items(), key=operator.itemgetter(1), reverse=True)
        data = sorted(perc_data.items(), key=operator.itemgetter(1), reverse=True)

        for repartition in data:
            if isinstance(repartition[0],str):
                row = [repartition[0],
                       repartition[0],
                       "{:.3f}".format(repartition[1])]
            else:
                row = [repartition[0].name,
                       repartition[0].type.name,
                       repartition[1]]
            writer.writerow(row)
    download_not_empty = True
    return download_not_empty


class VarietyMapRepartitionViewer(LoginRequiredMixin, View):
    """
      
      :var str template: html template for the rendering of the view
      :var str title: title displayed on the template
      :var dict tag_fields: dictionary containing the parameters passed to the template
    """
    template = 'dataview/map_repartition_viewer.html'
    title = "Variety Repartition Map"
    
    def get(self, request, *args, **kwargs):
        formproject = DataviewProjectForm()
        """username = request.user.login
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)"""
        
        projects = Project.objects.filter(users=request.user.id)
        
        formproject.fields['project'].queryset=projects.order_by('name')

        n_page = 0
        return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formproject':formproject})
    
    def post(self, request, *args, **kwargs):
        formproject = DataviewProjectForm(request.POST)
        #Get the actual page number
        n_page = int(request.POST['n_page'])
    
        if int(n_page) == 1:
#            query_time = datetime.datetime.now()
            if formproject.is_valid():
                projects = request.POST.getlist('project')
                request.session['projects'] = projects
            else:
                return render(request,self.template,{'title':self.title, 'n_page':0, 'formproject':formproject})
            
            
            filtered_accessions = AccessionLocal.objects.filter(projects__in=request.session['projects']).values_list('accession__id', flat=True).distinct()
            repartitions = VarietyRepartition.objects.select_related('annual_area', 'annual_area__land_unit','accession').filter(accession__in=filtered_accessions, data_type=1)
            list_years = repartitions.values('annual_area__year').order_by('annual_area__year').distinct()
            
            years = []
            for year in list_years:
                years.append([year['annual_area__year'],year['annual_area__year']])
            
            if 'year' in request.POST.keys():
                formyear = DataviewMapYearListForm(request.POST)
            
                if formyear.is_valid():
                    year = request.POST.getlist('year')[0]
                    print(year)
                else:
                    messages.add_message(request, messages.ERROR, "An error occurred.")
                    formyear = DataviewMapYearListForm()
                    year = years[-1][0]
            else:
                formyear = DataviewMapYearListForm()
                year = years[-1][0]
                print(year)
            
            request.session['year'] = year
            
            formyear.fields['year'].choices = years 
            formyear.fields['year'].initial = year
        
            repartitions = VarietyRepartition.objects.select_related('annual_area', 'annual_area__land_unit','accession').filter(accession__in=filtered_accessions, annual_area__year=year, data_type=1).distinct()
            
            ####### Ajouts demo ######
            
            country_select = repartitions.values_list('annual_area__land_unit__country', flat=True).order_by('annual_area__land_unit__country').distinct()
            land_unit_select = repartitions.values_list('annual_area__land_unit', flat=True).order_by('annual_area__land_unit').distinct()
            
            selected_lu = []
            for country in country_select:
                land_units = LandUnit.objects.filter(country=country, id__in=land_unit_select)
                nuts_min = land_units.aggregate(Min('nuts'))
                for lu in LandUnit.objects.filter(country=country, nuts=nuts_min["nuts__min"]).distinct():
                    selected_lu.append(lu)
            
            selected_aa = AnnualArea.objects.filter(land_unit__in=selected_lu, year=year).distinct()
            
            repartitions = VarietyRepartition.objects.select_related('annual_area', 'annual_area__land_unit','accession').filter(accession__in=filtered_accessions, annual_area__in=selected_aa, data_type=1).distinct()
            
            ####### Fin ajouts demo ######
            
            repartitions = validate_queryset_repartition_data(repartitions)
            
#            
#            """repartitions = VarietyRepartition.objects.select_related('annual_area', 'annual_area__land_unit','accession').filter(accession__in=filtered_accessions, annual_area__year=year, data_type=1).distinct()
#            repartitions = validate_queryset_repartition_data(repartitions)"""
#            
#            #repartitions = VarietyRepartition.objects.select_related('annual_area', 'annual_area__land_unit','accession').filter(accession__in=filtered_accessions, annual_area__year=year, data_type=1)
#            repartitions_set1 = repartitions.filter(Q(annual_area__year=year), Q(perc_status=1)|Q(depl_status=1))
#            repartitions_set2 = repartitions.filter(Q(annual_area__year=year),~Q(perc_status=1),~Q(depl_status=1)).exclude(accession__in=repartitions_set1.values('accession'))
            data = {}
            acc_color_dict = {}
            
#            for rep in repartitions_set1.union(repartitions_set2):
            for rep in repartitions:   
                if rep.annual_area.land_unit.country.code not in data.keys():
                    data[rep.annual_area.land_unit.country.code] = [rep]
                else:
                    data[rep.annual_area.land_unit.country.code].append(rep)
        
                if rep.accession.name not in acc_color_dict.keys():
                    acc_color_dict[rep.accession.name] = ''
#            end_query = datetime.datetime.now()
#            print("SQL query and organize data : {0}".format(end_query-query_time))
            j = 1
            col_palette = []
#             for i in range(int(len(acc_color_dict.keys()) / 256)):
            i = 0
            while i <= int(len(acc_color_dict.keys()) / 256):
                if j == 1:
                    col_palette.extend(cc.glasbey_cool)
                    j = 2
                elif j == 2:
                    col_palette.extend(cc.b_glasbey_category10)
                    j = 3
                elif j == 3:
                    col_palette.extend(cc.b_glasbey_hv)
                    j = 4
                elif j == 4:
                    col_palette.extend(cc.glasbey_light)
                    j = 1
                i += 1
            
            i = 0
            for acc in acc_color_dict.keys():
                acc_color_dict[acc] = col_palette[i]
                i += 1
#            end_color = datetime.datetime.now()
#            print("Building color palette : {0}".format(end_color-end_query))
#             print(acc_color_dict)
            
#             geo_file_url = staticfiles_storage.url('geo_files/nutspt_0.json')
#             geo_file_path = staticfiles_storage.path('geo_files/nutspt_0.json')
            geo_file_media = os.path.join(settings.BASE_DIR, 'media/geo_files/nutspt_0.json')

#             with open('/home/melanie/Geo_files/nutspt_0.json') as f:
            with open(geo_file_media, 'r', encoding='utf-8') as f:
                data_file = json.load(f)
            
            coords = {}
            for feature in data_file['features']:
                for key in data.keys():
                    if key == feature['properties']['id']:
                        coords[key] = feature['geometry']['coordinates']
#             print(coords)
            
            plots = {}
            for country, values in data.items():
                rep_p_data = {}
                rep_lu_values = []
                for repartition in values:
                    if repartition.accession.name not in rep_p_data.keys():
                        rep_p_data[repartition.accession.name] = repartition.percentage
                    else:
                        rep_p_data[repartition.accession.name] += repartition.percentage
                    
                    if repartition.annual_area.land_unit not in rep_lu_values:
                        rep_lu_values.append(repartition.annual_area.land_unit)
                
                for acc_name in rep_p_data.keys():
                    rep_p_data[acc_name] = rep_p_data[acc_name] / len(rep_lu_values)
                
                rep_list = sorted(rep_p_data.items(), key=operator.itemgetter(1), reverse=True)
                
                palette = []
                for acc in rep_list:
                    palette.append(acc_color_dict[acc[0]])
                
                pie_data = pd.Series(rep_p_data).reset_index(name='value').rename(columns={'index':'accessions'}).sort_values(by='value', ascending=False)
                pie_data['angle'] = pie_data['value']/pie_data['value'].sum() * 2*pi
                #pie_data['color'] = linear_palette(cc.glasbey_cool, len(rep_p_data))
                pie_data['color'] = palette
                
                plot = figure(plot_height=500,plot_width=500, x_range=(-0.4, 0.4), y_range=(0.5, 1.5), background_fill_color=None, border_fill_color=None)
                plot.wedge(x=0, y=1, radius=0.4,
                           start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
                           line_color="white", fill_color='color', source=pie_data)
                plot.axis.axis_label=None
                plot.axis.visible=False
                plot.grid.grid_line_color = None
                plot.toolbar.logo = None
                plot.toolbar_location = None
                plot.background_fill_alpha = 0.0 
                plot.border_fill_alpha = 0.0
                plot.outline_line_color = None

                #Usage of a webdriver to replace PhantomJS (Bokeh 2.3.3)
                options = Options()
                options.add_argument('--headless')     
                driver = webdriver.Firefox(executable_path=GeckoDriverManager().install(), log_path=settings.GECKO_LOG_PATH, options=options)
                
#                 png = export_png(plot, filename=settings.MEDIA_ROOT+'tmp/'+"plot_"+country+".png", height=50, width=50, webdriver = driver)
                
                plot.output_backend = "svg"
                png = export_svgs(plot, filename=settings.MEDIA_ROOT+'tmp/'+"plot_"+country+".svg", height=50, width=50, webdriver = driver)
                driver.quit()
                
                title = "Repartition of cultivated acreages (%) by varieties ({0}, {1})".format(country, year)
                legend = "Accession"
                
                popup_plot = figure(plot_height=500,plot_width=500, title=title, tooltips="@accessions: @value", x_range=(-0.5, 1.1))
                popup_plot.wedge(x=0, y=1, radius=0.4,
                                 start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
                                 line_color="white", fill_color='color', legend_field='accessions', source=pie_data)
                popup_plot.axis.axis_label=None
                popup_plot.axis.visible=False
                popup_plot.grid.grid_line_color = None
                popup_plot.legend.title = legend
                
                html = file_html(popup_plot, INLINE, "my plot")
                
                plots[country] = [png, html]
                
#            end_plots = datetime.datetime.now()
#            print("Building plot files icons : {0}".format(end_plots-end_color)) 
#             print(plots)
        
        
            fig = folium.Figure()
            m = folium.Map(
                width=960,
                height=600,
                location=[47.4993, 9.6016],
                zoom_start=4,
            )
            m.add_to(fig)
             
            for country, plot in plots.items():
#                 path_head, file_name = os.path.split(plot[0])
                path_head, file_name = os.path.split(plot[0][0])
#                 print(file_name)
                coords[country].reverse()
#                 print(coords)
                
                #To place the middle of the pie chart on the coordinates (instead of the top left corner)
                coords[country][0] += 1.5
                coords[country][1] -= 1.5
                
                iframe = branca.element.IFrame(html=plot[1], width=525, height=500)
                popup = folium.Popup(iframe, max_width=500)
                
                
                 
                marker = folium.Marker(coords[country])
                #icon = folium.DivIcon(html='<img src="{{ MEDIA_URL }}/tmp/'+file_name+'">')
                icon = folium.DivIcon(html='<img src="'+settings.MEDIA_URL+'/tmp/'+file_name+'">')
#                 icon = folium.CustomIcon(icon_image=plot[0])
                marker.add_child(icon)
                marker.add_child(popup)
                m.add_child(marker)
                
            fig.render()
#            end_map = datetime.datetime.now()
#            print("Building Map : {0}".format(end_map-end_plots)) 
            tag_fields = {'title':self.title + ' in {0}'.format(year),
                          'n_page':n_page,
                          'formyear':formyear,
                          'project':request.session['projects'],
                          'map':fig}
            
            return render(request, self.template, tag_fields)


      
def get_data_and_countries_codes(repartitions, countries, data, no_val_countries):
    """
      Get the data according to countries codes.
      :param VarietyRepartition repartitions: repartitions filtered according to the query
      :param list countries: countries of the scale conversion file
      :param dict data: contains the repartitions splitted by countries
      :param list no_val_countries: countries that are not in the scale conversion file
    """
    for rep in repartitions:
        if rep.annual_area.land_unit.country.code in countries:
            if rep.annual_area.land_unit.country.code not in data.keys():
                data[rep.annual_area.land_unit.country.code] = [rep]
            else:
                data[rep.annual_area.land_unit.country.code].append(rep)
        else:
            if rep.annual_area.land_unit.country.code not in no_val_countries: 
                no_val_countries.append(rep.annual_area.land_unit.country.code) 
    
    return data, no_val_countries

def get_map_data_from_raw_data(raw_data, res_p_data, rep_p_data, acc, acc_h_data):
    """
      Get the data to display on the map from RawData object.
      :param RawData raw_data: raw data to display on the bar plot and pie chart
      :param dict res_p_data: trait data
      :param dict rep_p_data: repartition data
      :param Accession acc: accession linked to the raw data
      :param dict acc_h_data: count the number of accessions for values of the common scale
    """
    if raw_data and raw_data[0].elaborated_data:
        if raw_data[0].elaborated_data not in res_p_data.keys():
            res_p_data[raw_data[0].elaborated_data] = rep_p_data[acc]
        else:
            res_p_data[raw_data[0].elaborated_data] += rep_p_data[acc]
             
        if raw_data[0].elaborated_data not in acc_h_data.keys():
            acc_h_data[raw_data[0].elaborated_data] = 1
        else:
            acc_h_data[raw_data[0].elaborated_data] += 1
             
    else:
        if 'NA' not in res_p_data.keys():
            res_p_data['NA'] = rep_p_data[acc]
        else:
            res_p_data['NA'] += rep_p_data[acc]
     
        if 'NA' not in acc_h_data.keys():
            acc_h_data['NA'] = 1
        else:
            acc_h_data['NA'] += 1
            
    return res_p_data, acc_h_data
    
def format_barplot_data(na_data, res_color_dict, res_p_data, acc_h_data):
    """
      Format the data that will be displayed on the graphs and prepare 
      the color palettes for pie charts and bar plots.
      :param boolean na_data: if NA data should be displayed or not
      :param dict res_color_dict: contains the color associated to the common scale
      :param dict res_p_data: data to display on the first bar plot
      :param dict acc_h_data: data to display on the second bar plot
    """
    # If NA data are not displayed
    if na_data == "None":
        if 'NA' in res_color_dict.keys():
            del res_color_dict['NA']
        if 'NA' in res_p_data.keys():
            del res_p_data['NA']
        if 'NA' in acc_h_data.keys():
            del acc_h_data['NA']
        
    for grade in res_color_dict.keys():
        if grade not in res_p_data.keys():
            res_p_data[grade] = 0
        
        if grade not in acc_h_data.keys():
            acc_h_data[grade] = 0 
            
    res_list = sorted(res_p_data.items(), key=operator.itemgetter(0))
#     print(res_list)
    
    acc_list = sorted(acc_h_data.items(), key=operator.itemgetter(0))
#     print(acc_list)
    
    palette = []
    for value in res_list:
        palette.append(res_color_dict[value[0]])
    
    palette_acc = []
    for value in acc_list:
        palette_acc.append(res_color_dict[value[0]])
        
    return res_p_data, acc_h_data, palette, palette_acc


class VarietyMapResistanceViewer(LoginRequiredMixin, View):
    """
      
      :var str template: html template for the rendering of the view
      :var str title: title displayed on the template
      :var dict tag_fields: dictionary containing the parameters passed to the template
    """
    template = 'dataview/map_resistance_viewer.html'
    title = "Trait Deployment Map"
       
    def get(self, request, *args, **kwargs):
        formproject = DataviewProjectForm()
        username = request.user.login
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
   
        formproject.fields['project'].queryset=projects.order_by('name')
   
        n_page = 0
        return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formproject':formproject})
       
    def post(self, request, *args, **kwargs):
#         print(request.POST)
        formproject = DataviewProjectForm(request.POST)
        #Get the actual page number
        n_page = int(request.POST['n_page'])
          
        if int(n_page) == 1:
            if formproject.is_valid():
                projects = request.POST.getlist('project')
                request.session['projects'] = projects
            else:
                return render(request,self.template,{'title':self.title, 'n_page':0, 'formproject':formproject})
          
            accessions_initial = AccessionLocal.objects.filter(projects__in=request.session['projects']).values_list('accession').distinct()
            traits = Trait.objects.filter(projects__in=request.session['projects'], category=2).distinct()

            #To remove traits that have no scale file
            for t in traits:
                if not t.scale_file:
                    traits = traits.exclude(id=t.id)

            raw_data = RawData.objects.filter(type=1, accession__in=accessions_initial, trait__in=traits)
            traits = Trait.objects.filter(id__in=raw_data.values_list('trait', flat=True)).distinct()
              
            formtrait = DataviewTraitListForm()
            formtrait.fields['trait'].queryset = traits
            
            form_na = DataviewDisplayNAData()
              
            return render(request,self.template,{'title':self.title, 'n_page':n_page, 'formtrait':formtrait, 'form_na':form_na})
          
        if int(n_page) == 2:
              
            formtrait = DataviewTraitListForm(request.POST)
            form_na = DataviewDisplayNAData(request.POST)
            
            if formtrait.is_valid() and form_na.is_valid():
                trait = request.POST.get('trait')
                na_data = str(request.POST.get('na_data'))
                request.session['trait'] = trait
                request.session['na_data'] = na_data
            else:
                return render(request,self.template,{'title':self.title, 'n_page':1, 'formtrait':formtrait, 'form_na':form_na})
            
            trait = Trait.objects.get(id=trait)
              
            filtered_accessions = AccessionLocal.objects.filter(projects__in=request.session['projects']).values_list('accession__id', flat=True).distinct()
            repartitions = VarietyRepartition.objects.filter(accession__in=filtered_accessions, data_type=1)
            filtered_raw_data = RawData.objects.filter(type=1, accession__in=filtered_accessions, trait=trait)
            
            list_years = repartitions.values_list('annual_area__year', flat=True).order_by('annual_area__year').distinct()
            list_years_rd = filtered_raw_data.values_list('year', flat=True).order_by('year').distinct()
            
            years = []
            for year in list_years:
                if year in list_years_rd:
                    years.append([year,year])

            if 'year' in request.POST.keys():
                formyear = DataviewMapYearListForm(request.POST)
              
                if formyear.is_valid():
                    year = request.POST.getlist('year')[0]
                    print(year)
                else:
                    messages.add_message(request, messages.ERROR, "An error occurred.")
                    formyear = DataviewMapYearListForm()
                    year = years[-1][0] #To get last element of the list
            else:
                if not years:
                    messages.add_message(request, messages.INFO, "There is no data to display for this trait.")
                    return render(request,self.template,{'title':self.title, 'n_page':2})
                else:
                    formyear = DataviewMapYearListForm()
                    year = years[-1][0]
                    print(year)
              
            request.session['year'] = year
              
            formyear.fields['year'].choices = years 
            formyear.fields['year'].initial = year
              
            repartitions = VarietyRepartition.objects.select_related('annual_area', 'annual_area__land_unit','accession').filter(accession__in=filtered_accessions, annual_area__year=year, data_type=1).distinct()
            
            ####### Ajouts demo ######
            
            country_select = repartitions.values_list('annual_area__land_unit__country', flat=True).order_by('annual_area__land_unit__country').distinct()
            land_unit_select = repartitions.values_list('annual_area__land_unit', flat=True).order_by('annual_area__land_unit').distinct()
            
#             print(country_select)
#             print(land_unit_select)
            selected_lu = []
            for country in country_select:
                land_units = LandUnit.objects.filter(country=country, id__in=land_unit_select)
                nuts_min = land_units.aggregate(Min('nuts'))
                for lu in LandUnit.objects.filter(country=country, nuts=nuts_min["nuts__min"]).distinct():
                    selected_lu.append(lu)
#             print(selected_lu)
            
            selected_aa = AnnualArea.objects.filter(land_unit__in=selected_lu, year=year).distinct()
#             print(selected_aa)
            
            repartitions = VarietyRepartition.objects.select_related('annual_area', 'annual_area__land_unit','accession').filter(accession__in=filtered_accessions, annual_area__in=selected_aa, data_type=1).distinct()
            
            ####### Fin ajouts demo ######
            
            repartitions = validate_queryset_repartition_data(repartitions)
            
            #Get the countries of the scale conversion file
            countries, country_values = get_country_values_from_scale_file(request, trait.scale_file)
            
            data = {}
            no_val_countries = []

            data, no_val_countries = get_data_and_countries_codes(repartitions, countries, data, no_val_countries)
             
            #Display a message for the countries that can't be displayed on the map (no conversion in the file)
            if no_val_countries:
                if len(no_val_countries) == 1:
                    messages.add_message(request, messages.WARNING, "There is no conversion to the common scale in the file for the country {0}. The data can't be displayed on the map.".format(no_val_countries[0]))
                else:
                    messages.add_message(request, messages.WARNING, "There is no conversion to the common scale in the file for the countries: {0}. The data can't be displayed on the map.".format(', '.join(no_val_countries)))
                
                
            #Exemple de comment utiliser les palettes ensuite
#             res_grades = ['1','2','3','4','5','6','7','8','9']
#             
#             colors = linear_palette(cc.fire, len(res_grades))
#             print(colors)
#             
#             res_color_dict = {}
#             for nb in range(len(res_grades)):
#                 res_color_dict[res_grades[nb]] = colors[nb]
#             res_color_dict['NA'] = '#0086B3'
#             res_color_dict['Unknown'] = '#808080'
             
#             res_color_dict = {'1':'#C00000',
#                               '2':'#C00000',
#                               '3':'#FF0000',
#                               '4':'#FF0000',
#                               '5':'#FFC000',
#                               '6':'#FFC000',
#                               '7':'#FFFF00',
#                               '8':'#FFFF00',
#                               '9':'#92D050',
#                               'NA':'#0086B3',
#                               'Unknown':'#808080'}
            
#             test_colors = linear_palette(list(reversed(cc.CET_R2)), 5)
#             print(test_colors)
#             test_colors2 = linear_palette(list(reversed(cc.CET_R2)), 9)
#             print(test_colors2)

#             cs_values = ['1','2','3','4','5','6','7','8','9','NA']
            cs_values = get_common_scale_values(country_values)

#             colors = linear_palette(list(reversed(cc.CET_R2)), len(cs_values)-1)
            
            colors_list = list(reversed(cc.CET_R2))
            del colors_list[194:256]
            
            colors = linear_palette(colors_list, len(cs_values)-1)
            
            res_color_dict = {}
            for nb in range(len(cs_values)-1):
                res_color_dict[cs_values[nb]] = colors[nb]
                
            res_color_dict['NA'] = '#808080'            
              
            geo_file_media = os.path.join(settings.BASE_DIR, 'media/geo_files/nutspt_0.json')
          
            with open(geo_file_media, 'r', encoding='utf-8') as f:
                data_file = json.load(f)
              
            coords = {}
            for feature in data_file['features']:
                for key in data.keys():
                    if key == feature['properties']['id']:
                        coords[key] = feature['geometry']['coordinates']
              
              
            raw_data_qs = RawData.objects.filter(type=1, trait=trait, year=year, accession__in=filtered_accessions, country__in=data.keys())
                       
            plots = {}
                
            for country, values in data.items():
                rep_p_data = {}
                rep_lu_values = []
                res_p_data = {}
                acc_h_data = {}
                for repartition in values:
                    if repartition.accession not in rep_p_data.keys():
                        rep_p_data[repartition.accession] = repartition.percentage
                    else:
                        rep_p_data[repartition.accession] += repartition.percentage
                     
                    if repartition.annual_area.land_unit not in rep_lu_values:
                        rep_lu_values.append(repartition.annual_area.land_unit)
                 
#                 print("nb acc ", len(rep_p_data.keys()))
                 
                for acc in rep_p_data.keys():
                    rep_p_data[acc] = rep_p_data[acc] / len(rep_lu_values)
                     
                    raw_data = raw_data_qs.filter(type=1, trait=trait, year=year, accession=acc, country=country).distinct()
                    res_p_data, acc_h_data = get_map_data_from_raw_data(raw_data, res_p_data, rep_p_data, acc, acc_h_data)
                
#                 print(acc_h_data)
                
                res_p_data, acc_h_data, palette, palette_acc = format_barplot_data(na_data, res_color_dict, res_p_data, acc_h_data)
                
                pie_data = pd.Series(res_p_data).reset_index(name='value').rename(columns={'index':'scale'}).sort_values(by='scale')
                pie_data['angle'] = pie_data['value']/pie_data['value'].sum() * 2*pi
                pie_data['color'] = palette
                #pie_data['legend'] = list(range(0, len(palette)))

#                 print(pie_data)
                
                acc_data = pd.Series(acc_h_data).reset_index(name='value').rename(columns={'index':'scale'}).sort_values(by='scale')
                acc_data['color'] = palette_acc
                
#                 print(acc_data)
                
                plot = figure(plot_height=500,plot_width=500, background_fill_color = None, border_fill_color = None, x_range=(-0.4, 0.4), y_range=(0.5, 1.5))
                plot.wedge(x=0, y=1, radius=0.4,
                           start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
                           line_color="white", fill_color='color', source=pie_data)
                plot.axis.axis_label=None
                plot.axis.visible=False
                plot.grid.grid_line_color = None
                plot.toolbar.logo = None
                plot.toolbar_location = None
                plot.background_fill_alpha = 0.0
                plot.border_fill_alpha = 0.0
                plot.outline_line_color = None
                
                #Usage of a webdriver to replace PhantomJS (Bokeh 2.3.3)
                options = Options()
                options.add_argument('--headless')     
                driver = webdriver.Firefox(executable_path=GeckoDriverManager().install(), log_path=settings.GECKO_LOG_PATH, options=options)
                
#                 png = export_png(plot, filename=settings.MEDIA_ROOT+'tmp/'+"plot_"+country+".png", height=50, width=50)
                
                plot.output_backend = "svg"
                png = export_svgs(plot, filename=settings.MEDIA_ROOT+'tmp/'+"plot_"+country+".svg", height=50, width=50, webdriver = driver)
                driver.quit()
#                 legend_title = "Scale"
                
                # First plot: Repartition
                popup_plot = figure(plot_height=450,plot_width=450, tooltips="@scale: @value", x_range=pie_data['scale'],
                                    x_axis_label="Scale", y_axis_label='Cultivated acreages (%)', y_range=Range1d(0,100))
                popup_plot.vbar(x='scale', top='value', width=0.9, source=pie_data, #legend='scale',
                                    line_color='white', fill_color='color')
                
                # To display title on 2 lines
#                 popup_plot.add_layout(Title(text="repartition of cultivated acreages (%) ({0}, {1})".format(country, year), align="center"), 'above')
#                 popup_plot.add_layout(Title(text="Resistance scale of varieties according to the", align="center"), 'above')
                popup_plot.add_layout(Title(text="Weighted by acreages (%) ({0}, {1})".format(country, year), align="center"), 'above')
                
#                 popup_plot.legend.orientation = "horizontal"
#                 popup_plot.legend.location = "top_center"
#                 popup_plot.legend.background_fill_alpha = 0.6
                
                # Second plot: Number of varieties
                popup_plot_acc = figure(plot_height=450,plot_width=450, tooltips="@scale: @value", x_range=acc_data['scale'],
                                    x_axis_label="Scale", y_axis_label='Number of varieties')
                popup_plot_acc.y_range.start = 0
                
                popup_plot_acc.vbar(x='scale', top='value', width=0.9, source=acc_data, #legend='scale',
                                    line_color='white', fill_color='color')
                
                # To display title on 2 lines
#                 popup_plot_acc.add_layout(Title(text="the number of varieties ({0}, {1})".format(country, year), align="center"), 'above')
#                 popup_plot_acc.add_layout(Title(text="Resistance scale of varieties according to", align="center"), 'above')
                popup_plot_acc.add_layout(Title(text="Weighted by number of varieties ({0}, {1})".format(country, year), align="center"), 'above')
                
#                 popup_plot_acc.legend.orientation = "horizontal"
#                 popup_plot_acc.legend.location = "top_center"
#                 popup_plot_acc.legend.background_fill_alpha = 0.6
                
#                 #########  For pop up pie chart
#                 popup_plot = figure(plot_height=500,plot_width=500, tooltips="@scale: @value", x_range=(-0.5, 1.1))
#                 w = popup_plot.wedge(x=0, y=1, radius=0.4,
#                                      start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
#                                      line_color="white", fill_color='color', legend='scale', source=pie_data)
# #                                 line_color="white", fill_color='color', source=pie_data)
#                 popup_plot.axis.axis_label=None
#                 popup_plot.axis.visible=False
#                 popup_plot.grid.grid_line_color = None
#                 
#                 # To display title on 2 lines
#                 popup_plot.add_layout(Title(text="of cultivated acreages (%) ({0}, {1})".format(country, year), align="center"), 'above')
#                 popup_plot.add_layout(Title(text="Resistance scale of varieties according to the repartition", align="center"), 'above')
                
                # To have the legend in order of the scale (low to high)
#                 legend_items = []
#                 for scale, color in res_color_dict.items():
#                     index = pie_data[pie_data['scale']==scale].index.values
#                     id_val = pie_data.loc[index[0]]['legend']
#                     item = LegendItem(label=scale, renderers=[w], index=id_val)
#                     legend_items.append(item)
#               
#                 legend = Legend(items=legend_items)
#                 popup_plot.add_layout(legend)
                
#                 popup_plot.legend.title = legend_title
#                 popup_plot_acc.legend.title = legend_title
            
                title = Div(text='<a>Resistance to '+trait.name+'</a>', style={'text-align':'center',
                                                                               'width':'900px',
                                                                               'font-weight': 'bold',
                                                                               'color': '#474747',
                                                                               'font-family':'Helvetica'})

            
                html = file_html(column(title, row(popup_plot, popup_plot_acc)), INLINE, "my plot")
            
                plots[country] = [png, html]
                
            fig = folium.Figure()
            m = folium.Map(
                width=960,
                height=600,
                location=[47.4993, 9.6016],
                zoom_start=4,
            )
            m.add_to(fig)
            
            
            for country, plot in plots.items():
#                 path_head, file_name = os.path.split(plot[0])
                path_head, file_name = os.path.split(plot[0][0])
                coords[country].reverse()
                
                #To place the middle of the pie chart on the coordinates (instead of the top left corner)
                coords[country][0] += 1.5
                coords[country][1] -= 1.5
                
                
                iframe = branca.element.IFrame(html=plot[1], width=925, height=500)
                popup = folium.Popup(iframe, max_width=900)
                
                 
                marker = folium.Marker(coords[country])
                icon = folium.DivIcon(html='<img src="'+settings.MEDIA_URL+'/tmp/'+file_name+'">')
                marker.add_child(icon)
                marker.add_child(popup)
                m.add_child(marker)
 
            fig.render()

            tag_fields = {'title':self.title + ' in {0}'.format(year),
                          'n_page':n_page,
                          'formyear':formyear,
                          'project':request.session['projects'],
                          'trait':trait,
                          'na_data':na_data,
                          'map':fig}
            
            return render(request, self.template, tag_fields)
                
                
                
                
