"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from django.db import models
from django.db.models import Max
from django_countries import countries as dc_countries
from django_countries.fields import CountryField
from django.core.exceptions import ValidationError
from django.dispatch import receiver

from commonfct.constants import LANDSCAPE_DATA_TYPES, LANDSCAPE_UNITS, LANDSCAPE_STATUS
from landscape.managers import LandUnitManager, AnnualAreaManager, VarietyRepartitionManager
from landscape.signals import multiplications_updated, estimated_data_submitted,\
                              repartition_data_submitted

NUTS_TYPES = ((1, 'NUTS 1'),
              (2, 'NUTS 2'),
              (3, 'NUTS 3')
              )

class LandUnit(models.Model):
    """
      The LandUnit model gives informations about the land units
      (portions of countries).
      
      :var CharField name: name of the land unit
      :var CountryField country: country of the land unit
      :var IntegerField nuts: NUTS level of the land unit
      :var CharField code: code of the land unit
    """
    name = models.CharField(max_length = 100)
    country = CountryField()
    nuts = models.IntegerField(choices=NUTS_TYPES)
    code = models.CharField(max_length = 50, blank=True, null=True)
    
    objects = LandUnitManager()
    
    def __str__(self):
        return '%s (%s)'%(self.name, self.country)
    
    class Meta:
        unique_together=("name","country")
        ordering = ['name','country']


        
class AnnualArea(models.Model):
    """
      The AnnualArea model gives informations about the annual repartition of varieties.
      
      :var ForeignKey land_unit: land unit of the cultivated area
      :var IntegerField year: year where the cultivated area was assigned in the land unit
      :var CharField cultivated_area: value of the cultivated area allowed on this year and land unit
      :var CharField multiplied_area: value of the multiplied area allowed on this year and land unit
    """
    land_unit = models.ForeignKey('LandUnit', on_delete=models.CASCADE)
    year = models.IntegerField()
    data_type = models.IntegerField(choices=LANDSCAPE_DATA_TYPES)
    deployment = models.FloatField(blank=True, null=True)
    unit = models.IntegerField(choices=LANDSCAPE_UNITS)
    description = models.TextField(max_length=500, blank=True, null=True)
    auto_generated = models.BooleanField(default=False)
    
    objects = AnnualAreaManager()
    
    def __str__(self):
        return '%s in %s'%(self.land_unit, self.year)
    
    class Meta:
        unique_together=("land_unit","year","data_type","auto_generated")
        ordering = ['land_unit','year','data_type']
        
    def save(self, *args, **kwargs):
        self.clean()
        super(AnnualArea, self).save(*args, **kwargs)
        
    def clean(self):
        #To delete auto generated annual area when a user submit annual areas
        if self.data_type == 1 and self.auto_generated == False:
            auto_g_aa = AnnualArea.objects.filter(land_unit=self.land_unit, year=self.year, data_type=self.data_type, auto_generated=True).distinct()
            if auto_g_aa:
                auto_g_aa[0].delete()


@receiver(models.signals.post_delete, sender=AnnualArea)
def recalculate_g_data_after_aa_deletion(sender, instance, **kwargs):
    """
      To automatically (re)generate general annual areas and repartitions 
      when a user delete a general annual area (user created)
    """
#     print(instance)
     
    country = instance.land_unit.country
    land_units = LandUnit.objects.filter(country=country)
    
    #Occurs when a user delete a cultivated annual area linked to a country scale land unit, that has been manually created
    if instance.data_type == 1 and instance.land_unit.nuts == 1 and instance.auto_generated == False:
        nuts_max = land_units.aggregate(Max('nuts'))
        #We check that there is data for this NUTS, else we select another level of NUTS
        for nuts_value in range(nuts_max["nuts__max"], 0, -1):
            annual_areas = AnnualArea.objects.filter(land_unit__in=LandUnit.objects.filter(country=country, nuts=nuts_value), year=instance.year, data_type=1)
            repartitions = VarietyRepartition.objects.filter(annual_area__in=annual_areas, data_type=1).distinct()
            if repartitions:
                nuts_max["nuts__max"] = nuts_value
                break
             
        acc_val = {}
        if repartitions:
            repartitions = validate_queryset_repartition_data(repartitions)
             
            for rep in repartitions:
                if rep.accession not in acc_val.keys():
                    acc_val[rep.accession] = rep.deployment
                else:
                    acc_val[rep.accession] += rep.deployment
             
            #We get back the unit to create annual areas and repartitions 
            unit = repartitions.values_list('unit', flat=True).distinct('unit').order_by('unit')
            if len(unit) > 1:
                print("Different units for the global repartition, the first one was saved.")
            aa = AnnualArea.objects.create(land_unit=instance.land_unit,
                                           year=instance.year,
                                           data_type=1,
                                           deployment=0,
                                           unit=unit[0],
                                           auto_generated=True)
             
            total_depl = 0
            list_reps = []
            for acc, depl in acc_val.items():
                total_depl += depl
                 
                rep = VarietyRepartition.objects.create(annual_area=aa,
                                                       accession=acc,
                                                       data_type=1,
                                                       percentage=0,
                                                       perc_status=4,
                                                       deployment=depl,
                                                       depl_status=4,
                                                       unit=unit[0])
                 
                rep.save()
                 
                list_reps.append(rep)
                 
            aa.deployment = total_depl
            aa.save()

            for rep in list_reps:
                rep.percentage = (rep.deployment / aa.deployment) * 100
                rep.save()
                    

class Tag(models.Model):
    """
      The Tag model stores tags' names and descriptions.
      
      :var CharField name: Name of the tag
      :var TextField description: Description of the tag
    """
    name = models.CharField(max_length = 100, unique=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    
    class Meta:
        ordering = ['name']
        
    def clean(self):
        aa_fields = AnnualArea._meta.get_fields()
        aa_fields_names = [f.name for f in aa_fields]
        
        if self.name.lower().replace(' ','_') in aa_fields_names:
            raise ValidationError('A field with this name already exists for annual area.')
    
    def __str__(self):
        return self.name


class LandscapeRawData(models.Model):
    """
      The LanscapeRawData model stores data linked to a Tag and an 
      AnnualArea.
      
      :var CharField value: Value of the data
      :var ForeignKey tag: Link to a tag
      :var ForeignKey annual_area: Link to an annual area
    """
    value = models.CharField(max_length = 100)
    tag = models.ForeignKey('Tag', on_delete=models.CASCADE)
    annual_area = models.ForeignKey('AnnualArea', on_delete=models.CASCADE)
    
    class Meta:
        unique_together=("tag","annual_area")

    def __str__(self):
        return self.value

class VarietyRepartition(models.Model):
    """
      The VarietyRepartition model gives informations about the repartition of a variety.
      
      :var CharField cultivated_percentage: percentage of the cultivated area on the total allowed surface
      :var CharField cultivated_area : value of the the cultivated area
      :var CharField multiplied_proportion: proportion of the multiplied area on the total allowed surface
      :var CharField multiplied_quantity: quantity of seeds used
      :var CharField multiplied_area: value of the multiplied area
      :var ForeignKey accession: accession for which the values are measured
      :var ForeignKey annual_repartition : annual repartition to compute the different values
    """
    annual_area = models.ForeignKey('AnnualArea', on_delete=models.CASCADE)
    accession = models.ForeignKey('accession.Accession', on_delete=models.CASCADE)
    data_type = models.IntegerField(choices=LANDSCAPE_DATA_TYPES)
    percentage = models.FloatField(blank=True, null=True)
    perc_status = models.IntegerField(choices=LANDSCAPE_STATUS)
    deployment = models.FloatField(blank=True, null=True)
    depl_status = models.IntegerField(choices=LANDSCAPE_STATUS)
    unit = models.IntegerField(choices=LANDSCAPE_UNITS)
    
    tags = models.ManyToManyField('Tag', blank = True)
    
    objects = VarietyRepartitionManager()
    
    def __str__(self):
        return 'Variety repartition for %s on %s'%(self.accession, self.annual_area)
    
    class Meta:
        unique_together=("annual_area","accession","data_type","perc_status","depl_status")
        ordering = ['annual_area','accession','data_type']
        
    def save(self, *args, **kwargs):
        self.clean()
        super(VarietyRepartition, self).save(*args, **kwargs)
    
    def clean(self):
        status = [1,2,3]
        if self.data_type == 2:
            repartitions = VarietyRepartition.objects.filter(annual_area=self.annual_area, accession=self.accession, data_type=self.data_type).distinct()
            if len(repartitions) > 1 :
                raise ValidationError('This Variety Repartition already exists in the database : {0}.'.format(repartitions[0]))
        
        else:
            if self.perc_status != 3 or self.depl_status != 3:
                repartitions = VarietyRepartition.objects.filter(annual_area=self.annual_area, accession=self.accession, data_type=self.data_type, perc_status__in=status, depl_status__in=status).distinct()
                repartitions = repartitions.exclude(perc_status=3, depl_status=3)
                if len(repartitions) > 1:
                    if repartitions[0].perc_status == 2 or repartitions[0].depl_status == 2:
                        type = "Raw calculated data"
                    else:
                        type = "Raw data"
                    raise ValidationError('This Variety Repartition already exists in the database : {0} ({1}).'.format(repartitions[0], type))
                

@receiver(models.signals.post_delete, sender=VarietyRepartition)
def delete_estimated_data_and_calculate_new_value(sender, instance, **kwargs):
    """
      Delete cultivated estimated data calculated from multiplied data when 
      the multiplied data is deleted.
      Calculate the new deployment value for the annual area, and 
      re-calculate the percentages values of the variety repartitions.
      Calculate the estimated values according to new percentages values.
    """
#     print(instance)
    
    if instance.data_type == 1:
        recalculate_general_data_after_rep_deletion(instance)
    
    if instance.data_type == 2:
        next_year = instance.annual_area.year + 1
        next_annual_areas = AnnualArea.objects.filter(land_unit=instance.annual_area.land_unit, year=next_year, data_type="1").distinct()
        next_annual_area = None
        
        if next_annual_areas :
            next_annual_area = next_annual_areas[0]
            estimated_reps = VarietyRepartition.objects.filter(annual_area=next_annual_area, accession=instance.accession, data_type=1, perc_status=3, depl_status=3).distinct()
            if estimated_reps:
                for data in estimated_reps:
                    data.delete()
        
        #Re-calculate the total deployment value of the annual area without the deleted instance
        annual_area = AnnualArea.objects.get(id=instance.annual_area.id)
        annual_area.deployment -= instance.deployment
        annual_area.save()
        
        repartitions = VarietyRepartition.objects.filter(annual_area=annual_area, data_type='2')
        
        #Percentages are re-calculated according to the new value
        for repartition in repartitions:
            if repartition.perc_status == 3:
                percentage = (repartition.deployment / annual_area.deployment) * 100.0
                repartition.percentage = percentage
                repartition.save()
                
        #Remaining estimated data are re-calculated according to new percentages values 
        if next_annual_area and next_annual_area.deployment != 0:
            other_estimated_reps = VarietyRepartition.objects.filter(annual_area=next_annual_area, data_type="1", perc_status=3, depl_status=3).distinct()
            if other_estimated_reps:
                rep_ids = other_estimated_reps.values_list('id', flat=True)
                estimated_data_submitted.send(sender=VarietyRepartition, rep_ids=rep_ids)
                aa_test_ids = other_estimated_reps.values_list('annual_area__id', flat=True)
                repartition_data_submitted.send(sender=VarietyRepartition, aa_ids=aa_test_ids)

        if not repartitions and annual_area.deployment == 0:
            annual_area.delete()

@receiver(multiplications_updated, sender=VarietyRepartition)
def update_total_multiplied_area(sender, **kwargs):
    """
      Get the list of annual area for which multiplied variety repartition
      has been created or updated and calculate the sum of areas.
      Recalculate the percentages of repartitions based on the annual areas.
    """
    aa_ids = kwargs['aa_ids']
    
    annual_areas = AnnualArea.objects.filter(id__in=aa_ids)
    
    for annual_area in annual_areas:
        total_area = 0
        repartitions = VarietyRepartition.objects.filter(annual_area=annual_area, data_type='2')

        for repartition in repartitions:
            total_area += repartition.deployment
        
        annual_area.deployment = total_area
        annual_area.save()
    
        #Percentages are re-calculated according to the new value
        for repartition in repartitions:
            if repartition.perc_status == 3:
                percentage = (repartition.deployment / annual_area.deployment) * 100.0
                repartition.percentage = percentage
                repartition.save()
                

@receiver(estimated_data_submitted, sender=VarietyRepartition)
def calculate_estimated_data(sender, **kwargs):
    """
      Calculate estimated data based on multiplied data.
    """
    rep_ids = kwargs['rep_ids']
    
    estimated_reps = VarietyRepartition.objects.filter(id__in=rep_ids)
    for estimated_rep in estimated_reps:
        past_year = estimated_rep.annual_area.year-1
        multi_annual_area = AnnualArea.objects.get(land_unit=estimated_rep.annual_area.land_unit, year=past_year, data_type=2)
        multi_rep = VarietyRepartition.objects.get(annual_area=multi_annual_area, accession=estimated_rep.accession, data_type=2)
        
        estimated_rep.deployment = (multi_rep.percentage / 100.0) * estimated_rep.annual_area.deployment
        estimated_rep.percentage = multi_rep.percentage
        
        estimated_rep.save()

@receiver(repartition_data_submitted, sender=VarietyRepartition)
def calculate_country_scale_data(sender, **kwargs):
    """
      When variety repartition data are saved at land unit scale inferior to country level 
      new land unit, annual areas and variety repartitions are saved at country level 
      to process information easily.
    """
    aa_ids = kwargs['aa_ids']
#     print(aa_ids)
    
    mult_aas = AnnualArea.objects.filter(id__in=aa_ids, data_type=2)
    
    if mult_aas:
        for mult_aa in mult_aas:
            next_aa = AnnualArea.objects.filter(land_unit=mult_aa.land_unit, year=mult_aa.year+1, data_type=1).distinct()
            if next_aa:
                est_repartitions = VarietyRepartition.objects.filter(annual_area=next_aa[0], data_type=1).distinct()
                if not est_repartitions:
                    aa_ids.remove(mult_aa.id)
            else:
                aa_ids.remove(mult_aa.id)
    
    country_ids = AnnualArea.objects.filter(id__in=aa_ids).values_list('land_unit__country',flat=True).distinct('land_unit__country').order_by('land_unit__country')
                
    for country_id in country_ids:
        land_units = LandUnit.objects.filter(country=country_id)
        lu_country = land_units.filter(nuts=1)
        
        dict_years = get_country_scale_data(land_units, country_id)
            
        #No general land unit for the country, we create it 
        if not lu_country:
#             print("pas de lu general")
            
            #Creation of land unit
            lu = LandUnit.objects.create(name=dict(dc_countries)[country_id],
                                         country=country_id,
                                         nuts=1)
            lu.save()
            
            
            for year, values in dict_years.items():
                aa = AnnualArea.objects.create(land_unit=lu,
                                               year=year,
                                               data_type=1,
                                               deployment=0,
                                               unit=values["unit"],
                                               auto_generated=True)
                aa.save()
                total_depl = 0
                
                list_reps = []
                for acc, depl in values["acc_val"].items():
                    total_depl += depl
                    
                    rep = VarietyRepartition(annual_area=aa,
                                             accession=acc,
                                             data_type=1,
                                             percentage=0,
                                             perc_status=4,
                                             deployment=depl,
                                             depl_status=4,
                                             unit=values["unit"])
                    list_reps.append(rep)
                    
                aa.deployment = total_depl
                aa.save()
                
                for rep in list_reps:
                    rep.percentage = (rep.deployment / aa.deployment) * 100
            
                VarietyRepartition.objects.bulk_create(list_reps)
                    
        #A general land unit exists but we don't know if its one created by user or by the database
        else:
#             print("lu country exists") 
        
            for year, values in dict_years.items():
                user_aa = AnnualArea.objects.filter(land_unit__in=lu_country, year=year, data_type=1, auto_generated=False)
                
                if not user_aa:
                    aa, result = AnnualArea.objects.get_or_create(land_unit__in=lu_country, year=year, data_type=1, defaults={'land_unit':lu_country[0],
                                                                                                                              'year':year,
                                                                                                                              'data_type':1,
                                                                                                                              'deployment':0,
                                                                                                                              'unit':values["unit"],
                                                                                                                              'auto_generated':True})
                    
                    total_depl = 0
                    
                    list_reps = []
                    for acc, depl in values["acc_val"].items():
                        total_depl += depl
                        
                        rep, result_rep = VarietyRepartition.objects.get_or_create(annual_area=aa, accession=acc, data_type=1, perc_status=4, depl_status=4, defaults={'annual_area':aa,
                                                                                                                                                                       'accession':acc,
                                                                                                                                                                       'data_type':1,
                                                                                                                                                                       'percentage':0,
                                                                                                                                                                       'perc_status':4,
                                                                                                                                                                       'deployment':depl,
                                                                                                                                                                       'depl_status':4,
                                                                                                                                                                       'unit':values["unit"]})
                        rep.deployment = depl
                        rep.unit = values["unit"]
                        rep.save()
                        
                        list_reps.append(rep)
                        
                    aa.deployment = total_depl
                    aa.unit = values["unit"]
                    aa.save()
                    
                    for rep in list_reps:
                        rep.percentage = (rep.deployment / aa.deployment) * 100
                        rep.save()


def get_country_scale_data(land_units, country_id):
    """
      Get the data needed to compute general repartitions and annual areas.
    """
    # We select the finest level of nuts to add and create global repartitions
    nuts_max = land_units.aggregate(Max('nuts'))

    #We check that there is data for this NUTS, else we select another level of NUTS
    for nuts_value in range(nuts_max["nuts__max"], 0, -1):
        annual_areas = AnnualArea.objects.filter(land_unit__in=LandUnit.objects.filter(country=country_id, nuts=nuts_value), data_type=1)
        repartitions = VarietyRepartition.objects.filter(annual_area__in=annual_areas, data_type=1).distinct()
        if repartitions:
            nuts_max["nuts__max"] = nuts_value
            break
        
    years = annual_areas.values_list('year', flat=True).distinct('year').order_by('year')
    
    dict_years = {}
    for year in years:
        print(year)
        
        acc_val = {}
        repartitions = VarietyRepartition.objects.select_related('accession', 'annual_area').filter(annual_area__in=annual_areas.filter(year=year), data_type=1)
        
        if repartitions:
            repartitions = validate_queryset_repartition_data(repartitions)
    
            for rep in repartitions:
                if rep.accession not in acc_val.keys():
                    acc_val[rep.accession] = rep.deployment
                else:
                    acc_val[rep.accession] += rep.deployment
            
            #We get back the unit to create annual areas and repartitions
            unit = repartitions.values_list('unit', flat=True).distinct('unit').order_by('unit')
            if len(unit) > 1:
                print("Different units for the global repartition, the first one was saved.")
    
            dict_years[year] = {'acc_val': acc_val,
                                'unit': unit[0]}
            
    return dict_years

        

def recalculate_general_data_after_rep_deletion(instance):
    """
      To recalculate data at country scale when a user 
      submitted repartition is deleted.
    """
    instance_lu = instance.annual_area.land_unit
    instance_year = instance.annual_area.year
    country = instance_lu.country
    lu_country = LandUnit.objects.filter(country=country, nuts=1)
    
    #There is a general land unit and it's not the land unit of the instance
    if instance_lu.nuts != 1 and lu_country:

        #If the instance is an estimated data, we have to check if there is a raw data
        if instance.perc_status == 3 and instance.depl_status == 3:
            #Pas sure que l'instance soit dedans car deja supprimee ?
            query = VarietyRepartition.objects.filter(annual_area=instance.annual_area, accession=instance.accession, data_type=1)
            
            #If there is a raw data, it was used for the general calculation so nothing to do
            if len(query) > 0:
                return
            
            #No raw data so this one was used for the general calculation
            else:        
                #We check if there is automatically generated data
                existing_reps = VarietyRepartition.objects.filter(annual_area__in=AnnualArea.objects.filter(land_unit=lu_country[0], year=instance_year, data_type=1), data_type=1)
                
                #If so, we update total deployment
                if existing_reps.filter(perc_status=4, depl_status=4):
                    
                    rep = existing_reps.get(accession=instance.accession)
                    rep.deployment -= instance.deployment
                    
                    aa = AnnualArea.objects.get(land_unit=lu_country[0], year=instance_year, data_type=1)
                    aa.deployment -= instance.deployment
                    
                    rep.save()
                    aa.save()
                    
                    #And delete annual area if deployment equals 0 (will delete repartitions)
                    if aa.deployment == 0:
                        aa.delete()
                    
                    #Else we update the percentages of repartitions
                    else:
                        for repartition in existing_reps:
                            repartition.percentage = (repartition.deployment / aa.deployment) * 100
                            repartition.save()
                            
                            if repartition.deployment == 0 and repartition.percentage == 0:
                                repartition.delete()
                            
        
        else:
            query = VarietyRepartition.objects.filter(annual_area=instance.annual_area, accession=instance.accession, data_type=1)
        
            #We check if there is automatically generated data
            existing_reps = VarietyRepartition.objects.filter(annual_area__in=AnnualArea.objects.filter(land_unit=lu_country[0], year=instance_year, data_type=1), data_type=1)
    
            #If so, we update total deployment
            if existing_reps.filter(perc_status=4, depl_status=4):
                
                rep = existing_reps.get(accession=instance.accession)
                rep.deployment -= instance.deployment
                
                aa = AnnualArea.objects.get(land_unit=lu_country[0], year=instance_year, data_type=1)
                aa.deployment -= instance.deployment
                
                #We add estimated data deployment in place of raw data deployment if there is estimated data
                est_rep = query.filter(perc_status=3, depl_status=3)
                if est_rep:
                    rep.deployment += est_rep[0].deployment
                    aa.deployment += est_rep[0].deployment
                
                rep.save()
                aa.save()
                
                #And delete annual area if deployment equals 0 (will delete repartitions)
                if aa.deployment == 0:
                    aa.delete()
                
                #Else we update the percentages of repartitions
                else:
                    for repartition in existing_reps:
                        repartition.percentage = (repartition.deployment / aa.deployment) * 100
                        repartition.save()
            
                        if repartition.deployment == 0 and repartition.percentage == 0:
                            repartition.delete()



def validate_queryset_repartition_data(repartitions):
    """
      Function used to keep only one value for repartition data.
      As two cultivated data area are allowed, this function 
      keeps the raw data and delete the estimated 
      data value of the queryset.
    """
    ids_to_remove = []
    for rep in repartitions:
        query = repartitions.filter(annual_area=rep.annual_area, accession=rep.accession, data_type=1)
        if len(query) > 1:
            estimated = query.filter(perc_status=3, depl_status=3)
            if estimated: 
                if estimated[0].id not in ids_to_remove:
                    ids_to_remove.append(estimated[0].id)
                
    repartitions = repartitions.exclude(id__in=ids_to_remove)
    
    return repartitions


