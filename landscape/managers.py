"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from operator import ior, iand

from django.apps import apps
from django.db import models
from django.db.models import Q

from commonfct.constants import landscape_units, landscape_data_types


class LandUnitManager(models.Manager):
    """
      Manager for land unit model.
    """
    def refine_search(self, or_and, dico_get):
        """
          Search in all land units according to some parameters
    
          :param string or_and : link between parameters (or, and)
          :param dict dico_get : data to search for
        """
        # Managers are called in accession.model
        # To avoid crossed imports we use apps to get objects from the model
        LandUnit = apps.get_model('landscape','landunit')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
            
        query=Q()
        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                var_search = str( data_type + "__icontains" )
                query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))
        
        return LandUnit.objects.filter(query).distinct()



class AnnualAreaManager(models.Manager):
    """
      Manager for annual area model.
    """
    def refine_search(self, or_and, dico_get):
        """
          Search in all annual areas according to some parameters
    
          :param string or_and : link between parameters (or, and)
          :param dict dico_get : data to search for
        """
        # Managers are called in accession.model
        # To avoid crossed imports we use apps to get objects from the model
        AnnualArea = apps.get_model('landscape','annualarea')
        LandUnit = apps.get_model('landscape','landunit')
        LandscapeRawData = apps.get_model('landscape','landscaperawdata')
        Tag = apps.get_model('landscape','tag')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
            
        query=Q()
        dict_attr={}
        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                
                if data_type == "land unit":
                    query_lu=Q()
                    query_lu |= Q(name__icontains=dico_type_data[data_type])
                    query_lu |= Q(country__icontains=dico_type_data[data_type])
                    query = myoperator(query, Q(land_unit__in=LandUnit.objects.filter(query_lu)))
                
                elif data_type == "data type":
                    list_t = []
                    for key, value in landscape_data_types.items():
                        if dico_type_data[data_type].lower() in value.lower():
                            list_t.append(key)
                    query = myoperator(query, Q(data_type__in=list_t))
                
                elif data_type == "unit":
                    list_u = []
                    for key, value in landscape_units.items():
                        if dico_type_data[data_type].lower() in value.lower():
                            list_u.append(key)
                    query = myoperator(query, Q(unit__in=list_u))
                
                
                else:
                    for i in Tag.objects.all():
                        if str(data_type) == i.name.lower():
                            dict_attr[data_type] = i.id
                    if data_type in dict_attr.keys():
                        tag = Tag.objects.get(id=dict_attr[data_type])
                        list_id = LandscapeRawData.objects.filter(tag=tag, value__icontains=value).values_list("annual_area",flat=True)
                    
                        query = myoperator(query, Q(id__in = list_id))

                    else:        
                        var_search = str( data_type + "__icontains" )
                        var_search = var_search.replace(' ','_')
                        query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))
        
        return AnnualArea.objects.filter(query).distinct()


class VarietyRepartitionManager(models.Manager):
    """
      Manager for variety repartition model.
    """
    def refine_search(self, or_and, dico_get):
        """
          Search in all variety repartitions according to some parameters
    
          :param string or_and : link between parameters (or, and)
          :param dict dico_get : data to search for
        """
        # Managers are called in landscape.model
        # To avoid crossed imports we use apps to get objects from the model
        VarietyRepartition = apps.get_model('landscape','varietyrepartition')
        LandUnit = apps.get_model('landscape','landunit')
        AnnualArea = apps.get_model('landscape','annualarea')
        AccessionLocal = apps.get_model('accession','accessionlocal')
        Accession = apps.get_model('accession','accession')
        Tag = apps.get_model('landscape','tag')
        
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
            
        query=Q()
        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                
                var_search = str( data_type + "__icontains" )
                var_search = var_search.replace(' ','_')
                
                if data_type == "land unit":
                    query_lu=Q()
                    query_lu |= Q(name__icontains=dico_type_data[data_type])
                    query_lu |= Q(country__icontains=dico_type_data[data_type])
                    query = myoperator(query, Q(annual_area__in=AnnualArea.objects.filter(land_unit__in=LandUnit.objects.filter(query_lu))))
                elif data_type == "year":
                    query = myoperator(query, Q(annual_area__in=AnnualArea.objects.filter(year__icontains=dico_type_data[data_type])))
                elif data_type == "accession":
                    query = myoperator(query, Q(accession__in=Accession.objects.filter(name__icontains=dico_type_data[data_type])))
                elif data_type == "data type":
                    list_t = []
                    for key, value in landscape_data_types.items():
                        if dico_type_data[data_type].lower() in value.lower():
                            list_t.append(key)
                    query = myoperator(query, Q(data_type__in=list_t))
                elif data_type == "unit":
                    list_u = []
                    for key, value in landscape_units.items():
                        if dico_type_data[data_type].lower() in value.lower():
                            list_u.append(key)
                    query = myoperator(query, Q(unit__in=list_u))
                
                elif data_type == "tags":
                    if dico_type_data['tags'] == '':
                        query = myoperator(query, Q(tags__isnull=True))
                    else:
                        tags=(Tag.objects.filter(name__icontains=dico_type_data['tags']))
                        query = myoperator(query, Q(tags__in=tags))
                else:
                    query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))

        return VarietyRepartition.objects.filter(query).distinct()



    def viewer_refine_search(self, or_and, dico_get):
        """
          Search in displayed variety repartitions informations according to some parameters
     
          :param string or_and : link between parameters (or, and)
          :param dict dico_get : data to search for
        """
        # Managers are called in landscape.model
        # To avoid crossed imports we use apps to get objects from the model
        AccessionType = apps.get_model('accession','accessiontype')
        VarietyRepartition = apps.get_model('landscape','varietyrepartition')
        Accession = apps.get_model('accession','accession')
        
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand  
         
        query=Q()
        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                 
                var_search = str( data_type + "__icontains" )
                var_search = var_search.replace(' ','_')
                 
                if data_type == "accession":
                    query = myoperator(query, Q(accession__in=Accession.objects.filter(name__icontains=dico_type_data[data_type])))
                elif data_type == "type":
                    query = myoperator(query, Q(accession__in=Accession.objects.filter(type__in=AccessionType.objects.filter(name__icontains=dico_type_data[data_type]))))
                elif data_type == "repartition":
                    query = myoperator(query, Q(percentage__icontains=dico_type_data[data_type]))

        return VarietyRepartition.objects.filter(query).distinct()
        