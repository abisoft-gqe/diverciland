"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import datetime

from dal import autocomplete
from django_countries.fields import CountryField
from django_countries import countries, Countries

from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.forms import ModelForm
from django.forms.widgets import NumberInput

from accession.models import AccessionLocal, Accession
from commonfct.constants import LANDSCAPE_DATA_TYPES, LANDSCAPE_UNITS
from landscape.models import LandUnit, AnnualArea, VarietyRepartition, Tag

class LandUnitForm(ModelForm):
    """
      Form based on the LandUnit model to create land units.
    """
    class Meta:
        fields = '__all__'
        model = LandUnit
        
class LandUnitUploadForm(forms.Form):
    """
      Form used to upload a file to create or update land units
    """
    file = forms.FileField(label='Select the file to upload ')

class TagForm(forms.ModelForm):
    """
      Form based on the Tag model managing data of this model
    """
    class Meta:
        model = Tag
        fields = '__all__'

class AnnualAreaForm(ModelForm):
    """
      Form based on the AnnualArea model to create land units.
    """
    land_unit = forms.ModelChoiceField(required=True,
                                       queryset=LandUnit.objects.all(),
                                       widget=autocomplete.ModelSelect2(url='landunit-autocomplete'))
    year = forms.IntegerField(min_value=1900, max_value=datetime.date.today().year,
                              widget=NumberInput(attrs = {'class': 'form_number_input'}))
    
    deployment = forms.FloatField(required=False, min_value=0, widget=NumberInput(attrs = {'class': 'form_number_input'}))
    
    unit = forms.ChoiceField(required=True,
                             choices=LANDSCAPE_UNITS)
    
    select_tags = forms.ModelMultipleChoiceField(required=False,
                                                 queryset=Tag.objects.all(),
                                                 widget=autocomplete.ModelSelect2Multiple())
    
    class Meta:
        exclude = ['data_type', 'auto_generated']
        model = AnnualArea
        
    def __init__(self, *args, **kwargs):
        if 'tags' in kwargs.keys():
            tags = kwargs['tags']
            if 'data' in kwargs.keys():
                if 'instance' in kwargs.keys():
                    super(AnnualAreaForm, self).__init__(data=kwargs['data'], instance=kwargs['instance'])
                else :
                    super(AnnualAreaForm, self).__init__(data=kwargs['data'])
            else :
                super(AnnualAreaForm, self).__init__()
            
            for tag in tags:
                self.fields[tag.name.lower().replace(' ','_')] = forms.CharField(required = False)
        
        else :
            if 'data' in kwargs.keys():
                super(AnnualAreaForm, self).__init__(data=kwargs['data'])
            else :
                super(AnnualAreaForm, self).__init__()
        
class AnnualAreaUploadForm(forms.Form):
    """
      Form used to upload a file to create or update annual areas
    """
#     data_type = forms.ChoiceField(choices=LANDSCAPE_DATA_TYPES)
    file = forms.FileField(label='Select the file to upload ')


class VarietyRepartitionForm(forms.Form):
    """
      Form based on the VarietyRepartition model to create variety repartition.
    """
    land_unit = forms.ModelChoiceField(required=True,
                                       queryset=LandUnit.objects.all(),
                                       widget=autocomplete.ModelSelect2(url='landunit-autocomplete'))
    year = forms.IntegerField(min_value=1900, max_value=datetime.date.today().year,
                              widget=NumberInput(attrs = {'class': 'form_number_input'}))
    accession = forms.ModelChoiceField(required=True,
                                       queryset=Accession.objects.all(),
                                       widget=autocomplete.ModelSelect2(url='accession-autocomplete'))
     
    data_type = forms.ChoiceField(required=True,
                                  choices=LANDSCAPE_DATA_TYPES)
    
    percentage = forms.FloatField(required=False, min_value=1, max_value=100, widget=NumberInput(attrs = {'class': 'form_number_input'}))
    deployment = forms.FloatField(required=False, min_value=1, widget=NumberInput(attrs = {'class': 'form_number_input'}))

    unit = forms.ChoiceField(required=True,
                             choices=LANDSCAPE_UNITS)
    
    tags = forms.ModelMultipleChoiceField(required=False,
                                          queryset=Tag.objects.all())


class VarietyRepartitionUploadForm(forms.Form):
    """
      Form used to upload a file to create or update variety repartition
    """
    file_data_type = forms.ChoiceField(choices=LANDSCAPE_DATA_TYPES, label='Data type')
    is_calculated = forms.BooleanField(required=False)
    calculate_data = forms.BooleanField(required=False)
    file = forms.FileField(label='Select the file to upload ')

year_choices = []
for year in range(1900,datetime.date.today().year+1):
    year_choices.append([year, year])
    

countries_list = []
for code, name in list(countries):
    countries_list.append([code, name])
countries_list = [('','Choose a Country')] + countries_list

class DataviewCountryFormListOnChange(forms.Form):
    """
      Dataview form to select a country in a list for which data will be displayed.
    """
    country = forms.ChoiceField(required=True,
                                widget=forms.Select(attrs={'onchange': 'submit();'}),
                                choices=countries_list)

year_choices = []
for year in range(1900,datetime.date.today().year+1):
    year_choices.append([year, year])

class DataviewYearForm(forms.Form):
    """
      Dataview form to select one or several year(s) in a list for which data will be displayed.
    """
    year = forms.MultipleChoiceField(choices=year_choices,
                                     widget=forms.SelectMultiple(attrs={'style':'min-width: 100px;','size':'13'}),
                                     required=True)


class DataviewLandUnitListForm(forms.Form):
    """
      Dataview form to select a land unit in a list.
    """
    land_unit = forms.ModelChoiceField(required=True,
                                       widget=forms.Select(attrs={'onchange': 'submit();'}),
                                       queryset=LandUnit.objects.all(),
                                       empty_label="Choose a Land Unit")
    
class DataviewYearListForm(forms.Form):
    """
      Dataview form to select a year in a list.
    """
    year = forms.ChoiceField(required=True,
#                              widget=forms.Select(attrs={'onchange': 'submit();'}),
                             choices= year_choices)


class DataviewMapYearListForm(forms.Form):
    """
      Dataview form to select a year in a list without a label (map dataview).
    """
    year = forms.ChoiceField(required=True,
                             widget=forms.Select(attrs={'onchange': 'submit();'}),
                             choices= year_choices,
                             label=False)


class DataviewAccessionLifeCycleForm(forms.Form):
    """
      Dataview form to select accessions with autocomplete.
      The clean method checks that one of the two fields is filled.
    """
    accession_search = forms.ModelMultipleChoiceField(required=True,
                                               queryset=Accession.objects.all(),
                                               widget=autocomplete.ModelSelect2Multiple())
    
