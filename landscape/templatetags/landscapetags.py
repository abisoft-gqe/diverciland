"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from django import template

from commonfct.constants import landscape_status, landscape_data_types
from landscape.models import VarietyRepartition, AnnualArea
register = template.Library()

@register.simple_tag
def get_attr_status(rep, attr):
    try:
        repartition = VarietyRepartition.objects.get(id=rep.id)
    except:
        return ''
    
    if attr == 'percentage':
        return landscape_status[repartition.perc_status]
    elif attr == 'deployment':
        return landscape_status[repartition.depl_status]
    else:
        return ''

@register.simple_tag
def get_attr_color(rep, attr):
    try:
        repartition = VarietyRepartition.objects.get(id=rep.id)
    except:
        return ''
    
    colors = {1:'color:black;',
              2:'color:#0eaf88;',
              3:'color:#0e88af;',
              4:'color:#0e88af;'}
    
    if attr == 'percentage':
        return colors[repartition.perc_status]
    elif attr == 'deployment':
        return colors[repartition.depl_status]
    else:
        return ''
    

@register.simple_tag
def get_aa_status(aa):
    try:
        annual_area = AnnualArea.objects.get(id=aa.id)
    except:
        return ''
    
    if annual_area.data_type == 2:
        return 'Database computed'
    elif annual_area.data_type == 1 and annual_area.auto_generated == True:
        return 'Automatically generated'
    else:
        return 'Raw data'

@register.simple_tag
def get_aa_color(aa):
    try:
        annual_area = AnnualArea.objects.get(id=aa.id)
    except:
        return ''
    
    if annual_area.data_type == 2:
        return 'color:#0e88af;'
    elif annual_area.data_type == 1 and annual_area.auto_generated == True:
        return 'color:#0e88af;'
    else:
        return 'color:black;'
    