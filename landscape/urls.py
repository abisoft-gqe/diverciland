"""
    DiverCILand is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2019, Melanie Polart-Donat

    This file is part of DiverCILand

    DiverCILand is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from django.urls import path
from landscape.views import LandUnitManagement, LandUnitEdit, AnnualAreaManagement, AnnualAreaEdit,\
                            VarietyRepartitionManagement, LifeCycleViewer,\
                            VarietyRepartitionViewer, VarietyMapRepartitionViewer, TagManagement,\
                            TagEdit, AnnualAreaHeadersRenderer, VarietyMapResistanceViewer

urlpatterns = [
    path('landunit/', LandUnitManagement.as_view(), name='land_unit_management'),
    path('landunit/<int:object_id>/', LandUnitEdit.as_view(), name='land_unit_edit'),
    path('annualarea/', AnnualAreaManagement.as_view(), name='annual_area_management'),
    path('annualarea/<int:object_id>/', AnnualAreaEdit.as_view(), name='annual_area_edit'),
    path('varietyrepartition/', VarietyRepartitionManagement.as_view(), name='repartition_management'),
#    path('varietyrepartition/<int:object_id>/', VarietyRepartitionEdit.as_view(), name='repartition_edit'),
    path('lifecycleviewer/', LifeCycleViewer.as_view(), name='life_cycle_viewer'),
    path('varietyrepartitionviewer/', VarietyRepartitionViewer.as_view(), name='repartition_viewer'),
    path('varietymaprepartitionviewer/', VarietyMapRepartitionViewer.as_view(), name='map_repartition_viewer'),
    path('varietymapresistanceviewer/', VarietyMapResistanceViewer.as_view(), name='map_resistance_viewer'),
    path('tag/', TagManagement.as_view(), name='tag_management'),
    path('tag/<int:object_id>/', TagEdit.as_view(), name='tag_edit'),
    
    path('annualareaheadersrenderer/',AnnualAreaHeadersRenderer.as_view(), name='annualareaheadersrenderer')
]