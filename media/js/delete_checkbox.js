$(document).ready(		
		function () {			
			$('.check_all').on('change',function(){
			    var checkbox_delete = document.getElementsByClassName('checkbox_delete');
			    var case_all = $(this);			    
			    if (case_all[0].checked==true){
			    	for (var checkbox in checkbox_delete){
			    		checkbox_delete.item(checkbox).checked=true;
			    	}
			    }
			    else{
			    	for (var checkbox in checkbox_delete){
			    		checkbox_delete.item(checkbox).checked=false;
			    	}
			    }
			}
			);   
			
			
			$('.delete_all').on('click',function(){
				var elements=document.getElementsByClassName('checkbox_delete');
				var len=elements.length;
				var id_list = [];
				var model = "";
				var app = "";
				if(confirm("Do you really want to delete these elements ?")){
					for (var i=(len-1); i>=0; i--){
						if (elements[i].checked==true){
							var element=$(elements[i]);
							var object = element.next('.generic_delete').next('[name=objectid]');
							model = object.next('[name=model]');
							app = model.next('[name=app]');
							id_list.push(object.val());
						}
					}
					//console.log(id_list.toString());
					if (app && model){
						var id_str = id_list.toString();
						var id_str_final = id_str.replace(/,/g, "-");
						//console.log(id_str);
						//$('.redirect_delete').attr("href",'/deleteall/'+app.val()+'/'+model.val()+'/'+id_str_final+'/');
						$.ajax({
							url:'/deleteall/'+app.val()+'/'+model.val()+'/',
							type: "POST",
							data: { id_str_final: id_str_final },
							success:function(response){
								console.log(response);
								window.location.replace(response.url);
							},
							complete:function(){
							},
							error:function (xhr, textStatus, thrownError){}
						});

					}
				}
			})
		}
)
