$(document).ready(function() {
 	        
	$('[name="close_list"]').on('click',function() {
		$('#rawdata_div').hide();
	});
 	        
 	        
	$(document).on("click", "span.handclick", function() {
		var id = $(this).attr("value");
		$.ajax({
			url: ROOT_URL.concat("phenotyping/cataloguedata/edit/"+id+"/"),
			type: "GET", 
			//data: {},
			dataType: "json",
			success: function(result){
				var form = jQuery.parseJSON(result['form']);
				$('#rawdata_form').text('');
				$('#rawdata_form_message').text('');
				$('#rawdata_form').attr('action',ROOT_URL.concat("phenotyping/cataloguedata/edit/"+id+"/"));
				showFormInDiv(form);
				$('#rawdata_div').show();
				$('.datepicker').each(function(){
					$(this).removeClass('hasDatepicker').datepicker({ dateFormat: 'dd/mm/yy' });
				});
			},
			async: false,
			error: function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.responseText);
			}
		});
	});
 	        
	function showFormInDiv(form) {
		var formString = '<table class="tableeditpopup"><thead></thead><tbody>'
		$.each(form['fields'], function(field_name, field) {
			required_string = '';
			formString += '<tr>';
			if (field['widget']['is_required'] == true) {
				required_string = 'required';
			}
			formString += '<th><b>'+field['label']+'</b> :</th>';
			if (form['errors'][field_name]) {
				formString += '<span class="errorlist">';
				formString += '<ul>';
				$.each(form['errors'][field_name], function(error) {
					formString += '<li>'+form['errors'][field_name][error]+'</li>';
				});
				formString += '</ul>';
			}
			if (field['widget']['attrs']['disabled'] == true) {
				if (field['title'] == 'ModelChoiceField' || field['title'] == 'ChoiceField') {
					$.each(field['choices'], function(choice){

						if (field['choices'][choice]['value'] == field['initial']) {
							formString += '<td>'+field['choices'][choice]['display']+'</td>';
							formString += '<input type="hidden" id="'+field_name+'" name="'+field_name+'" value="'+field['choices'][choice]['value']+'" />';
						}
					});
				} else {
					formString += '<td>'+field['initial']+'</td>';
					formString += '<input type="hidden" id="'+field_name+'" name="'+field_name+'" value="'+field['initial']+'" />';
				}
			}
			else {
				if (field['widget']['title'] == 'Select' ) {
					formString += '<td><select width="250px" name="'+field_name+'" '+required_string+' id="id_'+field_name+'" >\n';
					$.each(field['choices'], function(choice){
						if (field['choices'][choice]['value'] == field['initial']) {
							formString += '<option selected value="'+field['choices'][choice]['value']+'">'+field['choices'][choice]['display']+'</option>\n';
						} else {
							formString += '<option value="'+field['choices'][choice]['value']+'">'+field['choices'][choice]['display']+'</option>\n';
						}
					});
					formString += '</select></td>';
				}
				else if ((field['widget']['title'] == 'Textarea' ) ) {
					formString += '<td><textarea rows="'+field['widget']['attrs']['rows']+'" cols="'+field['widget']['attrs']['cols']+'" name="'+field_name+'" id="id_'+field_name+'" '+required_string+'>'+field['initial']+'</textarea></td>';
				} else if ((field['widget']['title'] == 'DateInput' ) ) {
					initial_date = '';
					if (field['initial'] != null) {
						initial_date = field['initial'];
					}
					formString += '<td><input type="text" id="'+field_name+'" name="'+field_name+'" class="datepicker hasDatepicker" value="'+initial_date+'"/></td>';
				}
			}
			formString += '</tr>';
		});
		formString += '<tr><td></td><td><input type="submit" class="ui-button ui-widget ui-state-default ui-corner-all" value="Update" /></td></tr></tbody></table>';
		$('#rawdata_form').append(formString);
	};
 	        
	$('#rawdata_form').submit(function(e){
		$.post($(this).attr('action'), $(this).serialize(), function(data){
			results = jQuery.parseJSON(data);
			if (results['form']) {
				var form = jQuery.parseJSON(results['form']);
				$('#rawdata_form').text('');
				$('#rawdata_form_message').append(results['message']);
				showFormInDiv(form);
				$('.datepicker').each(function(){
					$(this).removeClass('hasDatepicker').datepicker({ dateFormat: 'dd/mm/yy' });
				});
			} else if (results['data']) {
				var data = jQuery.parseJSON(results['data']);
				
				$('#rawdata_'+data[0]['pk']).text(data[0]['fields']['raw_data']);

				$('#rawdata_div').hide();
			}
		});
		e.preventDefault();
	});	        
});