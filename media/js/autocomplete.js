$(document).ready(
	function() {		
		/*Autocompletion*/
		var availableTags = [];
		//alert(ROOT_URL);
		$.ajax({
            url: ROOT_URL.concat("getattributes/"),
            type: "POST",
            success: function(html) {
            	availableTags = html;
            	
            },
            async: false,
            error: function(jqXHR, textStatus, errorThrown) {
            	alert(jqXHR.responseText);
            }
        });

		$("#attributes_autocomplete").autocomplete({
		      source: availableTags,
		      minLength: 0
		});
		
		$("#attributes_autocomplete").on("autocompleteselect", function( event, ui ) {
			//types = ['Short Text','Long Text','Number','URL','File'];
			
			var values = ui.item.value.match(/(.+?)\((.+)\)/);
			$('#attributes_autocomplete').val(values[1].trim());
			var selected_type = types.indexOf(values[2].trim())+1;
			$('#id_type').val(selected_type);
			return false;
		} );
			
	}
);