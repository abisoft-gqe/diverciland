$(document).ready(function() {
	$('#no_export').show();
	$('#export_ok').hide();
	var thread = $("[name='thread_name']");
	var thread_name = thread.val();
	
	if (thread_name != null){
		var check = setInterval(function(){checkThreadActivity(thread_name, check);}, 3000);
	}	
})

function checkThreadActivity(thread_name, check){	
	$.ajax({
		url:'/checkthread/',
		type: "POST",
		data: { thread_name: thread_name },
		success:function(response){
			//console.log(response);
			if (response.status == false){
				clearInterval(check);
				validExportIcon();
			}
		},
		complete:function(){},
		error:function (xhr, textStatus, thrownError){}
	});
}

function validExportIcon(){
	$('#export_ok').show();
	$('#no_export').hide();
}