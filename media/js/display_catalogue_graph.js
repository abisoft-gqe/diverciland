$(document).ready(function() {
	
	$('[name="close_list"]').on('click',function() {
		$('#graph_div').hide();
		$('#tablecard_cdata').removeClass("tablecard_div_cdata");
		$('#tablecard_cdata').addClass("tablecard_div");
		resetTableColors();
	});
	
	$(document).on("click", "span.display_graph", function() {
		var accession_trait = $(this).attr("value");
		var span = $(this)
		resetTableColors();
		$.ajax({
			url: ROOT_URL.concat("phenotyping/cataloguedatacardviewer/"+accession_trait+"/"),
			type: "GET",
			dataType: "json",
			success: function(result){
				var div = jQuery.parseJSON(result['div']);
				var script = jQuery.parseJSON(result['script']);
				$('#bokeh_graph').text('');
				showGraphInDiv(div, script);
				
				span.parent('td').parent('tr').children('th').css('color', '#DF7401');
				$('#tablecard_cdata').removeClass("tablecard_div");
				$('#tablecard_cdata').addClass("tablecard_div_cdata");
				$('#graph_div').css('display', 'inline-block');
			},
			async: false,
			error: function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.responseText);
			}
		});
	});
	
	function showGraphInDiv(div, script) {
		$('#bokeh_graph').append(script);
		$('#bokeh_graph').append(div);
	};
	
	function resetTableColors(){
		var myTable = document.getElementById("tablecard_cdata");
		var ths = myTable.getElementsByTagName("th");
		for (var i = 0 ; i < ths.length; i++) {
			ths[i].style.color = '#0e88af'
		}
	};
});